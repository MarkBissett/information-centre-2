namespace WoodPlan5
{
    partial class frm_UT_Mapping_Map_Snapshot_Map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Mapping_Map_Snapshot_Map));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.resizablePictureBox_MapScaleBar = new BaseObjects.MoveablePictureBox();
            this.resizablePictureBox_NorthArrow = new BaseObjects.MoveablePictureBox();
            this.resizablePictureBox_Legend = new BaseObjects.MoveablePictureBox();
            this.resizableMemoEditBoxOSRef = new BaseObjects.MoveableMemoEditBox();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01300ATWorkOrderMapPageSizesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPageHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageSizeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPageWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter();
            this.ceCustomSizing = new DevExpress.XtraEditors.CheckEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.richEditBarController1 = new DevExpress.XtraRichEdit.UI.RichEditBarController();
            this.changeFontNameItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontNameItem();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.changeFontSizeItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontSizeItem();
            this.repositoryItemRichEditFontSizeEdit1 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit();
            this.changeFontColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontColorItem();
            this.changeFontBackColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem();
            this.toggleFontBoldItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontBoldItem();
            this.toggleFontItalicItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontItalicItem();
            this.toggleFontUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem();
            this.toggleFontDoubleUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem();
            this.toggleFontStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem();
            this.toggleFontDoubleStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem();
            this.toggleFontSuperscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem();
            this.toggleFontSubscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem();
            this.fontSizeIncreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem();
            this.fontSizeDecreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem();
            this.clearFormattingItem1 = new DevExpress.XtraRichEdit.UI.ClearFormattingItem();
            this.showFontFormItem1 = new DevExpress.XtraRichEdit.UI.ShowFontFormItem();
            this.cutItem1 = new DevExpress.XtraRichEdit.UI.CutItem();
            this.copyItem1 = new DevExpress.XtraRichEdit.UI.CopyItem();
            this.pasteItem1 = new DevExpress.XtraRichEdit.UI.PasteItem();
            this.fileNewItem1 = new DevExpress.XtraRichEdit.UI.FileNewItem();
            this.fileOpenItem1 = new DevExpress.XtraRichEdit.UI.FileOpenItem();
            this.fileSaveItem1 = new DevExpress.XtraRichEdit.UI.FileSaveItem();
            this.fileSaveAsItem1 = new DevExpress.XtraRichEdit.UI.FileSaveAsItem();
            this.quickPrintItem1 = new DevExpress.XtraRichEdit.UI.QuickPrintItem();
            this.printItem1 = new DevExpress.XtraRichEdit.UI.PrintItem();
            this.printPreviewItem1 = new DevExpress.XtraRichEdit.UI.PrintPreviewItem();
            this.undoItem1 = new DevExpress.XtraRichEdit.UI.UndoItem();
            this.redoItem1 = new DevExpress.XtraRichEdit.UI.RedoItem();
            this.insertPictureItem1 = new DevExpress.XtraRichEdit.UI.InsertPictureItem();
            this.zoomOutItem1 = new DevExpress.XtraRichEdit.UI.ZoomOutItem();
            this.zoomInItem1 = new DevExpress.XtraRichEdit.UI.ZoomInItem();
            this.findItem1 = new DevExpress.XtraRichEdit.UI.FindItem();
            this.replaceItem1 = new DevExpress.XtraRichEdit.UI.ReplaceItem();
            this.fontBar1 = new DevExpress.XtraRichEdit.UI.FontBar();
            this.clipboardBar1 = new DevExpress.XtraRichEdit.UI.ClipboardBar();
            this.illustrationsBar1 = new DevExpress.XtraRichEdit.UI.IllustrationsBar();
            this.zoomBar1 = new DevExpress.XtraRichEdit.UI.ZoomBar();
            this.editingBar1 = new DevExpress.XtraRichEdit.UI.EditingBar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiSelectedDrawingTool = new DevExpress.XtraBars.BarSubItem();
            this.bciNoDrawing = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawFreehand = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawRectangle = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawElypsis = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawLine = new DevExpress.XtraBars.BarCheckItem();
            this.bciDrawArrow = new DevExpress.XtraBars.BarCheckItem();
            this.beiPenWidth = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.beiArrowSize = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.beiPenColour = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorPickEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.bbiClearDrawing = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddText = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddRichText = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddImage = new DevExpress.XtraBars.BarButtonItem();
            this.bciLegend = new DevExpress.XtraBars.BarCheckItem();
            this.bciOSRef = new DevExpress.XtraBars.BarCheckItem();
            this.bciNorthArrow = new DevExpress.XtraBars.BarCheckItem();
            this.bciScaleBar = new DevExpress.XtraBars.BarCheckItem();
            this.bbiPreview = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.bbiDeleteObject = new DevExpress.XtraBars.BarButtonItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_MapScaleBar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_NorthArrow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_Legend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizableMemoEditBoxOSRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01300ATWorkOrderMapPageSizesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCustomSizing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichEditFontSizeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1026, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 700);
            this.barDockControlBottom.Size = new System.Drawing.Size(1026, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 674);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1026, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 674);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.fontBar1,
            this.clipboardBar1,
            this.illustrationsBar1,
            this.zoomBar1,
            this.editingBar1,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.changeFontNameItem1,
            this.changeFontSizeItem1,
            this.changeFontColorItem1,
            this.changeFontBackColorItem1,
            this.toggleFontBoldItem1,
            this.toggleFontItalicItem1,
            this.toggleFontUnderlineItem1,
            this.toggleFontDoubleUnderlineItem1,
            this.toggleFontStrikeoutItem1,
            this.toggleFontDoubleStrikeoutItem1,
            this.toggleFontSuperscriptItem1,
            this.toggleFontSubscriptItem1,
            this.fontSizeIncreaseItem1,
            this.fontSizeDecreaseItem1,
            this.clearFormattingItem1,
            this.showFontFormItem1,
            this.cutItem1,
            this.copyItem1,
            this.pasteItem1,
            this.fileNewItem1,
            this.fileOpenItem1,
            this.fileSaveItem1,
            this.fileSaveAsItem1,
            this.quickPrintItem1,
            this.printItem1,
            this.printPreviewItem1,
            this.undoItem1,
            this.redoItem1,
            this.insertPictureItem1,
            this.zoomOutItem1,
            this.zoomInItem1,
            this.findItem1,
            this.replaceItem1,
            this.bbiAddText,
            this.bbiAddRichText,
            this.bbiAddImage,
            this.bciLegend,
            this.bciOSRef,
            this.bciNorthArrow,
            this.bciScaleBar,
            this.bbiPreview,
            this.bbiDeleteObject,
            this.beiPenWidth,
            this.bbiClearDrawing,
            this.beiPenColour,
            this.bsiSelectedDrawingTool,
            this.bciNoDrawing,
            this.bciDrawFreehand,
            this.bciDrawRectangle,
            this.bciDrawElypsis,
            this.bciDrawLine,
            this.bciDrawArrow,
            this.beiArrowSize});
            this.barManager1.MaxItemId = 85;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemFontEdit1,
            this.repositoryItemRichEditFontSizeEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemColorPickEdit1,
            this.repositoryItemSpinEdit2});
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(976, 642);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraScrollableControl1.Controls.Add(this.resizablePictureBox_MapScaleBar);
            this.xtraScrollableControl1.Controls.Add(this.resizablePictureBox_NorthArrow);
            this.xtraScrollableControl1.Controls.Add(this.resizablePictureBox_Legend);
            this.xtraScrollableControl1.Controls.Add(this.resizableMemoEditBoxOSRef);
            this.xtraScrollableControl1.Controls.Add(this.pictureBox1);
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 83);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1024, 615);
            this.xtraScrollableControl1.TabIndex = 5;
            // 
            // resizablePictureBox_MapScaleBar
            // 
            this.resizablePictureBox_MapScaleBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.resizablePictureBox_MapScaleBar.Location = new System.Drawing.Point(107, 37);
            this.resizablePictureBox_MapScaleBar.MenuManager = this.barManager1;
            this.resizablePictureBox_MapScaleBar.Name = "resizablePictureBox_MapScaleBar";
            this.resizablePictureBox_MapScaleBar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.resizablePictureBox_MapScaleBar.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.resizablePictureBox_MapScaleBar.Properties.ShowMenu = false;
            this.resizablePictureBox_MapScaleBar.Size = new System.Drawing.Size(225, 43);
            this.resizablePictureBox_MapScaleBar.TabIndex = 22;
            this.resizablePictureBox_MapScaleBar.Visible = false;
            this.resizablePictureBox_MapScaleBar.Resize += new System.EventHandler(this.resizablePictureBox_MapScaleBar_Resize);
            // 
            // resizablePictureBox_NorthArrow
            // 
            this.resizablePictureBox_NorthArrow.Cursor = System.Windows.Forms.Cursors.Default;
            this.resizablePictureBox_NorthArrow.EditValue = ((object)(resources.GetObject("resizablePictureBox_NorthArrow.EditValue")));
            this.resizablePictureBox_NorthArrow.Location = new System.Drawing.Point(12, 37);
            this.resizablePictureBox_NorthArrow.MenuManager = this.barManager1;
            this.resizablePictureBox_NorthArrow.Name = "resizablePictureBox_NorthArrow";
            this.resizablePictureBox_NorthArrow.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.resizablePictureBox_NorthArrow.Properties.ShowMenu = false;
            this.resizablePictureBox_NorthArrow.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.resizablePictureBox_NorthArrow.Size = new System.Drawing.Size(89, 101);
            this.resizablePictureBox_NorthArrow.TabIndex = 21;
            this.resizablePictureBox_NorthArrow.Visible = false;
            // 
            // resizablePictureBox_Legend
            // 
            this.resizablePictureBox_Legend.Cursor = System.Windows.Forms.Cursors.Default;
            this.resizablePictureBox_Legend.Location = new System.Drawing.Point(12, 144);
            this.resizablePictureBox_Legend.MenuManager = this.barManager1;
            this.resizablePictureBox_Legend.Name = "resizablePictureBox_Legend";
            this.resizablePictureBox_Legend.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.resizablePictureBox_Legend.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.resizablePictureBox_Legend.Properties.ShowMenu = false;
            this.resizablePictureBox_Legend.Size = new System.Drawing.Size(275, 222);
            this.resizablePictureBox_Legend.TabIndex = 20;
            this.resizablePictureBox_Legend.Visible = false;
            // 
            // resizableMemoEditBoxOSRef
            // 
            this.resizableMemoEditBoxOSRef.Cursor = System.Windows.Forms.Cursors.Default;
            this.resizableMemoEditBoxOSRef.Location = new System.Drawing.Point(11, 13);
            this.resizableMemoEditBoxOSRef.MenuManager = this.barManager1;
            this.resizableMemoEditBoxOSRef.Name = "resizableMemoEditBoxOSRef";
            this.resizableMemoEditBoxOSRef.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.resizableMemoEditBoxOSRef.Size = new System.Drawing.Size(296, 18);
            this.resizableMemoEditBoxOSRef.TabIndex = 18;
            this.resizableMemoEditBoxOSRef.UseOptimizedRendering = true;
            this.resizableMemoEditBoxOSRef.Visible = false;
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.EditValue = "1";
            this.gridLookUpEdit1.Location = new System.Drawing.Point(72, 29);
            this.gridLookUpEdit1.MenuManager = this.barManager1;
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.DataSource = this.sp01300ATWorkOrderMapPageSizesBindingSource;
            this.gridLookUpEdit1.Properties.DisplayMember = "PageSizeDescription";
            this.gridLookUpEdit1.Properties.NullText = "No Size Specified";
            this.gridLookUpEdit1.Properties.ValueMember = "PageSizeID";
            this.gridLookUpEdit1.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEdit1.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEdit1.TabIndex = 3;
            this.gridLookUpEdit1.EditValueChanged += new System.EventHandler(this.gridLookUpEdit1_EditValueChanged);
            // 
            // sp01300ATWorkOrderMapPageSizesBindingSource
            // 
            this.sp01300ATWorkOrderMapPageSizesBindingSource.DataMember = "sp01300_AT_WorkOrder_Map_Page_Sizes";
            this.sp01300ATWorkOrderMapPageSizesBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPageHeight,
            this.colPageSizeDescription,
            this.colPageSizeID,
            this.colPageSizeOrder,
            this.colPageWidth});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPageSizeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPageHeight
            // 
            this.colPageHeight.Caption = "Height";
            this.colPageHeight.FieldName = "PageHeight";
            this.colPageHeight.Name = "colPageHeight";
            this.colPageHeight.Visible = true;
            this.colPageHeight.VisibleIndex = 2;
            this.colPageHeight.Width = 52;
            // 
            // colPageSizeDescription
            // 
            this.colPageSizeDescription.Caption = "Size Description";
            this.colPageSizeDescription.FieldName = "PageSizeDescription";
            this.colPageSizeDescription.Name = "colPageSizeDescription";
            this.colPageSizeDescription.Visible = true;
            this.colPageSizeDescription.VisibleIndex = 0;
            this.colPageSizeDescription.Width = 326;
            // 
            // colPageSizeID
            // 
            this.colPageSizeID.Caption = "Size ID";
            this.colPageSizeID.FieldName = "PageSizeID";
            this.colPageSizeID.Name = "colPageSizeID";
            this.colPageSizeID.Width = 44;
            // 
            // colPageSizeOrder
            // 
            this.colPageSizeOrder.Caption = "Order";
            this.colPageSizeOrder.FieldName = "PageSizeOrder";
            this.colPageSizeOrder.Name = "colPageSizeOrder";
            this.colPageSizeOrder.Width = 62;
            // 
            // colPageWidth
            // 
            this.colPageWidth.Caption = "Width";
            this.colPageWidth.FieldName = "PageWidth";
            this.colPageWidth.Name = "colPageWidth";
            this.colPageWidth.Visible = true;
            this.colPageWidth.VisibleIndex = 1;
            this.colPageWidth.Width = 49;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(10, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Image Size:";
            // 
            // sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter
            // 
            this.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter.ClearBeforeFill = true;
            // 
            // ceCustomSizing
            // 
            this.ceCustomSizing.Location = new System.Drawing.Point(278, 30);
            this.ceCustomSizing.MenuManager = this.barManager1;
            this.ceCustomSizing.Name = "ceCustomSizing";
            this.ceCustomSizing.Properties.Caption = "Enable Manual Resize";
            this.ceCustomSizing.Size = new System.Drawing.Size(127, 19);
            toolTipTitleItem19.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipTitleItem19.Appearance.Options.UseImage = true;
            toolTipTitleItem19.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem19.Image")));
            toolTipTitleItem19.Text = "Enable Manual Resize - Information";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Click me to manually set the size of the map snapshot area. \r\n\r\nOnce ticked, posi" +
    "tion the mouse over the border on the snapshot object then drag when the cursor " +
    "changes to an arrow to adjust the size.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.ceCustomSizing.SuperTip = superToolTip19;
            this.ceCustomSizing.TabIndex = 9;
            this.ceCustomSizing.CheckedChanged += new System.EventHandler(this.ceCustomSizing_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(905, 28);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(114, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save and Close";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(418, 10);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(45, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Remarks:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 8);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(57, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Description:";
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "";
            this.textEdit2.Location = new System.Drawing.Point(72, 5);
            this.textEdit2.MenuManager = this.barManager1;
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.MaxLength = 100;
            this.textEdit2.Properties.NullValuePrompt = "No Description";
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit2, true);
            this.textEdit2.Size = new System.Drawing.Size(333, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit2, optionsSpelling1);
            this.textEdit2.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(905, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(114, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel and Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit1.Location = new System.Drawing.Point(469, 5);
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.MaxLength = 1000;
            this.memoEdit1.Properties.NullValuePrompt = "No Remarks";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEdit1, true);
            this.memoEdit1.Size = new System.Drawing.Size(430, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEdit1, optionsSpelling2);
            this.memoEdit1.TabIndex = 2;
            this.memoEdit1.UseOptimizedRendering = true;
            // 
            // richEditBarController1
            // 
            this.richEditBarController1.BarItems.Add(this.changeFontNameItem1);
            this.richEditBarController1.BarItems.Add(this.changeFontSizeItem1);
            this.richEditBarController1.BarItems.Add(this.changeFontColorItem1);
            this.richEditBarController1.BarItems.Add(this.changeFontBackColorItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontBoldItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontItalicItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontUnderlineItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontDoubleUnderlineItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontStrikeoutItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontDoubleStrikeoutItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontSuperscriptItem1);
            this.richEditBarController1.BarItems.Add(this.toggleFontSubscriptItem1);
            this.richEditBarController1.BarItems.Add(this.fontSizeIncreaseItem1);
            this.richEditBarController1.BarItems.Add(this.fontSizeDecreaseItem1);
            this.richEditBarController1.BarItems.Add(this.clearFormattingItem1);
            this.richEditBarController1.BarItems.Add(this.showFontFormItem1);
            this.richEditBarController1.BarItems.Add(this.cutItem1);
            this.richEditBarController1.BarItems.Add(this.copyItem1);
            this.richEditBarController1.BarItems.Add(this.pasteItem1);
            this.richEditBarController1.BarItems.Add(this.fileNewItem1);
            this.richEditBarController1.BarItems.Add(this.fileOpenItem1);
            this.richEditBarController1.BarItems.Add(this.fileSaveItem1);
            this.richEditBarController1.BarItems.Add(this.fileSaveAsItem1);
            this.richEditBarController1.BarItems.Add(this.quickPrintItem1);
            this.richEditBarController1.BarItems.Add(this.printItem1);
            this.richEditBarController1.BarItems.Add(this.printPreviewItem1);
            this.richEditBarController1.BarItems.Add(this.undoItem1);
            this.richEditBarController1.BarItems.Add(this.redoItem1);
            this.richEditBarController1.BarItems.Add(this.insertPictureItem1);
            this.richEditBarController1.BarItems.Add(this.zoomOutItem1);
            this.richEditBarController1.BarItems.Add(this.zoomInItem1);
            this.richEditBarController1.BarItems.Add(this.findItem1);
            this.richEditBarController1.BarItems.Add(this.replaceItem1);
            // 
            // changeFontNameItem1
            // 
            this.changeFontNameItem1.Caption = "Font";
            this.changeFontNameItem1.Edit = this.repositoryItemFontEdit1;
            this.changeFontNameItem1.Enabled = false;
            this.changeFontNameItem1.Id = 26;
            this.changeFontNameItem1.Name = "changeFontNameItem1";
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.AutoHeight = false;
            this.repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // changeFontSizeItem1
            // 
            this.changeFontSizeItem1.Caption = "Font Size";
            this.changeFontSizeItem1.Edit = this.repositoryItemRichEditFontSizeEdit1;
            this.changeFontSizeItem1.Enabled = false;
            this.changeFontSizeItem1.Id = 27;
            this.changeFontSizeItem1.Name = "changeFontSizeItem1";
            // 
            // repositoryItemRichEditFontSizeEdit1
            // 
            this.repositoryItemRichEditFontSizeEdit1.AutoHeight = false;
            this.repositoryItemRichEditFontSizeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemRichEditFontSizeEdit1.Control = null;
            this.repositoryItemRichEditFontSizeEdit1.Name = "repositoryItemRichEditFontSizeEdit1";
            // 
            // changeFontColorItem1
            // 
            this.changeFontColorItem1.Caption = "Font Color";
            this.changeFontColorItem1.Enabled = false;
            this.changeFontColorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("changeFontColorItem1.Glyph")));
            this.changeFontColorItem1.Id = 28;
            this.changeFontColorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("changeFontColorItem1.LargeGlyph")));
            this.changeFontColorItem1.Name = "changeFontColorItem1";
            // 
            // changeFontBackColorItem1
            // 
            this.changeFontBackColorItem1.Caption = "Text Highlight Color";
            this.changeFontBackColorItem1.Enabled = false;
            this.changeFontBackColorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("changeFontBackColorItem1.Glyph")));
            this.changeFontBackColorItem1.Id = 29;
            this.changeFontBackColorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("changeFontBackColorItem1.LargeGlyph")));
            this.changeFontBackColorItem1.Name = "changeFontBackColorItem1";
            // 
            // toggleFontBoldItem1
            // 
            this.toggleFontBoldItem1.Caption = "Bold";
            this.toggleFontBoldItem1.Enabled = false;
            this.toggleFontBoldItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontBoldItem1.Glyph")));
            this.toggleFontBoldItem1.Id = 30;
            this.toggleFontBoldItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontBoldItem1.LargeGlyph")));
            this.toggleFontBoldItem1.Name = "toggleFontBoldItem1";
            // 
            // toggleFontItalicItem1
            // 
            this.toggleFontItalicItem1.Caption = "Italic";
            this.toggleFontItalicItem1.Enabled = false;
            this.toggleFontItalicItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontItalicItem1.Glyph")));
            this.toggleFontItalicItem1.Id = 31;
            this.toggleFontItalicItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontItalicItem1.LargeGlyph")));
            this.toggleFontItalicItem1.Name = "toggleFontItalicItem1";
            // 
            // toggleFontUnderlineItem1
            // 
            this.toggleFontUnderlineItem1.Caption = "Underline";
            this.toggleFontUnderlineItem1.Enabled = false;
            this.toggleFontUnderlineItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontUnderlineItem1.Glyph")));
            this.toggleFontUnderlineItem1.Id = 32;
            this.toggleFontUnderlineItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontUnderlineItem1.LargeGlyph")));
            this.toggleFontUnderlineItem1.Name = "toggleFontUnderlineItem1";
            // 
            // toggleFontDoubleUnderlineItem1
            // 
            this.toggleFontDoubleUnderlineItem1.Caption = "Double Underline";
            this.toggleFontDoubleUnderlineItem1.Enabled = false;
            this.toggleFontDoubleUnderlineItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontDoubleUnderlineItem1.Glyph")));
            this.toggleFontDoubleUnderlineItem1.Id = 33;
            this.toggleFontDoubleUnderlineItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontDoubleUnderlineItem1.LargeGlyph")));
            this.toggleFontDoubleUnderlineItem1.Name = "toggleFontDoubleUnderlineItem1";
            // 
            // toggleFontStrikeoutItem1
            // 
            this.toggleFontStrikeoutItem1.Caption = "Strikethrough";
            this.toggleFontStrikeoutItem1.Enabled = false;
            this.toggleFontStrikeoutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontStrikeoutItem1.Glyph")));
            this.toggleFontStrikeoutItem1.Id = 34;
            this.toggleFontStrikeoutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontStrikeoutItem1.LargeGlyph")));
            this.toggleFontStrikeoutItem1.Name = "toggleFontStrikeoutItem1";
            // 
            // toggleFontDoubleStrikeoutItem1
            // 
            this.toggleFontDoubleStrikeoutItem1.Caption = "Double Strikethrough";
            this.toggleFontDoubleStrikeoutItem1.Enabled = false;
            this.toggleFontDoubleStrikeoutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontDoubleStrikeoutItem1.Glyph")));
            this.toggleFontDoubleStrikeoutItem1.Id = 35;
            this.toggleFontDoubleStrikeoutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontDoubleStrikeoutItem1.LargeGlyph")));
            this.toggleFontDoubleStrikeoutItem1.Name = "toggleFontDoubleStrikeoutItem1";
            // 
            // toggleFontSuperscriptItem1
            // 
            this.toggleFontSuperscriptItem1.Caption = "Superscript";
            this.toggleFontSuperscriptItem1.Enabled = false;
            this.toggleFontSuperscriptItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontSuperscriptItem1.Glyph")));
            this.toggleFontSuperscriptItem1.Id = 36;
            this.toggleFontSuperscriptItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontSuperscriptItem1.LargeGlyph")));
            this.toggleFontSuperscriptItem1.Name = "toggleFontSuperscriptItem1";
            // 
            // toggleFontSubscriptItem1
            // 
            this.toggleFontSubscriptItem1.Caption = "Subscript";
            this.toggleFontSubscriptItem1.Enabled = false;
            this.toggleFontSubscriptItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("toggleFontSubscriptItem1.Glyph")));
            this.toggleFontSubscriptItem1.Id = 37;
            this.toggleFontSubscriptItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("toggleFontSubscriptItem1.LargeGlyph")));
            this.toggleFontSubscriptItem1.Name = "toggleFontSubscriptItem1";
            // 
            // fontSizeIncreaseItem1
            // 
            this.fontSizeIncreaseItem1.Caption = "Grow Font";
            this.fontSizeIncreaseItem1.Enabled = false;
            this.fontSizeIncreaseItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fontSizeIncreaseItem1.Glyph")));
            this.fontSizeIncreaseItem1.Id = 38;
            this.fontSizeIncreaseItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fontSizeIncreaseItem1.LargeGlyph")));
            this.fontSizeIncreaseItem1.Name = "fontSizeIncreaseItem1";
            // 
            // fontSizeDecreaseItem1
            // 
            this.fontSizeDecreaseItem1.Caption = "Shrink Font";
            this.fontSizeDecreaseItem1.Enabled = false;
            this.fontSizeDecreaseItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fontSizeDecreaseItem1.Glyph")));
            this.fontSizeDecreaseItem1.Id = 39;
            this.fontSizeDecreaseItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fontSizeDecreaseItem1.LargeGlyph")));
            this.fontSizeDecreaseItem1.Name = "fontSizeDecreaseItem1";
            // 
            // clearFormattingItem1
            // 
            this.clearFormattingItem1.Caption = "Clear Formatting";
            this.clearFormattingItem1.Enabled = false;
            this.clearFormattingItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("clearFormattingItem1.Glyph")));
            this.clearFormattingItem1.Id = 40;
            this.clearFormattingItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("clearFormattingItem1.LargeGlyph")));
            this.clearFormattingItem1.Name = "clearFormattingItem1";
            toolTipTitleItem1.Text = "Clear Formatting";
            toolTipItem1.Text = "Clear all the formatting from the selection, leaving only plain text.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.clearFormattingItem1.SuperTip = superToolTip1;
            // 
            // showFontFormItem1
            // 
            this.showFontFormItem1.Caption = "Font";
            this.showFontFormItem1.Enabled = false;
            this.showFontFormItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("showFontFormItem1.Glyph")));
            this.showFontFormItem1.Id = 41;
            this.showFontFormItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("showFontFormItem1.LargeGlyph")));
            this.showFontFormItem1.Name = "showFontFormItem1";
            // 
            // cutItem1
            // 
            this.cutItem1.Caption = "Cut";
            this.cutItem1.Enabled = false;
            this.cutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("cutItem1.Glyph")));
            this.cutItem1.Id = 42;
            this.cutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("cutItem1.LargeGlyph")));
            this.cutItem1.Name = "cutItem1";
            // 
            // copyItem1
            // 
            this.copyItem1.Caption = "Copy";
            this.copyItem1.Enabled = false;
            this.copyItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("copyItem1.Glyph")));
            this.copyItem1.Id = 49;
            this.copyItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("copyItem1.LargeGlyph")));
            this.copyItem1.Name = "copyItem1";
            // 
            // pasteItem1
            // 
            this.pasteItem1.Caption = "Paste";
            this.pasteItem1.Enabled = false;
            this.pasteItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("pasteItem1.Glyph")));
            this.pasteItem1.Id = 43;
            this.pasteItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("pasteItem1.LargeGlyph")));
            this.pasteItem1.Name = "pasteItem1";
            // 
            // fileNewItem1
            // 
            this.fileNewItem1.Caption = "New";
            this.fileNewItem1.Enabled = false;
            this.fileNewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fileNewItem1.Glyph")));
            this.fileNewItem1.Id = 50;
            this.fileNewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fileNewItem1.LargeGlyph")));
            this.fileNewItem1.Name = "fileNewItem1";
            // 
            // fileOpenItem1
            // 
            this.fileOpenItem1.Caption = "Open";
            this.fileOpenItem1.Enabled = false;
            this.fileOpenItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fileOpenItem1.Glyph")));
            this.fileOpenItem1.Id = 51;
            this.fileOpenItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fileOpenItem1.LargeGlyph")));
            this.fileOpenItem1.Name = "fileOpenItem1";
            toolTipTitleItem15.Text = "Open (Ctrl+O)";
            toolTipItem15.Text = "Open";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.fileOpenItem1.SuperTip = superToolTip15;
            // 
            // fileSaveItem1
            // 
            this.fileSaveItem1.Caption = "Save";
            this.fileSaveItem1.Enabled = false;
            this.fileSaveItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fileSaveItem1.Glyph")));
            this.fileSaveItem1.Id = 52;
            this.fileSaveItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fileSaveItem1.LargeGlyph")));
            this.fileSaveItem1.Name = "fileSaveItem1";
            toolTipTitleItem16.Text = "Save (Ctrl+S)";
            toolTipItem16.Text = "Save";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.fileSaveItem1.SuperTip = superToolTip16;
            // 
            // fileSaveAsItem1
            // 
            this.fileSaveAsItem1.Caption = "Save As";
            this.fileSaveAsItem1.Enabled = false;
            this.fileSaveAsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("fileSaveAsItem1.Glyph")));
            this.fileSaveAsItem1.Id = 53;
            this.fileSaveAsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("fileSaveAsItem1.LargeGlyph")));
            this.fileSaveAsItem1.Name = "fileSaveAsItem1";
            toolTipTitleItem17.Text = "Save As (F12)";
            toolTipItem17.Text = "Save As";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.fileSaveAsItem1.SuperTip = superToolTip17;
            // 
            // quickPrintItem1
            // 
            this.quickPrintItem1.Caption = "&Quick Print";
            this.quickPrintItem1.Enabled = false;
            this.quickPrintItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("quickPrintItem1.Glyph")));
            this.quickPrintItem1.Id = 54;
            this.quickPrintItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("quickPrintItem1.LargeGlyph")));
            this.quickPrintItem1.Name = "quickPrintItem1";
            // 
            // printItem1
            // 
            this.printItem1.Caption = "&Print";
            this.printItem1.Enabled = false;
            this.printItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("printItem1.Glyph")));
            this.printItem1.Id = 55;
            this.printItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printItem1.LargeGlyph")));
            this.printItem1.Name = "printItem1";
            // 
            // printPreviewItem1
            // 
            this.printPreviewItem1.Caption = "Print Pre&view";
            this.printPreviewItem1.Enabled = false;
            this.printPreviewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("printPreviewItem1.Glyph")));
            this.printPreviewItem1.Id = 56;
            this.printPreviewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("printPreviewItem1.LargeGlyph")));
            this.printPreviewItem1.Name = "printPreviewItem1";
            toolTipTitleItem18.Text = "Print Pre&view";
            toolTipItem18.Text = "Preview and make changes to pages before printing.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.printPreviewItem1.SuperTip = superToolTip18;
            // 
            // undoItem1
            // 
            this.undoItem1.Caption = "Undo";
            this.undoItem1.Enabled = false;
            this.undoItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("undoItem1.Glyph")));
            this.undoItem1.Id = 57;
            this.undoItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("undoItem1.LargeGlyph")));
            this.undoItem1.Name = "undoItem1";
            // 
            // redoItem1
            // 
            this.redoItem1.Caption = "Redo";
            this.redoItem1.Enabled = false;
            this.redoItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("redoItem1.Glyph")));
            this.redoItem1.Id = 58;
            this.redoItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("redoItem1.LargeGlyph")));
            this.redoItem1.Name = "redoItem1";
            // 
            // insertPictureItem1
            // 
            this.insertPictureItem1.Caption = "Insert Picture from File";
            this.insertPictureItem1.Enabled = false;
            this.insertPictureItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("insertPictureItem1.Glyph")));
            this.insertPictureItem1.Id = 44;
            this.insertPictureItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("insertPictureItem1.LargeGlyph")));
            this.insertPictureItem1.Name = "insertPictureItem1";
            toolTipTitleItem2.Text = "Insert Picture from File";
            toolTipItem2.Text = "Insert a picture from a file.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.insertPictureItem1.SuperTip = superToolTip2;
            // 
            // zoomOutItem1
            // 
            this.zoomOutItem1.Caption = "Zoom Out";
            this.zoomOutItem1.Enabled = false;
            this.zoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("zoomOutItem1.Glyph")));
            this.zoomOutItem1.Id = 45;
            this.zoomOutItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Subtract));
            this.zoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("zoomOutItem1.LargeGlyph")));
            this.zoomOutItem1.Name = "zoomOutItem1";
            // 
            // zoomInItem1
            // 
            this.zoomInItem1.Caption = "Zoom In";
            this.zoomInItem1.Enabled = false;
            this.zoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("zoomInItem1.Glyph")));
            this.zoomInItem1.Id = 46;
            this.zoomInItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Add));
            this.zoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("zoomInItem1.LargeGlyph")));
            this.zoomInItem1.Name = "zoomInItem1";
            // 
            // findItem1
            // 
            this.findItem1.Caption = "Find";
            this.findItem1.Enabled = false;
            this.findItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("findItem1.Glyph")));
            this.findItem1.Id = 47;
            this.findItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("findItem1.LargeGlyph")));
            this.findItem1.Name = "findItem1";
            // 
            // replaceItem1
            // 
            this.replaceItem1.Caption = "Replace";
            this.replaceItem1.Enabled = false;
            this.replaceItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("replaceItem1.Glyph")));
            this.replaceItem1.Id = 48;
            this.replaceItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("replaceItem1.LargeGlyph")));
            this.replaceItem1.Name = "replaceItem1";
            // 
            // fontBar1
            // 
            this.fontBar1.BarName = "";
            this.fontBar1.Control = null;
            this.fontBar1.DockCol = 1;
            this.fontBar1.DockRow = 0;
            this.fontBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.fontBar1.FloatLocation = new System.Drawing.Point(440, 172);
            this.fontBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.changeFontNameItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeFontSizeItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeFontColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeFontBackColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontBoldItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontItalicItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontUnderlineItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontDoubleUnderlineItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontStrikeoutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontDoubleStrikeoutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontSuperscriptItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleFontSubscriptItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.fontSizeIncreaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.fontSizeDecreaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.clearFormattingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.showFontFormItem1)});
            this.fontBar1.OptionsBar.DisableClose = true;
            this.fontBar1.OptionsBar.DrawDragBorder = false;
            this.fontBar1.Text = "";
            // 
            // clipboardBar1
            // 
            this.clipboardBar1.BarName = "";
            this.clipboardBar1.Control = null;
            this.clipboardBar1.DockCol = 3;
            this.clipboardBar1.DockRow = 0;
            this.clipboardBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.clipboardBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.cutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pasteItem1)});
            this.clipboardBar1.Text = "";
            // 
            // illustrationsBar1
            // 
            this.illustrationsBar1.BarName = "";
            this.illustrationsBar1.Control = null;
            this.illustrationsBar1.DockCol = 2;
            this.illustrationsBar1.DockRow = 0;
            this.illustrationsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.illustrationsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.insertPictureItem1)});
            this.illustrationsBar1.OptionsBar.DrawDragBorder = false;
            this.illustrationsBar1.Text = "";
            // 
            // zoomBar1
            // 
            this.zoomBar1.BarName = "";
            this.zoomBar1.Control = null;
            this.zoomBar1.DockCol = 4;
            this.zoomBar1.DockRow = 0;
            this.zoomBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.zoomBar1.FloatLocation = new System.Drawing.Point(1434, 171);
            this.zoomBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.zoomOutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.zoomInItem1)});
            this.zoomBar1.Text = "";
            // 
            // editingBar1
            // 
            this.editingBar1.BarName = "";
            this.editingBar1.Control = null;
            this.editingBar1.DockCol = 5;
            this.editingBar1.DockRow = 0;
            this.editingBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.editingBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.findItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.replaceItem1)});
            this.editingBar1.Text = "";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 7";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(1275, 177);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedDrawingTool),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPenWidth),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiArrowSize),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPenColour),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearDrawing),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddText, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddRichText),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddImage),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciLegend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciOSRef),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciNorthArrow),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciScaleBar),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPreview, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Add Controls";
            // 
            // bsiSelectedDrawingTool
            // 
            this.bsiSelectedDrawingTool.Caption = "Draw [None]";
            this.bsiSelectedDrawingTool.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSelectedDrawingTool.Glyph")));
            this.bsiSelectedDrawingTool.Id = 77;
            this.bsiSelectedDrawingTool.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciNoDrawing),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawFreehand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawRectangle),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawElypsis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawLine),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciDrawArrow)});
            this.bsiSelectedDrawingTool.Name = "bsiSelectedDrawingTool";
            this.bsiSelectedDrawingTool.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bciNoDrawing
            // 
            this.bciNoDrawing.Caption = "No Drawing Object";
            this.bciNoDrawing.Glyph = ((System.Drawing.Image)(resources.GetObject("bciNoDrawing.Glyph")));
            this.bciNoDrawing.GroupIndex = 1;
            this.bciNoDrawing.Id = 78;
            this.bciNoDrawing.Name = "bciNoDrawing";
            this.bciNoDrawing.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciNoDrawing_CheckedChanged);
            // 
            // bciDrawFreehand
            // 
            this.bciDrawFreehand.Caption = "Freehand";
            this.bciDrawFreehand.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawFreehand.Glyph")));
            this.bciDrawFreehand.GroupIndex = 1;
            this.bciDrawFreehand.Id = 79;
            this.bciDrawFreehand.Name = "bciDrawFreehand";
            this.bciDrawFreehand.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawFreehand_CheckedChanged);
            // 
            // bciDrawRectangle
            // 
            this.bciDrawRectangle.Caption = "Rectangle";
            this.bciDrawRectangle.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawRectangle.Glyph")));
            this.bciDrawRectangle.GroupIndex = 1;
            this.bciDrawRectangle.Id = 80;
            this.bciDrawRectangle.Name = "bciDrawRectangle";
            this.bciDrawRectangle.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawRectangle_CheckedChanged);
            // 
            // bciDrawElypsis
            // 
            this.bciDrawElypsis.Caption = "Elypsis";
            this.bciDrawElypsis.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawElypsis.Glyph")));
            this.bciDrawElypsis.GroupIndex = 1;
            this.bciDrawElypsis.Id = 81;
            this.bciDrawElypsis.Name = "bciDrawElypsis";
            this.bciDrawElypsis.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawElypsis_CheckedChanged);
            // 
            // bciDrawLine
            // 
            this.bciDrawLine.Caption = "Line";
            this.bciDrawLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawLine.Glyph")));
            this.bciDrawLine.GroupIndex = 1;
            this.bciDrawLine.Id = 82;
            this.bciDrawLine.Name = "bciDrawLine";
            this.bciDrawLine.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawLine_CheckedChanged);
            // 
            // bciDrawArrow
            // 
            this.bciDrawArrow.Caption = "Arrow";
            this.bciDrawArrow.Glyph = ((System.Drawing.Image)(resources.GetObject("bciDrawArrow.Glyph")));
            this.bciDrawArrow.GroupIndex = 1;
            this.bciDrawArrow.Id = 83;
            this.bciDrawArrow.Name = "bciDrawArrow";
            this.bciDrawArrow.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciDrawArrow_CheckedChanged);
            // 
            // beiPenWidth
            // 
            this.beiPenWidth.Caption = "Pen Width";
            this.beiPenWidth.Edit = this.repositoryItemSpinEdit1;
            this.beiPenWidth.EditValue = 5;
            this.beiPenWidth.Id = 70;
            this.beiPenWidth.Name = "beiPenWidth";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Pen Width - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I control the width of the pen when drawing over the top of the map.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiPenWidth.SuperTip = superToolTip3;
            this.beiPenWidth.Width = 28;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            90,
            0,
            0,
            65536});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit1_EditValueChanged);
            // 
            // beiArrowSize
            // 
            this.beiArrowSize.Caption = "Arrow Size";
            this.beiArrowSize.Edit = this.repositoryItemSpinEdit2;
            this.beiArrowSize.EditValue = 3;
            this.beiArrowSize.Enabled = false;
            this.beiArrowSize.Id = 84;
            this.beiArrowSize.Name = "beiArrowSize";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Arrow Head Size - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I control the size of the arrow heads when drawing over the top of the map with t" +
    "he Arrow Drawing Tool. \r\n\r\nIf the Arrow Drawing Tool is not selected, I am disab" +
    "led.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.beiArrowSize.SuperTip = superToolTip4;
            this.beiArrowSize.Width = 28;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2_EditValueChanged);
            // 
            // beiPenColour
            // 
            this.beiPenColour.Caption = "Pen Colour";
            this.beiPenColour.Edit = this.repositoryItemColorPickEdit1;
            this.beiPenColour.Id = 73;
            this.beiPenColour.Name = "beiPenColour";
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Pen Colour - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "I control the colourof the pen when drawing over the top of the map.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.beiPenColour.SuperTip = superToolTip5;
            this.beiPenColour.Width = 45;
            // 
            // repositoryItemColorPickEdit1
            // 
            this.repositoryItemColorPickEdit1.AutoHeight = false;
            this.repositoryItemColorPickEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit1.Name = "repositoryItemColorPickEdit1";
            this.repositoryItemColorPickEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemColorPickEdit1_EditValueChanged);
            // 
            // bbiClearDrawing
            // 
            this.bbiClearDrawing.Caption = "Clear Drawing";
            this.bbiClearDrawing.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearDrawing.Glyph")));
            this.bbiClearDrawing.Id = 72;
            this.bbiClearDrawing.Name = "bbiClearDrawing";
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem6.Image")));
            toolTipTitleItem6.Text = "Clear Drawing - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Clear Drawing (remove all drawing on top of the map).";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiClearDrawing.SuperTip = superToolTip6;
            this.bbiClearDrawing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearDrawing_ItemClick);
            // 
            // bbiAddText
            // 
            this.bbiAddText.Caption = "Add Text Box";
            this.bbiAddText.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddText.Glyph")));
            this.bbiAddText.Id = 59;
            this.bbiAddText.Name = "bbiAddText";
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem7.Image")));
            toolTipTitleItem7.Text = "Add Label - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to add a label to the map.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiAddText.SuperTip = superToolTip7;
            this.bbiAddText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddText_ItemClick);
            // 
            // bbiAddRichText
            // 
            this.bbiAddRichText.Caption = "Add Rich Text Box";
            this.bbiAddRichText.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddRichText.Glyph")));
            this.bbiAddRichText.Id = 61;
            this.bbiAddRichText.Name = "bbiAddRichText";
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem8.Image")));
            toolTipTitleItem8.Text = "Add Rich Text - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to add a rich text box to the map.\r\n\r\nA rich text box can contain format" +
    "ted text and images.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiAddRichText.SuperTip = superToolTip8;
            this.bbiAddRichText.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddRichText_ItemClick);
            // 
            // bbiAddImage
            // 
            this.bbiAddImage.Caption = "Add Image Box";
            this.bbiAddImage.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddImage.Glyph")));
            this.bbiAddImage.Id = 62;
            this.bbiAddImage.Name = "bbiAddImage";
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem9.Image")));
            toolTipTitleItem9.Text = "Add Picture - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to add a picture to the map";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiAddImage.SuperTip = superToolTip9;
            this.bbiAddImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddImage_ItemClick);
            // 
            // bciLegend
            // 
            this.bciLegend.Caption = "Legend";
            this.bciLegend.Enabled = false;
            this.bciLegend.Glyph = ((System.Drawing.Image)(resources.GetObject("bciLegend.Glyph")));
            this.bciLegend.Id = 63;
            this.bciLegend.Name = "bciLegend";
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem10.Image")));
            toolTipTitleItem10.Text = "Legend - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to show / hide the Legend on the map.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bciLegend.SuperTip = superToolTip10;
            this.bciLegend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciLegend_CheckedChanged);
            // 
            // bciOSRef
            // 
            this.bciOSRef.Caption = "barCheckItem1";
            this.bciOSRef.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Scale;
            this.bciOSRef.Id = 64;
            this.bciOSRef.Name = "bciOSRef";
            toolTipTitleItem11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem11.Image")));
            toolTipTitleItem11.Text = "OS Copyright Reference - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to show / hide the OS Copyright Reference on the map.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bciOSRef.SuperTip = superToolTip11;
            this.bciOSRef.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciOSRef_CheckedChanged);
            // 
            // bciNorthArrow
            // 
            this.bciNorthArrow.Caption = "North Arrow";
            this.bciNorthArrow.Glyph = ((System.Drawing.Image)(resources.GetObject("bciNorthArrow.Glyph")));
            this.bciNorthArrow.Id = 65;
            this.bciNorthArrow.Name = "bciNorthArrow";
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem12.Image")));
            toolTipTitleItem12.Text = "North Arrow - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to show / hide the North Arrow on the map.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bciNorthArrow.SuperTip = superToolTip12;
            this.bciNorthArrow.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciNorthArrow_CheckedChanged);
            // 
            // bciScaleBar
            // 
            this.bciScaleBar.Caption = "Scale Bar";
            this.bciScaleBar.Glyph = ((System.Drawing.Image)(resources.GetObject("bciScaleBar.Glyph")));
            this.bciScaleBar.Id = 66;
            this.bciScaleBar.Name = "bciScaleBar";
            toolTipTitleItem13.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem13.Image")));
            toolTipTitleItem13.Text = "Scalebar - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to show / hide the scale bar on the map.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bciScaleBar.SuperTip = superToolTip13;
            this.bciScaleBar.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciScaleBar_CheckedChanged);
            // 
            // bbiPreview
            // 
            this.bbiPreview.Caption = "Print";
            this.bbiPreview.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.bbiPreview.Id = 67;
            this.bbiPreview.Name = "bbiPreview";
            toolTipTitleItem14.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem14.Image")));
            toolTipTitleItem14.Text = "Print Preview - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to print preview the map (you can print the map from the print preview s" +
    "creen).";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bbiPreview.SuperTip = superToolTip14;
            this.bbiPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPreview_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.textEdit2);
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.gridLookUpEdit1);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.ceCustomSizing);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Location = new System.Drawing.Point(2, 27);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1022, 54);
            this.panelControl1.TabIndex = 17;
            // 
            // bbiDeleteObject
            // 
            this.bbiDeleteObject.Caption = "Delete Object";
            this.bbiDeleteObject.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteObject.Glyph")));
            this.bbiDeleteObject.Id = 68;
            this.bbiDeleteObject.Name = "bbiDeleteObject";
            this.bbiDeleteObject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteObject_ItemClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "delete_16.png");
            // 
            // frm_UT_Mapping_Map_Snapshot_Map
            // 
            this.ClientSize = new System.Drawing.Size(1026, 700);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.xtraScrollableControl1);
            this.DoubleBuffered = true;
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Mapping_Map_Snapshot_Map";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapping - Save Map Snapshot";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Mapping_Map_Snapshot_Map_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Mapping_Map_Snapshot_Map_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraScrollableControl1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_MapScaleBar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_NorthArrow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizablePictureBox_Legend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resizableMemoEditBoxOSRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01300ATWorkOrderMapPageSizesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCustomSizing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichEditFontSizeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource sp01300ATWorkOrderMapPageSizesBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter sp01300_AT_WorkOrder_Map_Page_SizesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPageHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPageSizeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPageWidth;
        private DevExpress.XtraEditors.CheckEdit ceCustomSizing;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraRichEdit.UI.FontBar fontBar1;
        private DevExpress.XtraRichEdit.UI.ChangeFontNameItem changeFontNameItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraRichEdit.UI.ChangeFontSizeItem changeFontSizeItem1;
        private DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit repositoryItemRichEditFontSizeEdit1;
        private DevExpress.XtraRichEdit.UI.ChangeFontColorItem changeFontColorItem1;
        private DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem changeFontBackColorItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontBoldItem toggleFontBoldItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontItalicItem toggleFontItalicItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem toggleFontUnderlineItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem toggleFontDoubleUnderlineItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem toggleFontStrikeoutItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem toggleFontDoubleStrikeoutItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem toggleFontSuperscriptItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem toggleFontSubscriptItem1;
        private DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem fontSizeIncreaseItem1;
        private DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem fontSizeDecreaseItem1;
        private DevExpress.XtraRichEdit.UI.ClearFormattingItem clearFormattingItem1;
        private DevExpress.XtraRichEdit.UI.ShowFontFormItem showFontFormItem1;
        private DevExpress.XtraRichEdit.UI.RichEditBarController richEditBarController1;
        private DevExpress.XtraRichEdit.UI.ClipboardBar clipboardBar1;
        private DevExpress.XtraRichEdit.UI.CutItem cutItem1;
        private DevExpress.XtraRichEdit.UI.PasteItem pasteItem1;
        private DevExpress.XtraRichEdit.UI.IllustrationsBar illustrationsBar1;
        private DevExpress.XtraRichEdit.UI.InsertPictureItem insertPictureItem1;
        private DevExpress.XtraRichEdit.UI.ZoomBar zoomBar1;
        private DevExpress.XtraRichEdit.UI.ZoomOutItem zoomOutItem1;
        private DevExpress.XtraRichEdit.UI.ZoomInItem zoomInItem1;
        private DevExpress.XtraRichEdit.UI.EditingBar editingBar1;
        private DevExpress.XtraRichEdit.UI.FindItem findItem1;
        private DevExpress.XtraRichEdit.UI.ReplaceItem replaceItem1;
        private DevExpress.XtraRichEdit.UI.CopyItem copyItem1;
        private DevExpress.XtraRichEdit.UI.FileNewItem fileNewItem1;
        private DevExpress.XtraRichEdit.UI.FileOpenItem fileOpenItem1;
        private DevExpress.XtraRichEdit.UI.FileSaveItem fileSaveItem1;
        private DevExpress.XtraRichEdit.UI.FileSaveAsItem fileSaveAsItem1;
        private DevExpress.XtraRichEdit.UI.QuickPrintItem quickPrintItem1;
        private DevExpress.XtraRichEdit.UI.PrintItem printItem1;
        private DevExpress.XtraRichEdit.UI.PrintPreviewItem printPreviewItem1;
        private DevExpress.XtraRichEdit.UI.UndoItem undoItem1;
        private DevExpress.XtraRichEdit.UI.RedoItem redoItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiAddText;
        private DevExpress.XtraBars.BarButtonItem bbiAddRichText;
        private DevExpress.XtraBars.BarButtonItem bbiAddImage;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.BarCheckItem bciLegend;
        private BaseObjects.MoveableMemoEditBox resizableMemoEditBoxOSRef;
        private DevExpress.XtraBars.BarCheckItem bciOSRef;
        private DevExpress.XtraBars.BarCheckItem bciNorthArrow;
        private BaseObjects.MoveablePictureBox resizablePictureBox_NorthArrow;
        private BaseObjects.MoveablePictureBox resizablePictureBox_Legend;
        private DevExpress.XtraBars.BarCheckItem bciScaleBar;
        private BaseObjects.MoveablePictureBox resizablePictureBox_MapScaleBar;
        private DevExpress.XtraBars.BarButtonItem bbiPreview;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteObject;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarEditItem beiPenWidth;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiClearDrawing;
        private DevExpress.XtraBars.BarEditItem beiPenColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit1;
        private DevExpress.XtraBars.BarSubItem bsiSelectedDrawingTool;
        private DevExpress.XtraBars.BarCheckItem bciNoDrawing;
        private DevExpress.XtraBars.BarCheckItem bciDrawFreehand;
        private DevExpress.XtraBars.BarCheckItem bciDrawRectangle;
        private DevExpress.XtraBars.BarCheckItem bciDrawElypsis;
        private DevExpress.XtraBars.BarCheckItem bciDrawLine;
        private DevExpress.XtraBars.BarCheckItem bciDrawArrow;
        private DevExpress.XtraBars.BarEditItem beiArrowSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;

    }
}

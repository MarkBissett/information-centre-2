using System;
using System.Collections;  // Required for Images Hashtable //
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Permission_Document_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInPermissionDocumentIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";
        private string i_str_selected_surveyor_ids = "";
        private string i_str_selected_surveyor_names = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strPermissionFilePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strLinkedDocumentsPath = "";
        private string strMapsPath = "";
        private string strPicturesPath = "";
        Hashtable Images = new Hashtable();

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_UT_Permission_Document_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Permission_Document_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10015;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();
            
            popupContainerControlSurveyors.Size = new System.Drawing.Size(312, 423);

            sp07400_UT_PD_Unpermissioned_WorkTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "ActionID");

            sp07191_UT_Permission_Document_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "PermissionDocumentID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedDocumentID");

            sp07392_UT_PD_Linked_WorkTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "ActionPermissionID");

            sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "LinkedMapID");

            sp07329_UT_Surveyor_List_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07329_UT_Surveyor_List_No_BlankTableAdapter.Fill(this.dataSet_UT_Reporting.sp07329_UT_Surveyor_List_No_Blank);
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            gridControl7.ForceInitialize();

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strMapsPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strMapsPath.EndsWith("\\")) strMapsPath += "\\";

            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Signature Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strSignaturePath.EndsWith("\\")) strSignaturePath += "\\";

            try
            {
                strPermissionFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Permission Document Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strPermissionFilePath.EndsWith("\\")) strPermissionFilePath += "\\";
            try
            {
                 strLinkedDocumentsPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                strPicturesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (strPassedInPermissionDocumentIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit2.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInPermissionDocumentIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_UT_Permission_Document_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Permission_Document_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            base.ImagePreviewClear();  // Clear any previewed map //
            GC.GetTotalMemory(true);

            if (strPassedInPermissionDocumentIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDateFilter", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDateFilter", dateEditToDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorFilter", i_str_selected_surveyor_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //
                int intFoundRow = 0;

                // Surveyor Filter
                string strSurveyorFilter = default_screen_settings.RetrieveSetting("SurveyorFilter");
                if (!string.IsNullOrEmpty(strSurveyorFilter))
                {
                    Array arraySurveyors = strSurveyorFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewSurveyors = (GridView)gridControl7.MainView;
                    viewSurveyors.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arraySurveyors)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewSurveyors.LocateByValue(0, viewSurveyors.Columns["StaffID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewSurveyors.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewSurveyors.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewSurveyors.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit3_Get_Selected();
                }

                // Date Range Filter //
                string strFromDateFilter = default_screen_settings.RetrieveSetting("FromDateFilter");
                if (!(string.IsNullOrEmpty(strFromDateFilter) || Convert.ToDateTime(strFromDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditFromDate.DateTime = Convert.ToDateTime(strFromDateFilter);
                }
                string strToDateFilter = default_screen_settings.RetrieveSetting("ToDateFilter");
                if (!(string.IsNullOrEmpty(strToDateFilter) || Convert.ToDateTime(strToDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditToDate.DateTime = Convert.ToDateTime(strToDateFilter);
                }
                i_str_date_range = PopupContainerEditDateRange_Get_Selected();
                popupContainerDateRange.EditValue = i_str_date_range;

                bbiRefresh.PerformClick();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs5)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            else if (i_int_FocusedGrid == 3)
            {
                view = (GridView)gridControl3.MainView;
            }
            else if (i_int_FocusedGrid == 5)
            {
                view = (GridView)gridControl5.MainView;
            }
            else if (i_int_FocusedGrid == 4)
            {
                view = (GridView)gridControl4.MainView;
            }
            else if (i_int_FocusedGrid == 6)
            {
                view = (GridView)gridControl6.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Permission Documents //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;                  
                }
                bbiBlockEdit.Enabled = false;
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 4)  // Unpermissioned Work //
            {
                if (iBool_AllowAdd && intRowHandles.Length > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowDelete && intRowHandles.Length > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length == 1);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            
            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intRowHandles.Length > 0);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowAdd && intRowHandles.Length > 0);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length == 1);
            
            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1);
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerEdit2.EditValue.ToString() == "Custom Filter" && strPassedInPermissionDocumentIDs != "")  // Load passed in Records //
            {
                sp07191_UT_Permission_Document_ManagerTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07191_UT_Permission_Document_Manager, null, null, strPassedInPermissionDocumentIDs, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                if (i_dt_FromDate == Convert.ToDateTime("01/01/0001")) i_dt_FromDate = new DateTime(1900, 1, 1);
                if (i_dt_ToDate == Convert.ToDateTime("01/01/0001")) i_dt_ToDate = new DateTime(2900, 1, 1);

                sp07191_UT_Permission_Document_ManagerTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07191_UT_Permission_Document_Manager, i_dt_FromDate, i_dt_ToDate, "", i_str_selected_surveyor_ids);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PermissionDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
            Load_Unpermissioned_Work();

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Unpermissioned_Work()
        {
            GridView view = (GridView)gridControl4.MainView;
            view.BeginUpdate();
            sp07400_UT_PD_Unpermissioned_WorkTableAdapter.Fill(dataSet_UT_WorkOrder.sp07400_UT_PD_Unpermissioned_Work);
            RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["PermissionDocumentID"])) + ',';
            }

            // Populate Linked Documents //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 53, strLinkedDocumentsPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            // Populate Linked Work //
            gridControl5.MainView.BeginUpdate();
            this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work.Clear();
            }
            else
            {
                sp07392_UT_PD_Linked_WorkTableAdapter.Fill(dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl5.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs5 != "")
            {
                strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl5.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionPermissionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs5 = "";
            }

            // Populate Linked Maps //
            gridControl6.MainView.BeginUpdate();
            this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Mapping.sp07202_UT_Mapping_Snapshots_Linked_Snapshots.Clear();
            }
            else
            {
                sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Fill(dataSet_UT_Mapping.sp07202_UT_Mapping_Snapshots_Linked_Snapshots, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);  // 1 = Permission Doc, 2 = Work Order //
                this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl6.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs6 != "")
            {
                strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedMapID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Permission Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        // Get the Type of PD to created  //
                        frm_UT_Permission_Doc_Select_Add_Type fChild = new frm_UT_Permission_Doc_Select_Add_Type();
                        fChild.GlobalSettings = this.GlobalSettings;
                        if (fChild.ShowDialog() != DialogResult.OK) return;
                        string strLayout = fChild._SelectedDataEntryScreenName;
                        int ClientID = fChild._SelectedClientID;
                        if (string.IsNullOrEmpty(strLayout)) return;

                        Create_Permission_Document(strLayout, "", ClientID);
                    }
                    break;
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl5.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 53;  // Survey //

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PermissionDocumentID"));
                            fChildForm2.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "OwnerName")) + " - Date: " + Convert.ToDateTime(ParentView.GetRowCellValue(intRowHandles[0], "DateRaised")).ToString("dd/MM/yyyy");
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 4:  // Unpermissioned Work //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        string strLayout = "";
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Work Record to link to a new Permission Document before proceeding.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int LastClientID = 0;
                        int CurrentClientID = 0;
                        string ActionIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            CurrentClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                            if (LastClientID == 0)
                            {
                                LastClientID = CurrentClientID;
                                strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                            }
                            else if (CurrentClientID != LastClientID)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You have selected Work Records from more than one client!\n\nSelect jobs from just one client before proceeding.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            ActionIDs += view.GetRowCellValue(intRowHandle, "ActionID").ToString() + ",";
                        }

                        // Pick up Unique Map IDs from actions to pass to Mapping if open so Thematics can be updated if neccessary //
                        string strSelectedMapIDs = "";
                        string strMapID = "";
                        foreach (DataRow dr in dataSet_UT_WorkOrder.sp07400_UT_PD_Unpermissioned_Work.Rows)
                        {
                            strMapID = dr["MapID"].ToString();
                            if (!strSelectedMapIDs.Contains(strMapID)) strSelectedMapIDs += strMapID + ",";
                        }
                        if (this.ParentForm != null)
                        {
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Mapping")  // Mapping Form //
                                {
                                    var fParentForm = (frm_UT_Mapping)frmChild;
                                    fParentForm.RefreshMapObjects(strSelectedMapIDs, 1);
                                }
                            }
                        }

                        Create_Permission_Document(strLayout, ActionIDs, LastClientID);
                    }
                    break;
                default:
                    break;
            }
        }

        private void Create_Permission_Document(string strLayout, string strActionIDs, int ClientID)
        {
            // Create New Permission Document //
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter CreatePD = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            CreatePD.ChangeConnectionString(strConnectionString);
            int PermissionDocumentID = 0;
            string strReferenceNumber = GlobalSettings.UserForename.Substring(0, 1).ToUpper() + GlobalSettings.UserSurname.Substring(0, 1).ToUpper() + DateTime.Now.ToString("yyyyMMddHHmmss");
            try
            {
                PermissionDocumentID = Convert.ToInt32(CreatePD.sp07391_UT_PD_Insert(DateTime.Now, GlobalSettings.UserID, strReferenceNumber, strActionIDs, ClientID));
                if (PermissionDocumentID <= 0) return;
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Permission Document - [" + ex.Message + "].", "Create Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (PermissionDocumentID <= 0) return;
            UpdateRefreshStatus = 1;
            i_str_AddedRecordIDs1 = PermissionDocumentID.ToString() + ";";

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //

            System.Reflection.MethodInfo method = null;
            if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
            {
                frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
            {
                frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }
        private void Add_Work_To_Existing_Permission_Document()
        {
            if (!iBool_AllowAdd) return;
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            string strLayout = "";
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Work Record to link to an Existing Permission Document before proceeding.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int LastClientID = 0;
            int CurrentClientID = 0;
            string ActionIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                CurrentClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                if (LastClientID == 0)
                {
                    LastClientID = CurrentClientID;
                    strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                }
                else if (CurrentClientID != LastClientID)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You have selected Work Records from more than one client!\n\nSelect jobs from just one client before proceeding.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                ActionIDs += view.GetRowCellValue(intRowHandle, "ActionID").ToString() + ",";
            }
            frm_UT_Select_Permission_Document fChildForm = new frm_UT_Select_Permission_Document();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInClientID = LastClientID;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            int PermissionDocumentID = fChildForm.intSelectedID;
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddWorkToPermissionDocument = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            AddWorkToPermissionDocument.ChangeConnectionString(strConnectionString);
            try
            {
                Convert.ToInt32(AddWorkToPermissionDocument.sp07243_UT_Add_Work_To_Permission_Document(PermissionDocumentID, ActionIDs));
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add work to existing permission document - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (PermissionDocumentID <= 0) return;


            // Permission Document now created or existing one added to, so open it //
            UpdateRefreshStatus = 1;
            i_str_AddedRecordIDs1 = PermissionDocumentID.ToString() + ";";

            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
            if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
            {
                frm_UT_Permission_Doc_Edit_WPD fChildForm2 = new frm_UT_Permission_Doc_Edit_WPD();
                fChildForm2.MdiParent = this.MdiParent;
                fChildForm2.GlobalSettings = this.GlobalSettings;
                fChildForm2.strRecordIDs = PermissionDocumentID + ",";
                fChildForm2.strFormMode = "edit";
                fChildForm2.strCaller = this.Name;
                fChildForm2.intRecordCount = 1;
                fChildForm2.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm2.splashScreenManager = splashScreenManager1;
                fChildForm2.splashScreenManager.ShowWaitForm();
                fChildForm2.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm2, new object[] { null });
            }
            else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
            {
                frm_UT_Permission_Doc_Edit_UKPN fChildForm2 = new frm_UT_Permission_Doc_Edit_UKPN();
                fChildForm2.MdiParent = this.MdiParent;
                fChildForm2.GlobalSettings = this.GlobalSettings;
                fChildForm2.strRecordIDs = PermissionDocumentID + ",";
                fChildForm2.strFormMode = "edit";
                fChildForm2.strCaller = this.Name;
                fChildForm2.intRecordCount = 1;
                fChildForm2.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm2.splashScreenManager = splashScreenManager1;
                fChildForm2.splashScreenManager.ShowWaitForm();
                fChildForm2.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm2, new object[] { null });
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:  // Related Documents Link //
                    if (!iBool_AllowAdd) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "blockadd";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 53;  // Survey //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 53;  // Survey //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Permission Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        string strLayout = "";
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += view.GetRowCellValue(intRowHandle, "PermissionDocumentID").ToString() + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //

                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 53;  // Survey //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 4:  // Unpermissioned Work //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strPicturesPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Permission Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    
                // Check user created record or is a super user - if not de-select row //
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                    }
                    intRowHandles = view.GetSelectedRows();

                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Permission Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Permission Document" : Convert.ToString(intRowHandles.Length) + " Permission Documents") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Permission Document" : "these Permission Documents") + " will no longer be available for selection and any linked work will be unpermissioned!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PermissionDocumentID")) + ",";
                        }

                        // Get unique MapIds from PermissionDocumentIDs so the map can be updated after the records are deleted. //
                        string strMapIDs = "";
                        DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetMapIDs = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                        GetMapIDs.ChangeConnectionString(strConnectionString);
                        try
                        {
                            strMapIDs = GetMapIDs.sp07403_UT_Get_MapIDs_From_PermissionDocumentID(strRecordIDs).ToString();
                        }
                        catch (Exception)
                        {
                            return;
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("permission_document", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Data();
                        Update_Map_Tree_Thematics(strMapIDs);  // Update map //

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        LoadLinkedRecords();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 5:  // Permissioned Work //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl5.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Permissioned Work to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Permissioned Work" : Convert.ToString(intRowHandles.Length) + " Permissioned Works") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Permissioned Work" : "these Permissioned Works") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        string strMapIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionPermissionID")) + ",";
                            strMapIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "MapID")) + ",";
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("permissioned_work", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }

                        Update_Map_Tree_Thematics(strMapIDs);  // Update map //

                        LoadLinkedRecords();
                        Load_Unpermissioned_Work();
                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 4:  // Unpermissioned Work //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Work record to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Work Record" : Convert.ToString(intRowHandles.Length) + " Work Records") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Work Record" : "these Work Records") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("tree_work", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Unpermissioned_Work();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;

            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Permission Documents //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        string strLayout = "";
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to view before proceeding.", "View Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += view.GetRowCellValue(intRowHandle, "PermissionDocumentID").ToString() + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //

                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }                       
                    }
                    break;
                case 4:  // Unpermissioned Work //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strPicturesPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void Update_Map_Tree_Thematics(string strMapIDs)
        {
            // Update Map //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Mapping")  // Mapping Form //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.RefreshMapObjects(strMapIDs, 1);
                    }
                }
            }
        }


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        #region Surveyor Filter Panel

        private void btnSurveyorFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_surveyor_ids = "";    // Reset any prior values first //
            i_str_selected_surveyor_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_surveyor_ids = "";
                return "No Surveyor Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_surveyor_ids = "";
                return "No Surveyor Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_surveyor_ids += Convert.ToString(view.GetRowCellValue(i, "StaffID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_surveyor_names = Convert.ToString(view.GetRowCellValue(i, "StaffName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_surveyor_names += ", " + Convert.ToString(view.GetRowCellValue(i, "StaffName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_surveyor_names) ? "No Surveyor Filter" : i_str_selected_surveyor_names);
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Permission Documents - Click Load Data button";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Permisison Documents to view Linked Documents";
                    break;
                case "gridView5":
                    message = "No Permissioned Work Available - Select one or more Permission Documents to view Linked Permissioned Work";
                    break;
                case "gridView4":
                    message = "No Unpermissioned Work";
                    break;
                case "gridView6":
                    message = "No Linked Maps Available - Select one or more Permisison Documents to view Linked Maps";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView4":
                    LoadLinkedRecords();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //if (view.SelectedRowsCount == 1)
                    //{
                        //GridView gvChild = (GridView)gridControl5.MainView;
                        //bbiShowMap.Enabled = gvChild.DataRowCount > 0;
                    //}
                    //else
                    //{
                        bbiShowMap.Enabled = false;
                    //}
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SignatureFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PDFFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedToDoActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToDoActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SignatureFile").ToString())) e.Cancel = true;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PDFFile").ToString())) e.Cancel = true;
                    break;
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedToDoActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedToDoActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "LinkedActionCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount"));

                if (intValue <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else
                {
                    int intValue2 = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PermissionedCount"));
                    if (intValue2 >= intValue)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                        e.Appearance.BackColor2 = Color.PaleGreen;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
            else if (e.Column.FieldName == "LinkedToDoActionCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToDoActionCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "NotPermissionedCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "NotPermissionedCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "OnHoldCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "OnHoldCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            /*GridView view = (GridView)gridControl1.MainView;
            if (view.SelectedRowsCount != 1) return;
            
            GridView gvChild = (GridView)gridControl5.MainView;
            if (gvChild.DataRowCount <= 0) return;
            int[] intRowHandles = view.GetSelectedRows();
            int[] intRowHandlesChild = gvChild.GetSelectedRows();

            System.Reflection.MethodInfo method = null;
            int intPermissionDocumentID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PermissionDocumentID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "PermissionDocumentID")));
            int intClientID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID")));
 
            int intPoleID = 0;
            string strPoleNumber = "";
            if (intRowHandlesChild.Length > 0)
            {
                intPoleID = (string.IsNullOrEmpty(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleID").ToString()) ? 0 : Convert.ToInt32(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleNumber").ToString()) ? "" : gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleNumber").ToString());
            }
            else
            {
                intPoleID = (string.IsNullOrEmpty(gvChild.GetRowCellValue(0, "PoleID").ToString()) ? 0 : Convert.ToInt32(gvChild.GetRowCellValue(0, "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(gvChild.GetRowCellValue(0, "PoleNumber").ToString()) ? "" : gvChild.GetRowCellValue(0, "PoleNumber").ToString());
            }
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Activate();
                        frm_UT_Mapping frmMapping = (frm_UT_Mapping)frmChild;
                        frmMapping._CurrentPoleID = intPoleID;
                        frmMapping._SelectedPermissionDocumentID = intPermissionDocumentID;
                        frmMapping._SurveyMode = "PlotTrees"; 
                        frmMapping.LoadSurveyIntoMap();
                        frmMapping.SetModeToSurvey(strPoleNumber);
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            // Get the Workspace ID from the Region on the Survey //
            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(intPermissionDocumentID).ToString();
                char[] delimiters = new char[] { '|' };
                string[] strArray = null;
                strArray = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray[0]);
                    strWorkspaceName = strArray[1].ToString();
                    intRegionID = Convert.ToInt32(strArray[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to plot tree via Mapping - the current Survey is linked to a region which has no Mapping Workspace ID.\n\nUpdate the Linked Region with a Mapping Workspace [via the Region Manager] then try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string strSurveyDescription = "";

            // Get Surveyor Name from grid lookup Edit //
            string strSurveyor = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "Surveyor").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "Surveyor").ToString());

            if (string.IsNullOrEmpty(strSurveyor)) strSurveyor = "Surveyor Not Specified";

            strSurveyDescription = (String.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "Unknown Client" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " +
                (String.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "SurveyDate").ToString()) ? "Unknown Date" : Convert.ToDateTime(view.GetRowCellValue(intRowHandles[0], "SurveyDate")).ToString("dd/MM/yyyy")) + " \\ " +
                    strSurveyor;

            // Open Map and show Survey Panel //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedPermissionDocumentID = intPermissionDocumentID;
            frmInstance._SelectedSurveyClientID = intClientID;
            frmInstance._SelectedSurveyDescription = strSurveyDescription;
            frmInstance._PassedInClientIDs = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ClientID"))) ? "" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ClientID")) + ",");
            frmInstance._PassedInRegionIDs = (String.IsNullOrEmpty(intRegionID.ToString()) ? "" : intRegionID.ToString() + ",");
            frmInstance._SurveyMode = "PlotTrees";
            frmInstance._CurrentPoleID = intPoleID;
            frmInstance.SetModeToSurvey(strPoleNumber);
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }

            method = typeof(frm_UT_Mapping).GetMethod("SetToolToSelectPoint");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }*/
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colSignatureFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "SignatureFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strSignaturePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    System.Diagnostics.Process.Start(strFilePath);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (view.FocusedColumn.Name == "colPDFFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PDFFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strPermissionFilePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    //System.Diagnostics.Process.Start(strFilePath);

                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFilePath;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView5

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StatusDescription")
            {
                int intStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PermissionStatusID"));
                switch (intStatus)
                {
                    case 0: // Awaiting Permission //
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case 1:  // Permissioned //
                        e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                        e.Appearance.BackColor2 = Color.PaleGreen;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case 2:  // Not Permissioned //
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case 3:  // On-Hold //
                        e.Appearance.BackColor = Color.LightSteelBlue;
                        e.Appearance.BackColor2 = Color.CornflowerBlue;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:  // Unknown //
                        e.Appearance.BackColor = Color.Gainsboro;
                        e.Appearance.BackColor2 = Color.DarkGray;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                }
            }
        }

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            /*switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }*/
        }

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //if (view.SelectedRowsCount == 1)
                    //{
                    //GridView gvChild = (GridView)gridControl5.MainView;
                    //bbiShowMap.Enabled = gvChild.DataRowCount > 0;
                    //}
                    //else
                    //{
                    bbiShowMap.Enabled = false;
                    //}
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("add to existing".Equals(e.Button.Tag))
                    {
                        Add_Work_To_Existing_Permission_Document();
                    }   
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView6

        private void gridView6_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(33, 33);
            if (e.Column.FieldName == "gridColumnThumbNail" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = strMapsPath + view.GetRowCellValue(rHandle, "DocumentPath").ToString() + "_thumb.jpg";
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 33, 33);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("view".Equals(e.Button.Tag))
                    {
                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        GridView view = (GridView)gridControl6.MainView;
                        string strImagePath = "";
                        strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + ".gif";
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_MouseDown(object sender, MouseEventArgs e)
        {
            // Note that the form must have the Form frmMain2 as its Owner for the message to be passed through to show the Image in the preview pane. Clear message sent on Form Closing //
            GridView view = (GridView)gridControl6.MainView;

            Point ptMousePosition = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo ghi = view.CalcHitInfo(ptMousePosition);
            if (ghi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                if (ghi.Column.FieldName == "gridColumnThumbNail")
                {
                    string strImagePath = strMapsPath + view.GetRowCellValue(ghi.RowHandle, "DocumentPath").ToString() + ".gif";
                    try
                    {
                        Image PassedImage = Image.FromFile(strImagePath);
                        base.ImagePreview(PassedImage, "scale", "no");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion






    }
}


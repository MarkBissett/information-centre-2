namespace WoodPlan5
{
    partial class frm_UT_Jobs_In_Memory_Paste
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Jobs_In_Memory_Paste));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07383UTSurveyedTreesPasteJobsToBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditCopyMaterials = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditCopyEquipment = new DevExpress.XtraEditors.CheckEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07384UTCopyjobsfromMemoryActionsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPossibleFlail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07383UTSurveyedTreesPasteJobsToBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCopyMaterials.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCopyEquipment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07384UTCopyjobsfromMemoryActionsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(868, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 498);
            this.barDockControlBottom.Size = new System.Drawing.Size(868, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 472);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(868, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 472);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Surveyed Trees to Paste Against";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.buttonEdit1);
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnOK);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainerControl1.Panel2.Controls.Add(this.checkEditCopyMaterials);
            this.splitContainerControl1.Panel2.Controls.Add(this.checkEditCopyEquipment);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Actions To Paste";
            this.splitContainerControl1.Size = new System.Drawing.Size(868, 472);
            this.splitContainerControl1.SplitterPosition = 190;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(843, 187);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.sp07383UTSurveyedTreesPasteJobsToBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(843, 187);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07383UTSurveyedTreesPasteJobsToBindingSource
            // 
            this.sp07383UTSurveyedTreesPasteJobsToBindingSource.DataMember = "sp07383_UT_Surveyed_Trees_Paste_Jobs_To";
            this.sp07383UTSurveyedTreesPasteJobsToBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID1,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks2,
            this.colGUID1,
            this.colClientName2,
            this.colSurveyDate1,
            this.colSurveyDescription,
            this.colSurveyor1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colExchequerNumber,
            this.colContractValue,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.gridColumn1,
            this.gridColumn2,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID1,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptior,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colIsBaseClimbable,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colShutdownID,
            this.colSurveyedTreeID1,
            this.colReferenceNumber,
            this.colLinkedSpecies});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Date Surveyed";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 4;
            this.colSurveyedDate.Width = 93;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 7;
            this.colInfestationRate.Width = 100;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 8;
            this.colIsShutdownRequired.Width = 115;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 9;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 10;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance";
            this.colClearanceDistance.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 14;
            this.colClearanceDistance.Width = 113;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 15;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 16;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 17;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 3M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 18;
            this.colTreeWithin3Meters.Width = 93;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 19;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 21;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 224;
            // 
            // colSurveyDate1
            // 
            this.colSurveyDate1.Caption = "Survey - Date";
            this.colSurveyDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSurveyDate1.FieldName = "SurveyDate";
            this.colSurveyDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate1.Name = "colSurveyDate1";
            this.colSurveyDate1.OptionsColumn.AllowEdit = false;
            this.colSurveyDate1.OptionsColumn.AllowFocus = false;
            this.colSurveyDate1.OptionsColumn.ReadOnly = true;
            this.colSurveyDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate1.Width = 88;
            // 
            // colSurveyDescription
            // 
            this.colSurveyDescription.Caption = "Survey - Description";
            this.colSurveyDescription.FieldName = "SurveyDescription";
            this.colSurveyDescription.Name = "colSurveyDescription";
            this.colSurveyDescription.OptionsColumn.AllowEdit = false;
            this.colSurveyDescription.OptionsColumn.AllowFocus = false;
            this.colSurveyDescription.OptionsColumn.ReadOnly = true;
            this.colSurveyDescription.Width = 268;
            // 
            // colSurveyor1
            // 
            this.colSurveyor1.Caption = "Surveyor";
            this.colSurveyor1.FieldName = "Surveyor";
            this.colSurveyor1.Name = "colSurveyor1";
            this.colSurveyor1.OptionsColumn.AllowEdit = false;
            this.colSurveyor1.OptionsColumn.AllowFocus = false;
            this.colSurveyor1.OptionsColumn.ReadOnly = true;
            this.colSurveyor1.Visible = true;
            this.colSurveyor1.VisibleIndex = 29;
            this.colSurveyor1.Width = 112;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Width = 110;
            // 
            // colExchequerNumber
            // 
            this.colExchequerNumber.Caption = "Exchequer Number";
            this.colExchequerNumber.FieldName = "ExchequerNumber";
            this.colExchequerNumber.Name = "colExchequerNumber";
            this.colExchequerNumber.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber.Width = 112;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Span Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Width = 74;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 1;
            this.colPoleNumber.Width = 105;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Pole Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Width = 117;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Pole Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Width = 120;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Tranformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 22;
            this.colIsTransformer.Width = 87;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Circuit Number";
            this.gridColumn1.FieldName = "CircuitNumber";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 111;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Circuit Name";
            this.gridColumn2.FieldName = "CircuitName";
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 81;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 5;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID1
            // 
            this.colSurveyStatusID1.Caption = "Survey Status ID";
            this.colSurveyStatusID1.FieldName = "SurveyStatusID";
            this.colSurveyStatusID1.Name = "colSurveyStatusID1";
            this.colSurveyStatusID1.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID1.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID1.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID1.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 11;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 23;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 25;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 24;
            this.colDeferredUnits.Width = 91;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f2";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 20;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 28;
            this.colRevisitDate.Width = 79;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 26;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 27;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 12;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 13;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.Width = 100;
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.Width = 100;
            // 
            // colShutdownID
            // 
            this.colShutdownID.Caption = "Shutdown ID";
            this.colShutdownID.FieldName = "ShutdownID";
            this.colShutdownID.Name = "colShutdownID";
            this.colShutdownID.OptionsColumn.AllowEdit = false;
            this.colShutdownID.OptionsColumn.AllowFocus = false;
            this.colShutdownID.OptionsColumn.ReadOnly = true;
            this.colShutdownID.Width = 74;
            // 
            // colSurveyedTreeID1
            // 
            this.colSurveyedTreeID1.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID1.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID1.Name = "colSurveyedTreeID1";
            this.colSurveyedTreeID1.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID1.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID1.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID1.Width = 106;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Tree Reference #";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 2;
            this.colReferenceNumber.Width = 122;
            // 
            // colLinkedSpecies
            // 
            this.colLinkedSpecies.Caption = "Linked Species";
            this.colLinkedSpecies.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colLinkedSpecies.FieldName = "LinkedSpecies";
            this.colLinkedSpecies.Name = "colLinkedSpecies";
            this.colLinkedSpecies.OptionsColumn.ReadOnly = true;
            this.colLinkedSpecies.Visible = true;
            this.colLinkedSpecies.VisibleIndex = 3;
            this.colLinkedSpecies.Width = 110;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEdit1.Location = new System.Drawing.Point(131, 250);
            this.buttonEdit1.MenuManager = this.barManager1;
            this.buttonEdit1.Name = "buttonEdit1";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Job Reference Prefix - Choose - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the Choose Sequence screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Sequence", superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEdit1.Properties.MaxLength = 20;
            this.buttonEdit1.Size = new System.Drawing.Size(185, 20);
            this.buttonEdit1.TabIndex = 11;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            this.buttonEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEdit1_Validating);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 253);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Job Reference Prefix:";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(687, 247);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(768, 247);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // checkEditCopyMaterials
            // 
            this.checkEditCopyMaterials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditCopyMaterials.EditValue = 1;
            this.checkEditCopyMaterials.Location = new System.Drawing.Point(496, 250);
            this.checkEditCopyMaterials.MenuManager = this.barManager1;
            this.checkEditCopyMaterials.Name = "checkEditCopyMaterials";
            this.checkEditCopyMaterials.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditCopyMaterials.Properties.Appearance.Options.UseFont = true;
            this.checkEditCopyMaterials.Properties.Caption = "Copy Linked Materials";
            this.checkEditCopyMaterials.Properties.ValueChecked = 1;
            this.checkEditCopyMaterials.Properties.ValueUnchecked = 0;
            this.checkEditCopyMaterials.Size = new System.Drawing.Size(155, 19);
            this.checkEditCopyMaterials.TabIndex = 3;
            // 
            // checkEditCopyEquipment
            // 
            this.checkEditCopyEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditCopyEquipment.EditValue = 1;
            this.checkEditCopyEquipment.Location = new System.Drawing.Point(332, 251);
            this.checkEditCopyEquipment.MenuManager = this.barManager1;
            this.checkEditCopyEquipment.Name = "checkEditCopyEquipment";
            this.checkEditCopyEquipment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditCopyEquipment.Properties.Appearance.Options.UseFont = true;
            this.checkEditCopyEquipment.Properties.Caption = "Copy Linked Equipment";
            this.checkEditCopyEquipment.Properties.ValueChecked = 1;
            this.checkEditCopyEquipment.Properties.ValueUnchecked = 0;
            this.checkEditCopyEquipment.Size = new System.Drawing.Size(153, 19);
            this.checkEditCopyEquipment.TabIndex = 1;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(845, 244);
            this.gridSplitContainer2.TabIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07384UTCopyjobsfromMemoryActionsListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditHours2,
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditMeters});
            this.gridControl2.Size = new System.Drawing.Size(845, 244);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07384UTCopyjobsfromMemoryActionsListBindingSource
            // 
            this.sp07384UTCopyjobsfromMemoryActionsListBindingSource.DataMember = "sp07384_UT_Copy_jobs_from_Memory_Actions_List";
            this.sp07384UTCopyjobsfromMemoryActionsListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber1,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks1,
            this.colGUID3,
            this.colJobDescription,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.colAchievableClearance,
            this.colPossibleFlail,
            this.colClientName1,
            this.colRegionName,
            this.colRegionNumber1,
            this.colSubAreaName,
            this.colSubAreaNumber1,
            this.colPrimaryName,
            this.colPrimaryNumber1,
            this.colFeederName,
            this.colFeederNumber1,
            this.colCircuitName,
            this.colCircuitNumber,
            this.gridColumn3,
            this.colTreeReference,
            this.colVoltage,
            this.colActionStatus,
            this.colActionStatusID,
            this.gridColumn4,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementWhips,
            this.colTreeReplacementStandards,
            this.gridColumn5,
            this.gridColumn6,
            this.colPossibleLiveWork});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber1
            // 
            this.colReferenceNumber1.Caption = "Job Reference #";
            this.colReferenceNumber1.FieldName = "ReferenceNumber";
            this.colReferenceNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber1.Name = "colReferenceNumber1";
            this.colReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber1.Visible = true;
            this.colReferenceNumber1.VisibleIndex = 1;
            this.colReferenceNumber1.Width = 102;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 9;
            this.colDateRaised.Width = 96;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 10;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 11;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 13;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 23;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 24;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 31;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 32;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 27;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 28;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 19;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 20;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Visible = true;
            this.colWorkOrderID.VisibleIndex = 36;
            this.colWorkOrderID.Width = 91;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Visible = true;
            this.colSelfBillingInvoiceID.VisibleIndex = 37;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.Visible = true;
            this.colFinanceSystemBillingID.VisibleIndex = 38;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 35;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 141;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 21;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 22;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 25;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 26;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 29;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 30;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 33;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 34;
            this.colActualMaterialsSell.Width = 101;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Achievable Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 15;
            this.colAchievableClearance.Width = 124;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "######0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colPossibleFlail
            // 
            this.colPossibleFlail.Caption = "Possible Flail";
            this.colPossibleFlail.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colPossibleFlail.FieldName = "PossibleFlail";
            this.colPossibleFlail.Name = "colPossibleFlail";
            this.colPossibleFlail.OptionsColumn.AllowEdit = false;
            this.colPossibleFlail.OptionsColumn.AllowFocus = false;
            this.colPossibleFlail.OptionsColumn.ReadOnly = true;
            this.colPossibleFlail.Visible = true;
            this.colPossibleFlail.VisibleIndex = 14;
            this.colPossibleFlail.Width = 80;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 2;
            this.colClientName1.Width = 128;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 3;
            this.colRegionName.Width = 110;
            // 
            // colRegionNumber1
            // 
            this.colRegionNumber1.Caption = "Region Number";
            this.colRegionNumber1.FieldName = "RegionNumber";
            this.colRegionNumber1.Name = "colRegionNumber1";
            this.colRegionNumber1.OptionsColumn.AllowEdit = false;
            this.colRegionNumber1.OptionsColumn.AllowFocus = false;
            this.colRegionNumber1.OptionsColumn.ReadOnly = true;
            this.colRegionNumber1.Width = 94;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 4;
            this.colSubAreaName.Width = 109;
            // 
            // colSubAreaNumber1
            // 
            this.colSubAreaNumber1.Caption = "Sub-Area Number";
            this.colSubAreaNumber1.FieldName = "SubAreaNumber";
            this.colSubAreaNumber1.Name = "colSubAreaNumber1";
            this.colSubAreaNumber1.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber1.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber1.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber1.Width = 106;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 5;
            this.colPrimaryName.Width = 101;
            // 
            // colPrimaryNumber1
            // 
            this.colPrimaryNumber1.Caption = "Primary Number";
            this.colPrimaryNumber1.FieldName = "PrimaryNumber";
            this.colPrimaryNumber1.Name = "colPrimaryNumber1";
            this.colPrimaryNumber1.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber1.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber1.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber1.Width = 97;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 6;
            this.colFeederName.Width = 93;
            // 
            // colFeederNumber1
            // 
            this.colFeederNumber1.Caption = "Feeder Number";
            this.colFeederNumber1.FieldName = "FeederNumber";
            this.colFeederNumber1.Name = "colFeederNumber1";
            this.colFeederNumber1.OptionsColumn.AllowEdit = false;
            this.colFeederNumber1.OptionsColumn.AllowFocus = false;
            this.colFeederNumber1.OptionsColumn.ReadOnly = true;
            this.colFeederNumber1.Width = 95;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 7;
            this.colCircuitName.Width = 117;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Width = 91;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Pole Number";
            this.gridColumn3.FieldName = "PoleNumber";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 8;
            this.gridColumn3.Width = 94;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 8;
            this.colTreeReference.Width = 109;
            // 
            // colVoltage
            // 
            this.colVoltage.Caption = "Voltage";
            this.colVoltage.FieldName = "Voltage";
            this.colVoltage.Name = "colVoltage";
            this.colVoltage.OptionsColumn.AllowEdit = false;
            this.colVoltage.OptionsColumn.AllowFocus = false;
            this.colVoltage.OptionsColumn.ReadOnly = true;
            // 
            // colActionStatus
            // 
            this.colActionStatus.Caption = "Action Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 12;
            this.colActionStatus.Width = 85;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.Caption = "Action Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            this.colActionStatusID.Width = 99;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Revisit Date";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn4.FieldName = "RevisitDate";
            this.gridColumn4.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 39;
            this.gridColumn4.Width = 79;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 40;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 41;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 42;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 43;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 44;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 45;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 46;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Actual Hours";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditHours2;
            this.gridColumn5.FieldName = "ActualHours";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 18;
            this.gridColumn5.Width = 82;
            // 
            // repositoryItemTextEditHours2
            // 
            this.repositoryItemTextEditHours2.AutoHeight = false;
            this.repositoryItemTextEditHours2.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours2.Name = "repositoryItemTextEditHours2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Estimated Hours";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEditHours2;
            this.gridColumn6.FieldName = "EstimatedHours";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 17;
            this.gridColumn6.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 16;
            this.colPossibleLiveWork.Width = 109;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter
            // 
            this.sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.bsiInformation.Caption = "Tick Surveyed Trees to Paste Against , Tick the Jobs to Paste and select the Job " +
    "Reference Prefix then click OK";
            this.bsiInformation.Id = 30;
            this.bsiInformation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiInformation.ImageOptions.Image")));
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.Size = new System.Drawing.Size(32, 0);
            this.bsiInformation.Width = 32;
            // 
            // sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter
            // 
            this.sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // frm_UT_Jobs_In_Memory_Paste
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(868, 528);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Jobs_In_Memory_Paste";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilities - Paste Jobs from Memory";
            this.Load += new System.EventHandler(this.frm_UT_Jobs_In_Memory_Paste_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07383UTSurveyedTreesPasteJobsToBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCopyMaterials.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCopyEquipment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07384UTCopyjobsfromMemoryActionsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource sp07383UTSurveyedPolesPasteJobsToBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownID;
        private System.Windows.Forms.BindingSource sp07384UTCopyjobsfromMemoryActionsListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleFlail;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltage;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.CheckEdit checkEditCopyEquipment;
        private DevExpress.XtraEditors.CheckEdit checkEditCopyMaterials;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private System.Windows.Forms.BindingSource sp07383UTSurveyedTreesPasteJobsToBindingSource;
        private DataSet_UT_EditTableAdapters.sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSpecies;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_UT_Permission_Document_Add_Unpermissioned_Confirm : DevExpress.XtraEditors.XtraForm
    {
        #region Instance Variables

        public string _CustomerName = "";
        #endregion

        public frm_UT_Permission_Document_Add_Unpermissioned_Confirm()
        {
            InitializeComponent();
        }

        private void frm_UT_Permission_Document_Add_Unpermissioned_Confirm_Load(object sender, EventArgs e)
        {
            labelCustomerName.Text = _CustomerName;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }





    }
}
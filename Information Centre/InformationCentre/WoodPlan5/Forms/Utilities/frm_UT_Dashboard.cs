﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraCharts;

using DevExpress.XtraPrinting;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Dashboard : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public bool iBool_AllowDelete = false;
        public bool iBool_AllowAdd = false;
        public bool iBool_AllowEdit = false;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string _FilteredClientIDs = "";
        private string _FilteredRegionIDs = "";
        private string _FilteredSubAreaIDs = "";
        private string _FilteredPrimaryIDs = "";
        private string _FilteredFeederIDs = "";
        private string _FilteredCircuitIDs = "";
        private string _FilteredCircuitNames = "";

        private string i_str_selected_voltage_ids = "";
        private string i_str_selected_voltage_descriptions = "";

        private string i_str_selected_records = "";

        private string i_str_selected_team_names = "";
        private string i_str_selected_team_ids = "";
        private string i_str_selected_surveyor_names = "";
        private string i_str_selected_surveyor_ids = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;

        #endregion

        public frm_UT_Dashboard()
        {
            InitializeComponent();
        }

        private void frm_UT_Dashboard_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10020;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            i_dtStart = DateTime.Today.AddYears(-1);
            i_dtEnd = DateTime.Today.AddDays(1).AddMilliseconds(-1);
            dateEditFrom.DateTime = i_dtStart;
            dateEditTo.DateTime = i_dtEnd;
            barEditItemPopupDateFilter.EditValue = PopupContainerEditDateFilter_GetDateRange();

            spinEditYear.EditValue = DateTime.Now.Year;

            dateEditYTDStart.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dateEditGroupingEndDate.DateTime = DateTime.Today.AddDays(Convert.ToDouble(DaysToAdd(DateTime.Now.DayOfWeek, DayOfWeek.Friday))).AddDays(1).AddSeconds(-1);;  // Get Friday closest to todays date //
            spinEditDaysGrouping.EditValue = 7;

            sp07327_UT_Voltages_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07327_UT_Voltages_Filter_ListTableAdapter.Fill(dataSet_UT_Reporting.sp07327_UT_Voltages_Filter_List);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07329_UT_Surveyor_List_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07329_UT_Surveyor_List_No_BlankTableAdapter.Fill(dataSet_UT_Reporting.sp07329_UT_Surveyor_List_No_Blank);
            gridControl1.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp07330_UT_Team_List_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07330_UT_Team_List_No_BlankTableAdapter.Fill(dataSet_UT_Reporting.sp07330_UT_Team_List_No_Blank);
            gridControl3.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07339_UT_Reporting_Surveyor_metricsTableAdapter.Connection.ConnectionString = strConnectionString;

            sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter.Connection.ConnectionString = strConnectionString;
            sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter.Connection.ConnectionString = strConnectionString;
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            Application.DoEvents();  // Allow Form time to repaint itself //
            SetMenuStatus();

            splitContainerControl4.SplitterPosition = splitContainerControl4.Width / 3;
            splitContainerControl5.SplitterPosition = splitContainerControl5.Width / 2;
            splitContainerControl6.SplitterPosition = splitContainerControl6.Height / 2;
            splitContainerControl7.SplitterPosition = splitContainerControl7.Height / 2;
            splitContainerControl8.SplitterPosition = splitContainerControl8.Height / 2;
            splitContainerControl1.SplitterPosition = splitContainerControl1.Height / 2;
            
            LoadLastSavedUserScreenSettings();
            Load_Data();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_UT_Dashboard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();

        }


        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            bbiDelete.Enabled = false;
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        public override void PostLoadView(object objParameter)
        {
            Set_Column_Captions(Convert.ToInt32(spinEditDaysGrouping.EditValue));
        }

        private void ProcessPermissionsForForm()
        {
            /*for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }*/
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            Load_Data();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Voltage Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("VoltageFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayValues = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayValues)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    barEditItemPopupVoltageFilter.EditValue = popupContainerControlVoltageFilter_Get_Selected();
                }

                // Surveyor Filter //
                intFoundRow = 0;
                strFilter = default_screen_settings.RetrieveSetting("SurveyorFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayValues = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl1.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayValues)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["StaffID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    barEditItemPopupSurveyorFilter.EditValue = popupContainerControlSurveyorFilter_Get_Selected();
                }

                // Team Filter //
                intFoundRow = 0;
                strFilter = default_screen_settings.RetrieveSetting("TeamFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayValues = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayValues)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["TeamID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    barEditItemPopupTeamFilter.EditValue = popupContainerControlTeamFilter_Get_Selected();
                }

                strFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredClientIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("RegionFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredRegionIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("SubAreaFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredSubAreaIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("PrimaryFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredPrimaryIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("FeederFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredFeederIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("CircuitFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredCircuitIDs = strFilter;
                }

                strFilter = default_screen_settings.RetrieveSetting("CircuitNamesFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    _FilteredCircuitNames = strFilter;
                    buttonEditFilterCircuits.EditValue = _FilteredCircuitNames;
                }

                strFilter = default_screen_settings.RetrieveSetting("Chart1ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart1ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart1ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart1ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart3ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart3ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart3ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart3ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart2ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart2ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart2ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart2ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart4ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart4ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart4ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart4ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart5ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart5ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart5ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart5ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart5ShowTarget");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart5ShowTarget.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart6ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart6ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart6ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart6ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart7ShowLegend");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart7ShowLegend.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("Chart7ShowLabels");
                if (!string.IsNullOrEmpty(strFilter)) checkEditChart7ShowLabels.Checked = Convert.ToBoolean(strFilter);

                strFilter = default_screen_settings.RetrieveSetting("SurveyorPerformanceMetric1");
                if (!string.IsNullOrEmpty(strFilter)) comboBoxEditMetric1.EditValue = strFilter;

                strFilter = default_screen_settings.RetrieveSetting("SurveyorPerformanceDateDivision1");
                if (!string.IsNullOrEmpty(strFilter)) comboBoxEditDateDivision1.EditValue = strFilter;

                strFilter = default_screen_settings.RetrieveSetting("SurveyorPerformanceMetric2");
                if (!string.IsNullOrEmpty(strFilter)) comboBoxEditMetric2.EditValue = strFilter;

                strFilter = default_screen_settings.RetrieveSetting("SurveyorPerformanceDateDivision2");
                if (!string.IsNullOrEmpty(strFilter)) comboBoxEditDateDivision2.EditValue = strFilter;

            }
        }

        private void Load_Data()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            try
            {
                sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter.Fill(dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed, i_dtStart, i_dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids);
                sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter.Fill(dataSet_UT_Reporting.sp07331_UT_Reporting_Teams_by_Spans_Worked_On, i_dtStart, i_dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_team_ids);
                
                DateTime dtStart = new DateTime(Convert.ToInt32(spinEditYear.EditValue), 1, 1);
                DateTime dtEnd = new DateTime(Convert.ToInt32(spinEditYear.EditValue), 12, 31);
                sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter.Fill(dataSet_UT_Reporting.sp07335_UT_Reporting_Clear_Span_Target_V_Actual, dtStart, dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids);
                sp07339_UT_Reporting_Surveyor_metricsTableAdapter.Fill(dataSet_UT_Reporting.sp07339_UT_Reporting_Surveyor_metrics, i_dtStart, i_dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids);

                // Surveyor Metrics 2 on Page 3 //
                DateTime dtGroupingStartDate = dateEditGroupingEndDate.DateTime.AddDays(Convert.ToInt32(spinEditDaysGrouping.EditValue) * -1);
                sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter.Fill(dataSet_UT_Reporting.sp07349_UT_Reporting_Surveyor_metrics_2, dateEditYTDStart.DateTime, DateTime.Today.AddDays(1).AddSeconds(-1), dtGroupingStartDate, dateEditGroupingEndDate.DateTime, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids);
                Set_Column_Captions(Convert.ToInt32(spinEditDaysGrouping.EditValue));
            }
            catch (Exception) { }
            Load_Chart1();
            Load_Chart2();
            Load_Chart5();
            Load_Chart6();
            Load_Chart7();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region Print Preview

        private void bbiPrintPreviewScreen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintingSystem ps = new PrintingSystem();  // Create a PrintingSystem component //          
            DevExpress.XtraPrintingLinks.CompositeLink compositeLink = new DevExpress.XtraPrintingLinks.CompositeLink();
            compositeLink.PrintingSystem = ps;
            compositeLink.PaperKind = System.Drawing.Printing.PaperKind.A4;   // Set the paper format //
            compositeLink.Landscape = true;
            compositeLink.Margins = new System.Drawing.Printing.Margins(10, 10, 30, 30);
            compositeLink.CreateReportHeaderArea += new CreateAreaEventHandler(compositeLink_CreateReportHeaderArea);
            compositeLink.CreateReportFooterArea += new CreateAreaEventHandler(compositeLink_CreateReportFooterArea);

            PrintableComponentLink link = new PrintableComponentLink();
            link.Component = layoutControl1;
            compositeLink.Links.Add(link);
            compositeLink.CreateDocument();
            compositeLink.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            compositeLink.ShowRibbonPreviewDialog(this.LookAndFeel);
        }

        private void compositeLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            string reportHeader = "Utilties - Dashboard";
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Near);
            e.Graph.Font = new Font("Tahoma", 14, FontStyle.Bold);
            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 30);
            e.Graph.DrawString(reportHeader, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None);
        }

        private void compositeLink_CreateReportFooterArea(object sender, CreateAreaEventArgs e)
        {
            string reportFooter = "Date Printed: " + String.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Near);
            e.Graph.Font = new Font("Tahoma", 10, FontStyle.Regular);
            RectangleF rec = new RectangleF(0, 10, e.Graph.ClientPageSize.Width, 18);
            e.Graph.DrawString(reportFooter, Color.Black, rec, DevExpress.XtraPrinting.BorderSide.None);
        }

        #endregion


        #region Popup Date Filter

        private void repositoryItemPopupContainerEditDateFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateFilter_GetDateRange();
        }

        private string PopupContainerEditDateFilter_GetDateRange()
        {
            string strValue = "";
            i_dtStart = dateEditFrom.DateTime;
            i_dtEnd = dateEditTo.DateTime;
            strValue = i_dtStart.ToString("dd/MM/yyyy HH:mm") +" - " + i_dtEnd.ToString("dd/MM/yyyy HH:mm");
            return strValue;
        }

        private void btnOK1_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        #region Popup Voltage Filter

        private void VoltageFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditVoltageFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlVoltageFilter_Get_Selected();
        }

        private string popupContainerControlVoltageFilter_Get_Selected()
        {
            i_str_selected_voltage_ids = "";    // Reset any prior values first //
            i_str_selected_voltage_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_voltage_ids = "";
                return "No Voltage Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_voltage_ids = "";
                return "No Voltage Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_voltage_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_voltage_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_voltage_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_voltage_descriptions) ? "No Voltage Filter" : i_str_selected_voltage_descriptions);
        }

        #endregion


        #region Popup Surveyor Filter

        private void SurveyorFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditSurveyorFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlSurveyorFilter_Get_Selected();
        }

        private string popupContainerControlSurveyorFilter_Get_Selected()
        {
            i_str_selected_surveyor_ids = "";    // Reset any prior values first //
            i_str_selected_surveyor_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_surveyor_ids = "";
                return "No Surveyor Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_surveyor_ids = "";
                return "No Surveyor Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_surveyor_ids += Convert.ToString(view.GetRowCellValue(i, "StaffID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_surveyor_names = Convert.ToString(view.GetRowCellValue(i, "DisplayName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_surveyor_names += ", " + Convert.ToString(view.GetRowCellValue(i, "DisplayName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_surveyor_names) ? "No Surveyor Filter" : i_str_selected_surveyor_names);
        }

        #endregion


        #region Popup Team Filter

        private void TeamFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditTeamFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlTeamFilter_Get_Selected();
        }

        private string popupContainerControlTeamFilter_Get_Selected()
        {
            i_str_selected_team_ids = "";    // Reset any prior values first //
            i_str_selected_team_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_team_ids = "";
                return "No Team Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_team_ids = "";
                return "No Team Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_team_ids += Convert.ToString(view.GetRowCellValue(i, "TeamID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_team_names = Convert.ToString(view.GetRowCellValue(i, "TeamName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_team_names += ", " + Convert.ToString(view.GetRowCellValue(i, "TeamName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_team_names) ? "No Team Filter" : i_str_selected_team_names);
        }

        #endregion


        #region Popup Surveyor Performance

        private void repositoryItemPopupContainerEditSurveyorPerformance_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateFilter_GetSurveyorPerformance();
        }

        private string PopupContainerEditDateFilter_GetSurveyorPerformance()
        {
            return comboBoxEditMetric1.EditValue + " By " + comboBoxEditDateDivision1.EditValue + "," + comboBoxEditMetric2.EditValue + " By " + comboBoxEditDateDivision2.EditValue;
        }

        private void btnOk_5_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        #endregion



        private void repositoryItemButtonEditFilterCircuits_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_UT_Mapping_Pole_Filter fChildForm = new frm_UT_Mapping_Pole_Filter();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInClientIDs = _FilteredClientIDs;
                fChildForm._PassedInRegionIDs = _FilteredRegionIDs;
                fChildForm._PassedInSubAreaIDs = _FilteredSubAreaIDs;
                fChildForm._PassedInPrimaryIDs = _FilteredPrimaryIDs;
                fChildForm._PassedInFeederIDs = _FilteredFeederIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    _FilteredClientIDs = fChildForm.i_str_selected_client_ids;
                    _FilteredRegionIDs = fChildForm.i_str_selected_region_ids;
                    _FilteredSubAreaIDs = fChildForm.i_str_selected_subarea_ids;
                    _FilteredPrimaryIDs = fChildForm.i_str_selected_primary_ids;
                    _FilteredFeederIDs = fChildForm.i_str_selected_feeder_ids;
                    _FilteredCircuitIDs = fChildForm.i_str_selected_circuit_ids;
                    _FilteredCircuitNames = fChildForm.i_str_selected_circuit_names;
                    buttonEditFilterCircuits.EditValue = _FilteredCircuitNames;
                }
            }
        }

        private void frm_UT_Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", _FilteredClientIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RegionFilter", _FilteredRegionIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SubAreaFilter", _FilteredSubAreaIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PrimaryFilter", _FilteredPrimaryIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FeederFilter", _FilteredFeederIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CircuitFilter", _FilteredCircuitIDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CircuitNamesFilter", _FilteredCircuitNames);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "VoltageFilter", i_str_selected_voltage_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorFilter", i_str_selected_surveyor_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TeamFilter", i_str_selected_team_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart1ShowLegend", checkEditChart1ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart1ShowLabels", checkEditChart1ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart3ShowLegend", checkEditChart3ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart3ShowLabels", checkEditChart3ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart2ShowLegend", checkEditChart2ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart2ShowLabels", checkEditChart2ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart4ShowLegend", checkEditChart4ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart4ShowLabels", checkEditChart4ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart5ShowLegend", checkEditChart5ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart5ShowLabels", checkEditChart5ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart5ShowTarget", checkEditChart5ShowTarget.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart6ShowLegend", checkEditChart6ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart6ShowLabels", checkEditChart6ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart7ShowLegend", checkEditChart7ShowLegend.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Chart7ShowLabels", checkEditChart7ShowLabels.Checked.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorPerformanceMetric1", comboBoxEditMetric1.EditValue.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorPerformanceDateDivision1", comboBoxEditDateDivision1.EditValue.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorPerformanceMetric2", comboBoxEditMetric2.EditValue.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SurveyorPerformanceDateDivision2", comboBoxEditDateDivision2.EditValue.ToString());
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        private void chartControl3_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LegendText += " (" + String.Format("{0}", ((DataRowView)e.SeriesPoint.Tag)["SurveyedSpanCount"]) + ")";
        }


        #region Chart Control 1

        private void Load_Chart1()
        {
            chartControl1.Series.Clear();
            chartControl1.Legend.Visibility = (checkEditChart1ShowLegend.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            //chartControl1.Titles.Clear();

            // Create 5 Series //
            Series series1 = new Series("Clear Spans", ViewType.StackedBar);
            series1.View.Color = Color.FromArgb(0, 176, 80);
            series1.LabelsVisibility = (checkEditChart1ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series2 = new Series("Spans Requiring Work", ViewType.StackedBar);
            series2.View.Color = Color.FromArgb(0x6B, 0x6F, 0xC2);
            series2.LabelsVisibility = (checkEditChart1ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series3 = new Series("Spans To Be Surveyed", ViewType.StackedBar);
            series3.View.Color = Color.Yellow;
            series3.LabelsVisibility = (checkEditChart1ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series4 = new Series("Total Spans Surveyed", ViewType.Line);
            series4.View.Color = Color.Tomato;
            series4.LabelsVisibility = (checkEditChart1ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series5 = new Series("Total Spans Allocated", ViewType.Line);
            series5.View.Color = Color.Gray;
            series5.LabelsVisibility = (checkEditChart1ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            chartControl1.Legend.FillStyle.FillMode = FillMode.Solid;
           
            // Add points to them //
            foreach (DataRow dr in this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed.Rows)
            {
                SeriesPoint sp1 = new SeriesPoint(dr["SurveyorName"], dr["SurveyedSpanClear"]);
                sp1.Tag = dr["StaffID"];
                series1.Points.Add(sp1);

                SeriesPoint sp2 = new SeriesPoint(dr["SurveyorName"], dr["SurveyedSpanNotClear"]);
                sp2.Tag = dr["StaffID"];
                series2.Points.Add(sp2);

                SeriesPoint sp3 = new SeriesPoint(dr["SurveyorName"], dr["ToBeSurveyedCount"]);
                sp3.Tag = dr["StaffID"];
                series3.Points.Add(sp3);

                SeriesPoint sp4 = new SeriesPoint(dr["SurveyorName"], dr["SurveyedSpanCount"]);
                sp4.Tag = dr["StaffID"];
                series4.Points.Add(sp4);

                SeriesPoint sp5 = new SeriesPoint(dr["SurveyorName"], dr["AllocatedSpanCount"]);
                sp5.Tag = dr["StaffID"];
                series5.Points.Add(sp5);
            }
            chartControl1.Series.AddRange(new Series[] { series1, series2, series3, series4, series5 });
            ((XYDiagram)chartControl1.Diagram).EnableAxisXZooming = true;
            ((XYDiagram)chartControl1.Diagram).EnableAxisXScrolling = true;

            for (int i = 0; i < chartControl1.Series.Count; i++)
            {
                ViewType vt = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chartControl1.Series[i].View);

                if (vt == ViewType.StackedBar)
                {
                    BarSeriesView view = (BarSeriesView)chartControl1.Series[i].View;
                    view.FillStyle.FillMode = FillMode.Solid;    
                }
                else if (vt == ViewType.Line)
                {
                    LineSeriesView view = (LineSeriesView)chartControl1.Series[i].View;
                    view.MarkerVisibility = DefaultBoolean.True;
                }
                //chartControl1.Series[i].ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True;
            }
            //ChartTitle chartTitle = new ChartTitle() { Text = "Surveyed Spans" , Font = new Font("Tahoma", 10) };
            //chartControl1.Titles.Add(chartTitle);

            chartControl3.Legend.Visibility = (checkEditChart3ShowLegend.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            for (int i = 0; i < chartControl3.Series.Count; i++)
            {
                chartControl3.Series[i].LabelsVisibility = (checkEditChart3ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            }              
        }

        private void chartControl1_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)
            {
                //MessageBox.Show(e.HitInfo.Series.ToString());
            }
            else
            {
                string strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "Clear Spans" || strCurrentObject == "Spans Requiring Work" || strCurrentObject == "Spans To Be Surveyed" || strCurrentObject == "Total Spans Surveyed" || strCurrentObject == "Total Spans Allocated")
                {
                    chartControl1.ClearSelection();
                }
                else
                {
                    e.Cancel = true;
                }
            }
         }

        private void chartControl1_ObjectSelected(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            string strCurrentObject = "";
            int intStaffID = 0;
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)  // Legend //
            {
                intStaffID = 0;
                strCurrentObject = e.HitInfo.Series.ToString();
            }
            else
            {
                strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "Clear Spans" || strCurrentObject == "Spans Requiring Work" || strCurrentObject == "Spans To Be Surveyed" || strCurrentObject == "Total Spans Surveyed" || strCurrentObject == "Total Spans Allocated")  // Bar of Graph //
                {
                    SeriesPoint sp = (SeriesPoint)e.AdditionalObject;
                    if (sp == null) return;
                    if (sp.Tag == null) return;
                    intStaffID = Convert.ToInt32(sp.Tag);
                }
            }
            if (strCurrentObject == "Clear Spans" || strCurrentObject == "Spans Requiring Work" || strCurrentObject == "Spans To Be Surveyed" || strCurrentObject == "Total Spans Surveyed" || strCurrentObject == "Total Spans Allocated")  // Bar of Graph //
            {

                DataSet_UT_ReportingTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UT_ReportingTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strRecordIDs = "";

                string strChartType = "spans surveyed by surveyor";
                int intType = 0;
                switch (strCurrentObject)
                {
                    case "Clear Spans":
                        {
                            intType = 0;
                            break;
                        }
                    case "Spans Requiring Work":
                        {
                            intType = 1;
                            break;
                        }
                    case "Spans To Be Surveyed":
                        {
                            intType = 2;
                            break;
                        }
                    case "Total Spans Surveyed":
                        {
                            intType = 99;
                            break;
                        }
                    case "Total Spans Allocated":
                        {
                            intType = 999;
                            break;
                        }
                    default:
                        break;
                }
                strRecordIDs = GetSetting.sp07337_UT_DrillDown_Get_Surveyed_Pole_IDs(strChartType, intType, i_dtStart, i_dtEnd, 0, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids, i_str_selected_team_ids, intStaffID, 0, 0).ToString();
                if (string.IsNullOrEmpty(strRecordIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No records to drill down to.", "Data Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion


        #region Chart Control 2

        private void Load_Chart2()
        {
            chartControl2.Series.Clear();
            chartControl2.Legend.Visibility = (checkEditChart2ShowLegend.Checked ?  DevExpress.Utils.DefaultBoolean.True : DevExpress.Utils.DefaultBoolean.False);
            //chartControl1.Titles.Clear();

            // Create 5 Series //
            Series series1 = new Series("To Be Started", ViewType.StackedBar);
            series1.View.Color = Color.FromArgb(0x6B, 0x6F, 0xC2);
            series1.LabelsVisibility = (checkEditChart2ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series2 = new Series("Started", ViewType.StackedBar);
            series2.View.Color = Color.Yellow;
            series2.LabelsVisibility = (checkEditChart2ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series3 = new Series("On-Hold", ViewType.StackedBar);
            series3.View.Color = Color.FromArgb(0xFF, 0x99, 0xFF);
            series3.LabelsVisibility = (checkEditChart2ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series4 = new Series("Completed", ViewType.StackedBar);
            series4.View.Color = Color.FromArgb(0, 176, 80);
            series4.LabelsVisibility = (checkEditChart2ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series5 = new Series("Total Jobs", ViewType.Line);
            series5.View.Color = Color.Tomato;
            series5.LabelsVisibility = (checkEditChart2ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            chartControl2.Legend.FillStyle.FillMode = FillMode.Solid;

            // Add points to them //
            foreach (DataRow dr in this.dataSet_UT_Reporting.sp07331_UT_Reporting_Teams_by_Spans_Worked_On.Rows)
            {
                SeriesPoint sp1 = new SeriesPoint(dr["TeamName"], dr["ToBeStartedCount"]) { Tag = dr["TeamID"] };
                series1.Points.Add(sp1);

                SeriesPoint sp2 = new SeriesPoint(dr["TeamName"], dr["StartedCount"]) { Tag = dr["TeamID"] };
                series2.Points.Add(sp2);

                SeriesPoint sp3 = new SeriesPoint(dr["TeamName"], dr["OnHoldCount"]) { Tag = dr["TeamID"] };
                series3.Points.Add(sp3);

                SeriesPoint sp4 = new SeriesPoint(dr["TeamName"], dr["CompletedCount"]) { Tag = dr["TeamID"] };
                series4.Points.Add(sp4);

                SeriesPoint sp5 = new SeriesPoint(dr["TeamName"], dr["WorkedOnSpanCount"]) { Tag = dr["TeamID"] };
                series5.Points.Add(sp5);
            }
            chartControl2.Series.AddRange(new Series[] { series1, series2, series3, series4, series5 });
            ((XYDiagram)chartControl2.Diagram).EnableAxisXZooming = true;
            ((XYDiagram)chartControl2.Diagram).EnableAxisXScrolling = true;

            for (int i = 0; i < chartControl2.Series.Count; i++)
            {
                ViewType vt = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chartControl2.Series[i].View);

                if (vt == ViewType.StackedBar)
                {
                    BarSeriesView view = (BarSeriesView)chartControl2.Series[i].View;
                    view.FillStyle.FillMode = FillMode.Solid;

                }
                else if (vt == ViewType.Line)
                {
                    LineSeriesView view = (LineSeriesView)chartControl2.Series[i].View;
                    view.MarkerVisibility = DefaultBoolean.True;
                }
                //chartControl2.Series[i].ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True;
            }
            //ChartTitle chartTitle = new ChartTitle() { Text = "Surveyed Spans" , Font = new Font("Tahoma", 10) };
            //chartControl2.Titles.Add(chartTitle);

            chartControl4.Legend.Visibility = (checkEditChart4ShowLegend.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            for (int i = 0; i < chartControl4.Series.Count; i++)
            {
                chartControl4.Series[i].LabelsVisibility = (checkEditChart4ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            }
        }

        private void chartControl2_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)
            {
                //MessageBox.Show(e.HitInfo.Series.ToString());
            }
            else
            {
                string strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "To Be Started" || strCurrentObject == "Started" || strCurrentObject == "On-Hold" || strCurrentObject == "Completed" || strCurrentObject == "Total Jobs")
                {
                    chartControl2.ClearSelection();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void chartControl2_ObjectSelected(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            string strCurrentObject = "";
            int intTeamID = 0;
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)  // Legend //
            {
                intTeamID = 0;
                strCurrentObject = e.HitInfo.Series.ToString();
            }
            else
            {
                strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "To Be Started" || strCurrentObject == "Started" || strCurrentObject == "On-Hold" || strCurrentObject == "Completed" || strCurrentObject == "Total Jobs")
                {
                    SeriesPoint sp = (SeriesPoint)e.AdditionalObject;
                    if (sp == null) return;
                    if (sp.Tag == null) return;
                    intTeamID = Convert.ToInt32(sp.Tag);
                }
            }
            if (strCurrentObject == "To Be Started" || strCurrentObject == "Started" || strCurrentObject == "On-Hold" || strCurrentObject == "Completed" || strCurrentObject == "Total Jobs")
            {

                DataSet_UT_ReportingTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UT_ReportingTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strRecordIDs = "";

                string strChartType = "jobs by team";
                int intType = 0;
                switch (strCurrentObject)
                {
                    case "To Be Started":
                        {
                            intType = 0;
                            break;
                        }
                    case "Started":
                        {
                            intType = 1;
                            break;
                        }
                    case "On-Hold":
                        {
                            intType = 2;
                            break;
                        }
                    case "Completed":
                        {
                            intType = 3;
                            break;
                        }
                    case "Total Jobs":
                        {
                            intType = 99;
                            break;
                        }
                    default:
                        break;
                }
                strRecordIDs = GetSetting.sp07337_UT_DrillDown_Get_Surveyed_Pole_IDs(strChartType, intType, i_dtStart, i_dtEnd, 0, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids, i_str_selected_team_ids, 0, intTeamID, 0).ToString();
                if (string.IsNullOrEmpty(strRecordIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No records to drill down to.", "Data Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_action");
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion


        #region Chart Control 5

        private void Load_Chart5()
        {
            chartControl5.Series.Clear();
            chartControl5.Legend.Visibility = (checkEditChart5ShowLegend.Checked ? DevExpress.Utils.DefaultBoolean.True : DevExpress.Utils.DefaultBoolean.False);
            //chartControl1.Titles.Clear();

            // Create 5 Series //
            Series series1 = new Series("Cumulative Actual Clear", ViewType.StackedBar);
            series1.View.Color = Color.FromArgb(0, 176, 80);
            series1.LabelsVisibility = (checkEditChart5ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series2 = new Series("Cumulative Actual Spans Cut", ViewType.StackedBar);
            series2.View.Color = Color.FromArgb(0x6B, 0x6F, 0xC2);
            series2.LabelsVisibility = (checkEditChart5ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);

            Series series3 = new Series("Cumulative Clear Span Target", ViewType.Line);
            series3.View.Color = Color.Tomato;
            series3.LabelsVisibility = (checkEditChart5ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            chartControl5.Legend.FillStyle.FillMode = FillMode.Solid;

            // Add points to them //
            foreach (DataRow dr in this.dataSet_UT_Reporting.sp07335_UT_Reporting_Clear_Span_Target_V_Actual.Rows)
            {
                SeriesPoint sp1 = new SeriesPoint(dr["MonthName"], dr["ClearSpanCount"]) { Tag = dr["MonthID"] };
                series1.Points.Add(sp1);

                SeriesPoint sp2 = new SeriesPoint(dr["MonthName"], dr["NotClearSpanCount"]) { Tag = dr["MonthID"] };
                series2.Points.Add(sp2);

                if (checkEditChart5ShowTarget.Checked)
                {
                    SeriesPoint sp3 = new SeriesPoint(dr["MonthName"], dr["OperationalDays"]) { Tag = dr["MonthID"] };
                    series3.Points.Add(sp3);
                }

             }
            chartControl5.Series.AddRange(new Series[] { series1, series2 });
            if (checkEditChart5ShowTarget.Checked)
            {
                chartControl5.Series.Add(series3);
            }
            ((XYDiagram)chartControl5.Diagram).EnableAxisXZooming = true;
            ((XYDiagram)chartControl5.Diagram).EnableAxisXScrolling = true;
            
            XYDiagram diagram = (XYDiagram)chartControl5.Diagram;
            diagram.AxisX.DateTimeScaleOptions.GridAlignment = DateTimeGridAlignment.Month;
            diagram.AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Month;
            //diagram.AxisX.Label.DateTimeOptions.Format = DateTimeFormat.Custom;
            //diagram.AxisX.Label.DateTimeOptions.FormatString = "MMM yyyy";
            diagram.AxisX.Label.TextPattern = "{A:MMM yyyy}";
            diagram.AxisX.NumericScaleOptions.GridSpacing = 1;  // Force all 12 months to display as labels //

            for (int i = 0; i < chartControl5.Series.Count; i++)
            {
                ViewType vt = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chartControl5.Series[i].View);

                if (vt == ViewType.StackedBar)
                {
                    BarSeriesView view = (BarSeriesView)chartControl5.Series[i].View;
                    view.FillStyle.FillMode = FillMode.Solid;

                }
                //else if (vt == ViewType.Line)
                //{
                //    LineSeriesView view = (LineSeriesView)chartControl5.Series[i].View;
                //    view.MarkerVisibility = DefaultBoolean.True;
                //}
                //chartControl5.Series[i].ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True;
            }

            //chartControl6.Legend.Visible = checkEditChart6ShowLegend.Checked;
            //for (int i = 0; i < chartControl6.Series.Count; i++)
            //{
            //    chartControl6.Series[i].LabelsVisibility = (checkEditChart6ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            //}
        }

        private void chartControl5_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)
            {
                string strCurrentObject = e.HitInfo.Series.ToString();
                if (strCurrentObject == "Cumulative Clear Span Target")  // Don't allow this to be selected on Legend //
                {
                    e.Cancel = true;
                    return;
                }
                if (strCurrentObject == "Cumulative Actual Clear" || strCurrentObject == "Cumulative Actual Spans Cut")
                {
                    chartControl5.ClearSelection();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                string strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "Cumulative Actual Clear" || strCurrentObject == "Cumulative Actual Spans Cut")
                {
                    chartControl5.ClearSelection();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void chartControl5_ObjectSelected(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            string strCurrentObject = "";
            int intMonthID = 0;
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)  // Legend //
            {
                intMonthID = 0;
                strCurrentObject = e.HitInfo.Series.ToString();
            }
            else
            {
                strCurrentObject = e.Object.ToString();
                if (strCurrentObject == "Cumulative Actual Clear" || strCurrentObject == "Cumulative Actual Spans Cut" || strCurrentObject == "Cumulative Clear Span Target")  // Bar of Graph //
                {
                    SeriesPoint sp = (SeriesPoint)e.AdditionalObject;
                    if (sp == null) return;
                    if (sp.Tag == null) return;
                    intMonthID = Convert.ToInt32(sp.Tag);
                }
            }
            if (strCurrentObject == "Cumulative Actual Clear" || strCurrentObject == "Cumulative Actual Spans Cut" || strCurrentObject == "Cumulative Clear Span Target")
            {

                DateTime dtStart = new DateTime(Convert.ToInt32(spinEditYear.EditValue), 1, 1);
                DateTime dtEnd = new DateTime(Convert.ToInt32(spinEditYear.EditValue), 12, 31);
                DataSet_UT_ReportingTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UT_ReportingTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strRecordIDs = "";

                string strChartType = "clear span target v actual";
                int intType = 0;
                switch (strCurrentObject)
                {
                    case "Cumulative Actual Clear":
                        {
                            intType = 0;
                            break;
                        }
                    case "Cumulative Actual Spans Cut":
                        {
                            intType = 1;
                            break;
                        }
                    case "Cumulative Clear Span Target":
                        {
                            intType = 99;
                            return;
                        }
                    default:
                        break;
                }
                strRecordIDs = GetSetting.sp07337_UT_DrillDown_Get_Surveyed_Pole_IDs(strChartType, intType, dtStart, dtEnd, Convert.ToInt32(spinEditYear.EditValue), _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids, i_str_selected_team_ids, 0, 0, intMonthID).ToString();
                if (string.IsNullOrEmpty(strRecordIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No records to drill down to.", "Data Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion


        #region Chart Control 6

        private void Load_Chart6()
        {
            int intMetricType = 1;
            string strTotalColumn = "";
            string strArgumentColumn = "";
            int intDateDivisionID = 1;
            string strViewType = "";
            switch (comboBoxEditMetric1.EditValue.ToString())
            {
                case "Spans Surveyed Total":
                    intMetricType = 1;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
                case "Spans Surveyed Total Accumulated":
                    intMetricType = 1;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Permissioned Total":
                    intMetricType = 2;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                   break;
                case "Spans Permissioned Total Accumulated":
                    intMetricType = 2;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Completed Total":
                    intMetricType = 3;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                   break;
                case "Spans Completed Total Accumulated":
                    intMetricType = 3;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Invoiced Total":
                    intMetricType = 4;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                   break;
                case "Spans Invoiced Total Accumulated":
                    intMetricType = 4;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Not Invoiced Total":
                    intMetricType = 5;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                   break;
                case "Spans Not Invoiced Total Accumulated":
                    intMetricType = 5;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                default:
                    intMetricType = 1;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                   break;
            }
            switch (comboBoxEditDateDivision1.EditValue.ToString())
            {
                case "Date":
                    intDateDivisionID = 1;
                    strArgumentColumn = "DateRange";
                   break;
                case "Week":
                    intDateDivisionID = 2;
                    strArgumentColumn = "DateDescription";
                    break;
                case "Month":
                    intDateDivisionID = 3;
                    strArgumentColumn = "DateDescription";
                    break;
                case "Quarter":
                    intDateDivisionID = 4;
                    strArgumentColumn = "DateDescription";
                    break;
                default:
                    intDateDivisionID = 1;
                    strArgumentColumn = "DateRange";
                    break;
            }

            chartControl6.SeriesTemplate.ValueDataMembers.Clear();
            chartControl6.SeriesTemplate.ValueDataMembers.AddRange(new string[] { strTotalColumn });
            chartControl6.SeriesTemplate.ArgumentScaleType = ScaleType.Auto;  // set to auto first //
            chartControl6.SeriesTemplate.ArgumentDataMember = strArgumentColumn;
            chartControl6.SeriesTemplate.ArgumentScaleType = (strArgumentColumn == "DateRange" ? ScaleType.DateTime : ScaleType.Auto);
            if (strViewType == "LineView")
            {
                LineSeriesView seriesView = new LineSeriesView();
                seriesView.RangeControlOptions.ViewType = DevExpress.XtraCharts.RangeControlViewType.Line;
                chartControl6.SeriesTemplate.View = seriesView;
                seriesView.MarkerVisibility = DefaultBoolean.True;
            }
            else
            {
                AreaSeriesView seriesView = new AreaSeriesView();
                seriesView.RangeControlOptions.ViewType = DevExpress.XtraCharts.RangeControlViewType.Area;
                chartControl6.SeriesTemplate.View = seriesView;
                seriesView.MarkerVisibility = DefaultBoolean.True;
            }
            try
            {
                sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter.Fill(dataSet_UT_Reporting.sp07340_UT_Reporting_Surveyor_Metrics_By_Date, i_dtStart, i_dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids, intDateDivisionID, intMetricType);
            }
            catch (Exception) { }

            chartControl6.Legend.Visibility = (checkEditChart6ShowLegend.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            for (int i = 0; i < chartControl6.Series.Count; i++)
            {
                chartControl6.Series[i].LabelsVisibility = (checkEditChart6ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            }

            chartControl6.Titles.Clear();
            ChartTitle chartTitle = new ChartTitle();
            chartTitle.Text = comboBoxEditMetric1.EditValue.ToString() + " by " + comboBoxEditDateDivision1.EditValue.ToString();
            chartTitle.EnableAntialiasing = DefaultBoolean.False;
            chartTitle.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle.Indent = 0;
            chartControl6.Titles.Add(chartTitle);
        }

        private void chartControl6_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or Legend to Be Selected //
            if (!(e.Object is Series || e.Object is Legend)) e.Cancel = true;
        }

        private void chartControl6_ObjectSelected(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            string strCurrentObject = "";
            string strValue = "";
            if (!(e.Object is Series || e.Object is Legend))
            {
                e.Cancel = true;
                return;
            }
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)  // Legend //
            {
                strValue = "";
                strCurrentObject = e.HitInfo.Series.ToString();
            }
            else
            {
                strCurrentObject = e.Object.ToString();
                SeriesPoint sp = (SeriesPoint)e.AdditionalObject;
                if (sp == null) return;
                strValue = sp.Argument.ToString();
                //intStaffID = (sp.Tag);
            }

            // Get Date Range //
            DateTime dtFromDate = DateTime.MinValue;
            DateTime dtToDate = DateTime.MaxValue;
            DateTime dtAccumulatedStartDate = i_dtStart;
            if (string.IsNullOrEmpty(strValue))  // In Legend //
            {
                dtFromDate = i_dtStart;
                dtToDate = i_dtEnd;
            }
            else
            {
                switch (comboBoxEditDateDivision1.EditValue.ToString())
                {
                    case "Date":
                        {
                            dtFromDate = Convert.ToDateTime(strValue);
                            dtToDate = Convert.ToDateTime(strValue).AddDays(1).AddSeconds(-1);
                            break;
                        }
                    case "Week":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            dtFromDate = FirstDateOfWeek(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]));
                            dtToDate = dtFromDate.AddDays(8).AddSeconds(-1);
                            break;
                        }
                    case "Month":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            dtFromDate = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), 1);
                            dtToDate = dtFromDate.AddMonths(1).AddSeconds(-1);
                            break;
                        }
                    case "Quarter":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            DateTime StartOfQuarter = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]) * 3, 1);
                            DateTime[] StartAndEnd = DatesOfQuarter(StartOfQuarter);
                            dtFromDate = StartAndEnd[0];
                            dtToDate = StartAndEnd[1].AddDays(1).AddSeconds(-1); ;
                            break;
                        }
                    default:
                        return;
                }
            }

            int intMetricType = 1;
            switch (comboBoxEditMetric1.EditValue.ToString())
            {
                case "Spans Surveyed Total":
                    intMetricType = 1;
                    break;
                case "Spans Surveyed Total Accumulated":
                    intMetricType = 1;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                   break;
                case "Spans Permissioned Total":
                    intMetricType = 2;
                   break;
                case "Spans Permissioned Total Accumulated":
                    intMetricType = 2;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Completed Total":
                    intMetricType = 3;
                   break;
                case "Spans Completed Total Accumulated":
                    intMetricType = 3;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Invoiced Total":
                    intMetricType = 4;
                    break;
                case "Spans Invoiced Total Accumulated":
                    intMetricType = 4;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Not Invoiced Total":
                    intMetricType = 5;
                    break;
                case "Spans Not Invoiced Total Accumulated":
                    intMetricType = 5;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                default:
                    intMetricType = 1;
                    break;
            }

            DataSet_UT_ReportingTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UT_ReportingTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = GetSetting.sp07343_UT_DrillDown_Get_Record_IDs_From_Surveyor_Metrics(strCurrentObject, intMetricType, Convert.ToDateTime(dtFromDate), Convert.ToDateTime(dtToDate), _FilteredCircuitIDs, i_str_selected_voltage_ids).ToString();
            if (string.IsNullOrEmpty(strRecordIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No records to drill down to.", "Data Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
        }

        private void chartControl6_Enter(object sender, EventArgs e)
        {
            ChartControl cc = (ChartControl)sender;
            rangeControl1.Client = null;
            rangeControl1.Client = cc;
            rangeControl1.Refresh();
            XYDiagram diagram = (XYDiagram)cc.Diagram;  // Need the following 3 lines to force the range control to draw it's range in the correct place //
            diagram.EnableAxisXScrolling = false;
            diagram.EnableAxisXScrolling = true;

        }

        #endregion


        #region Chart Control 7

        private void Load_Chart7()
        {
            int intMetricType = 1;
            string strTotalColumn = "";
            string strArgumentColumn = "";
            int intDateDivisionID = 1;
            string strViewType = "";
            switch (comboBoxEditMetric2.EditValue.ToString())
            {
                case "Spans Surveyed Total":
                    intMetricType = 1;
                    strTotalColumn = "SurveyedTotal";                  
                    strViewType = "LineView";
                   break;
                case "Spans Surveyed Total Accumulated":
                    intMetricType = 1;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Permissioned Total":
                    intMetricType = 2;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
                case "Spans Permissioned Total Accumulated":
                    intMetricType = 2;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Completed Total":
                    intMetricType = 3;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
                case "Spans Completed Total Accumulated":
                    intMetricType = 3;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Invoiced Total":
                    intMetricType = 4;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
                case "Spans Invoiced Total Accumulated":
                    intMetricType = 4;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                case "Spans Not Invoiced Total":
                    intMetricType = 5;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
                case "Spans Not Invoiced Total Accumulated":
                    intMetricType = 5;
                    strTotalColumn = "RunningTotal";
                    strViewType = "AreaView";
                    break;
                default:
                    intMetricType = 1;
                    strTotalColumn = "SurveyedTotal";
                    strViewType = "LineView";
                    break;
            }
            switch (comboBoxEditDateDivision2.EditValue.ToString())
            {
                case "Date":
                    intDateDivisionID = 1;
                    strArgumentColumn = "DateRange";
                    break;
                case "Week":
                    intDateDivisionID = 2;
                    strArgumentColumn = "DateDescription";
                    break;
                case "Month":
                    intDateDivisionID = 3;
                    strArgumentColumn = "DateDescription";
                    break;
                case "Quarter":
                    intDateDivisionID = 4;
                    strArgumentColumn = "DateDescription";
                    break;
                default:
                    intDateDivisionID = 1;
                    strArgumentColumn = "DateRange";
                    break;
            }

            chartControl7.SeriesTemplate.ValueDataMembers.Clear();
            chartControl7.SeriesTemplate.ValueDataMembers.AddRange(new string[] { strTotalColumn });
            chartControl7.SeriesTemplate.ArgumentScaleType = ScaleType.Auto;  // set to auto first //
            chartControl7.SeriesTemplate.ArgumentDataMember = strArgumentColumn;
            chartControl7.SeriesTemplate.ArgumentScaleType = (strArgumentColumn == "DateRange" ? ScaleType.DateTime : ScaleType.Auto);
            if (strViewType == "LineView")
            {
                LineSeriesView seriesView = new LineSeriesView();
                seriesView.RangeControlOptions.ViewType = DevExpress.XtraCharts.RangeControlViewType.Line;
                chartControl7.SeriesTemplate.View = seriesView;
                seriesView.MarkerVisibility = DefaultBoolean.True;
            }
            else
            {
                AreaSeriesView seriesView = new AreaSeriesView();
                seriesView.RangeControlOptions.ViewType = DevExpress.XtraCharts.RangeControlViewType.Area;
                chartControl7.SeriesTemplate.View = seriesView;
                seriesView.MarkerVisibility = DefaultBoolean.True;
            }
            try
            {
                sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter.Fill(dataSet_UT_Reporting.sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2, i_dtStart, i_dtEnd, _FilteredCircuitIDs, i_str_selected_voltage_ids, i_str_selected_surveyor_ids, intDateDivisionID, intMetricType);
            }
            catch (Exception) { }

            chartControl7.Legend.Visibility = (checkEditChart7ShowLegend.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            for (int i = 0; i < chartControl7.Series.Count; i++)
            {
                chartControl7.Series[i].LabelsVisibility = (checkEditChart7ShowLabels.Checked ? DefaultBoolean.True : DefaultBoolean.False);
            }
            chartControl7.Titles.Clear();
            ChartTitle chartTitle = new ChartTitle();
            chartTitle.Text = comboBoxEditMetric2.EditValue.ToString() + " by " + comboBoxEditDateDivision2.EditValue.ToString();
            chartTitle.EnableAntialiasing = DefaultBoolean.False;
            chartTitle.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle.Indent = 0;
            chartControl7.Titles.Add(chartTitle);
        }

        private void chartControl7_ObjectHotTracked(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or Legend to Be Selected //
            if (!(e.Object is Series || e.Object is Legend)) e.Cancel = true;
        }

        private void chartControl7_ObjectSelected(object sender, HotTrackEventArgs e)
        {
            // Allow only the Series or legend to Be Selected //
            string strCurrentObject = "";
            string strValue = "";
            if (!(e.Object is Series || e.Object is Legend))
            {
                e.Cancel = true;
                return;
            }
            if (e.HitInfo.InLegend && e.HitInfo.Series != null)  // Legend //
            {
                strValue = "";
                strCurrentObject = e.HitInfo.Series.ToString();
            }
            else
            {
                strCurrentObject = e.Object.ToString();
                SeriesPoint sp = (SeriesPoint)e.AdditionalObject;
                if (sp == null) return;
                strValue = sp.Argument.ToString();
                //intStaffID = (sp.Tag);
            }

            // Get Date Range //
            DateTime dtFromDate = DateTime.MinValue;
            DateTime dtToDate = DateTime.MaxValue;
            DateTime dtAccumulatedStartDate = i_dtStart;
            if (string.IsNullOrEmpty(strValue))  // In Legend //
            {
                dtFromDate = i_dtStart;
                dtToDate = i_dtEnd;
            }
            else
            {
                switch (comboBoxEditDateDivision2.EditValue.ToString())
                {
                    case "Date":
                        {
                            dtFromDate = Convert.ToDateTime(strValue);
                            dtToDate = Convert.ToDateTime(strValue).AddDays(1).AddSeconds(-1);
                            break;
                        }
                    case "Week":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            dtFromDate = FirstDateOfWeek(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]));
                            dtToDate = dtFromDate.AddDays(8).AddSeconds(-1);
                            break;
                        }
                    case "Month":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            dtFromDate = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), 1);
                            dtToDate = dtFromDate.AddMonths(1).AddSeconds(-1);
                            break;
                        }
                    case "Quarter":
                        {
                            string[] split = strValue.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                            if (split.Length != 2) return;
                            DateTime StartOfQuarter = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]) * 3, 1);
                            DateTime[] StartAndEnd = DatesOfQuarter(StartOfQuarter);
                            dtFromDate = StartAndEnd[0];
                            dtToDate = StartAndEnd[1].AddDays(1).AddSeconds(-1); ;
                            break;
                        }
                    default:
                        return;
                }
            }

            int intMetricType = 1;
            switch (comboBoxEditMetric2.EditValue.ToString())
            {
                case "Spans Surveyed Total":
                    intMetricType = 1;
                    break;
                case "Spans Surveyed Total Accumulated":
                    intMetricType = 1;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Permissioned Total":
                    intMetricType = 2;
                    break;
                case "Spans Permissioned Total Accumulated":
                    intMetricType = 2;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Completed Total":
                    intMetricType = 3;
                    break;
                case "Spans Completed Total Accumulated":
                    intMetricType = 3;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Invoiced Total":
                    intMetricType = 4;
                    break;
                case "Spans Invoiced Total Accumulated":
                    intMetricType = 4;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                case "Spans Not Invoiced Total":
                    intMetricType = 5;
                    break;
                case "Spans Not Invoiced Total Accumulated":
                    intMetricType = 5;
                    dtFromDate = i_dtStart;  // adjust for Accumulated totals //
                    break;
                default:
                    intMetricType = 1;
                    break;
            }

            DataSet_UT_ReportingTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UT_ReportingTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = GetSetting.sp07343_UT_DrillDown_Get_Record_IDs_From_Surveyor_Metrics(strCurrentObject, intMetricType, Convert.ToDateTime(dtFromDate), Convert.ToDateTime(dtToDate), _FilteredCircuitIDs, i_str_selected_voltage_ids).ToString();
            if (string.IsNullOrEmpty(strRecordIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No records to drill down to.", "Data Drill Down", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
        }

        private void chartControl7_Enter(object sender, EventArgs e)
        {
            ChartControl cc = (ChartControl)sender;
            rangeControl1.Client = null;
            rangeControl1.Client = cc;
            rangeControl1.Refresh();
            XYDiagram diagram = (XYDiagram)cc.Diagram;  // Need the following 3 lines to force the range control to draw it's range in the correct place //
            diagram.EnableAxisXScrolling = false;
            diagram.EnableAxisXScrolling = true;
        }

        #endregion


        #region Popup Chart Setup

        #endregion


        #region GridView4

        private void gridView4_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Surveyor Metrics Available - Try adjusting filters and click Refresh");
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                //bbiShowMap.Enabled = (view.SelectedRowsCount == 1 ? true : false);
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView4_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView4_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView4_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        #endregion





        static DateTime FirstDateOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);

            int daysOffset = (int)System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;

            DateTime firstMonday = jan1.AddDays(daysOffset);

            int firstWeek = System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(jan1, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);

            if (firstWeek <= 1)
            {
                weekOfYear -= 1;
            }

            return firstMonday.AddDays(weekOfYear * 7);
        }


        public DateTime[] DatesOfQuarter(DateTime PassedInDate)
        {
            DateTime[] dtReturn = new DateTime[2];
            if (PassedInDate.Month >= 1 && PassedInDate.Month <= 3)
            {
                dtReturn[0] = new DateTime(PassedInDate.Year, 1, 1);
                dtReturn[1] = new DateTime(PassedInDate.Year, 3, 31);
            }
            else if (PassedInDate.Month >= 4 && PassedInDate.Month <= 6)
            {
                dtReturn[0] = new DateTime(PassedInDate.Year, 4, 1);
                dtReturn[1] = new DateTime(PassedInDate.Year, 6, 30);
            }
            else if (PassedInDate.Month >= 7 && PassedInDate.Month <= 9)
            {
                dtReturn[0] = new DateTime(PassedInDate.Year, 7, 1);
                dtReturn[1] = new DateTime(PassedInDate.Year, 9, 30);
            }
            else if (PassedInDate.Month >= 10 && PassedInDate.Month <= 12)
            {
                dtReturn[0] = new DateTime(PassedInDate.Year, 10, 1);
                dtReturn[1] = new DateTime(PassedInDate.Year, 12, 31);
            }
            return dtReturn;
        }


        public static int DaysToAdd(DayOfWeek current, DayOfWeek desired)
        {
            int c = (int)current;
            int d = (int)desired;
            return (7 - c + d) % 7;
        }

        private void spinEditDaysGrouping_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void Set_Column_Captions(int intDayCount)
        {
            GridView view = (GridView)gridControl5.MainView;
            string strCaption = intDayCount.ToString() + " Days";
            view.Columns["WeekSurveyedSpanCount"].Caption = strCaption;
            view.Columns["WeekSurveyedSpanCount"].CustomizationCaption = intDayCount.ToString() + " Days Surveyed Spans";
            view.Columns["WeekAverageInfestation"].Caption = strCaption;
            view.Columns["WeekAverageInfestation"].CustomizationCaption = intDayCount.ToString() + " Days Avg. Infestation";
            view.Columns["WeekCompletedSpanCount"].Caption = strCaption;
            view.Columns["WeekCompletedSpanCount"].CustomizationCaption = intDayCount.ToString() + " Days Cut Spans";
            view.Columns["WeekPermissionedSpanCount"].Caption = strCaption;
            view.Columns["WeekPermissionedSpanCount"].CustomizationCaption = intDayCount.ToString() + " Days Permissioned Spans";
            view.Columns["WeekSpansNeedingWork"].Caption = strCaption;
            view.Columns["WeekSpansNeedingWork"].CustomizationCaption = intDayCount.ToString() + " Days Spans Requiring Work";
            view.Columns["WeekSurveyedSpanClear"].Caption = strCaption;
            view.Columns["WeekSurveyedSpanClear"].CustomizationCaption = intDayCount.ToString() + " Days Clear Spans";
            view.Columns["ReactiveSpanCompleteCount"].Caption = strCaption;
            view.Columns["ReactiveSpanCompleteCount"].CustomizationCaption = intDayCount.ToString() + " Days Reactive Spans Complete";
        }

    }
}

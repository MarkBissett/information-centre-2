namespace WoodPlan5
{
    partial class frm_UT_Survey_Edit_Costing_Item_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Survey_Edit_Costing_Item_Add));
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gridLookUpEditEquipmentType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07148UTEquipmentMasterSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlEquipmentType = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditCostingType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07231UTSurveyCostingCostTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCostingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlCostingType = new DevExpress.XtraEditors.LabelControl();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp07231_UT_Survey_Costing_Cost_TypesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07231_UT_Survey_Costing_Cost_TypesTableAdapter();
            this.sp07148_UT_Equipment_Master_SelectTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07148_UT_Equipment_Master_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEquipmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07148UTEquipmentMasterSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07231UTSurveyCostingCostTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(481, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 124);
            this.barDockControlBottom.Size = new System.Drawing.Size(481, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 124);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(481, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 124);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(283, 94);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "Add";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(381, 94);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Images = this.imageList1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 1;
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Select Costing Type - If Kit selected, enter Equipment Type and click Add";
            this.barStaticItemInformation.Id = 0;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(481, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 124);
            this.barDockControl2.Size = new System.Drawing.Size(481, 26);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 124);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(481, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 124);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add_16.png");
            this.imageList1.Images.SetKeyName(1, "edit_16.png");
            this.imageList1.Images.SetKeyName(2, "info_16.png");
            this.imageList1.Images.SetKeyName(3, "attention_16.png");
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.gridLookUpEditEquipmentType);
            this.groupControl3.Controls.Add(this.labelControlEquipmentType);
            this.groupControl3.Controls.Add(this.gridLookUpEditCostingType);
            this.groupControl3.Controls.Add(this.labelControlCostingType);
            this.groupControl3.Location = new System.Drawing.Point(7, 8);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(466, 78);
            this.groupControl3.TabIndex = 13;
            this.groupControl3.Text = "Choose Costing Type:";
            // 
            // gridLookUpEditEquipmentType
            // 
            this.gridLookUpEditEquipmentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditEquipmentType.EditValue = "";
            this.gridLookUpEditEquipmentType.Enabled = false;
            this.gridLookUpEditEquipmentType.Location = new System.Drawing.Point(78, 51);
            this.gridLookUpEditEquipmentType.MenuManager = this.barManager1;
            this.gridLookUpEditEquipmentType.Name = "gridLookUpEditEquipmentType";
            this.gridLookUpEditEquipmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditEquipmentType.Properties.DataSource = this.sp07148UTEquipmentMasterSelectBindingSource;
            this.gridLookUpEditEquipmentType.Properties.DisplayMember = "Description";
            this.gridLookUpEditEquipmentType.Properties.NullText = "";
            this.gridLookUpEditEquipmentType.Properties.ValueMember = "EquipmentID";
            this.gridLookUpEditEquipmentType.Properties.View = this.gridView1;
            this.gridLookUpEditEquipmentType.Size = new System.Drawing.Size(383, 20);
            this.gridLookUpEditEquipmentType.TabIndex = 3;
            this.gridLookUpEditEquipmentType.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditEquipmentType_Validating);
            // 
            // sp07148UTEquipmentMasterSelectBindingSource
            // 
            this.sp07148UTEquipmentMasterSelectBindingSource.DataMember = "sp07148_UT_Equipment_Master_Select";
            this.sp07148UTEquipmentMasterSelectBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID,
            this.gridColumn1,
            this.colCode,
            this.colCostPerHour,
            this.colSellPerHour,
            this.colRemarks1,
            this.colGUID,
            this.colLinkedRecordCount});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.Caption = "Equipment ID";
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.Width = 85;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Equipment Description";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 293;
            // 
            // colCode
            // 
            this.colCode.Caption = "Code";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.OptionsColumn.AllowEdit = false;
            this.colCode.OptionsColumn.AllowFocus = false;
            this.colCode.OptionsColumn.ReadOnly = true;
            this.colCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 1;
            this.colCode.Width = 95;
            // 
            // colCostPerHour
            // 
            this.colCostPerHour.Caption = "Cost Per Hour";
            this.colCostPerHour.FieldName = "CostPerHour";
            this.colCostPerHour.Name = "colCostPerHour";
            this.colCostPerHour.OptionsColumn.AllowEdit = false;
            this.colCostPerHour.OptionsColumn.AllowFocus = false;
            this.colCostPerHour.OptionsColumn.ReadOnly = true;
            this.colCostPerHour.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostPerHour.Visible = true;
            this.colCostPerHour.VisibleIndex = 2;
            this.colCostPerHour.Width = 88;
            // 
            // colSellPerHour
            // 
            this.colSellPerHour.Caption = "Sell Per Hour";
            this.colSellPerHour.FieldName = "SellPerHour";
            this.colSellPerHour.Name = "colSellPerHour";
            this.colSellPerHour.OptionsColumn.AllowEdit = false;
            this.colSellPerHour.OptionsColumn.AllowFocus = false;
            this.colSellPerHour.OptionsColumn.ReadOnly = true;
            this.colSellPerHour.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSellPerHour.Visible = true;
            this.colSellPerHour.VisibleIndex = 3;
            this.colSellPerHour.Width = 82;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordCount
            // 
            this.colLinkedRecordCount.Caption = "Linked Job Count";
            this.colLinkedRecordCount.FieldName = "LinkedRecordCount";
            this.colLinkedRecordCount.Name = "colLinkedRecordCount";
            this.colLinkedRecordCount.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedRecordCount.Visible = true;
            this.colLinkedRecordCount.VisibleIndex = 4;
            this.colLinkedRecordCount.Width = 103;
            // 
            // labelControlEquipmentType
            // 
            this.labelControlEquipmentType.Enabled = false;
            this.labelControlEquipmentType.Location = new System.Drawing.Point(6, 54);
            this.labelControlEquipmentType.Name = "labelControlEquipmentType";
            this.labelControlEquipmentType.Size = new System.Drawing.Size(43, 13);
            this.labelControlEquipmentType.TabIndex = 2;
            this.labelControlEquipmentType.Text = "Kit Type:";
            // 
            // gridLookUpEditCostingType
            // 
            this.gridLookUpEditCostingType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditCostingType.EditValue = "";
            this.gridLookUpEditCostingType.Location = new System.Drawing.Point(78, 25);
            this.gridLookUpEditCostingType.MenuManager = this.barManager1;
            this.gridLookUpEditCostingType.Name = "gridLookUpEditCostingType";
            this.gridLookUpEditCostingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditCostingType.Properties.DataSource = this.sp07231UTSurveyCostingCostTypesBindingSource;
            this.gridLookUpEditCostingType.Properties.DisplayMember = "Description";
            this.gridLookUpEditCostingType.Properties.NullText = "";
            this.gridLookUpEditCostingType.Properties.ValueMember = "CostingTypeID";
            this.gridLookUpEditCostingType.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditCostingType.Size = new System.Drawing.Size(383, 20);
            this.gridLookUpEditCostingType.TabIndex = 1;
            this.gridLookUpEditCostingType.EditValueChanged += new System.EventHandler(this.gridLookUpEditCostingType_EditValueChanged);
            this.gridLookUpEditCostingType.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditCostingType_Validating);
            // 
            // sp07231UTSurveyCostingCostTypesBindingSource
            // 
            this.sp07231UTSurveyCostingCostTypesBindingSource.DataMember = "sp07231_UT_Survey_Costing_Cost_Types";
            this.sp07231UTSurveyCostingCostTypesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCostingTypeID,
            this.colDescription,
            this.colSortOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCostingTypeID
            // 
            this.colCostingTypeID.Caption = "Costing Type ID";
            this.colCostingTypeID.FieldName = "CostingTypeID";
            this.colCostingTypeID.Name = "colCostingTypeID";
            this.colCostingTypeID.OptionsColumn.AllowEdit = false;
            this.colCostingTypeID.OptionsColumn.AllowFocus = false;
            this.colCostingTypeID.OptionsColumn.ReadOnly = true;
            this.colCostingTypeID.Width = 118;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Costing Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 186;
            // 
            // colSortOrder
            // 
            this.colSortOrder.Caption = "Sort Order";
            this.colSortOrder.FieldName = "SortOrder";
            this.colSortOrder.Name = "colSortOrder";
            this.colSortOrder.OptionsColumn.AllowEdit = false;
            this.colSortOrder.OptionsColumn.AllowFocus = false;
            this.colSortOrder.OptionsColumn.ReadOnly = true;
            this.colSortOrder.Width = 119;
            // 
            // labelControlCostingType
            // 
            this.labelControlCostingType.Location = new System.Drawing.Point(5, 28);
            this.labelControlCostingType.Name = "labelControlCostingType";
            this.labelControlCostingType.Size = new System.Drawing.Size(67, 13);
            this.labelControlCostingType.TabIndex = 0;
            this.labelControlCostingType.Text = "Costing Type:";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // sp07231_UT_Survey_Costing_Cost_TypesTableAdapter
            // 
            this.sp07231_UT_Survey_Costing_Cost_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07148_UT_Equipment_Master_SelectTableAdapter
            // 
            this.sp07148_UT_Equipment_Master_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Survey_Edit_Costing_Item_Add
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(481, 150);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Survey_Edit_Costing_Item_Add";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Survey Costing Item";
            this.Load += new System.EventHandler(this.frm_UT_Survey_Edit_Costing_Item_Add_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEquipmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07148UTEquipmentMasterSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07231UTSurveyCostingCostTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem barStaticItemInformation;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditCostingType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControlCostingType;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditEquipmentType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControlEquipmentType;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07231UTSurveyCostingCostTypesBindingSource;
        private DataSet_UTTableAdapters.sp07231_UT_Survey_Costing_Cost_TypesTableAdapter sp07231_UT_Survey_Costing_Cost_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCostingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSortOrder;
        private System.Windows.Forms.BindingSource sp07148UTEquipmentMasterSelectBindingSource;
        private DataSet_UTTableAdapters.sp07148_UT_Equipment_Master_SelectTableAdapter sp07148_UT_Equipment_Master_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordCount;
    }
}

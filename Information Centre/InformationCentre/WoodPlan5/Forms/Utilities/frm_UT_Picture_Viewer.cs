﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Tile;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Picture_Viewer : WoodPlan5.frmBase_Modal
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        public string _SelectedValues = "";
        public int _SelectedTypeID = 0;
        public string _ImagePath = "";
        Hashtable Images = new Hashtable();

        private int _RowCount = 3;
        private string _GroupBy = "Survey Date";

        #endregion
        
        public frm_UT_Picture_Viewer()
        {
            InitializeComponent();
        }

        private void frm_UT_Picture_Viewer_Load(object sender, EventArgs e)
        {
            this.FormID = 500224;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07477_UT_Pictures_Group_ByTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07477_UT_Pictures_Group_ByTableAdapter.Fill(dataSet_UT.sp07477_UT_Pictures_Group_By);

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                _ImagePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp07476_UT_PicturesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07478_UT_Pictures_TreesTableAdapter.Connection.ConnectionString = strConnectionString;

            PostOpen();
        }

        private void frm_UT_Picture_Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RowCount", _RowCount.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "GroupBy", _GroupBy);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();

            if (_SelectedTypeID != 0)
            {
                xtraTabPage1.PageEnabled = false;
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
            }
            else
            {
                xtraTabPage1.PageEnabled = true;
                xtraTabControl1.SelectedTabPage = xtraTabPage1;
            }
        }

        public override void PostLoadView(object objParameter)
        {
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys != Keys.Control)  // Only load settings if user is not holding down Ctrl key //
                {
                    // Row Count //
                    string strRowCount = default_screen_settings.RetrieveSetting("RowCount");
                    if (!string.IsNullOrEmpty(strRowCount))
                    {
                        try
                        {
                            _RowCount = Convert.ToInt32(strRowCount);
                        }
                        catch (Exception) { }
                        if (_RowCount <= 0) _RowCount = 3;
                        beiRowCount.EditValue = _RowCount;
                    }

                    // Group By //
                    string strGroupBy = default_screen_settings.RetrieveSetting("GroupBy");
                    if (!string.IsNullOrEmpty(strGroupBy))
                    {
                        _GroupBy = strGroupBy;
                        if (string.IsNullOrWhiteSpace(_GroupBy)) _GroupBy = "Survey Date";
                        barEditItemGroupBy.EditValue = _GroupBy;

                    }
                }
                bbiRefresh.PerformClick();
            }
        }


        #region TileView 1

        private void tileView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            TileView view = (TileView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Pole Pictures", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void tileView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(250, 170);
            if (e.Column.FieldName == "dummyImage" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Tile.TileView view = (DevExpress.XtraGrid.Views.Tile.TileView)sender;
                int rHandle = view.GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = view.GetRowCellValue(rHandle, "PicturePath").ToString();
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 250, 170);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void tileView1_ItemDoubleClick(object sender, TileViewItemClickEventArgs e)
        {
            TileView view = (TileView)sender;
            if (e.Item == null) return;
            string strFile = view.GetRowCellValue(e.Item.RowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region TileView 2

        private void tileView2_ItemDoubleClick(object sender, TileViewItemClickEventArgs e)
        {
            TileView view = (TileView)sender;
            if (e.Item == null) return;
            string strFile = view.GetRowCellValue(e.Item.RowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void tileView2_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(250, 170);
            if (e.Column.FieldName == "dummyImage2" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Tile.TileView view = (DevExpress.XtraGrid.Views.Tile.TileView)sender;
                int rHandle = view.GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = view.GetRowCellValue(rHandle, "PicturePath").ToString();
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 250, 170);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void tileView2_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            TileView view = (TileView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Tree Pictures", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        #endregion


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _RowCount = Convert.ToInt32(beiRowCount.EditValue);
            _GroupBy = barEditItemGroupBy.EditValue.ToString();
            tileView1.OptionsTiles.RowCount = _RowCount;
            tileView1.ColumnSet.GroupColumn = (_GroupBy == "Survey Date" ? colSurveyDate : colDescription2);
            TileView view = (TileView)gridControl1.MainView;

            TileViewItemElement element = (TileViewItemElement)view.TileTemplate[2];
            element.Text = (_GroupBy != "Survey Date" ? "Survey Date" : "Pole:");

            element = (TileViewItemElement)view.TileTemplate[3];
            element.Column = (_GroupBy != "Survey Date" ? colSurveyDate : colDescription2);
            element.Text = (_GroupBy != "Survey Date" ? "colSurveyDate" : "colDescription2");


            tileView2.OptionsTiles.RowCount = _RowCount;
            tileView2.ColumnSet.GroupColumn = (_GroupBy == "Survey Date" ? tileViewColumn12 : tileViewColumn9);
            view = (TileView)gridControl2.MainView;

            element = (TileViewItemElement)view.TileTemplate[2];
            element.Text = (_GroupBy != "Survey Date" ? "Survey Date" : "Pole:");

            element = (TileViewItemElement)view.TileTemplate[3];
            element.Column = (_GroupBy != "Survey Date" ? tileViewColumn12 : tileViewColumn9);
            element.Text = (_GroupBy != "Survey Date" ? "tileViewColumn12" : "tileViewColumn9");


            gridControl1.Refresh();
            try
            {
                sp07476_UT_PicturesTableAdapter.Fill(dataSet_UT.sp07476_UT_Pictures, _SelectedValues, _ImagePath);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the images. \n\nPlease close the screen and try again. If the problem persists, contact Technical Support.", "Load Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl2.Refresh();
            try
            {
                sp07478_UT_Pictures_TreesTableAdapter.Fill(dataSet_UT.sp07478_UT_Pictures_Trees, _SelectedTypeID, _SelectedValues, _ImagePath);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the images. \n\nPlease close the screen and try again. If the problem persists, contact Technical Support.", "Load Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }



 




    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Select_Pole_Multiple : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection1;

        public string strSelectedIDs = "";
        public string strSelectedPoleNumbers = "";
        public int intPassedInClientID = 0;
        public int intPassedInCircuitID = 0;
        public int intPassedInPoleID = 0;

        public int intFilterActiveClient = 0;
        public int intFilterUtilityArbClient = 0;
        public int intFilterSummerMaintenanceClient = 0;
        public int intWinterMaintenanceClient = 0;
        public int intAmenityArbClient = 0;

        #endregion

        public frm_UT_Select_Pole_Multiple()
        {
            InitializeComponent();
        }

        private void frm_UT_Select_Pole_Multiple_Load(object sender, EventArgs e)
        {
            this.FormID = 500039;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07003_UT_Select_CircuitTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07042_UT_Pole_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp03054_EP_Select_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp03054_EP_Select_Client_ListTableAdapter.Fill(dataSet_EP.sp03054_EP_Select_Client_List, intFilterActiveClient, intFilterUtilityArbClient, intFilterSummerMaintenanceClient, intWinterMaintenanceClient, intAmenityArbClient);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInClientID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], intPassedInClientID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInCircuitID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["CircuitID"], intPassedInCircuitID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl3.ForceInitialize();
            view = (GridView)gridControl3.MainView;
            if (intPassedInPoleID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["PoleID"], intPassedInPoleID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;
        private int GetTopDataRowHandle(int ARowHandle, GridView view)
        {
            if (view.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = view.GetVisibleRowHandle(view.GetVisibleIndex(ARowHandle) - 1);
                if (view.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle, view);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle, view);
                }
            }
            else return -1;
        }

        private int GetBottomDataRowHandle(int ARowHandle, GridView view)
        {
            int rowHandle = ARowHandle;
            if (view.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = view.GetChildRowHandle(rowHandle, view.GetChildRowCount(rowHandle) - 1);
                } while (view.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Circuits Available For Selection - Select a Client to see Related Circuits";
                    break;
                case "gridView3":
                    message = "No Poles Available For Selection - Select one or more Circuits to see Related Poles";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView2

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            LoadLinkedData2();
            GridView view = (GridView)gridControl3.MainView;
            view.ExpandAllGroups();
         }


        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT.sp07003_UT_Select_Circuit.Clear();
            }
            else
            {
                try
                {
                    sp07003_UT_Select_CircuitTableAdapter.Fill(dataSet_UT.sp07003_UT_Select_Circuit, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related circuits.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["CircuitID"])) + ',';
            }

            //Populate Linked Map Links //
            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT.sp07042_UT_Pole_Select.Clear();
            }
            else
            {
                try
                {
                    sp07042_UT_Pole_SelectTableAdapter.Fill(dataSet_UT.sp07042_UT_Pole_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related poles.\n\nTry selecting a circuit again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl3.MainView;
            strSelectedIDs = "";    // Reset any prior values first //
            strSelectedPoleNumbers = "";  // Reset any prior values first //
            if (view.DataRowCount <= 0)
            {
                strSelectedIDs = "";
                return;

            }
            else if (selection1.SelectedCount <= 0)
            {
                strSelectedIDs = "";
                return;
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "PoleID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedPoleNumbers = Convert.ToString(view.GetRowCellValue(i, "PoleNumber"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedPoleNumbers += ", " + Convert.ToString(view.GetRowCellValue(i, "PoleNumber"));
                        }
                        intCount++;
                    }
                }
            }
            return;
        }





    }
}


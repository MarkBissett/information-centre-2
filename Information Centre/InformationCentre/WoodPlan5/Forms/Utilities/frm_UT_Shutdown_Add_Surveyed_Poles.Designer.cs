namespace WoodPlan5
{
    partial class frm_UT_Shutdown_Add_Surveyed_Poles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Shutdown_Add_Surveyed_Poles));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.dateEditDateTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsiblePerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(954, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            this.barDockControlBottom.Size = new System.Drawing.Size(954, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(954, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageList1;
            this.barManager1.MaxItemId = 31;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.pictureEdit1);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnRefresh);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(954, 415);
            this.splitContainerControl1.SplitterPosition = 82;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Location = new System.Drawing.Point(26, 9);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(532, 13);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "Adjust Filters and click Load Data if required. <b>Tick Surveyed Poles</b> to add" +
    " to shutdown then click <b>OK</b> button.";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(3, 4);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(21, 22);
            this.pictureEdit1.TabIndex = 25;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.dateEditDateFrom);
            this.groupControl2.Controls.Add(this.dateEditDateTo);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Location = new System.Drawing.Point(3, 29);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(308, 52);
            this.groupControl2.TabIndex = 24;
            this.groupControl2.Text = "Date Range";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "From:";
            // 
            // dateEditDateFrom
            // 
            this.dateEditDateFrom.EditValue = null;
            this.dateEditDateFrom.Location = new System.Drawing.Point(38, 26);
            this.dateEditDateFrom.MenuManager = this.barManager1;
            this.dateEditDateFrom.Name = "dateEditDateFrom";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, superToolTip1, true)});
            this.dateEditDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateFrom.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullValuePrompt = "No Date";
            this.dateEditDateFrom.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateFrom.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateFrom.TabIndex = 21;
            this.dateEditDateFrom.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateFrom_ButtonClick);
            // 
            // dateEditDateTo
            // 
            this.dateEditDateTo.EditValue = null;
            this.dateEditDateTo.Location = new System.Drawing.Point(183, 27);
            this.dateEditDateTo.MenuManager = this.barManager1;
            this.dateEditDateTo.Name = "dateEditDateTo";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, superToolTip2, true)});
            this.dateEditDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateTo.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullValuePrompt = "No Date";
            this.dateEditDateTo.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateTo.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateTo.TabIndex = 22;
            this.dateEditDateTo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateTo_ButtonClick);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(163, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "To:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(317, 30);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(410, 51);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Surveyed Pole Type";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(156, 27);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Surveyed Poles Already Linked to a Shutdown";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(247, 19);
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Surveyed Poles Already Linked To a Shutdown - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.checkEdit3.SuperTip = superToolTip3;
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Unlinked Surveyed Poles";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(144, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(733, 57);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(73, 23);
            this.btnRefresh.TabIndex = 20;
            this.btnRefresh.Text = "Load Data";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(954, 327);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(954, 327);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource
            // 
            this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource.DataMember = "sp07381_UT_Shutdown_Surveyed_Poles_Available_For_Adding";
            this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID1,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks2,
            this.colGUID1,
            this.colClientName2,
            this.colSurveyDate1,
            this.colSurveyDescription,
            this.colSurveyor1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colExchequerNumber,
            this.colContractValue,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.colCircuitNumber,
            this.colCircuitName,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID1,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptior,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colIsBaseClimbable,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colShutdownID,
            this.colClearanceDistanceUnder,
            this.colHeldUpType,
            this.colHeldUpTypeID,
            this.colRegionName,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colPrimaryName,
            this.colResponsiblePerson,
            this.colSubAreaName,
            this.colShutdownHours});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Date Surveyed";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 13;
            this.colSurveyedDate.Width = 93;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 15;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 16;
            this.colInfestationRate.Width = 100;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 17;
            this.colIsShutdownRequired.Width = 115;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 19;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 20;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance (Side)";
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 24;
            this.colClearanceDistance.Width = 144;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 26;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 27;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 28;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 3M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 29;
            this.colTreeWithin3Meters.Width = 93;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 30;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 32;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName2.Width = 224;
            // 
            // colSurveyDate1
            // 
            this.colSurveyDate1.Caption = "Survey - Date";
            this.colSurveyDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSurveyDate1.FieldName = "SurveyDate";
            this.colSurveyDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate1.Name = "colSurveyDate1";
            this.colSurveyDate1.OptionsColumn.AllowEdit = false;
            this.colSurveyDate1.OptionsColumn.AllowFocus = false;
            this.colSurveyDate1.OptionsColumn.ReadOnly = true;
            this.colSurveyDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate1.Width = 88;
            // 
            // colSurveyDescription
            // 
            this.colSurveyDescription.Caption = "Survey - Description";
            this.colSurveyDescription.FieldName = "SurveyDescription";
            this.colSurveyDescription.Name = "colSurveyDescription";
            this.colSurveyDescription.OptionsColumn.AllowEdit = false;
            this.colSurveyDescription.OptionsColumn.AllowFocus = false;
            this.colSurveyDescription.OptionsColumn.ReadOnly = true;
            this.colSurveyDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyDescription.Visible = true;
            this.colSurveyDescription.VisibleIndex = 18;
            this.colSurveyDescription.Width = 268;
            // 
            // colSurveyor1
            // 
            this.colSurveyor1.Caption = "Surveyor";
            this.colSurveyor1.FieldName = "Surveyor";
            this.colSurveyor1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colSurveyor1.Name = "colSurveyor1";
            this.colSurveyor1.OptionsColumn.AllowEdit = false;
            this.colSurveyor1.OptionsColumn.AllowFocus = false;
            this.colSurveyor1.OptionsColumn.ReadOnly = true;
            this.colSurveyor1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyor1.Visible = true;
            this.colSurveyor1.VisibleIndex = 42;
            this.colSurveyor1.Width = 120;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Width = 110;
            // 
            // colExchequerNumber
            // 
            this.colExchequerNumber.Caption = "Exchequer Number";
            this.colExchequerNumber.FieldName = "ExchequerNumber";
            this.colExchequerNumber.Name = "colExchequerNumber";
            this.colExchequerNumber.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExchequerNumber.Width = 112;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Span Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 11;
            this.colContractValue.Width = 74;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 0;
            this.colPoleNumber.Width = 105;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Pole Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Visible = true;
            this.colPoleLastInspectionDate.VisibleIndex = 9;
            this.colPoleLastInspectionDate.Width = 117;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Pole Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Visible = true;
            this.colPoleNextInspectionDate.VisibleIndex = 10;
            this.colPoleNextInspectionDate.Width = 120;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Tranformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 33;
            this.colIsTransformer.Width = 87;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 5;
            this.colCircuitNumber.Width = 111;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Width = 81;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 14;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 12;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID1
            // 
            this.colSurveyStatusID1.Caption = "Survey Status ID";
            this.colSurveyStatusID1.FieldName = "SurveyStatusID";
            this.colSurveyStatusID1.Name = "colSurveyStatusID1";
            this.colSurveyStatusID1.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID1.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID1.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyStatusID1.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 21;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 34;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 36;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 35;
            this.colDeferredUnits.Width = 91;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f2";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 31;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 39;
            this.colRevisitDate.Width = 79;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colInvoiceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 37;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 38;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 22;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 23;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAccessMapPath.Visible = true;
            this.colAccessMapPath.VisibleIndex = 40;
            this.colAccessMapPath.Width = 100;
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrafficMapPath.Visible = true;
            this.colTrafficMapPath.VisibleIndex = 41;
            this.colTrafficMapPath.Width = 100;
            // 
            // colShutdownID
            // 
            this.colShutdownID.Caption = "Shutdown ID";
            this.colShutdownID.FieldName = "ShutdownID";
            this.colShutdownID.Name = "colShutdownID";
            this.colShutdownID.OptionsColumn.AllowEdit = false;
            this.colShutdownID.OptionsColumn.AllowFocus = false;
            this.colShutdownID.OptionsColumn.ReadOnly = true;
            this.colShutdownID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShutdownID.Width = 74;
            // 
            // colClearanceDistanceUnder
            // 
            this.colClearanceDistanceUnder.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder.Name = "colClearanceDistanceUnder";
            this.colClearanceDistanceUnder.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClearanceDistanceUnder.Visible = true;
            this.colClearanceDistanceUnder.VisibleIndex = 25;
            this.colClearanceDistanceUnder.Width = 153;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 8;
            this.colHeldUpType.Width = 100;
            // 
            // colHeldUpTypeID
            // 
            this.colHeldUpTypeID.Caption = "Held-Up Type ID";
            this.colHeldUpTypeID.FieldName = "HeldUpTypeID";
            this.colHeldUpTypeID.Name = "colHeldUpTypeID";
            this.colHeldUpTypeID.OptionsColumn.AllowEdit = false;
            this.colHeldUpTypeID.OptionsColumn.AllowFocus = false;
            this.colHeldUpTypeID.OptionsColumn.ReadOnly = true;
            this.colHeldUpTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeldUpTypeID.Width = 98;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 103;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageID.Width = 69;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 6;
            this.colVoltageType.Width = 94;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 4;
            this.colFeederName.Width = 111;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 3;
            this.colPrimaryName.Width = 111;
            // 
            // colResponsiblePerson
            // 
            this.colResponsiblePerson.Caption = "Responsible Person";
            this.colResponsiblePerson.FieldName = "ResponsiblePerson";
            this.colResponsiblePerson.Name = "colResponsiblePerson";
            this.colResponsiblePerson.OptionsColumn.AllowEdit = false;
            this.colResponsiblePerson.OptionsColumn.AllowFocus = false;
            this.colResponsiblePerson.OptionsColumn.ReadOnly = true;
            this.colResponsiblePerson.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colResponsiblePerson.Visible = true;
            this.colResponsiblePerson.VisibleIndex = 7;
            this.colResponsiblePerson.Width = 112;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 2;
            this.colSubAreaName.Width = 111;
            // 
            // colShutdownHours
            // 
            this.colShutdownHours.Caption = "Shutdown Hours";
            this.colShutdownHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colShutdownHours.FieldName = "ShutdownHours";
            this.colShutdownHours.Name = "colShutdownHours";
            this.colShutdownHours.OptionsColumn.AllowEdit = false;
            this.colShutdownHours.OptionsColumn.AllowFocus = false;
            this.colShutdownHours.OptionsColumn.ReadOnly = true;
            this.colShutdownHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShutdownHours.Visible = true;
            this.colShutdownHours.VisibleIndex = 18;
            this.colShutdownHours.Width = 98;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(873, 449);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(789, 449);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add_16.png");
            this.imageList1.Images.SetKeyName(1, "edit_16.png");
            this.imageList1.Images.SetKeyName(2, "info_16.png");
            this.imageList1.Images.SetKeyName(3, "attention_16.png");
            this.imageList1.Images.SetKeyName(4, "delete_16.png");
            this.imageList1.Images.SetKeyName(5, "arrow_up_blue_round.png");
            this.imageList1.Images.SetKeyName(6, "arrow_down_blue_round.png");
            // 
            // sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter
            // 
            this.sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Shutdown_Add_Surveyed_Poles
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(954, 478);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Shutdown_Add_Surveyed_Poles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilities Held-Up - Add Surveyed Poles";
            this.Load += new System.EventHandler(this.frm_UT_Shutdown_Add_Surveyed_Poles_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.DateEdit dateEditDateTo;
        private DevExpress.XtraEditors.DateEdit dateEditDateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp07381UTShutdownSurveyedPolesAvailableForAddingBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownID;
        private DataSet_UT_EditTableAdapters.sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter sp07381_UT_Shutdown_Surveyed_Poles_Available_For_AddingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsiblePerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours;
    }
}

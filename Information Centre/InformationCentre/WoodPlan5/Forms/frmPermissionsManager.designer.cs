namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmPermissionsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPermissionsManager));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00093permissionmanagergroupslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Groups = new WoodPlan5.DataSet_Groups();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00094permissionmanagergroupmembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnRemoveAll1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveOne1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddAll1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddOne1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00095permissionmanagerfunctionalityavailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProgramPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00123permissionmanagerfunctionalityselectedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProgramPartID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubPartID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colColour1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReadAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdateAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeleteAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSwitchboardVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReadAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreateAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colUpdateAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDeleteAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colProgramAccessID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00093_permission_manager_groups_listTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00093_permission_manager_groups_listTableAdapter();
            this.sp00094_permission_manager_group_membersTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00094_permission_manager_group_membersTableAdapter();
            this.sp00095_permission_manager_functionality_availableTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00095_permission_manager_functionality_availableTableAdapter();
            this.sp00123_permission_manager_functionality_selectedTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00123_permission_manager_functionality_selectedTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00093permissionmanagergroupslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00094permissionmanagergroupmembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00095permissionmanagerfunctionalityavailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00123permissionmanagerfunctionalityselectedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1008, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 661);
            this.barDockControlBottom.Size = new System.Drawing.Size(1008, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 661);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1008, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 661);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colColour
            // 
            this.colColour.Caption = "Colour";
            this.colColour.FieldName = "Colour";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowEdit = false;
            this.colColour.OptionsColumn.AllowFocus = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsColumn.ShowInCustomizationForm = false;
            this.colColour.Width = 43;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1008, 661);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1003, 635);
            this.xtraTabPage1.Text = "Screen Permissions";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1003, 635);
            this.splitContainerControl1.SplitterPosition = 257;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Screen \\ Report Access Groups";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Selected Group Contents  [READ ONLY - Set by Permission Groups Manager]";
            this.splitContainerControl3.Size = new System.Drawing.Size(1003, 257);
            this.splitContainerControl3.SplitterPosition = 375;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(618, 233);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00093permissionmanagergroupslistBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(618, 233);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00093permissionmanagergroupslistBindingSource
            // 
            this.sp00093permissionmanagergroupslistBindingSource.DataMember = "sp00093_permission_manager_groups_list";
            this.sp00093permissionmanagergroupslistBindingSource.DataSource = this.dataSet_Groups;
            // 
            // dataSet_Groups
            // 
            this.dataSet_Groups.DataSetName = "DataSet_Groups";
            this.dataSet_Groups.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupID,
            this.colGroupTypeID,
            this.colGroupDescription,
            this.colGroupTypeDescription,
            this.colGroupRemarks,
            this.colCreatedByName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colGroupID
            // 
            this.colGroupID.Caption = "Group ID";
            this.colGroupID.FieldName = "GroupID";
            this.colGroupID.Name = "colGroupID";
            this.colGroupID.OptionsColumn.AllowEdit = false;
            this.colGroupID.OptionsColumn.AllowFocus = false;
            this.colGroupID.OptionsColumn.ReadOnly = true;
            // 
            // colGroupTypeID
            // 
            this.colGroupTypeID.Caption = "Group Type ID";
            this.colGroupTypeID.FieldName = "GroupTypeID";
            this.colGroupTypeID.Name = "colGroupTypeID";
            this.colGroupTypeID.OptionsColumn.AllowEdit = false;
            this.colGroupTypeID.OptionsColumn.AllowFocus = false;
            this.colGroupTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colGroupDescription
            // 
            this.colGroupDescription.Caption = "Group Description";
            this.colGroupDescription.FieldName = "GroupDescription";
            this.colGroupDescription.Name = "colGroupDescription";
            this.colGroupDescription.OptionsColumn.AllowEdit = false;
            this.colGroupDescription.OptionsColumn.AllowFocus = false;
            this.colGroupDescription.OptionsColumn.ReadOnly = true;
            this.colGroupDescription.Visible = true;
            this.colGroupDescription.VisibleIndex = 0;
            this.colGroupDescription.Width = 532;
            // 
            // colGroupTypeDescription
            // 
            this.colGroupTypeDescription.Caption = "Group Type";
            this.colGroupTypeDescription.FieldName = "GroupTypeDescription";
            this.colGroupTypeDescription.Name = "colGroupTypeDescription";
            this.colGroupTypeDescription.OptionsColumn.AllowEdit = false;
            this.colGroupTypeDescription.OptionsColumn.AllowFocus = false;
            this.colGroupTypeDescription.OptionsColumn.ReadOnly = true;
            this.colGroupTypeDescription.Width = 206;
            // 
            // colGroupRemarks
            // 
            this.colGroupRemarks.Caption = "Group Remarks";
            this.colGroupRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colGroupRemarks.FieldName = "GroupRemarks";
            this.colGroupRemarks.Name = "colGroupRemarks";
            this.colGroupRemarks.OptionsColumn.ReadOnly = true;
            this.colGroupRemarks.Visible = true;
            this.colGroupRemarks.VisibleIndex = 2;
            this.colGroupRemarks.Width = 101;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ReadOnly = true;
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 1;
            this.colCreatedByName.Width = 202;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl4;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer2.Size = new System.Drawing.Size(371, 233);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00094permissionmanagergroupmembersBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(371, 233);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00094permissionmanagergroupmembersBindingSource
            // 
            this.sp00094permissionmanagergroupmembersBindingSource.DataMember = "sp00094_permission_manager_group_members";
            this.sp00094permissionmanagergroupmembersBindingSource.DataSource = this.dataSet_Groups;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsBehavior.Editable = false;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowCellMerge = true;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Group Member ID";
            this.gridColumn1.FieldName = "GroupMemberID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Group ID";
            this.gridColumn2.FieldName = "GroupID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Group Type ID";
            this.gridColumn3.FieldName = "GroupTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Group Description";
            this.gridColumn4.FieldName = "GroupDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 189;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Group Type Description";
            this.gridColumn5.FieldName = "GroupTypeDescription";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 197;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Staff Name";
            this.gridColumn6.FieldName = "StaffName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 346;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel1.Controls.Add(this.btnRemoveAll1);
            this.splitContainerControl2.Panel1.Controls.Add(this.btnRemoveOne1);
            this.splitContainerControl2.Panel1.Controls.Add(this.btnAddAll1);
            this.splitContainerControl2.Panel1.Controls.Add(this.btnAddOne1);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Available Functionality [Excluded from Selected Group]";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer4);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Selected Functionality [Included in Selected Group]";
            this.splitContainerControl2.Size = new System.Drawing.Size(1003, 372);
            this.splitContainerControl2.SplitterPosition = 495;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // btnRemoveAll1
            // 
            this.btnRemoveAll1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveAll1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveAll1.ImageOptions.Image")));
            this.btnRemoveAll1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveAll1.Location = new System.Drawing.Point(444, 175);
            this.btnRemoveAll1.Name = "btnRemoveAll1";
            this.btnRemoveAll1.Size = new System.Drawing.Size(41, 33);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Remove All Users - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>remove all functionality</b> from the <b>current group</b>.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnRemoveAll1.SuperTip = superToolTip1;
            this.btnRemoveAll1.TabIndex = 4;
            this.btnRemoveAll1.Click += new System.EventHandler(this.btnRemoveAll1_Click);
            // 
            // btnRemoveOne1
            // 
            this.btnRemoveOne1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveOne1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveOne1.ImageOptions.Image")));
            this.btnRemoveOne1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemoveOne1.Location = new System.Drawing.Point(444, 140);
            this.btnRemoveOne1.Name = "btnRemoveOne1";
            this.btnRemoveOne1.Size = new System.Drawing.Size(41, 33);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Remove Selected Users - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>remove</b> the <b>selected functionality</b> from\r\n the <b>current" +
    " group</b>.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnRemoveOne1.SuperTip = superToolTip2;
            this.btnRemoveOne1.TabIndex = 3;
            this.btnRemoveOne1.Click += new System.EventHandler(this.btnRemoveOne1_Click);
            // 
            // btnAddAll1
            // 
            this.btnAddAll1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAll1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddAll1.ImageOptions.Image")));
            this.btnAddAll1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddAll1.Location = new System.Drawing.Point(444, 88);
            this.btnAddAll1.Name = "btnAddAll1";
            this.btnAddAll1.Size = new System.Drawing.Size(41, 33);
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Add All Users - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>add all functionality</b> to the <b>current group</b>.\r\n";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btnAddAll1.SuperTip = superToolTip3;
            this.btnAddAll1.TabIndex = 2;
            this.btnAddAll1.Click += new System.EventHandler(this.btnAddAll1_Click);
            // 
            // btnAddOne1
            // 
            this.btnAddOne1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddOne1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddOne1.ImageOptions.Image")));
            this.btnAddOne1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddOne1.Location = new System.Drawing.Point(444, 53);
            this.btnAddOne1.Name = "btnAddOne1";
            this.btnAddOne1.Size = new System.Drawing.Size(41, 33);
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Add Selected Users - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>add</b> the <b>selected functionality</b> to the <b>current group<" +
    "/b>.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.btnAddOne1.SuperTip = superToolTip4;
            this.btnAddOne1.TabIndex = 1;
            this.btnAddOne1.Click += new System.EventHandler(this.btnAddOne1_Click);
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer3.Grid = this.gridControl2;
            this.gridSplitContainer3.Location = new System.Drawing.Point(-2, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer3.Size = new System.Drawing.Size(440, 350);
            this.gridSplitContainer3.TabIndex = 5;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00095permissionmanagerfunctionalityavailableBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl2.Size = new System.Drawing.Size(440, 350);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00095permissionmanagerfunctionalityavailableBindingSource
            // 
            this.sp00095permissionmanagerfunctionalityavailableBindingSource.DataMember = "sp00095_permission_manager_functionality_available";
            this.sp00095permissionmanagerfunctionalityavailableBindingSource.DataSource = this.dataSet_Groups;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProgramPartID,
            this.colPartID,
            this.colSubPartID,
            this.colModuleID,
            this.colModuleName,
            this.colPartDescription,
            this.colPartOrder,
            this.colColour,
            this.colType,
            this.colRemarks});
            this.gridView2.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colColour;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.NotEqual;
            formatConditionRuleValue1.Value1 = "Black";
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseDown);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colProgramPartID
            // 
            this.colProgramPartID.Caption = "Program Part ID";
            this.colProgramPartID.FieldName = "ProgramPartID";
            this.colProgramPartID.Name = "colProgramPartID";
            this.colProgramPartID.OptionsColumn.AllowEdit = false;
            this.colProgramPartID.OptionsColumn.AllowFocus = false;
            this.colProgramPartID.OptionsColumn.ReadOnly = true;
            this.colProgramPartID.Width = 99;
            // 
            // colPartID
            // 
            this.colPartID.Caption = "Part ID";
            this.colPartID.FieldName = "PartID";
            this.colPartID.Name = "colPartID";
            this.colPartID.OptionsColumn.AllowEdit = false;
            this.colPartID.OptionsColumn.AllowFocus = false;
            this.colPartID.OptionsColumn.ReadOnly = true;
            this.colPartID.Width = 56;
            // 
            // colSubPartID
            // 
            this.colSubPartID.Caption = "Sub Part ID";
            this.colSubPartID.FieldName = "SubPartID";
            this.colSubPartID.Name = "colSubPartID";
            this.colSubPartID.OptionsColumn.AllowEdit = false;
            this.colSubPartID.OptionsColumn.AllowFocus = false;
            this.colSubPartID.OptionsColumn.ReadOnly = true;
            this.colSubPartID.Width = 77;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            this.colModuleID.Width = 70;
            // 
            // colModuleName
            // 
            this.colModuleName.Caption = "Module Name";
            this.colModuleName.FieldName = "ModuleName";
            this.colModuleName.Name = "colModuleName";
            this.colModuleName.OptionsColumn.AllowEdit = false;
            this.colModuleName.OptionsColumn.AllowFocus = false;
            this.colModuleName.OptionsColumn.ReadOnly = true;
            this.colModuleName.Width = 86;
            // 
            // colPartDescription
            // 
            this.colPartDescription.Caption = "Part Description";
            this.colPartDescription.FieldName = "PartDescription";
            this.colPartDescription.Name = "colPartDescription";
            this.colPartDescription.OptionsColumn.AllowEdit = false;
            this.colPartDescription.OptionsColumn.AllowFocus = false;
            this.colPartDescription.OptionsColumn.ReadOnly = true;
            this.colPartDescription.Visible = true;
            this.colPartDescription.VisibleIndex = 0;
            this.colPartDescription.Width = 365;
            // 
            // colPartOrder
            // 
            this.colPartOrder.Caption = "Part Order";
            this.colPartOrder.FieldName = "PartOrder";
            this.colPartOrder.Name = "colPartOrder";
            this.colPartOrder.OptionsColumn.AllowEdit = false;
            this.colPartOrder.OptionsColumn.AllowFocus = false;
            this.colPartOrder.OptionsColumn.ReadOnly = true;
            this.colPartOrder.Width = 73;
            // 
            // colType
            // 
            this.colType.Caption = "Type";
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowEdit = false;
            this.colType.OptionsColumn.AllowFocus = false;
            this.colType.OptionsColumn.ReadOnly = true;
            this.colType.Visible = true;
            this.colType.VisibleIndex = 2;
            this.colType.Width = 87;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            this.colRemarks.Width = 109;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl3;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer4.Size = new System.Drawing.Size(498, 348);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00123permissionmanagerfunctionalityselectedBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.First.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4,
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(498, 348);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00123permissionmanagerfunctionalityselectedBindingSource
            // 
            this.sp00123permissionmanagerfunctionalityselectedBindingSource.DataMember = "sp00123_permission_manager_functionality_selected";
            this.sp00123permissionmanagerfunctionalityselectedBindingSource.DataSource = this.dataSet_Groups;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProgramPartID1,
            this.colPartID1,
            this.colSubPartID1,
            this.colModuleID1,
            this.colModuleName1,
            this.colPartDescription1,
            this.colPartOrder1,
            this.colRemarks1,
            this.colColour1,
            this.colType1,
            this.colReadAvailable,
            this.colCreateAvailable,
            this.colUpdateAvailable,
            this.colDeleteAvailable,
            this.colSwitchboardVisible,
            this.colReadAccess,
            this.colCreateAccess,
            this.colUpdateAccess,
            this.colDeleteAccess,
            this.colProgramAccessID,
            this.colGroupDescription1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colType1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseDown);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // colProgramPartID1
            // 
            this.colProgramPartID1.FieldName = "ProgramPartID";
            this.colProgramPartID1.Name = "colProgramPartID1";
            this.colProgramPartID1.OptionsColumn.AllowEdit = false;
            this.colProgramPartID1.OptionsColumn.AllowFocus = false;
            this.colProgramPartID1.OptionsColumn.ReadOnly = true;
            this.colProgramPartID1.OptionsColumn.TabStop = false;
            this.colProgramPartID1.Width = 89;
            // 
            // colPartID1
            // 
            this.colPartID1.FieldName = "PartID";
            this.colPartID1.Name = "colPartID1";
            this.colPartID1.OptionsColumn.AllowEdit = false;
            this.colPartID1.OptionsColumn.AllowFocus = false;
            this.colPartID1.OptionsColumn.ReadOnly = true;
            this.colPartID1.OptionsColumn.TabStop = false;
            this.colPartID1.Width = 46;
            // 
            // colSubPartID1
            // 
            this.colSubPartID1.FieldName = "SubPartID";
            this.colSubPartID1.Name = "colSubPartID1";
            this.colSubPartID1.OptionsColumn.AllowEdit = false;
            this.colSubPartID1.OptionsColumn.AllowFocus = false;
            this.colSubPartID1.OptionsColumn.ReadOnly = true;
            this.colSubPartID1.OptionsColumn.TabStop = false;
            this.colSubPartID1.Width = 67;
            // 
            // colModuleID1
            // 
            this.colModuleID1.FieldName = "ModuleID";
            this.colModuleID1.Name = "colModuleID1";
            this.colModuleID1.OptionsColumn.AllowEdit = false;
            this.colModuleID1.OptionsColumn.AllowFocus = false;
            this.colModuleID1.OptionsColumn.ReadOnly = true;
            this.colModuleID1.OptionsColumn.TabStop = false;
            this.colModuleID1.Width = 60;
            // 
            // colModuleName1
            // 
            this.colModuleName1.FieldName = "ModuleName";
            this.colModuleName1.Name = "colModuleName1";
            this.colModuleName1.OptionsColumn.AllowEdit = false;
            this.colModuleName1.OptionsColumn.AllowFocus = false;
            this.colModuleName1.OptionsColumn.ReadOnly = true;
            this.colModuleName1.OptionsColumn.TabStop = false;
            this.colModuleName1.Width = 99;
            // 
            // colPartDescription1
            // 
            this.colPartDescription1.FieldName = "PartDescription";
            this.colPartDescription1.Name = "colPartDescription1";
            this.colPartDescription1.OptionsColumn.AllowEdit = false;
            this.colPartDescription1.OptionsColumn.AllowFocus = false;
            this.colPartDescription1.OptionsColumn.ReadOnly = true;
            this.colPartDescription1.OptionsColumn.TabStop = false;
            this.colPartDescription1.Visible = true;
            this.colPartDescription1.VisibleIndex = 0;
            this.colPartDescription1.Width = 365;
            // 
            // colPartOrder1
            // 
            this.colPartOrder1.FieldName = "PartOrder";
            this.colPartOrder1.Name = "colPartOrder1";
            this.colPartOrder1.OptionsColumn.AllowEdit = false;
            this.colPartOrder1.OptionsColumn.AllowFocus = false;
            this.colPartOrder1.OptionsColumn.ReadOnly = true;
            this.colPartOrder1.OptionsColumn.TabStop = false;
            this.colPartOrder1.Width = 76;
            // 
            // colRemarks1
            // 
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            this.colRemarks1.Width = 63;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colColour1
            // 
            this.colColour1.FieldName = "Colour";
            this.colColour1.Name = "colColour1";
            this.colColour1.OptionsColumn.AllowEdit = false;
            this.colColour1.OptionsColumn.AllowFocus = false;
            this.colColour1.OptionsColumn.ReadOnly = true;
            this.colColour1.OptionsColumn.ShowInCustomizationForm = false;
            this.colColour1.OptionsColumn.TabStop = false;
            this.colColour1.Width = 43;
            // 
            // colType1
            // 
            this.colType1.FieldName = "Type";
            this.colType1.Name = "colType1";
            this.colType1.OptionsColumn.AllowEdit = false;
            this.colType1.OptionsColumn.AllowFocus = false;
            this.colType1.OptionsColumn.ReadOnly = true;
            this.colType1.OptionsColumn.TabStop = false;
            this.colType1.Width = 59;
            // 
            // colReadAvailable
            // 
            this.colReadAvailable.FieldName = "ReadAvailable";
            this.colReadAvailable.Name = "colReadAvailable";
            this.colReadAvailable.OptionsColumn.AllowEdit = false;
            this.colReadAvailable.OptionsColumn.AllowFocus = false;
            this.colReadAvailable.OptionsColumn.ReadOnly = true;
            this.colReadAvailable.OptionsColumn.TabStop = false;
            this.colReadAvailable.Width = 83;
            // 
            // colCreateAvailable
            // 
            this.colCreateAvailable.FieldName = "CreateAvailable";
            this.colCreateAvailable.Name = "colCreateAvailable";
            this.colCreateAvailable.OptionsColumn.AllowEdit = false;
            this.colCreateAvailable.OptionsColumn.AllowFocus = false;
            this.colCreateAvailable.OptionsColumn.ReadOnly = true;
            this.colCreateAvailable.OptionsColumn.TabStop = false;
            this.colCreateAvailable.Width = 91;
            // 
            // colUpdateAvailable
            // 
            this.colUpdateAvailable.FieldName = "UpdateAvailable";
            this.colUpdateAvailable.Name = "colUpdateAvailable";
            this.colUpdateAvailable.OptionsColumn.AllowEdit = false;
            this.colUpdateAvailable.OptionsColumn.AllowFocus = false;
            this.colUpdateAvailable.OptionsColumn.ReadOnly = true;
            this.colUpdateAvailable.OptionsColumn.TabStop = false;
            this.colUpdateAvailable.Width = 93;
            // 
            // colDeleteAvailable
            // 
            this.colDeleteAvailable.FieldName = "DeleteAvailable";
            this.colDeleteAvailable.Name = "colDeleteAvailable";
            this.colDeleteAvailable.OptionsColumn.AllowEdit = false;
            this.colDeleteAvailable.OptionsColumn.AllowFocus = false;
            this.colDeleteAvailable.OptionsColumn.ReadOnly = true;
            this.colDeleteAvailable.OptionsColumn.TabStop = false;
            this.colDeleteAvailable.Width = 89;
            // 
            // colSwitchboardVisible
            // 
            this.colSwitchboardVisible.FieldName = "SwitchboardVisible";
            this.colSwitchboardVisible.Name = "colSwitchboardVisible";
            this.colSwitchboardVisible.OptionsColumn.AllowEdit = false;
            this.colSwitchboardVisible.OptionsColumn.AllowFocus = false;
            this.colSwitchboardVisible.OptionsColumn.ReadOnly = true;
            this.colSwitchboardVisible.OptionsColumn.TabStop = false;
            this.colSwitchboardVisible.Visible = true;
            this.colSwitchboardVisible.VisibleIndex = 6;
            this.colSwitchboardVisible.Width = 113;
            // 
            // colReadAccess
            // 
            this.colReadAccess.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReadAccess.FieldName = "ReadAccess";
            this.colReadAccess.Name = "colReadAccess";
            this.colReadAccess.Visible = true;
            this.colReadAccess.VisibleIndex = 1;
            this.colReadAccess.Width = 83;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colCreateAccess
            // 
            this.colCreateAccess.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCreateAccess.FieldName = "CreateAccess";
            this.colCreateAccess.Name = "colCreateAccess";
            this.colCreateAccess.Visible = true;
            this.colCreateAccess.VisibleIndex = 2;
            this.colCreateAccess.Width = 91;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // colUpdateAccess
            // 
            this.colUpdateAccess.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colUpdateAccess.FieldName = "UpdateAccess";
            this.colUpdateAccess.Name = "colUpdateAccess";
            this.colUpdateAccess.Visible = true;
            this.colUpdateAccess.VisibleIndex = 4;
            this.colUpdateAccess.Width = 93;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // colDeleteAccess
            // 
            this.colDeleteAccess.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDeleteAccess.FieldName = "DeleteAccess";
            this.colDeleteAccess.Name = "colDeleteAccess";
            this.colDeleteAccess.Visible = true;
            this.colDeleteAccess.VisibleIndex = 3;
            this.colDeleteAccess.Width = 89;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // colProgramAccessID
            // 
            this.colProgramAccessID.FieldName = "ProgramAccessID";
            this.colProgramAccessID.Name = "colProgramAccessID";
            this.colProgramAccessID.OptionsColumn.AllowEdit = false;
            this.colProgramAccessID.OptionsColumn.AllowFocus = false;
            this.colProgramAccessID.OptionsColumn.ReadOnly = true;
            this.colProgramAccessID.OptionsColumn.TabStop = false;
            this.colProgramAccessID.Width = 112;
            // 
            // colGroupDescription1
            // 
            this.colGroupDescription1.FieldName = "GroupDescription";
            this.colGroupDescription1.Name = "colGroupDescription1";
            this.colGroupDescription1.OptionsColumn.AllowEdit = false;
            this.colGroupDescription1.OptionsColumn.AllowFocus = false;
            this.colGroupDescription1.OptionsColumn.ReadOnly = true;
            this.colGroupDescription1.OptionsColumn.TabStop = false;
            this.colGroupDescription1.Visible = true;
            this.colGroupDescription1.VisibleIndex = 7;
            this.colGroupDescription1.Width = 107;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_Groups;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00093_permission_manager_groups_listTableAdapter
            // 
            this.sp00093_permission_manager_groups_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp00094_permission_manager_group_membersTableAdapter
            // 
            this.sp00094_permission_manager_group_membersTableAdapter.ClearBeforeFill = true;
            // 
            // sp00095_permission_manager_functionality_availableTableAdapter
            // 
            this.sp00095_permission_manager_functionality_availableTableAdapter.ClearBeforeFill = true;
            // 
            // sp00123_permission_manager_functionality_selectedTableAdapter
            // 
            this.sp00123_permission_manager_functionality_selectedTableAdapter.ClearBeforeFill = true;
            // 
            // frmPermissionsManager
            // 
            this.ClientSize = new System.Drawing.Size(1008, 661);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frmPermissionsManager";
            this.Text = "Permissions Manager";
            this.Activated += new System.EventHandler(this.frmPermissionsManager_Activated);
            this.Load += new System.EventHandler(this.frmPermissionsManager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00093permissionmanagergroupslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00094permissionmanagergroupmembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00095permissionmanagerfunctionalityavailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00123permissionmanagerfunctionalityselectedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnAddOne1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.SimpleButton btnAddAll1;
        private DevExpress.XtraEditors.SimpleButton btnRemoveAll1;
        private DevExpress.XtraEditors.SimpleButton btnRemoveOne1;
        private DataSet_Groups dataSet_Groups;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp00093permissionmanagergroupslistBindingSource;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00093_permission_manager_groups_listTableAdapter sp00093_permission_manager_groups_listTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource sp00094permissionmanagergroupmembersBindingSource;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00094_permission_manager_group_membersTableAdapter sp00094_permission_manager_group_membersTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp00095permissionmanagerfunctionalityavailableBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colProgramPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName;
        private DevExpress.XtraGrid.Columns.GridColumn colPartDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPartOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colColour;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00095_permission_manager_functionality_availableTableAdapter sp00095_permission_manager_functionality_availableTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private System.Windows.Forms.BindingSource sp00123permissionmanagerfunctionalityselectedBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colProgramPartID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPartID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubPartID1;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPartDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colPartOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colColour1;
        private DevExpress.XtraGrid.Columns.GridColumn colType1;
        private DevExpress.XtraGrid.Columns.GridColumn colReadAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdateAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colDeleteAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colSwitchboardVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colReadAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdateAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colDeleteAccess;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00123_permission_manager_functionality_selectedTableAdapter sp00123_permission_manager_functionality_selectedTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colProgramAccessID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
    }
}

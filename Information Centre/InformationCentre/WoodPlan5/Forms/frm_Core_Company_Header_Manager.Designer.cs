namespace WoodPlan5
{
    partial class frm_Core_Company_Header_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Company_Header_Manager));
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00228CompanayHeadersListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colintHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrSlogan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMobile1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMobile2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrFax1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrFax2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colstrEmail2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrWebSite1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrWebSite2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00228_Companay_Headers_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00228_Companay_Headers_ListTableAdapter();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00228CompanayHeadersListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1008, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 673);
            this.barDockControlBottom.Size = new System.Drawing.Size(1008, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 673);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1008, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 673);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00228CompanayHeadersListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1008, 673);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00228CompanayHeadersListBindingSource
            // 
            this.sp00228CompanayHeadersListBindingSource.DataMember = "sp00228_Companay_Headers_List";
            this.sp00228CompanayHeadersListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colintHeaderID,
            this.colstrDescription,
            this.colstrCompanyName,
            this.colstrSlogan,
            this.colstrAddressLine1,
            this.colstrAddressLine2,
            this.colstrAddressLine3,
            this.colstrAddressLine4,
            this.colstrAddressLine5,
            this.colstrPostcode,
            this.colstrTelephone1,
            this.colstrTelephone2,
            this.colstrMobile1,
            this.colstrMobile2,
            this.colstrFax1,
            this.colstrFax2,
            this.colstrEmail1,
            this.colstrEmail2,
            this.colstrWebSite1,
            this.colstrWebSite2,
            this.colstrRemarks});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colintHeaderID
            // 
            this.colintHeaderID.Caption = "Header ID";
            this.colintHeaderID.FieldName = "intHeaderID";
            this.colintHeaderID.Name = "colintHeaderID";
            this.colintHeaderID.OptionsColumn.AllowEdit = false;
            this.colintHeaderID.OptionsColumn.AllowFocus = false;
            this.colintHeaderID.OptionsColumn.ReadOnly = true;
            this.colintHeaderID.Width = 70;
            // 
            // colstrDescription
            // 
            this.colstrDescription.Caption = "Header Description";
            this.colstrDescription.FieldName = "strDescription";
            this.colstrDescription.Name = "colstrDescription";
            this.colstrDescription.OptionsColumn.AllowEdit = false;
            this.colstrDescription.OptionsColumn.AllowFocus = false;
            this.colstrDescription.OptionsColumn.ReadOnly = true;
            this.colstrDescription.Visible = true;
            this.colstrDescription.VisibleIndex = 0;
            this.colstrDescription.Width = 212;
            // 
            // colstrCompanyName
            // 
            this.colstrCompanyName.Caption = "Company Name";
            this.colstrCompanyName.FieldName = "strCompanyName";
            this.colstrCompanyName.Name = "colstrCompanyName";
            this.colstrCompanyName.OptionsColumn.AllowEdit = false;
            this.colstrCompanyName.OptionsColumn.AllowFocus = false;
            this.colstrCompanyName.OptionsColumn.ReadOnly = true;
            this.colstrCompanyName.Visible = true;
            this.colstrCompanyName.VisibleIndex = 1;
            this.colstrCompanyName.Width = 268;
            // 
            // colstrSlogan
            // 
            this.colstrSlogan.Caption = "Slogan";
            this.colstrSlogan.FieldName = "strSlogan";
            this.colstrSlogan.Name = "colstrSlogan";
            this.colstrSlogan.OptionsColumn.AllowEdit = false;
            this.colstrSlogan.OptionsColumn.AllowFocus = false;
            this.colstrSlogan.OptionsColumn.ReadOnly = true;
            this.colstrSlogan.Visible = true;
            this.colstrSlogan.VisibleIndex = 2;
            this.colstrSlogan.Width = 195;
            // 
            // colstrAddressLine1
            // 
            this.colstrAddressLine1.Caption = "Address Line 1";
            this.colstrAddressLine1.FieldName = "strAddressLine1";
            this.colstrAddressLine1.Name = "colstrAddressLine1";
            this.colstrAddressLine1.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine1.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine1.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine1.Visible = true;
            this.colstrAddressLine1.VisibleIndex = 3;
            this.colstrAddressLine1.Width = 161;
            // 
            // colstrAddressLine2
            // 
            this.colstrAddressLine2.Caption = "Address Line 2";
            this.colstrAddressLine2.FieldName = "strAddressLine2";
            this.colstrAddressLine2.Name = "colstrAddressLine2";
            this.colstrAddressLine2.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine2.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine2.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine2.Width = 91;
            // 
            // colstrAddressLine3
            // 
            this.colstrAddressLine3.Caption = "Address Line 3";
            this.colstrAddressLine3.FieldName = "strAddressLine3";
            this.colstrAddressLine3.Name = "colstrAddressLine3";
            this.colstrAddressLine3.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine3.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine3.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine3.Width = 91;
            // 
            // colstrAddressLine4
            // 
            this.colstrAddressLine4.Caption = "Address Line 4";
            this.colstrAddressLine4.FieldName = "strAddressLine4";
            this.colstrAddressLine4.Name = "colstrAddressLine4";
            this.colstrAddressLine4.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine4.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine4.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine4.Width = 91;
            // 
            // colstrAddressLine5
            // 
            this.colstrAddressLine5.Caption = "Address Line 5";
            this.colstrAddressLine5.FieldName = "strAddressLine5";
            this.colstrAddressLine5.Name = "colstrAddressLine5";
            this.colstrAddressLine5.OptionsColumn.AllowEdit = false;
            this.colstrAddressLine5.OptionsColumn.AllowFocus = false;
            this.colstrAddressLine5.OptionsColumn.ReadOnly = true;
            this.colstrAddressLine5.Width = 91;
            // 
            // colstrPostcode
            // 
            this.colstrPostcode.Caption = "Postcode";
            this.colstrPostcode.FieldName = "strPostcode";
            this.colstrPostcode.Name = "colstrPostcode";
            this.colstrPostcode.OptionsColumn.AllowEdit = false;
            this.colstrPostcode.OptionsColumn.AllowFocus = false;
            this.colstrPostcode.OptionsColumn.ReadOnly = true;
            this.colstrPostcode.Width = 65;
            // 
            // colstrTelephone1
            // 
            this.colstrTelephone1.Caption = "Telephone 1";
            this.colstrTelephone1.FieldName = "strTelephone1";
            this.colstrTelephone1.Name = "colstrTelephone1";
            this.colstrTelephone1.OptionsColumn.AllowEdit = false;
            this.colstrTelephone1.OptionsColumn.AllowFocus = false;
            this.colstrTelephone1.OptionsColumn.ReadOnly = true;
            this.colstrTelephone1.Visible = true;
            this.colstrTelephone1.VisibleIndex = 4;
            this.colstrTelephone1.Width = 80;
            // 
            // colstrTelephone2
            // 
            this.colstrTelephone2.Caption = "Telephone 2";
            this.colstrTelephone2.FieldName = "strTelephone2";
            this.colstrTelephone2.Name = "colstrTelephone2";
            this.colstrTelephone2.OptionsColumn.AllowEdit = false;
            this.colstrTelephone2.OptionsColumn.AllowFocus = false;
            this.colstrTelephone2.OptionsColumn.ReadOnly = true;
            this.colstrTelephone2.Visible = true;
            this.colstrTelephone2.VisibleIndex = 5;
            this.colstrTelephone2.Width = 80;
            // 
            // colstrMobile1
            // 
            this.colstrMobile1.Caption = "Mobile 1";
            this.colstrMobile1.FieldName = "strMobile1";
            this.colstrMobile1.Name = "colstrMobile1";
            this.colstrMobile1.OptionsColumn.AllowEdit = false;
            this.colstrMobile1.OptionsColumn.AllowFocus = false;
            this.colstrMobile1.OptionsColumn.ReadOnly = true;
            this.colstrMobile1.Visible = true;
            this.colstrMobile1.VisibleIndex = 6;
            this.colstrMobile1.Width = 84;
            // 
            // colstrMobile2
            // 
            this.colstrMobile2.Caption = "Mobile 2";
            this.colstrMobile2.FieldName = "strMobile2";
            this.colstrMobile2.Name = "colstrMobile2";
            this.colstrMobile2.OptionsColumn.AllowEdit = false;
            this.colstrMobile2.OptionsColumn.AllowFocus = false;
            this.colstrMobile2.OptionsColumn.ReadOnly = true;
            this.colstrMobile2.Visible = true;
            this.colstrMobile2.VisibleIndex = 7;
            this.colstrMobile2.Width = 84;
            // 
            // colstrFax1
            // 
            this.colstrFax1.Caption = "Fax 1";
            this.colstrFax1.FieldName = "strFax1";
            this.colstrFax1.Name = "colstrFax1";
            this.colstrFax1.OptionsColumn.AllowEdit = false;
            this.colstrFax1.OptionsColumn.AllowFocus = false;
            this.colstrFax1.OptionsColumn.ReadOnly = true;
            this.colstrFax1.Width = 48;
            // 
            // colstrFax2
            // 
            this.colstrFax2.Caption = "Fax 2";
            this.colstrFax2.FieldName = "strFax2";
            this.colstrFax2.Name = "colstrFax2";
            this.colstrFax2.OptionsColumn.AllowEdit = false;
            this.colstrFax2.OptionsColumn.AllowFocus = false;
            this.colstrFax2.OptionsColumn.ReadOnly = true;
            this.colstrFax2.Width = 48;
            // 
            // colstrEmail1
            // 
            this.colstrEmail1.Caption = "Email 1";
            this.colstrEmail1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colstrEmail1.FieldName = "strEmail1";
            this.colstrEmail1.Name = "colstrEmail1";
            this.colstrEmail1.OptionsColumn.ReadOnly = true;
            this.colstrEmail1.Visible = true;
            this.colstrEmail1.VisibleIndex = 8;
            this.colstrEmail1.Width = 109;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colstrEmail2
            // 
            this.colstrEmail2.Caption = "Email 2";
            this.colstrEmail2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colstrEmail2.FieldName = "strEmail2";
            this.colstrEmail2.Name = "colstrEmail2";
            this.colstrEmail2.OptionsColumn.ReadOnly = true;
            this.colstrEmail2.Visible = true;
            this.colstrEmail2.VisibleIndex = 9;
            this.colstrEmail2.Width = 111;
            // 
            // colstrWebSite1
            // 
            this.colstrWebSite1.Caption = "Website 1";
            this.colstrWebSite1.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colstrWebSite1.FieldName = "strWebSite1";
            this.colstrWebSite1.Name = "colstrWebSite1";
            this.colstrWebSite1.OptionsColumn.ReadOnly = true;
            this.colstrWebSite1.Visible = true;
            this.colstrWebSite1.VisibleIndex = 10;
            this.colstrWebSite1.Width = 138;
            // 
            // colstrWebSite2
            // 
            this.colstrWebSite2.Caption = "Website 2";
            this.colstrWebSite2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colstrWebSite2.FieldName = "strWebSite2";
            this.colstrWebSite2.Name = "colstrWebSite2";
            this.colstrWebSite2.OptionsColumn.ReadOnly = true;
            this.colstrWebSite2.Visible = true;
            this.colstrWebSite2.VisibleIndex = 11;
            this.colstrWebSite2.Width = 109;
            // 
            // colstrRemarks
            // 
            this.colstrRemarks.Caption = "Remarks";
            this.colstrRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colstrRemarks.FieldName = "strRemarks";
            this.colstrRemarks.Name = "colstrRemarks";
            this.colstrRemarks.OptionsColumn.ReadOnly = true;
            this.colstrRemarks.Visible = true;
            this.colstrRemarks.VisibleIndex = 12;
            this.colstrRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // sp00228_Companay_Headers_ListTableAdapter
            // 
            this.sp00228_Companay_Headers_ListTableAdapter.ClearBeforeFill = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1008, 673);
            this.gridSplitContainer1.TabIndex = 4;
            // 
            // frm_Core_Company_Header_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1008, 673);
            this.Controls.Add(this.gridSplitContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Company_Header_Manager";
            this.Text = "Company Header Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Company_Header_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_Core_Company_Header_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00228CompanayHeadersListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource sp00228CompanayHeadersListBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colintHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colstrCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colstrSlogan;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colstrAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colstrPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMobile1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMobile2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrFax1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrFax2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrEmail1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrEmail2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrWebSite1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrWebSite2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRemarks;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00228_Companay_Headers_ListTableAdapter sp00228_Companay_Headers_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}

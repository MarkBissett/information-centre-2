using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Client_Site : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime i_dtStart = DateTime.MinValue;
        public DateTime i_dtEnd = DateTime.MaxValue;

        public int intAllowChildSelection = 0;
        public int intMustSelectChildren = 0;

        public string strPassedInParentIDs = "";
        public string strPassedInChildIDs = "";
        public string strSelectedParentIDs = "";
        public string strSelectedChildIDs = "";
        public string strSelectedParentDescriptions = "";
        public string strSelectedChildDescriptions = "";
        public string strSelectedFullSiteDescriptions = "";

        public int intFilterActiveClient = 0;
        public int intFilterUtilityArbClient = 0;
        public int intFilterSummerMaintenanceClient = 0;
        public int intWinterMaintenanceClient = 0;
        public int intAmenityArbClient = 0;

        public bool boolCollapsePanel2 = false;

        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public int _SelectedCount1 = 0;
        public int _SelectedCount2 = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        #endregion

        public frm_Core_Select_Client_Site()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Client_Site_Load(object sender, EventArgs e)
        {
            this.FormID = 500124;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            if (boolCollapsePanel2)
            {
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Panel1;
            }
            else
            {
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Both; 
            }

            sp06021_OM_Select_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06022_OM_Select_Site_FilterTableAdapter.Connection.ConnectionString = strConnectionString;

            //i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
            //i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
            
            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();
            LoadData();

            Set_Child_Selection_Enabled_Status();

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            Array arrayRecords = strPassedInParentIDs.Split(',');  // Single quotes because char expected for delimeter //
            view.BeginUpdate();
            int intFoundRow = 0;
            foreach (string strElement in arrayRecords)
            {
                if (strElement == "") break;
                intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], Convert.ToInt32(strElement));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            view.EndUpdate();

            if (intAllowChildSelection == 1)
            {
                view = (GridView)gridControl2.MainView;
                gridControl2.ForceInitialize();
                LoadLinkedData();

                // Add record selection checkboxes to popup grid control //
                selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection2.CheckMarkColumn.VisibleIndex = 0;
                selection2.CheckMarkColumn.Width = 30;

                arrayRecords = strPassedInChildIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["SiteID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
            Set_Child_Selection_Enabled_Status();
        }


        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06021_OM_Select_Client_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06021_OM_Select_Client_Filter, i_dtStart, i_dtEnd, intFilterActiveClient, intFilterUtilityArbClient, intFilterSummerMaintenanceClient, intWinterMaintenanceClient, intAmenityArbClient);
            view.EndUpdate();
        }

        private void bciAllowSiteSelection_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //bciAllowSiteSelection.Checked = !bciAllowSiteSelection.Checked;
            Set_Child_Selection_Enabled_Status();
        }
        private void Set_Child_Selection_Enabled_Status()
        {
            if (intAllowChildSelection == 1)
            {
                gridControl2.Enabled = true;
                bbiRefreshSites.Enabled = true;
            }
            else
            {
                gridControl2.Enabled = false;
                bbiRefreshSites.Enabled = false;
            }
        }

        private void bbiRefreshClients_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void bbiRefreshSites_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadLinkedData();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Sites Available - Tick one or more Clients then click Refresh button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView2

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            string strSelectedIDs = "";    // Reset any prior values first //
            if (selection1.SelectedCount <= 0) return;
            int intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                    intCount++;
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_Summer_Core.sp06022_OM_Select_Site_Filter.Clear();
            }
            else
            {
                try
                {
                    sp06022_OM_Select_Site_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06022_OM_Select_Site_Filter, strSelectedIDs);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Sites.\n\nTry selecting one or more Clients again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedParentIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more client records by ticking them before proceeding.", "Select Client", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intMustSelectChildren == 1)
            {
                if (string.IsNullOrEmpty(strSelectedChildIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more site records by ticking them before proceeding.", "Select Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            strSelectedParentIDs = "";    // Reset any prior values first //
            strSelectedParentDescriptions = "";  // Reset any prior values first //

            if (selection1.SelectedCount <= 0) return;
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            
            int intCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedParentIDs += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                    if (intCount == 0)
                    {
                        strSelectedParentDescriptions = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                    }
                    else if (intCount >= 1)
                    {
                        strSelectedParentDescriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                    }
                    intCount++;
                }
            }
            if (intAllowChildSelection == 1)
            {
                if (selection2.SelectedCount <= 0) return;
                view = (GridView)gridControl2.MainView;
                if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
                intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedChildIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedChildDescriptions = Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                            strSelectedFullSiteDescriptions = "Client: " + Convert.ToString(view.GetRowCellValue(i, "ClientName")) +  ", Site: " + Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedChildDescriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                            strSelectedFullSiteDescriptions += "| Client: " + Convert.ToString(view.GetRowCellValue(i, "ClientName")) + ", Site: " + Convert.ToString(view.GetRowCellValue(i, "SiteName"));  // Pipe used as deimiter //
                        }
                        intCount++;
                    }
                }
            }
        }




    }
}


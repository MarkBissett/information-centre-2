using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Contractor_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private string strCertificatePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private int workTypeCount = 0;
        #endregion

        public frm_Core_Contractor_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Contractor_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 80011;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strCertificatePath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GroundControlContractorCertificatesPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Contractor Certificate Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get DefaultContractor Certificate Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            try
            {
                sp00186_Contractor_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00186_Contractor_Type_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00186_Contractor_Type_List_With_Blank);

                sp00244_Core_Legal_Statuses_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00244_Core_Legal_Statuses_PicklistTableAdapter.Fill(woodPlanDataSet.sp00244_Core_Legal_Statuses_Picklist, 1);
            }
            catch (Exception) { }

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp00183_Contractor_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00183_Contractor_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strName"] = "";
                        drNewRow["bDisabled"] = false;
                        drNewRow["InternalContractor"] = 1;
                        drNewRow["IsGritter"] = 0;
                        drNewRow["IsWoodPlanVisible"] = 0;
                        drNewRow["IsSnowClearer"] = 0;
                        drNewRow["IsUtilityArbTeam"] = 0;
                        drNewRow["IsOperationsTeam"] = 0;
                        drNewRow["LegalStatusID"] = 0;
                        drNewRow["IsPestControl"] = 0;
                        drNewRow["IsRailArb"] = 0;
                        drNewRow["IsConstruction"] = 0;
                        drNewRow["IsFencing"] = 0;
                        drNewRow["IsRoofing"] = 0;
                        drNewRow["IsWindowCleaning"] = 0;
                        drNewRow["IsPotholeTeam"] = 0;
                        drNewRow["BusinessAreaID"] = 0;
                        drNewRow["BusinessAreaName"] = "";
                        drNewRow["RegionID"] = 0;
                        drNewRow["RegionName"] = "";
                        drNewRow["OMPaidDiscountedVisitRate"] = 0;
                        drNewRow["IsTest"] = 0;
                        drNewRow["IsSpecialist"] = 0;
                        drNewRow["JoinedDate"] = DateTime.Today;
                        //drNewRow["ApprovedDate"] = null;  // Commented out since datetime fields can't be set to null //
                        //drNewRow["TerminationDate"] = null;
                        this.woodPlanDataSet.sp00183_Contractor_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00183_Contractor_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["strName"] = "";
                        this.woodPlanDataSet.sp00183_Contractor_Item.Rows.Add(drNewRow);
                        this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp00183_Contractor_ItemTableAdapter.Fill(this.woodPlanDataSet.sp00183_Contractor_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }

        
        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.woodPlanDataSet.sp00183_Contractor_Item.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Team", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strCodeTextEdit.Focus();

                        strCodeTextEdit.Properties.ReadOnly = false;
                        strNameTextEdit.Properties.ReadOnly = false;
                        intTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                        strAddressLine1TextEdit.Properties.ReadOnly = false;
                        strAddressLine2TextEdit.Properties.ReadOnly = false;
                        strAddressLine3TextEdit.Properties.ReadOnly = false;
                        strAddressLine4TextEdit.Properties.ReadOnly = false;
                        strAddressLine5TextEdit.Properties.ReadOnly = false;
                        strPostcodeTextEdit.Properties.ReadOnly = false;
                        strTel1TextEdit.Properties.ReadOnly = false;
                        strTel2TextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strWebsiteTextEdit.Properties.ReadOnly = false;
                        strEmailPasswordTextEdit.Properties.ReadOnly = false;
                        strVatRegTextEdit.Properties.ReadOnly = false;
                        
                        LatitudeSpinEdit.Properties.ReadOnly = false;
                        LongitudeSpinEdit.Properties.ReadOnly = false;
                        TextNumberMemoEdit.Properties.ReadOnly = false;

                        IsGritterCheckEdit.Properties.ValueUnchecked = 0;  // Make sure unchecked = 0 [can be Null when block editing so linked Gritter attributes are not accidentally deleted if the user unticks on a block edit] //
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strCodeTextEdit.Focus();

                        strCodeTextEdit.Properties.ReadOnly = false;
                        strNameTextEdit.Properties.ReadOnly = false;
                        intTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                        strAddressLine1TextEdit.Properties.ReadOnly = false;
                        strAddressLine2TextEdit.Properties.ReadOnly = false;
                        strAddressLine3TextEdit.Properties.ReadOnly = false;
                        strAddressLine4TextEdit.Properties.ReadOnly = false;
                        strAddressLine5TextEdit.Properties.ReadOnly = false;
                        strPostcodeTextEdit.Properties.ReadOnly = false;
                        strTel1TextEdit.Properties.ReadOnly = false;
                        strTel2TextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strWebsiteTextEdit.Properties.ReadOnly = false;
                        strEmailPasswordTextEdit.Properties.ReadOnly = false;
                        strVatRegTextEdit.Properties.ReadOnly = false;

                        LatitudeSpinEdit.Properties.ReadOnly = false;
                        LongitudeSpinEdit.Properties.ReadOnly = false;
                        TextNumberMemoEdit.Properties.ReadOnly = false;
                        
                        IsGritterCheckEdit.Properties.ValueUnchecked = 0;  // Make sure unchecked = 0 [can be Null when block editing so linked Griter attributes are not accidentally deleted if the user unticks on a block edit] //
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strRemarksMemoEdit.Focus();

                        strCodeTextEdit.Properties.ReadOnly = false;
                        strNameTextEdit.Properties.ReadOnly = false;
                        intTypeIDGridLookUpEdit.Properties.ReadOnly = false;
                        strAddressLine1TextEdit.Properties.ReadOnly = false;
                        strAddressLine2TextEdit.Properties.ReadOnly = false;
                        strAddressLine3TextEdit.Properties.ReadOnly = false;
                        strAddressLine4TextEdit.Properties.ReadOnly = false;
                        strAddressLine5TextEdit.Properties.ReadOnly = false;
                        strPostcodeTextEdit.Properties.ReadOnly = false;
                        strTel1TextEdit.Properties.ReadOnly = false;
                        strTel2TextEdit.Properties.ReadOnly = false;
                        strMobileTelTextEdit.Properties.ReadOnly = false;
                        strWebsiteTextEdit.Properties.ReadOnly = false;
                        strEmailPasswordTextEdit.Properties.ReadOnly = false;
                        strVatRegTextEdit.Properties.ReadOnly = false;
                        
                        LatitudeSpinEdit.Properties.ReadOnly = false;
                        LongitudeSpinEdit.Properties.ReadOnly = false;
                        TextNumberMemoEdit.Properties.ReadOnly = false;
                        
                        IsGritterCheckEdit.Properties.ValueUnchecked = 0;  // Make sure unchecked = 0 [can be Null when block editing so linked Griter attributes are not accidentally deleted if the user unticks on a block edit] //
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strUser1TextEdit.Focus();

                        strCodeTextEdit.Properties.ReadOnly = true;
                        strNameTextEdit.Properties.ReadOnly = true;
                        intTypeIDGridLookUpEdit.Properties.ReadOnly = true;
                        strAddressLine1TextEdit.Properties.ReadOnly = true;
                        strAddressLine2TextEdit.Properties.ReadOnly = true;
                        strAddressLine3TextEdit.Properties.ReadOnly = true;
                        strAddressLine4TextEdit.Properties.ReadOnly = true;
                        strAddressLine5TextEdit.Properties.ReadOnly = true;
                        strPostcodeTextEdit.Properties.ReadOnly = true;
                        strTel1TextEdit.Properties.ReadOnly = true;
                        strTel2TextEdit.Properties.ReadOnly = true;
                        strMobileTelTextEdit.Properties.ReadOnly = true;
                        strWebsiteTextEdit.Properties.ReadOnly = true;
                        strEmailPasswordTextEdit.Properties.ReadOnly = true;
                        strVatRegTextEdit.Properties.ReadOnly = true;

                        LatitudeSpinEdit.Properties.ReadOnly = true;
                        LongitudeSpinEdit.Properties.ReadOnly = true;
                        TextNumberMemoEdit.Properties.ReadOnly = true;
                        
                        IsGritterCheckEdit.Properties.ValueUnchecked = null;  // Make sure unchecked = null [can be Null when block editing so linked Griter attributes are not accidentally deleted if the user unticks on a block edit] //
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            Set_Control_Readonly_Status("");
            
            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_Core_Contractor_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Contractor_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;

            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            //When Blockedit - if IsSpecialist is set, then set Worktype selections to 0;
            //                 if any Worktypes selected, then set IsSpecialist to 0 
            // otherwise blockedit update will ignore NULL values
            if (strFormMode == "blockedit")
            {
                if ( workTypeCount > 0 )
                { this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsSpecialist"] = 0; }
                if (workTypeCount == 0 && SpecialistContractorCheckEdit.Checked)
                {
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsGritter"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsWoodPlanVisible"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsSnowClearer"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsUtilityArbTeam"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsOperationsTeam"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsPestControl"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsRailArb"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsConstruction"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsFencing"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsRoofing"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsWindowCleaning"] = 0;
                    this.woodPlanDataSet.sp00183_Contractor_Item.Rows[0]["IsPotholeTeam"] = 0;
                }
            }

            this.sp00183ContractorItemBindingSource.EndEdit();
            try
            {
                this.sp00183_Contractor_ItemTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00183ContractorItemBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["intContractorID"]) + ";";
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Contractor_Manager")
                    {
                        var fParentForm = (frm_Core_Contractor_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp00183_Contractor_Item.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp00183_Contractor_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                //if (!dxValidationProvider1.Validate())
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Control_Readonly_Status("");
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            //foreach (Control c in dxValidationProvider.GetInvalidControls())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    //strErrorMessages += "--> " + item.Text.ToString() + "  " + dxValidationProvider.GetValidationRule(c).ErrorText + "\n";
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void strNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strNameTextEdit, "");
            }
        }

        private void intTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 117, "Contractor Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    try
                    {
                        sp00186_Contractor_Type_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00186_Contractor_Type_List_With_Blank);
                    }
                    catch (Exception) { }
                }
            }
        }

        private void LegalStatusIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 356, "Core - Team Legal Statuses");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    try
                    {
                        sp00244_Core_Legal_Statuses_PicklistTableAdapter.Fill(woodPlanDataSet.sp00244_Core_Legal_Statuses_Picklist, 1);
                    }
                    catch (Exception) { }
                }
            }
        }

        private void IsVatRegisteredCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("IsVatRegistered");
        }

        private void BusinessAreaNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp00183ContractorItemBindingSource.Current;
            if (currentRow == null) return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                int intBusinessAreaID = (string.IsNullOrEmpty(currentRow["BusinessAreaID"].ToString()) ? 0 : Convert.ToInt32(currentRow["BusinessAreaID"]));
                var fChildForm = new frm_Core_Select_Business_Area();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intBusinessAreaID;
                fChildForm._Mode = "single";
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["BusinessAreaID"] = fChildForm.intSelectedID;
                    currentRow["BusinessAreaName"] = fChildForm.strSelectedDescription;
                    if (intBusinessAreaID != fChildForm.intSelectedID)
                    {
                        currentRow["RegionID"] = 0;
                        currentRow["RegionName"] = "";
                    }
                    sp00183ContractorItemBindingSource.EndEdit();
                }
            }
            else if ("clear".Equals(e.Button.Tag))
            {
                currentRow["BusinessAreaID"] = 0;
                currentRow["BusinessAreaName"] = "";
                currentRow["RegionID"] = 0;
                currentRow["RegionName"] = "";
                sp00183ContractorItemBindingSource.EndEdit();
            }
        }

        private void RegionNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp00183ContractorItemBindingSource.Current;
            if (currentRow == null) return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                int intRegionID = (string.IsNullOrEmpty(currentRow["RegionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RegionID"]));
                int intBusinessAreaID = (string.IsNullOrEmpty(currentRow["BusinessAreaID"].ToString()) ? 0 : Convert.ToInt32(currentRow["BusinessAreaID"]));
                string strBusinessArea = (string.IsNullOrEmpty(currentRow["BusinessAreaID"].ToString()) || currentRow["BusinessAreaID"].ToString() == "0" ? "" : currentRow["BusinessAreaID"].ToString() + ",");
               
                var fChildForm = new frm_HR_Select_Business_Area_Region();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInBusinessAreaFilter = strBusinessArea;
                fChildForm.intOriginalRegionID = intRegionID;
                fChildForm.intOriginalBusinessAreaID = intBusinessAreaID;
                fChildForm._Mode = "single";
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["RegionID"] = fChildForm.intSelectedRegionID;
                    currentRow["RegionName"] = fChildForm.strSelectedRegionNames;
                    if (intBusinessAreaID != fChildForm.intSelectedBusinessAreaID)
                    {
                        currentRow["BusinessAreaID"] = fChildForm.intSelectedBusinessAreaID;
                        currentRow["BusinessAreaName"] = fChildForm.strSelectedBusinessAreaNames;
                    }
                    sp00183ContractorItemBindingSource.EndEdit();
                }
            }
            else if ("clear".Equals(e.Button.Tag))
            {
                currentRow["RegionID"] = 0;
                currentRow["RegionName"] ="";
                sp00183ContractorItemBindingSource.EndEdit();
            }
        }

        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (strFormMode == "view") return;
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "IsVatRegistered")
            {
                strVatRegTextEdit.Properties.ReadOnly = !IsVatRegisteredCheckEdit.Checked;
            }
        }

        private void WorkTypeCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;

            if (ce.Checked)
            {
                workTypeCount++;
            }
            else
            {
                workTypeCount--;
            }

            //Auto-validate IsSpecialist if reduced worktype selections to zero
            if (workTypeCount == 0 && SpecialistContractorCheckEdit.Checked)
            {
                SpecialistContractorCheckEdit_Validating(SpecialistContractorCheckEdit, new CancelEventArgs());
            }
        }

        private void SpecialistContractorCheckEdit_Validating(object sender, CancelEventArgs e)
        {
            dxErrorProvider1.SetError(SpecialistContractorCheckEdit, "");

            //Cannot be checked if other worktypes have been selected.
            if (SpecialistContractorCheckEdit.Checked && workTypeCount > 0)
            {
                dxErrorProvider1.SetError(SpecialistContractorCheckEdit, "Cannot be a Specialist Contractor when Skill Sets have been selected.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
        }

        private void WorkTypeCheckEdit_Validating(object sender, CancelEventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;

            dxErrorProvider1.SetError(ce, "");

            //Cannot be checked if Specialist Contractor has been selected.
            if (ce.Checked && SpecialistContractorCheckEdit.Checked)
            {
                dxErrorProvider1.SetError(ce, "Cannot select Skill sets when Specialist Contractor is selected.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
        }

        private void SpecialistContractorCheckEdit_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion

    }
}


namespace WoodPlan5
{
    partial class frm_Core_Staff_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Staff_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.WorkPhoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp00197StaffItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.WoodPlanWebAdminUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanWebFileManagerAdminCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanWebWorkOrderDoneDateCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanWebActionDoneDateCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsUtilityArbSurveyorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WebSitePasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WebSiteUserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EstatePlanInspectionControlPanelCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strNetworkIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strForenameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.bActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.intToDoListCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.btnCreateLogin = new DevExpress.XtraEditors.SimpleButton();
            this.intCommentBankCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.bShowTipsOnStartCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.bUserConfigCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.bShowToolTipsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.strEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ShowConfirmationsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.MinimiseToTaskBarCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DefaultFontSizeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ShowPhotoPopupCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.strUserTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00202StaffUserTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDisplay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DefaultFontNameFontEdit = new DevExpress.XtraEditors.FontEdit();
            this.FirstScreenLoadedIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00047AvailableStartUpScreensForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bLoginCreatedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForintStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrNetworkID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrForename = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrUserType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForbActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForbShowTipsOnStart = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForbShowToolTips = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShowConfirmations = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMinimiseToTaskBar = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFirstScreenLoadedID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShowPhotoPopup = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForbUserConfig = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintToDoList = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCommentBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultFontName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultFontSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIsUtilityArbSurveyor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWoodPlanWebActionDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebSiteUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebSitePassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodPlanWebWorkOrderDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodPlanWebAdminUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodPlanWebFileManagerAdmin = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForbLoginCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWorkPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00197_Staff_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00197_Staff_ItemTableAdapter();
            this.sp00202_Staff_User_TypesTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00202_Staff_User_TypesTableAdapter();
            this.sp00047AvailableStartUpScreensForUserTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00047AvailableStartUpScreensForUserTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkPhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00197StaffItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebAdminUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebFileManagerAdminCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebActionDoneDateCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbSurveyorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSitePasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteUserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstatePlanInspectionControlPanelCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNetworkIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strForenameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intToDoListCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCommentBankCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bShowTipsOnStartCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bUserConfigCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bShowToolTipsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowConfirmationsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimiseToTaskBarCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultFontSizeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPhotoPopupCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUserTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00202StaffUserTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultFontNameFontEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstScreenLoadedIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00047AvailableStartUpScreensForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLoginCreatedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrNetworkID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrForename)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUserType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbShowTipsOnStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbShowToolTips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowConfirmations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimiseToTaskBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirstScreenLoadedID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowPhotoPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbUserConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintToDoList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCommentBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultFontName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbSurveyor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebActionDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSiteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSitePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebWorkOrderDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebAdminUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebFileManagerAdmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbLoginCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(843, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 628);
            this.barDockControlBottom.Size = new System.Drawing.Size(843, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 602);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(843, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 602);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // colPartID
            // 
            this.colPartID.Caption = "Screen ID";
            this.colPartID.FieldName = "PartID";
            this.colPartID.Name = "colPartID";
            this.colPartID.OptionsColumn.AllowEdit = false;
            this.colPartID.OptionsColumn.AllowFocus = false;
            this.colPartID.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(843, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 628);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(843, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 602);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(843, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 602);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.WorkPhoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebAdminUserCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebFileManagerAdminCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebWorkOrderDoneDateCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanWebActionDoneDateCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsUtilityArbSurveyorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSitePasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WebSiteUserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EstatePlanInspectionControlPanelCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strNetworkIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strForenameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.bActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.intToDoListCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.btnCreateLogin);
            this.dataLayoutControl1.Controls.Add(this.intCommentBankCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.bShowTipsOnStartCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.bUserConfigCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.bShowToolTipsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ShowConfirmationsCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.MinimiseToTaskBarCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultFontSizeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ShowPhotoPopupCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.strUserTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultFontNameFontEdit);
            this.dataLayoutControl1.Controls.Add(this.FirstScreenLoadedIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.bLoginCreatedCheckEdit);
            this.dataLayoutControl1.DataSource = this.sp00197StaffItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(453, 220, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(843, 602);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // WorkPhoneTextEdit
            // 
            this.WorkPhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WorkPhone", true));
            this.WorkPhoneTextEdit.Location = new System.Drawing.Point(165, 190);
            this.WorkPhoneTextEdit.MenuManager = this.barManager1;
            this.WorkPhoneTextEdit.Name = "WorkPhoneTextEdit";
            this.WorkPhoneTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkPhoneTextEdit, true);
            this.WorkPhoneTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkPhoneTextEdit, optionsSpelling1);
            this.WorkPhoneTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkPhoneTextEdit.TabIndex = 35;
            // 
            // sp00197StaffItemBindingSource
            // 
            this.sp00197StaffItemBindingSource.DataMember = "sp00197_Staff_Item";
            this.sp00197StaffItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // WoodPlanWebAdminUserCheckEdit
            // 
            this.WoodPlanWebAdminUserCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WoodPlanWebAdminUser", true));
            this.WoodPlanWebAdminUserCheckEdit.Location = new System.Drawing.Point(189, 375);
            this.WoodPlanWebAdminUserCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebAdminUserCheckEdit.Name = "WoodPlanWebAdminUserCheckEdit";
            this.WoodPlanWebAdminUserCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebAdminUserCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebAdminUserCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebAdminUserCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.WoodPlanWebAdminUserCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebAdminUserCheckEdit.TabIndex = 34;
            // 
            // WoodPlanWebFileManagerAdminCheckEdit
            // 
            this.WoodPlanWebFileManagerAdminCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WoodPlanWebFileManagerAdmin", true));
            this.WoodPlanWebFileManagerAdminCheckEdit.Location = new System.Drawing.Point(189, 398);
            this.WoodPlanWebFileManagerAdminCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebFileManagerAdminCheckEdit.Name = "WoodPlanWebFileManagerAdminCheckEdit";
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebFileManagerAdminCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebFileManagerAdminCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.WoodPlanWebFileManagerAdminCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebFileManagerAdminCheckEdit.TabIndex = 33;
            // 
            // WoodPlanWebWorkOrderDoneDateCheckEdit
            // 
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WoodPlanWebWorkOrderDoneDate", true));
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Location = new System.Drawing.Point(189, 444);
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Name = "WoodPlanWebWorkOrderDoneDateCheckEdit";
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebWorkOrderDoneDateCheckEdit.TabIndex = 32;
            // 
            // WoodPlanWebActionDoneDateCheckEdit
            // 
            this.WoodPlanWebActionDoneDateCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WoodPlanWebActionDoneDate", true));
            this.WoodPlanWebActionDoneDateCheckEdit.Location = new System.Drawing.Point(189, 421);
            this.WoodPlanWebActionDoneDateCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanWebActionDoneDateCheckEdit.Name = "WoodPlanWebActionDoneDateCheckEdit";
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanWebActionDoneDateCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanWebActionDoneDateCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.WoodPlanWebActionDoneDateCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanWebActionDoneDateCheckEdit.TabIndex = 31;
            // 
            // IsUtilityArbSurveyorCheckEdit
            // 
            this.IsUtilityArbSurveyorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "IsUtilityArbSurveyor", true));
            this.IsUtilityArbSurveyorCheckEdit.Location = new System.Drawing.Point(189, 327);
            this.IsUtilityArbSurveyorCheckEdit.MenuManager = this.barManager1;
            this.IsUtilityArbSurveyorCheckEdit.Name = "IsUtilityArbSurveyorCheckEdit";
            this.IsUtilityArbSurveyorCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsUtilityArbSurveyorCheckEdit.Properties.ValueChecked = 1;
            this.IsUtilityArbSurveyorCheckEdit.Properties.ValueUnchecked = 0;
            this.IsUtilityArbSurveyorCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.IsUtilityArbSurveyorCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsUtilityArbSurveyorCheckEdit.TabIndex = 30;
            // 
            // WebSitePasswordTextEdit
            // 
            this.WebSitePasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WebSitePassword", true));
            this.WebSitePasswordTextEdit.Location = new System.Drawing.Point(189, 351);
            this.WebSitePasswordTextEdit.MenuManager = this.barManager1;
            this.WebSitePasswordTextEdit.Name = "WebSitePasswordTextEdit";
            this.WebSitePasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSitePasswordTextEdit, true);
            this.WebSitePasswordTextEdit.Size = new System.Drawing.Size(601, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSitePasswordTextEdit, optionsSpelling2);
            this.WebSitePasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSitePasswordTextEdit.TabIndex = 29;
            // 
            // WebSiteUserNameTextEdit
            // 
            this.WebSiteUserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "WebSiteUserName", true));
            this.WebSiteUserNameTextEdit.Location = new System.Drawing.Point(189, 327);
            this.WebSiteUserNameTextEdit.MenuManager = this.barManager1;
            this.WebSiteUserNameTextEdit.Name = "WebSiteUserNameTextEdit";
            this.WebSiteUserNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WebSiteUserNameTextEdit, true);
            this.WebSiteUserNameTextEdit.Size = new System.Drawing.Size(601, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WebSiteUserNameTextEdit, optionsSpelling3);
            this.WebSiteUserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.WebSiteUserNameTextEdit.TabIndex = 28;
            // 
            // EstatePlanInspectionControlPanelCheckEdit
            // 
            this.EstatePlanInspectionControlPanelCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "EstatePlanInspectionControlPanel", true));
            this.EstatePlanInspectionControlPanelCheckEdit.Location = new System.Drawing.Point(189, 327);
            this.EstatePlanInspectionControlPanelCheckEdit.MenuManager = this.barManager1;
            this.EstatePlanInspectionControlPanelCheckEdit.Name = "EstatePlanInspectionControlPanelCheckEdit";
            this.EstatePlanInspectionControlPanelCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.EstatePlanInspectionControlPanelCheckEdit.Properties.ValueChecked = 1;
            this.EstatePlanInspectionControlPanelCheckEdit.Properties.ValueUnchecked = 0;
            this.EstatePlanInspectionControlPanelCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.EstatePlanInspectionControlPanelCheckEdit.StyleController = this.dataLayoutControl1;
            this.EstatePlanInspectionControlPanelCheckEdit.TabIndex = 27;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00197StaffItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(166, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(184, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 25;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intStaffIDTextEdit
            // 
            this.intStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "intStaffID", true));
            this.intStaffIDTextEdit.Location = new System.Drawing.Point(125, 12);
            this.intStaffIDTextEdit.MenuManager = this.barManager1;
            this.intStaffIDTextEdit.Name = "intStaffIDTextEdit";
            this.intStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intStaffIDTextEdit, true);
            this.intStaffIDTextEdit.Size = new System.Drawing.Size(474, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intStaffIDTextEdit, optionsSpelling4);
            this.intStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intStaffIDTextEdit.TabIndex = 4;
            // 
            // strNetworkIDTextEdit
            // 
            this.strNetworkIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strNetworkID", true));
            this.strNetworkIDTextEdit.Location = new System.Drawing.Point(165, 36);
            this.strNetworkIDTextEdit.MenuManager = this.barManager1;
            this.strNetworkIDTextEdit.Name = "strNetworkIDTextEdit";
            this.strNetworkIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strNetworkIDTextEdit, true);
            this.strNetworkIDTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strNetworkIDTextEdit, optionsSpelling5);
            this.strNetworkIDTextEdit.StyleController = this.dataLayoutControl1;
            this.strNetworkIDTextEdit.TabIndex = 5;
            this.strNetworkIDTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strNetworkIDTextEdit_Validating);
            this.strNetworkIDTextEdit.Validated += new System.EventHandler(this.strNetworkIDTextEdit_Validated);
            // 
            // strForenameTextEdit
            // 
            this.strForenameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strForename", true));
            this.strForenameTextEdit.Location = new System.Drawing.Point(165, 84);
            this.strForenameTextEdit.MenuManager = this.barManager1;
            this.strForenameTextEdit.Name = "strForenameTextEdit";
            this.strForenameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strForenameTextEdit, true);
            this.strForenameTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strForenameTextEdit, optionsSpelling6);
            this.strForenameTextEdit.StyleController = this.dataLayoutControl1;
            this.strForenameTextEdit.TabIndex = 6;
            // 
            // strSurnameTextEdit
            // 
            this.strSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strSurname", true));
            this.strSurnameTextEdit.Location = new System.Drawing.Point(165, 60);
            this.strSurnameTextEdit.MenuManager = this.barManager1;
            this.strSurnameTextEdit.Name = "strSurnameTextEdit";
            this.strSurnameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strSurnameTextEdit, true);
            this.strSurnameTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strSurnameTextEdit, optionsSpelling7);
            this.strSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.strSurnameTextEdit.TabIndex = 7;
            // 
            // strStaffNameTextEdit
            // 
            this.strStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strStaffName", true));
            this.strStaffNameTextEdit.Location = new System.Drawing.Point(165, 108);
            this.strStaffNameTextEdit.MenuManager = this.barManager1;
            this.strStaffNameTextEdit.Name = "strStaffNameTextEdit";
            this.strStaffNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strStaffNameTextEdit, true);
            this.strStaffNameTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strStaffNameTextEdit, optionsSpelling8);
            this.strStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strStaffNameTextEdit.TabIndex = 8;
            this.strStaffNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strStaffNameTextEdit_Validating);
            // 
            // bActiveCheckEdit
            // 
            this.bActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "bActive", true));
            this.bActiveCheckEdit.Location = new System.Drawing.Point(189, 327);
            this.bActiveCheckEdit.MenuManager = this.barManager1;
            this.bActiveCheckEdit.Name = "bActiveCheckEdit";
            this.bActiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bActiveCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.bActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.bActiveCheckEdit.TabIndex = 9;
            // 
            // intToDoListCheckEdit
            // 
            this.intToDoListCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "intToDoList", true));
            this.intToDoListCheckEdit.Location = new System.Drawing.Point(189, 465);
            this.intToDoListCheckEdit.MenuManager = this.barManager1;
            this.intToDoListCheckEdit.Name = "intToDoListCheckEdit";
            this.intToDoListCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.intToDoListCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.intToDoListCheckEdit.StyleController = this.dataLayoutControl1;
            this.intToDoListCheckEdit.TabIndex = 11;
            // 
            // btnCreateLogin
            // 
            this.btnCreateLogin.Location = new System.Drawing.Point(250, 224);
            this.btnCreateLogin.Name = "btnCreateLogin";
            this.btnCreateLogin.Size = new System.Drawing.Size(120, 22);
            this.btnCreateLogin.StyleController = this.dataLayoutControl1;
            this.btnCreateLogin.TabIndex = 26;
            this.btnCreateLogin.Text = "Create Login";
            this.btnCreateLogin.Click += new System.EventHandler(this.btnCreateLogin_Click);
            // 
            // intCommentBankCheckEdit
            // 
            this.intCommentBankCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "intCommentBank", true));
            this.intCommentBankCheckEdit.Location = new System.Drawing.Point(189, 488);
            this.intCommentBankCheckEdit.MenuManager = this.barManager1;
            this.intCommentBankCheckEdit.Name = "intCommentBankCheckEdit";
            this.intCommentBankCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.intCommentBankCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.intCommentBankCheckEdit.StyleController = this.dataLayoutControl1;
            this.intCommentBankCheckEdit.TabIndex = 12;
            // 
            // bShowTipsOnStartCheckEdit
            // 
            this.bShowTipsOnStartCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "bShowTipsOnStart", true));
            this.bShowTipsOnStartCheckEdit.Location = new System.Drawing.Point(189, 350);
            this.bShowTipsOnStartCheckEdit.MenuManager = this.barManager1;
            this.bShowTipsOnStartCheckEdit.Name = "bShowTipsOnStartCheckEdit";
            this.bShowTipsOnStartCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bShowTipsOnStartCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.bShowTipsOnStartCheckEdit.StyleController = this.dataLayoutControl1;
            this.bShowTipsOnStartCheckEdit.TabIndex = 14;
            // 
            // bUserConfigCheckEdit
            // 
            this.bUserConfigCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "bUserConfig", true));
            this.bUserConfigCheckEdit.Location = new System.Drawing.Point(189, 442);
            this.bUserConfigCheckEdit.MenuManager = this.barManager1;
            this.bUserConfigCheckEdit.Name = "bUserConfigCheckEdit";
            this.bUserConfigCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bUserConfigCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.bUserConfigCheckEdit.StyleController = this.dataLayoutControl1;
            this.bUserConfigCheckEdit.TabIndex = 16;
            // 
            // bShowToolTipsCheckEdit
            // 
            this.bShowToolTipsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "bShowToolTips", true));
            this.bShowToolTipsCheckEdit.Location = new System.Drawing.Point(189, 373);
            this.bShowToolTipsCheckEdit.MenuManager = this.barManager1;
            this.bShowToolTipsCheckEdit.Name = "bShowToolTipsCheckEdit";
            this.bShowToolTipsCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bShowToolTipsCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.bShowToolTipsCheckEdit.StyleController = this.dataLayoutControl1;
            this.bShowToolTipsCheckEdit.TabIndex = 17;
            // 
            // strEmailTextEdit
            // 
            this.strEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strEmail", true));
            this.strEmailTextEdit.Location = new System.Drawing.Point(165, 166);
            this.strEmailTextEdit.MenuManager = this.barManager1;
            this.strEmailTextEdit.Name = "strEmailTextEdit";
            this.strEmailTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmailTextEdit, true);
            this.strEmailTextEdit.Size = new System.Drawing.Size(649, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmailTextEdit, optionsSpelling9);
            this.strEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.strEmailTextEdit.TabIndex = 18;
            this.strEmailTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strEmailTextEdit_Validating);
            // 
            // ShowConfirmationsCheckEdit
            // 
            this.ShowConfirmationsCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "ShowConfirmations", true));
            this.ShowConfirmationsCheckEdit.Location = new System.Drawing.Point(189, 396);
            this.ShowConfirmationsCheckEdit.MenuManager = this.barManager1;
            this.ShowConfirmationsCheckEdit.Name = "ShowConfirmationsCheckEdit";
            this.ShowConfirmationsCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ShowConfirmationsCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.ShowConfirmationsCheckEdit.StyleController = this.dataLayoutControl1;
            this.ShowConfirmationsCheckEdit.TabIndex = 19;
            // 
            // MinimiseToTaskBarCheckEdit
            // 
            this.MinimiseToTaskBarCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "MinimiseToTaskBar", true));
            this.MinimiseToTaskBarCheckEdit.Location = new System.Drawing.Point(189, 511);
            this.MinimiseToTaskBarCheckEdit.MenuManager = this.barManager1;
            this.MinimiseToTaskBarCheckEdit.Name = "MinimiseToTaskBarCheckEdit";
            this.MinimiseToTaskBarCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.MinimiseToTaskBarCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.MinimiseToTaskBarCheckEdit.StyleController = this.dataLayoutControl1;
            this.MinimiseToTaskBarCheckEdit.TabIndex = 20;
            // 
            // DefaultFontSizeSpinEdit
            // 
            this.DefaultFontSizeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "DefaultFontSize", true));
            this.DefaultFontSizeSpinEdit.EditValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.DefaultFontSizeSpinEdit.Location = new System.Drawing.Point(189, 582);
            this.DefaultFontSizeSpinEdit.MenuManager = this.barManager1;
            this.DefaultFontSizeSpinEdit.Name = "DefaultFontSizeSpinEdit";
            this.DefaultFontSizeSpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DefaultFontSizeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Delete", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear default font size and use default system settings (Recommended)", "delete", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DefaultFontSizeSpinEdit.Properties.MaxValue = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.DefaultFontSizeSpinEdit.Properties.MinValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.DefaultFontSizeSpinEdit.Size = new System.Drawing.Size(601, 20);
            this.DefaultFontSizeSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultFontSizeSpinEdit.TabIndex = 22;
            this.DefaultFontSizeSpinEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DefaultFontSizeSpinEdit_ButtonClick);
            // 
            // ShowPhotoPopupCheckEdit
            // 
            this.ShowPhotoPopupCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "ShowPhotoPopup", true));
            this.ShowPhotoPopupCheckEdit.Location = new System.Drawing.Point(189, 419);
            this.ShowPhotoPopupCheckEdit.MenuManager = this.barManager1;
            this.ShowPhotoPopupCheckEdit.Name = "ShowPhotoPopupCheckEdit";
            this.ShowPhotoPopupCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ShowPhotoPopupCheckEdit.Size = new System.Drawing.Size(601, 19);
            this.ShowPhotoPopupCheckEdit.StyleController = this.dataLayoutControl1;
            this.ShowPhotoPopupCheckEdit.TabIndex = 23;
            // 
            // strUserTypeGridLookUpEdit
            // 
            this.strUserTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "strUserType", true));
            this.strUserTypeGridLookUpEdit.Location = new System.Drawing.Point(165, 142);
            this.strUserTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.strUserTypeGridLookUpEdit.Name = "strUserTypeGridLookUpEdit";
            this.strUserTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.strUserTypeGridLookUpEdit.Properties.DataSource = this.sp00202StaffUserTypesBindingSource;
            this.strUserTypeGridLookUpEdit.Properties.DisplayMember = "ItemDisplay";
            this.strUserTypeGridLookUpEdit.Properties.MaxLength = 10;
            this.strUserTypeGridLookUpEdit.Properties.NullText = "";
            this.strUserTypeGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.strUserTypeGridLookUpEdit.Properties.ValueMember = "ItemValue";
            this.strUserTypeGridLookUpEdit.Size = new System.Drawing.Size(649, 20);
            this.strUserTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.strUserTypeGridLookUpEdit.TabIndex = 10;
            this.strUserTypeGridLookUpEdit.TabStop = false;
            // 
            // sp00202StaffUserTypesBindingSource
            // 
            this.sp00202StaffUserTypesBindingSource.DataMember = "sp00202_Staff_User_Types";
            this.sp00202StaffUserTypesBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemOrder,
            this.colItemValue,
            this.colItemDisplay});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemOrder;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "-1";
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemDisplay, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemValue
            // 
            this.colItemValue.Caption = "User Type ID";
            this.colItemValue.FieldName = "ItemValue";
            this.colItemValue.Name = "colItemValue";
            this.colItemValue.OptionsColumn.AllowEdit = false;
            this.colItemValue.OptionsColumn.AllowFocus = false;
            this.colItemValue.OptionsColumn.ReadOnly = true;
            this.colItemValue.Width = 105;
            // 
            // colItemDisplay
            // 
            this.colItemDisplay.Caption = "User Type";
            this.colItemDisplay.FieldName = "ItemDisplay";
            this.colItemDisplay.Name = "colItemDisplay";
            this.colItemDisplay.OptionsColumn.AllowEdit = false;
            this.colItemDisplay.OptionsColumn.AllowFocus = false;
            this.colItemDisplay.OptionsColumn.ReadOnly = true;
            this.colItemDisplay.Visible = true;
            this.colItemDisplay.VisibleIndex = 0;
            this.colItemDisplay.Width = 302;
            // 
            // DefaultFontNameFontEdit
            // 
            this.DefaultFontNameFontEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "DefaultFontName", true));
            this.DefaultFontNameFontEdit.Location = new System.Drawing.Point(189, 558);
            this.DefaultFontNameFontEdit.MenuManager = this.barManager1;
            this.DefaultFontNameFontEdit.Name = "DefaultFontNameFontEdit";
            this.DefaultFontNameFontEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DefaultFontNameFontEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Delete", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear default font name and use default system settings (Recommended)", "delete", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DefaultFontNameFontEdit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DefaultFontNameFontEdit_Properties_ButtonClick);
            this.DefaultFontNameFontEdit.Size = new System.Drawing.Size(601, 20);
            this.DefaultFontNameFontEdit.StyleController = this.dataLayoutControl1;
            this.DefaultFontNameFontEdit.TabIndex = 21;
            this.DefaultFontNameFontEdit.TabStop = false;
            this.DefaultFontNameFontEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DefaultFontNameFontEdit_Properties_ButtonClick);
            // 
            // FirstScreenLoadedIDGridLookUpEdit
            // 
            this.FirstScreenLoadedIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "FirstScreenLoadedID", true));
            this.FirstScreenLoadedIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.FirstScreenLoadedIDGridLookUpEdit.Location = new System.Drawing.Point(189, 534);
            this.FirstScreenLoadedIDGridLookUpEdit.MenuManager = this.barManager1;
            this.FirstScreenLoadedIDGridLookUpEdit.Name = "FirstScreenLoadedIDGridLookUpEdit";
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.DataSource = this.sp00047AvailableStartUpScreensForUserBindingSource;
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.DisplayMember = "PartDescription";
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.NullText = "";
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.FirstScreenLoadedIDGridLookUpEdit.Properties.ValueMember = "PartID";
            this.FirstScreenLoadedIDGridLookUpEdit.Size = new System.Drawing.Size(601, 20);
            this.FirstScreenLoadedIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.FirstScreenLoadedIDGridLookUpEdit.TabIndex = 24;
            this.FirstScreenLoadedIDGridLookUpEdit.TabStop = false;
            // 
            // sp00047AvailableStartUpScreensForUserBindingSource
            // 
            this.sp00047AvailableStartUpScreensForUserBindingSource.DataMember = "sp00047AvailableStartUpScreensForUser";
            this.sp00047AvailableStartUpScreensForUserBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModuleID,
            this.colModuleName,
            this.colPartDescription,
            this.colPartID,
            this.colPartOrder,
            this.colType,
            this.colTypeDescription});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colPartID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colModuleName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPartDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            // 
            // colModuleName
            // 
            this.colModuleName.Caption = "Module Name";
            this.colModuleName.FieldName = "ModuleName";
            this.colModuleName.Name = "colModuleName";
            this.colModuleName.OptionsColumn.AllowEdit = false;
            this.colModuleName.OptionsColumn.AllowFocus = false;
            this.colModuleName.OptionsColumn.ReadOnly = true;
            this.colModuleName.Width = 196;
            // 
            // colPartDescription
            // 
            this.colPartDescription.Caption = "Screen Name";
            this.colPartDescription.FieldName = "PartDescription";
            this.colPartDescription.Name = "colPartDescription";
            this.colPartDescription.OptionsColumn.AllowEdit = false;
            this.colPartDescription.OptionsColumn.AllowFocus = false;
            this.colPartDescription.OptionsColumn.ReadOnly = true;
            this.colPartDescription.Visible = true;
            this.colPartDescription.VisibleIndex = 0;
            this.colPartDescription.Width = 330;
            // 
            // colPartOrder
            // 
            this.colPartOrder.Caption = "Order";
            this.colPartOrder.FieldName = "PartOrder";
            this.colPartOrder.Name = "colPartOrder";
            this.colPartOrder.OptionsColumn.AllowEdit = false;
            this.colPartOrder.OptionsColumn.AllowFocus = false;
            this.colPartOrder.OptionsColumn.ReadOnly = true;
            // 
            // colType
            // 
            this.colType.Caption = "Screen Type ID";
            this.colType.FieldName = "Type";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowEdit = false;
            this.colType.OptionsColumn.AllowFocus = false;
            this.colType.OptionsColumn.ReadOnly = true;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Screen Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Width = 276;
            // 
            // bLoginCreatedCheckEdit
            // 
            this.bLoginCreatedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00197StaffItemBindingSource, "bLoginCreated", true));
            this.bLoginCreatedCheckEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.bLoginCreatedCheckEdit.Enabled = false;
            this.bLoginCreatedCheckEdit.Location = new System.Drawing.Point(165, 224);
            this.bLoginCreatedCheckEdit.MenuManager = this.barManager1;
            this.bLoginCreatedCheckEdit.Name = "bLoginCreatedCheckEdit";
            this.bLoginCreatedCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.bLoginCreatedCheckEdit.Properties.Caption = "(Read Only)";
            this.bLoginCreatedCheckEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.bLoginCreatedCheckEdit.Properties.ValueChecked = 1;
            this.bLoginCreatedCheckEdit.Properties.ValueUnchecked = 0;
            this.bLoginCreatedCheckEdit.Size = new System.Drawing.Size(81, 19);
            this.bLoginCreatedCheckEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Login Created - Information";
            toolTipItem4.LeftIndent = 6;
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bLoginCreatedCheckEdit.SuperTip = superToolTip4;
            this.bLoginCreatedCheckEdit.TabIndex = 13;
            this.bLoginCreatedCheckEdit.TabStop = false;
            // 
            // ItemForintStaffID
            // 
            this.ItemForintStaffID.Control = this.intStaffIDTextEdit;
            this.ItemForintStaffID.CustomizationFormText = "Staff ID:";
            this.ItemForintStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintStaffID.Name = "ItemForintStaffID";
            this.ItemForintStaffID.Size = new System.Drawing.Size(591, 24);
            this.ItemForintStaffID.Text = "Staff ID:";
            this.ItemForintStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem6});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(826, 648);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrNetworkID,
            this.ItemForstrForename,
            this.ItemForstrStaffName,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.ItemForstrUserType,
            this.ItemForstrEmail,
            this.emptySpaceItem7,
            this.ItemForstrSurname,
            this.layoutControlGroup3,
            this.emptySpaceItem4,
            this.ItemForbLoginCreated,
            this.emptySpaceItem3,
            this.layoutControlItem2,
            this.emptySpaceItem5,
            this.ItemForWorkPhone});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(806, 618);
            // 
            // ItemForstrNetworkID
            // 
            this.ItemForstrNetworkID.Control = this.strNetworkIDTextEdit;
            this.ItemForstrNetworkID.CustomizationFormText = "Network ID:";
            this.ItemForstrNetworkID.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrNetworkID.Name = "ItemForstrNetworkID";
            this.ItemForstrNetworkID.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrNetworkID.Text = "Network ID:";
            this.ItemForstrNetworkID.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForstrForename
            // 
            this.ItemForstrForename.Control = this.strForenameTextEdit;
            this.ItemForstrForename.CustomizationFormText = "Forename:";
            this.ItemForstrForename.Location = new System.Drawing.Point(0, 72);
            this.ItemForstrForename.Name = "ItemForstrForename";
            this.ItemForstrForename.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrForename.Text = "Forename:";
            this.ItemForstrForename.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForstrStaffName
            // 
            this.ItemForstrStaffName.Control = this.strStaffNameTextEdit;
            this.ItemForstrStaffName.CustomizationFormText = "Staff Name:";
            this.ItemForstrStaffName.Location = new System.Drawing.Point(0, 96);
            this.ItemForstrStaffName.Name = "ItemForstrStaffName";
            this.ItemForstrStaffName.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrStaffName.Text = "Staff Name:";
            this.ItemForstrStaffName.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(154, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(154, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(154, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(154, 24);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(342, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(464, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrUserType
            // 
            this.ItemForstrUserType.Control = this.strUserTypeGridLookUpEdit;
            this.ItemForstrUserType.CustomizationFormText = "User Type:";
            this.ItemForstrUserType.Location = new System.Drawing.Point(0, 130);
            this.ItemForstrUserType.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForstrUserType.MinSize = new System.Drawing.Size(202, 24);
            this.ItemForstrUserType.Name = "ItemForstrUserType";
            this.ItemForstrUserType.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrUserType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUserType.Text = "User Type:";
            this.ItemForstrUserType.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForstrEmail
            // 
            this.ItemForstrEmail.Control = this.strEmailTextEdit;
            this.ItemForstrEmail.CustomizationFormText = "Email Address:";
            this.ItemForstrEmail.Location = new System.Drawing.Point(0, 154);
            this.ItemForstrEmail.Name = "ItemForstrEmail";
            this.ItemForstrEmail.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrEmail.Text = "Email Address:";
            this.ItemForstrEmail.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(806, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrSurname
            // 
            this.ItemForstrSurname.Control = this.strSurnameTextEdit;
            this.ItemForstrSurname.CustomizationFormText = "Surname:";
            this.ItemForstrSurname.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrSurname.Name = "ItemForstrSurname";
            this.ItemForstrSurname.Size = new System.Drawing.Size(806, 24);
            this.ItemForstrSurname.Text = "Surname:";
            this.ItemForstrSurname.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 248);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(806, 370);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(782, 324);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup7});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Staff Settings:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForbActive,
            this.ItemForbShowTipsOnStart,
            this.ItemForbShowToolTips,
            this.ItemForShowConfirmations,
            this.ItemForMinimiseToTaskBar,
            this.ItemForFirstScreenLoadedID,
            this.ItemForShowPhotoPopup,
            this.ItemForbUserConfig,
            this.ItemForintToDoList,
            this.ItemForintCommentBank,
            this.ItemForDefaultFontName,
            this.ItemForDefaultFontSize});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(758, 279);
            this.layoutControlGroup4.Text = "General Settings";
            // 
            // ItemForbActive
            // 
            this.ItemForbActive.Control = this.bActiveCheckEdit;
            this.ItemForbActive.CustomizationFormText = "Active:";
            this.ItemForbActive.Location = new System.Drawing.Point(0, 0);
            this.ItemForbActive.Name = "ItemForbActive";
            this.ItemForbActive.Size = new System.Drawing.Size(758, 23);
            this.ItemForbActive.Text = "Active:";
            this.ItemForbActive.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForbShowTipsOnStart
            // 
            this.ItemForbShowTipsOnStart.Control = this.bShowTipsOnStartCheckEdit;
            this.ItemForbShowTipsOnStart.CustomizationFormText = "Show Tips On Start:";
            this.ItemForbShowTipsOnStart.Location = new System.Drawing.Point(0, 23);
            this.ItemForbShowTipsOnStart.Name = "ItemForbShowTipsOnStart";
            this.ItemForbShowTipsOnStart.Size = new System.Drawing.Size(758, 23);
            this.ItemForbShowTipsOnStart.Text = "Show Tips On Start:";
            this.ItemForbShowTipsOnStart.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForbShowToolTips
            // 
            this.ItemForbShowToolTips.Control = this.bShowToolTipsCheckEdit;
            this.ItemForbShowToolTips.CustomizationFormText = "Show Tool Tips:";
            this.ItemForbShowToolTips.Location = new System.Drawing.Point(0, 46);
            this.ItemForbShowToolTips.Name = "ItemForbShowToolTips";
            this.ItemForbShowToolTips.Size = new System.Drawing.Size(758, 23);
            this.ItemForbShowToolTips.Text = "Show Tool Tips:";
            this.ItemForbShowToolTips.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForShowConfirmations
            // 
            this.ItemForShowConfirmations.Control = this.ShowConfirmationsCheckEdit;
            this.ItemForShowConfirmations.CustomizationFormText = "Show Confirmation Messages:";
            this.ItemForShowConfirmations.Location = new System.Drawing.Point(0, 69);
            this.ItemForShowConfirmations.Name = "ItemForShowConfirmations";
            this.ItemForShowConfirmations.Size = new System.Drawing.Size(758, 23);
            this.ItemForShowConfirmations.Text = "Show Confirmation Messages:";
            this.ItemForShowConfirmations.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForMinimiseToTaskBar
            // 
            this.ItemForMinimiseToTaskBar.Control = this.MinimiseToTaskBarCheckEdit;
            this.ItemForMinimiseToTaskBar.CustomizationFormText = "Minimise To Task Bar";
            this.ItemForMinimiseToTaskBar.Location = new System.Drawing.Point(0, 184);
            this.ItemForMinimiseToTaskBar.Name = "ItemForMinimiseToTaskBar";
            this.ItemForMinimiseToTaskBar.Size = new System.Drawing.Size(758, 23);
            this.ItemForMinimiseToTaskBar.Text = "Minimise To Task Bar:";
            this.ItemForMinimiseToTaskBar.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForFirstScreenLoadedID
            // 
            this.ItemForFirstScreenLoadedID.Control = this.FirstScreenLoadedIDGridLookUpEdit;
            this.ItemForFirstScreenLoadedID.CustomizationFormText = "First Screen Loaded ID";
            this.ItemForFirstScreenLoadedID.Location = new System.Drawing.Point(0, 207);
            this.ItemForFirstScreenLoadedID.Name = "ItemForFirstScreenLoadedID";
            this.ItemForFirstScreenLoadedID.Size = new System.Drawing.Size(758, 24);
            this.ItemForFirstScreenLoadedID.Text = "First Screen Loaded:";
            this.ItemForFirstScreenLoadedID.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForShowPhotoPopup
            // 
            this.ItemForShowPhotoPopup.Control = this.ShowPhotoPopupCheckEdit;
            this.ItemForShowPhotoPopup.CustomizationFormText = "Show Photo Popup";
            this.ItemForShowPhotoPopup.Location = new System.Drawing.Point(0, 92);
            this.ItemForShowPhotoPopup.Name = "ItemForShowPhotoPopup";
            this.ItemForShowPhotoPopup.Size = new System.Drawing.Size(758, 23);
            this.ItemForShowPhotoPopup.Text = "Show Photo Popup:";
            this.ItemForShowPhotoPopup.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForbUserConfig
            // 
            this.ItemForbUserConfig.Control = this.bUserConfigCheckEdit;
            this.ItemForbUserConfig.CustomizationFormText = "User Screen Configuration:";
            this.ItemForbUserConfig.Location = new System.Drawing.Point(0, 115);
            this.ItemForbUserConfig.Name = "ItemForbUserConfig";
            this.ItemForbUserConfig.Size = new System.Drawing.Size(758, 23);
            this.ItemForbUserConfig.Text = "User Screen Configuration:";
            this.ItemForbUserConfig.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForintToDoList
            // 
            this.ItemForintToDoList.Control = this.intToDoListCheckEdit;
            this.ItemForintToDoList.CustomizationFormText = "To Do List Visible:";
            this.ItemForintToDoList.Location = new System.Drawing.Point(0, 138);
            this.ItemForintToDoList.Name = "ItemForintToDoList";
            this.ItemForintToDoList.Size = new System.Drawing.Size(758, 23);
            this.ItemForintToDoList.Text = "To Do List Visible:";
            this.ItemForintToDoList.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForintCommentBank
            // 
            this.ItemForintCommentBank.Control = this.intCommentBankCheckEdit;
            this.ItemForintCommentBank.CustomizationFormText = "Comment Bank Visible:";
            this.ItemForintCommentBank.Location = new System.Drawing.Point(0, 161);
            this.ItemForintCommentBank.Name = "ItemForintCommentBank";
            this.ItemForintCommentBank.Size = new System.Drawing.Size(758, 23);
            this.ItemForintCommentBank.Text = "Comment Bank Visible:";
            this.ItemForintCommentBank.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForDefaultFontName
            // 
            this.ItemForDefaultFontName.Control = this.DefaultFontNameFontEdit;
            this.ItemForDefaultFontName.CustomizationFormText = "Default Font Name";
            this.ItemForDefaultFontName.Location = new System.Drawing.Point(0, 231);
            this.ItemForDefaultFontName.Name = "ItemForDefaultFontName";
            this.ItemForDefaultFontName.Size = new System.Drawing.Size(758, 24);
            this.ItemForDefaultFontName.Text = "Default Font Name";
            this.ItemForDefaultFontName.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForDefaultFontSize
            // 
            this.ItemForDefaultFontSize.Control = this.DefaultFontSizeSpinEdit;
            this.ItemForDefaultFontSize.CustomizationFormText = "Default Font Size";
            this.ItemForDefaultFontSize.Location = new System.Drawing.Point(0, 255);
            this.ItemForDefaultFontSize.Name = "ItemForDefaultFontSize";
            this.ItemForDefaultFontSize.Size = new System.Drawing.Size(758, 24);
            this.ItemForDefaultFontSize.Text = "Default Font Size";
            this.ItemForDefaultFontSize.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Asset Management Settings";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.emptySpaceItem8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(758, 279);
            this.layoutControlGroup5.Text = "Asset Management Settings";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.EstatePlanInspectionControlPanelCheckEdit;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(758, 23);
            this.layoutControlItem3.Text = "Show Inspection Control Panel:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(758, 256);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Utility ARB Settings";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIsUtilityArbSurveyor});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(758, 279);
            this.layoutControlGroup6.Text = "Utility ARB Settings";
            // 
            // ItemForIsUtilityArbSurveyor
            // 
            this.ItemForIsUtilityArbSurveyor.Control = this.IsUtilityArbSurveyorCheckEdit;
            this.ItemForIsUtilityArbSurveyor.CustomizationFormText = "Is Utility ARB Surveyor:";
            this.ItemForIsUtilityArbSurveyor.Location = new System.Drawing.Point(0, 0);
            this.ItemForIsUtilityArbSurveyor.Name = "ItemForIsUtilityArbSurveyor";
            this.ItemForIsUtilityArbSurveyor.Size = new System.Drawing.Size(758, 279);
            this.ItemForIsUtilityArbSurveyor.Text = "Is Utility ARB Surveyor:";
            this.ItemForIsUtilityArbSurveyor.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Web Access";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWoodPlanWebActionDoneDate,
            this.ItemForWebSiteUserName,
            this.ItemForWebSitePassword,
            this.ItemForWoodPlanWebWorkOrderDoneDate,
            this.ItemForWoodPlanWebAdminUser,
            this.ItemForWoodPlanWebFileManagerAdmin});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(758, 279);
            this.layoutControlGroup7.Text = "Web Access";
            // 
            // ItemForWoodPlanWebActionDoneDate
            // 
            this.ItemForWoodPlanWebActionDoneDate.Control = this.WoodPlanWebActionDoneDateCheckEdit;
            this.ItemForWoodPlanWebActionDoneDate.CustomizationFormText = "Set Action Completed:";
            this.ItemForWoodPlanWebActionDoneDate.Location = new System.Drawing.Point(0, 94);
            this.ItemForWoodPlanWebActionDoneDate.Name = "ItemForWoodPlanWebActionDoneDate";
            this.ItemForWoodPlanWebActionDoneDate.Size = new System.Drawing.Size(758, 23);
            this.ItemForWoodPlanWebActionDoneDate.Text = "Set Action Completed:";
            this.ItemForWoodPlanWebActionDoneDate.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForWebSiteUserName
            // 
            this.ItemForWebSiteUserName.Control = this.WebSiteUserNameTextEdit;
            this.ItemForWebSiteUserName.CustomizationFormText = "Website Username:";
            this.ItemForWebSiteUserName.Location = new System.Drawing.Point(0, 0);
            this.ItemForWebSiteUserName.Name = "ItemForWebSiteUserName";
            this.ItemForWebSiteUserName.Size = new System.Drawing.Size(758, 24);
            this.ItemForWebSiteUserName.Text = "Website Username:";
            this.ItemForWebSiteUserName.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForWebSitePassword
            // 
            this.ItemForWebSitePassword.Control = this.WebSitePasswordTextEdit;
            this.ItemForWebSitePassword.CustomizationFormText = "Website Password:";
            this.ItemForWebSitePassword.Location = new System.Drawing.Point(0, 24);
            this.ItemForWebSitePassword.Name = "ItemForWebSitePassword";
            this.ItemForWebSitePassword.Size = new System.Drawing.Size(758, 24);
            this.ItemForWebSitePassword.Text = "Website Password:";
            this.ItemForWebSitePassword.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForWoodPlanWebWorkOrderDoneDate
            // 
            this.ItemForWoodPlanWebWorkOrderDoneDate.Control = this.WoodPlanWebWorkOrderDoneDateCheckEdit;
            this.ItemForWoodPlanWebWorkOrderDoneDate.CustomizationFormText = "Set Work Order Completed:";
            this.ItemForWoodPlanWebWorkOrderDoneDate.Location = new System.Drawing.Point(0, 117);
            this.ItemForWoodPlanWebWorkOrderDoneDate.Name = "ItemForWoodPlanWebWorkOrderDoneDate";
            this.ItemForWoodPlanWebWorkOrderDoneDate.Size = new System.Drawing.Size(758, 162);
            this.ItemForWoodPlanWebWorkOrderDoneDate.Text = "Set Work Order Completed";
            this.ItemForWoodPlanWebWorkOrderDoneDate.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForWoodPlanWebAdminUser
            // 
            this.ItemForWoodPlanWebAdminUser.Control = this.WoodPlanWebAdminUserCheckEdit;
            this.ItemForWoodPlanWebAdminUser.CustomizationFormText = "Administrator Access:";
            this.ItemForWoodPlanWebAdminUser.Location = new System.Drawing.Point(0, 48);
            this.ItemForWoodPlanWebAdminUser.Name = "ItemForWoodPlanWebAdminUser";
            this.ItemForWoodPlanWebAdminUser.Size = new System.Drawing.Size(758, 23);
            this.ItemForWoodPlanWebAdminUser.Text = "Administrator Access:";
            this.ItemForWoodPlanWebAdminUser.TextSize = new System.Drawing.Size(150, 13);
            // 
            // ItemForWoodPlanWebFileManagerAdmin
            // 
            this.ItemForWoodPlanWebFileManagerAdmin.Control = this.WoodPlanWebFileManagerAdminCheckEdit;
            this.ItemForWoodPlanWebFileManagerAdmin.CustomizationFormText = "File Manager Administrator:";
            this.ItemForWoodPlanWebFileManagerAdmin.Location = new System.Drawing.Point(0, 71);
            this.ItemForWoodPlanWebFileManagerAdmin.Name = "ItemForWoodPlanWebFileManagerAdmin";
            this.ItemForWoodPlanWebFileManagerAdmin.Size = new System.Drawing.Size(758, 23);
            this.ItemForWoodPlanWebFileManagerAdmin.Text = "File Manager Administrator:";
            this.ItemForWoodPlanWebFileManagerAdmin.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 202);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(806, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForbLoginCreated
            // 
            this.ItemForbLoginCreated.Control = this.bLoginCreatedCheckEdit;
            this.ItemForbLoginCreated.CustomizationFormText = "Login Created:";
            this.ItemForbLoginCreated.Location = new System.Drawing.Point(0, 212);
            this.ItemForbLoginCreated.Name = "ItemForbLoginCreated";
            this.ItemForbLoginCreated.Size = new System.Drawing.Size(238, 26);
            this.ItemForbLoginCreated.Text = "Login Created:";
            this.ItemForbLoginCreated.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(362, 212);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(444, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCreateLogin;
            this.layoutControlItem2.CustomizationFormText = "Login \\ Password Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(238, 212);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(124, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(124, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(124, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Login \\ Password Button";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 238);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(806, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWorkPhone
            // 
            this.ItemForWorkPhone.Control = this.WorkPhoneTextEdit;
            this.ItemForWorkPhone.CustomizationFormText = "Work Phone Number:";
            this.ItemForWorkPhone.Location = new System.Drawing.Point(0, 178);
            this.ItemForWorkPhone.Name = "ItemForWorkPhone";
            this.ItemForWorkPhone.Size = new System.Drawing.Size(806, 24);
            this.ItemForWorkPhone.Text = "Work Phone Number:";
            this.ItemForWorkPhone.TextSize = new System.Drawing.Size(150, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 618);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(806, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00197_Staff_ItemTableAdapter
            // 
            this.sp00197_Staff_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00202_Staff_User_TypesTableAdapter
            // 
            this.sp00202_Staff_User_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp00047AvailableStartUpScreensForUserTableAdapter
            // 
            this.sp00047AvailableStartUpScreensForUserTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Staff_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(843, 656);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Staff_Edit";
            this.Text = "Edit Staff";
            this.Activated += new System.EventHandler(this.frm_Core_Staff_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Staff_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Staff_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkPhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00197StaffItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebAdminUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebFileManagerAdminCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebWorkOrderDoneDateCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanWebActionDoneDateCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbSurveyorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSitePasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSiteUserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstatePlanInspectionControlPanelCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNetworkIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strForenameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intToDoListCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCommentBankCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bShowTipsOnStartCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bUserConfigCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bShowToolTipsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowConfirmationsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimiseToTaskBarCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultFontSizeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowPhotoPopupCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUserTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00202StaffUserTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultFontNameFontEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstScreenLoadedIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00047AvailableStartUpScreensForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bLoginCreatedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrNetworkID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrForename)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUserType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbShowTipsOnStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbShowToolTips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowConfirmations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMinimiseToTaskBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirstScreenLoadedID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowPhotoPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbUserConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintToDoList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCommentBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultFontName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbSurveyor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebActionDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSiteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebSitePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebWorkOrderDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebAdminUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlanWebFileManagerAdmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbLoginCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource sp00197StaffItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00197_Staff_ItemTableAdapter sp00197_Staff_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit intStaffIDTextEdit;
        private DevExpress.XtraEditors.TextEdit strNetworkIDTextEdit;
        private DevExpress.XtraEditors.TextEdit strForenameTextEdit;
        private DevExpress.XtraEditors.TextEdit strSurnameTextEdit;
        private DevExpress.XtraEditors.TextEdit strStaffNameTextEdit;
        private DevExpress.XtraEditors.CheckEdit bActiveCheckEdit;
        private DevExpress.XtraEditors.CheckEdit intToDoListCheckEdit;
        private DevExpress.XtraEditors.CheckEdit intCommentBankCheckEdit;
        private DevExpress.XtraEditors.CheckEdit bShowTipsOnStartCheckEdit;
        private DevExpress.XtraEditors.CheckEdit bUserConfigCheckEdit;
        private DevExpress.XtraEditors.CheckEdit bShowToolTipsCheckEdit;
        private DevExpress.XtraEditors.TextEdit strEmailTextEdit;
        private DevExpress.XtraEditors.CheckEdit ShowConfirmationsCheckEdit;
        private DevExpress.XtraEditors.CheckEdit MinimiseToTaskBarCheckEdit;
        private DevExpress.XtraEditors.SpinEdit DefaultFontSizeSpinEdit;
        private DevExpress.XtraEditors.CheckEdit ShowPhotoPopupCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrNetworkID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrForename;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUserType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmail;
        private DevExpress.XtraEditors.GridLookUpEdit strUserTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.FontEdit DefaultFontNameFontEdit;
        private DevExpress.XtraEditors.GridLookUpEdit FirstScreenLoadedIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.CheckEdit bLoginCreatedCheckEdit;
        private DevExpress.XtraEditors.SimpleButton btnCreateLogin;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private System.Windows.Forms.BindingSource sp00202StaffUserTypesBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00202_Staff_User_TypesTableAdapter sp00202_Staff_User_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colItemValue;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDisplay;
        private System.Windows.Forms.BindingSource sp00047AvailableStartUpScreensForUserBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00047AvailableStartUpScreensForUserTableAdapter sp00047AvailableStartUpScreensForUserTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleName;
        private DevExpress.XtraGrid.Columns.GridColumn colPartDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colPartOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.CheckEdit EstatePlanInspectionControlPanelCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbActive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbShowTipsOnStart;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbShowToolTips;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShowConfirmations;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMinimiseToTaskBar;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFirstScreenLoadedID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShowPhotoPopup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbUserConfig;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintToDoList;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCommentBank;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultFontName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultFontSize;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbLoginCreated;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.TextEdit WebSitePasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit WebSiteUserNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSiteUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebSitePassword;
        private DevExpress.XtraEditors.CheckEdit IsUtilityArbSurveyorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsUtilityArbSurveyor;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebAdminUserCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebFileManagerAdminCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebWorkOrderDoneDateCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WoodPlanWebActionDoneDateCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebActionDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebWorkOrderDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebFileManagerAdmin;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlanWebAdminUser;
        private DevExpress.XtraEditors.TextEdit WorkPhoneTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkPhone;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

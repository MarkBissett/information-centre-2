namespace WoodPlan5
{
    partial class frm_Core_Contractor_Contact_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Contact_Edit));
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.IsOMSelfBillingEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp00187ContractorContactItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.IsActiveTotalViewUserCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TotalViewUserIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IsSnowMobileTelCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.GrittingEmailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intContactIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strContactNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strTelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strMobileTelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strFaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.bDisabledCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.bDefaultCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.intContractorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04026ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintContactID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrContactName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrTel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsSnowMobileTel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFroGrittingEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrMobileTel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsOMSelfBillingEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForbDefault = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForbDisabled = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsActiveTotalViewUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalViewUserID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04026_Contractor_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter();
            this.sp00187_Contractor_Contact_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00187_Contractor_Contact_ItemTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IsOMSelfBillingEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00187ContractorContactItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActiveTotalViewUserCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalViewUserIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowMobileTelCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContactIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strContactNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobileTelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDisabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDefaultCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContactID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrContactName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowMobileTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFroGrittingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobileTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOMSelfBillingEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActiveTotalViewUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalViewUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1086, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 428);
            this.barDockControlBottom.Size = new System.Drawing.Size(1086, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 402);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1086, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 402);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem4.Text = "Save Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormSave.SuperTip = superToolTip4;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem5.Text = "Cancel Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormCancel.SuperTip = superToolTip5;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Form Mode - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barStaticItemFormMode.SuperTip = superToolTip6;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1086, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 428);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1086, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 402);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1086, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 402);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.IsOMSelfBillingEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsActiveTotalViewUserCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalViewUserIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IsSnowMobileTelCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.checkEdit1);
            this.dataLayoutControl1.Controls.Add(this.GrittingEmailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intContactIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strContactNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strTelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strMobileTelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strFaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.bDisabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.bDefaultCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.intContractorIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp00187ContractorContactItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintContactID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(834, 476, 640, 480);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1086, 402);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // IsOMSelfBillingEmailCheckEdit
            // 
            this.IsOMSelfBillingEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "IsOMSelfBillingEmail", true));
            this.IsOMSelfBillingEmailCheckEdit.Location = new System.Drawing.Point(171, 365);
            this.IsOMSelfBillingEmailCheckEdit.MenuManager = this.barManager1;
            this.IsOMSelfBillingEmailCheckEdit.Name = "IsOMSelfBillingEmailCheckEdit";
            this.IsOMSelfBillingEmailCheckEdit.Properties.Caption = "(Summer Maintenance)";
            this.IsOMSelfBillingEmailCheckEdit.Properties.ValueChecked = 1;
            this.IsOMSelfBillingEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.IsOMSelfBillingEmailCheckEdit.Size = new System.Drawing.Size(142, 19);
            this.IsOMSelfBillingEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsOMSelfBillingEmailCheckEdit.TabIndex = 20;
            // 
            // sp00187ContractorContactItemBindingSource
            // 
            this.sp00187ContractorContactItemBindingSource.DataMember = "sp00187_Contractor_Contact_Item";
            this.sp00187ContractorContactItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // IsActiveTotalViewUserCheckEdit
            // 
            this.IsActiveTotalViewUserCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "IsActiveTotalViewUser", true));
            this.IsActiveTotalViewUserCheckEdit.Location = new System.Drawing.Point(426, 97);
            this.IsActiveTotalViewUserCheckEdit.MenuManager = this.barManager1;
            this.IsActiveTotalViewUserCheckEdit.Name = "IsActiveTotalViewUserCheckEdit";
            this.IsActiveTotalViewUserCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsActiveTotalViewUserCheckEdit.Properties.ValueChecked = 1;
            this.IsActiveTotalViewUserCheckEdit.Properties.ValueUnchecked = 0;
            this.IsActiveTotalViewUserCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.IsActiveTotalViewUserCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsActiveTotalViewUserCheckEdit.TabIndex = 18;
            this.IsActiveTotalViewUserCheckEdit.EditValueChanged += new System.EventHandler(this.IsActiveTotalViewUserCheckEdit_EditValueChanged);
            this.IsActiveTotalViewUserCheckEdit.Validated += new System.EventHandler(this.IsActiveTotalViewUserCheckEdit_Validated);
            // 
            // TotalViewUserIDTextEdit
            // 
            this.TotalViewUserIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "TotalViewUserID", true));
            this.TotalViewUserIDTextEdit.Location = new System.Drawing.Point(426, 120);
            this.TotalViewUserIDTextEdit.MenuManager = this.barManager1;
            this.TotalViewUserIDTextEdit.Name = "TotalViewUserIDTextEdit";
            this.TotalViewUserIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalViewUserIDTextEdit, true);
            this.TotalViewUserIDTextEdit.Size = new System.Drawing.Size(186, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalViewUserIDTextEdit, optionsSpelling9);
            this.TotalViewUserIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalViewUserIDTextEdit.TabIndex = 19;
            // 
            // IsSnowMobileTelCheckEdit
            // 
            this.IsSnowMobileTelCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "IsSnowMobileTel", true));
            this.IsSnowMobileTelCheckEdit.Location = new System.Drawing.Point(171, 238);
            this.IsSnowMobileTelCheckEdit.MenuManager = this.barManager1;
            this.IsSnowMobileTelCheckEdit.Name = "IsSnowMobileTelCheckEdit";
            this.IsSnowMobileTelCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsSnowMobileTelCheckEdit.Properties.ValueChecked = 1;
            this.IsSnowMobileTelCheckEdit.Properties.ValueUnchecked = 0;
            this.IsSnowMobileTelCheckEdit.Size = new System.Drawing.Size(105, 19);
            this.IsSnowMobileTelCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsSnowMobileTelCheckEdit.TabIndex = 17;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "IsSelfBillingEmail", true));
            this.checkEdit1.Location = new System.Drawing.Point(171, 342);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "(Winter Maintenance)";
            this.checkEdit1.Properties.ValueChecked = 1;
            this.checkEdit1.Properties.ValueUnchecked = 0;
            this.checkEdit1.Size = new System.Drawing.Size(142, 19);
            this.checkEdit1.StyleController = this.dataLayoutControl1;
            this.checkEdit1.TabIndex = 16;
            // 
            // GrittingEmailCheckEdit
            // 
            this.GrittingEmailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "GrittingEmail", true));
            this.GrittingEmailCheckEdit.Location = new System.Drawing.Point(171, 319);
            this.GrittingEmailCheckEdit.MenuManager = this.barManager1;
            this.GrittingEmailCheckEdit.Name = "GrittingEmailCheckEdit";
            this.GrittingEmailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GrittingEmailCheckEdit.Properties.ValueChecked = 1;
            this.GrittingEmailCheckEdit.Properties.ValueUnchecked = 0;
            this.GrittingEmailCheckEdit.Size = new System.Drawing.Size(142, 19);
            this.GrittingEmailCheckEdit.StyleController = this.dataLayoutControl1;
            this.GrittingEmailCheckEdit.TabIndex = 15;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00187ContractorContactItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(166, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 14;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intContactIDTextEdit
            // 
            this.intContactIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "intContactID", true));
            this.intContactIDTextEdit.Location = new System.Drawing.Point(96, 341);
            this.intContactIDTextEdit.MenuManager = this.barManager1;
            this.intContactIDTextEdit.Name = "intContactIDTextEdit";
            this.intContactIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intContactIDTextEdit, true);
            this.intContactIDTextEdit.Size = new System.Drawing.Size(520, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intContactIDTextEdit, optionsSpelling1);
            this.intContactIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intContactIDTextEdit.TabIndex = 4;
            // 
            // strContactNameTextEdit
            // 
            this.strContactNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strContactName", true));
            this.strContactNameTextEdit.Location = new System.Drawing.Point(159, 61);
            this.strContactNameTextEdit.MenuManager = this.barManager1;
            this.strContactNameTextEdit.Name = "strContactNameTextEdit";
            this.strContactNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strContactNameTextEdit, true);
            this.strContactNameTextEdit.Size = new System.Drawing.Size(453, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strContactNameTextEdit, optionsSpelling2);
            this.strContactNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strContactNameTextEdit.TabIndex = 6;
            this.strContactNameTextEdit.EditValueChanged += new System.EventHandler(this.strContactNameTextEdit_EditValueChanged);
            this.strContactNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strContactNameTextEdit_Validating);
            this.strContactNameTextEdit.Validated += new System.EventHandler(this.strContactNameTextEdit_Validated);
            // 
            // strTelTextEdit
            // 
            this.strTelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strTel", true));
            this.strTelTextEdit.Location = new System.Drawing.Point(171, 190);
            this.strTelTextEdit.MenuManager = this.barManager1;
            this.strTelTextEdit.Name = "strTelTextEdit";
            this.strTelTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTelTextEdit, true);
            this.strTelTextEdit.Size = new System.Drawing.Size(200, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTelTextEdit, optionsSpelling3);
            this.strTelTextEdit.StyleController = this.dataLayoutControl1;
            this.strTelTextEdit.TabIndex = 7;
            // 
            // strMobileTelTextEdit
            // 
            this.strMobileTelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strMobileTel", true));
            this.strMobileTelTextEdit.Location = new System.Drawing.Point(171, 214);
            this.strMobileTelTextEdit.MenuManager = this.barManager1;
            this.strMobileTelTextEdit.Name = "strMobileTelTextEdit";
            this.strMobileTelTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strMobileTelTextEdit, true);
            this.strMobileTelTextEdit.Size = new System.Drawing.Size(200, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strMobileTelTextEdit, optionsSpelling4);
            this.strMobileTelTextEdit.StyleController = this.dataLayoutControl1;
            this.strMobileTelTextEdit.TabIndex = 8;
            this.strMobileTelTextEdit.EditValueChanged += new System.EventHandler(this.strMobileTelTextEdit_EditValueChanged);
            this.strMobileTelTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strMobileTelTextEdit_Validating);
            this.strMobileTelTextEdit.Validated += new System.EventHandler(this.strMobileTelTextEdit_Validated);
            // 
            // strFaxTextEdit
            // 
            this.strFaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strFax", true));
            this.strFaxTextEdit.Location = new System.Drawing.Point(171, 271);
            this.strFaxTextEdit.MenuManager = this.barManager1;
            this.strFaxTextEdit.Name = "strFaxTextEdit";
            this.strFaxTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strFaxTextEdit, true);
            this.strFaxTextEdit.Size = new System.Drawing.Size(200, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strFaxTextEdit, optionsSpelling5);
            this.strFaxTextEdit.StyleController = this.dataLayoutControl1;
            this.strFaxTextEdit.TabIndex = 9;
            // 
            // strEmailTextEdit
            // 
            this.strEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strEmail", true));
            this.strEmailTextEdit.Location = new System.Drawing.Point(171, 295);
            this.strEmailTextEdit.MenuManager = this.barManager1;
            this.strEmailTextEdit.Name = "strEmailTextEdit";
            this.strEmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmailTextEdit, true);
            this.strEmailTextEdit.Size = new System.Drawing.Size(429, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmailTextEdit, optionsSpelling6);
            this.strEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.strEmailTextEdit.TabIndex = 10;
            this.strEmailTextEdit.EditValueChanged += new System.EventHandler(this.strEmailTextEdit_EditValueChanged);
            this.strEmailTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strEmailTextEdit_Validating);
            this.strEmailTextEdit.Validated += new System.EventHandler(this.strEmailTextEdit_Validated);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(628, 189);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(417, 205);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling7);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 11;
            // 
            // bDisabledCheckEdit
            // 
            this.bDisabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "bDisabled", true));
            this.bDisabledCheckEdit.Location = new System.Drawing.Point(159, 120);
            this.bDisabledCheckEdit.MenuManager = this.barManager1;
            this.bDisabledCheckEdit.Name = "bDisabledCheckEdit";
            this.bDisabledCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bDisabledCheckEdit.Size = new System.Drawing.Size(116, 19);
            this.bDisabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.bDisabledCheckEdit.TabIndex = 12;
            this.bDisabledCheckEdit.EditValueChanged += new System.EventHandler(this.bDisabledCheckEdit_EditValueChanged);
            this.bDisabledCheckEdit.Validated += new System.EventHandler(this.bDisabledCheckEdit_Validated);
            // 
            // bDefaultCheckEdit
            // 
            this.bDefaultCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "bDefault", true));
            this.bDefaultCheckEdit.Location = new System.Drawing.Point(159, 97);
            this.bDefaultCheckEdit.MenuManager = this.barManager1;
            this.bDefaultCheckEdit.Name = "bDefaultCheckEdit";
            this.bDefaultCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bDefaultCheckEdit.Size = new System.Drawing.Size(116, 19);
            this.bDefaultCheckEdit.StyleController = this.dataLayoutControl1;
            this.bDefaultCheckEdit.TabIndex = 13;
            this.bDefaultCheckEdit.EditValueChanged += new System.EventHandler(this.bDefaultCheckEdit_EditValueChanged);
            this.bDefaultCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.bDefaultCheckEdit_Validating);
            this.bDefaultCheckEdit.Validated += new System.EventHandler(this.bDefaultCheckEdit_Validated);
            // 
            // intContractorIDGridLookUpEdit
            // 
            this.intContractorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00187ContractorContactItemBindingSource, "intContractorID", true));
            this.intContractorIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intContractorIDGridLookUpEdit.Location = new System.Drawing.Point(159, 35);
            this.intContractorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intContractorIDGridLookUpEdit.Name = "intContractorIDGridLookUpEdit";
            editorButtonImageOptions3.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions4.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intContractorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intContractorIDGridLookUpEdit.Properties.DataSource = this.sp04026ContractorListWithBlankBindingSource;
            this.intContractorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.intContractorIDGridLookUpEdit.Properties.NullText = "";
            this.intContractorIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.intContractorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.intContractorIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intContractorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.intContractorIDGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intContractorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intContractorIDGridLookUpEdit.TabIndex = 5;
            this.intContractorIDGridLookUpEdit.TabStop = false;
            this.intContractorIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intContractorIDSpinEdit_ButtonClick);
            this.intContractorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intContractorIDGridLookUpEdit_EditValueChanged);
            this.intContractorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intContractorIDGridLookUpEdit_Validating);
            this.intContractorIDGridLookUpEdit.Validated += new System.EventHandler(this.intContractorIDGridLookUpEdit_Validated);
            // 
            // sp04026ContractorListWithBlankBindingSource
            // 
            this.sp04026ContractorListWithBlankBindingSource.DataMember = "sp04026_Contractor_List_With_Blank";
            this.sp04026ContractorListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colContractorID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // ItemForintContactID
            // 
            this.ItemForintContactID.Control = this.intContactIDTextEdit;
            this.ItemForintContactID.CustomizationFormText = "Contact ID:";
            this.ItemForintContactID.Location = new System.Drawing.Point(0, 329);
            this.ItemForintContactID.Name = "ItemForintContactID";
            this.ItemForintContactID.Size = new System.Drawing.Size(608, 24);
            this.ItemForintContactID.Text = "Contact ID:";
            this.ItemForintContactID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup5,
            this.emptySpaceItem6});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1069, 418);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintContractorID,
            this.ItemForstrContactName,
            this.emptySpaceItem1,
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.ItemForbDefault,
            this.emptySpaceItem9,
            this.ItemForbDisabled,
            this.ItemForIsActiveTotalViewUser,
            this.ItemForTotalViewUserID,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(604, 398);
            // 
            // ItemForintContractorID
            // 
            this.ItemForintContractorID.Control = this.intContractorIDGridLookUpEdit;
            this.ItemForintContractorID.CustomizationFormText = "Team:";
            this.ItemForintContractorID.Location = new System.Drawing.Point(0, 23);
            this.ItemForintContractorID.MaxSize = new System.Drawing.Size(604, 26);
            this.ItemForintContractorID.MinSize = new System.Drawing.Size(604, 26);
            this.ItemForintContractorID.Name = "ItemForintContractorID";
            this.ItemForintContractorID.Size = new System.Drawing.Size(604, 26);
            this.ItemForintContractorID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintContractorID.Text = "Team:";
            this.ItemForintContractorID.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForstrContactName
            // 
            this.ItemForstrContactName.Control = this.strContactNameTextEdit;
            this.ItemForstrContactName.CustomizationFormText = "Contact Name:";
            this.ItemForstrContactName.Location = new System.Drawing.Point(0, 49);
            this.ItemForstrContactName.Name = "ItemForstrContactName";
            this.ItemForstrContactName.Size = new System.Drawing.Size(604, 24);
            this.ItemForstrContactName.Text = "Contact Name:";
            this.ItemForstrContactName.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 73);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(604, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Contact Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.ItemForstrTel,
            this.ItemForIsSnowMobileTel,
            this.emptySpaceItem7,
            this.emptySpaceItem5,
            this.ItemForstrFax,
            this.ItemForstrEmail,
            this.ItemFroGrittingEmail,
            this.emptySpaceItem8,
            this.layoutControlItem2,
            this.emptySpaceItem11,
            this.ItemForstrMobileTel,
            this.emptySpaceItem12,
            this.ItemForIsOMSelfBillingEmail});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 142);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(604, 256);
            this.layoutControlGroup3.Text = "Contact Details";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 198);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(580, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrTel
            // 
            this.ItemForstrTel.Control = this.strTelTextEdit;
            this.ItemForstrTel.CustomizationFormText = "Telephone:";
            this.ItemForstrTel.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrTel.MaxSize = new System.Drawing.Size(351, 24);
            this.ItemForstrTel.MinSize = new System.Drawing.Size(351, 24);
            this.ItemForstrTel.Name = "ItemForstrTel";
            this.ItemForstrTel.Size = new System.Drawing.Size(351, 24);
            this.ItemForstrTel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrTel.Text = "Telephone:";
            this.ItemForstrTel.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForIsSnowMobileTel
            // 
            this.ItemForIsSnowMobileTel.Control = this.IsSnowMobileTelCheckEdit;
            this.ItemForIsSnowMobileTel.CustomizationFormText = "Is Snow Clearance Mobile Tel:";
            this.ItemForIsSnowMobileTel.Location = new System.Drawing.Point(0, 48);
            this.ItemForIsSnowMobileTel.MaxSize = new System.Drawing.Size(256, 23);
            this.ItemForIsSnowMobileTel.MinSize = new System.Drawing.Size(256, 23);
            this.ItemForIsSnowMobileTel.Name = "ItemForIsSnowMobileTel";
            this.ItemForIsSnowMobileTel.Size = new System.Drawing.Size(256, 23);
            this.ItemForIsSnowMobileTel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsSnowMobileTel.Text = "Is Snow Clearance Mobile Tel:";
            this.ItemForIsSnowMobileTel.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(256, 48);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(324, 23);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(580, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrFax
            // 
            this.ItemForstrFax.Control = this.strFaxTextEdit;
            this.ItemForstrFax.CustomizationFormText = "Fax:";
            this.ItemForstrFax.Location = new System.Drawing.Point(0, 81);
            this.ItemForstrFax.Name = "ItemForstrFax";
            this.ItemForstrFax.Size = new System.Drawing.Size(351, 24);
            this.ItemForstrFax.Text = "Fax:";
            this.ItemForstrFax.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForstrEmail
            // 
            this.ItemForstrEmail.Control = this.strEmailTextEdit;
            this.ItemForstrEmail.CustomizationFormText = "Email:";
            this.ItemForstrEmail.Location = new System.Drawing.Point(0, 105);
            this.ItemForstrEmail.Name = "ItemForstrEmail";
            this.ItemForstrEmail.Size = new System.Drawing.Size(580, 24);
            this.ItemForstrEmail.Text = "Email:";
            this.ItemForstrEmail.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemFroGrittingEmail
            // 
            this.ItemFroGrittingEmail.Control = this.GrittingEmailCheckEdit;
            this.ItemFroGrittingEmail.CustomizationFormText = "Is Gritting Email:";
            this.ItemFroGrittingEmail.Location = new System.Drawing.Point(0, 129);
            this.ItemFroGrittingEmail.MaxSize = new System.Drawing.Size(293, 23);
            this.ItemFroGrittingEmail.MinSize = new System.Drawing.Size(293, 23);
            this.ItemFroGrittingEmail.Name = "ItemFroGrittingEmail";
            this.ItemFroGrittingEmail.Size = new System.Drawing.Size(293, 23);
            this.ItemFroGrittingEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFroGrittingEmail.Text = "Is Gritting Email:";
            this.ItemFroGrittingEmail.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(293, 129);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(287, 69);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEdit1;
            this.layoutControlItem2.CustomizationFormText = "Is Self-Billing Email:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 152);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(293, 23);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(293, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(293, 23);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Is Self-Billing \\ Team PO Email:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(351, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(229, 48);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrMobileTel
            // 
            this.ItemForstrMobileTel.Control = this.strMobileTelTextEdit;
            this.ItemForstrMobileTel.CustomizationFormText = "Mobile:";
            this.ItemForstrMobileTel.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrMobileTel.MaxSize = new System.Drawing.Size(351, 24);
            this.ItemForstrMobileTel.MinSize = new System.Drawing.Size(351, 24);
            this.ItemForstrMobileTel.Name = "ItemForstrMobileTel";
            this.ItemForstrMobileTel.Size = new System.Drawing.Size(351, 24);
            this.ItemForstrMobileTel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrMobileTel.Text = "Mobile:";
            this.ItemForstrMobileTel.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(351, 81);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(229, 24);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsOMSelfBillingEmail
            // 
            this.ItemForIsOMSelfBillingEmail.Control = this.IsOMSelfBillingEmailCheckEdit;
            this.ItemForIsOMSelfBillingEmail.Location = new System.Drawing.Point(0, 175);
            this.ItemForIsOMSelfBillingEmail.MaxSize = new System.Drawing.Size(293, 23);
            this.ItemForIsOMSelfBillingEmail.MinSize = new System.Drawing.Size(293, 23);
            this.ItemForIsOMSelfBillingEmail.Name = "ItemForIsOMSelfBillingEmail";
            this.ItemForIsOMSelfBillingEmail.Size = new System.Drawing.Size(293, 23);
            this.ItemForIsOMSelfBillingEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsOMSelfBillingEmail.Text = "Is Self-Billing Email:";
            this.ItemForIsOMSelfBillingEmail.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(170, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(170, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(170, 23);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(170, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(434, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForbDefault
            // 
            this.ItemForbDefault.Control = this.bDefaultCheckEdit;
            this.ItemForbDefault.CustomizationFormText = "Default Contact:";
            this.ItemForbDefault.Location = new System.Drawing.Point(0, 85);
            this.ItemForbDefault.MaxSize = new System.Drawing.Size(267, 23);
            this.ItemForbDefault.MinSize = new System.Drawing.Size(267, 23);
            this.ItemForbDefault.Name = "ItemForbDefault";
            this.ItemForbDefault.Size = new System.Drawing.Size(267, 23);
            this.ItemForbDefault.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForbDefault.Text = "Default Contact:";
            this.ItemForbDefault.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 132);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(604, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForbDisabled
            // 
            this.ItemForbDisabled.Control = this.bDisabledCheckEdit;
            this.ItemForbDisabled.CustomizationFormText = "Disabled:";
            this.ItemForbDisabled.Location = new System.Drawing.Point(0, 108);
            this.ItemForbDisabled.MaxSize = new System.Drawing.Size(267, 23);
            this.ItemForbDisabled.MinSize = new System.Drawing.Size(267, 23);
            this.ItemForbDisabled.Name = "ItemForbDisabled";
            this.ItemForbDisabled.Size = new System.Drawing.Size(267, 24);
            this.ItemForbDisabled.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForbDisabled.Text = "Disabled:";
            this.ItemForbDisabled.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForIsActiveTotalViewUser
            // 
            this.ItemForIsActiveTotalViewUser.Control = this.IsActiveTotalViewUserCheckEdit;
            this.ItemForIsActiveTotalViewUser.Location = new System.Drawing.Point(267, 85);
            this.ItemForIsActiveTotalViewUser.Name = "ItemForIsActiveTotalViewUser";
            this.ItemForIsActiveTotalViewUser.Size = new System.Drawing.Size(227, 23);
            this.ItemForIsActiveTotalViewUser.Text = "Total View Active:";
            this.ItemForIsActiveTotalViewUser.TextSize = new System.Drawing.Size(144, 13);
            // 
            // ItemForTotalViewUserID
            // 
            this.ItemForTotalViewUserID.Control = this.TotalViewUserIDTextEdit;
            this.ItemForTotalViewUserID.Location = new System.Drawing.Point(267, 108);
            this.ItemForTotalViewUserID.Name = "ItemForTotalViewUserID";
            this.ItemForTotalViewUserID.Size = new System.Drawing.Size(337, 24);
            this.ItemForTotalViewUserID.Text = "Total View User ID:";
            this.ItemForTotalViewUserID.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(494, 85);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(110, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(604, 141);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(445, 257);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(421, 209);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(604, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 141);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 141);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(445, 141);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04026_Contractor_List_With_BlankTableAdapter
            // 
            this.sp04026_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00187_Contractor_Contact_ItemTableAdapter
            // 
            this.sp00187_Contractor_Contact_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Contractor_Contact_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1086, 456);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Contact_Edit";
            this.Text = "Edit Team Contact";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Contact_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Contact_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Contact_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IsOMSelfBillingEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00187ContractorContactItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsActiveTotalViewUserCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalViewUserIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowMobileTelCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrittingEmailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContactIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strContactNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobileTelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strFaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDisabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDefaultCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContactID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrContactName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowMobileTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFroGrittingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobileTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOMSelfBillingEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsActiveTotalViewUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalViewUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit intContactIDTextEdit;
        private DevExpress.XtraEditors.TextEdit strContactNameTextEdit;
        private DevExpress.XtraEditors.TextEdit strTelTextEdit;
        private DevExpress.XtraEditors.TextEdit strMobileTelTextEdit;
        private DevExpress.XtraEditors.TextEdit strFaxTextEdit;
        private DevExpress.XtraEditors.TextEdit strEmailTextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit bDisabledCheckEdit;
        private DevExpress.XtraEditors.CheckEdit bDefaultCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintContactID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrContactName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbDisabled;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbDefault;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit intContractorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private System.Windows.Forms.BindingSource sp04026ContractorListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter sp04026_Contractor_List_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp00187ContractorContactItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp00187_Contractor_Contact_ItemTableAdapter sp00187_Contractor_Contact_ItemTableAdapter;
        private DevExpress.XtraEditors.CheckEdit GrittingEmailCheckEdit;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit IsSnowMobileTelCheckEdit;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.CheckEdit IsActiveTotalViewUserCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsActiveTotalViewUser;
        private DevExpress.XtraEditors.TextEdit TotalViewUserIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsSnowMobileTel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemFroGrittingEmail;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrMobileTel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalViewUserID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.CheckEdit IsOMSelfBillingEmailCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsOMSelfBillingEmail;
    }
}

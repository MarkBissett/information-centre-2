﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_AS_Equipment_Allocation : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strEquipmentIDs = "";
        private DateTime? dtStartDate;
        private DateTime? dtEndDate;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_AS_Equipment_Allocation()
        {
            InitializeComponent();
        }

        public rpt_AS_Equipment_Allocation(BaseObjects.GlobalSettings GlobalSettings, string EquipmentIDs, DateTime? StartDate, DateTime? EndDate)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strEquipmentIDs = EquipmentIDs;
            dtStartDate = StartDate;
            dtEndDate = EndDate;
        }


        private void rpt_AS_Equipment_Allocation_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                
                strMainTableName = "Table_Equipment_Kept";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp_AS_11150_Report_Equipment_Kept", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EquipmentIDs", strEquipmentIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);
                
                // Add First Relation (Linked Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp_AS_11149_Report_Equipment_Keeper", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@EquipmentIDs", strEquipmentIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
                sdaDataAdapter =  new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Equipment_Keepers");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Equipment_Kept"].Columns["EquipmentID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Equipment_Keepers"].Columns["EquipmentID"] };
                    DataRelation relation = new DataRelation("Linked Allocation", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);

                    //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Equipment_Kept"].Columns["EquipmentID"], dsDataSet.Tables["Table_Equipment_Keepers"].Columns["EquipmentID"]);
                    //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();
                

                this.Report.Name = "Equipment Allocation Breakdown";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_AS_Equipment_Allocation_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {

            //this.sp_AS_11150_Report_Equipment_KeptTableAdapter.Fill(this.dataSet_AS_Report1.sp_AS_11150_Report_Equipment_Kept, "", null, null);
            //this.dataSet_AS_Report1.sp_AS_11149_Report_Equipment_Keeper.Fill(this.dataSet_AS_Report1.sp_AS_11150_Report_Equipment_Kept, "", null, null);
            
            strMainTableName = "Table_Equipment_Kept";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp_AS_11150_Report_Equipment_Kept", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EquipmentIDs", strEquipmentIDs));
            cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp_AS_11149_Report_Equipment_Keeper", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@EquipmentIDs", strEquipmentIDs));
            cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Equipment_Keepers");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Equipment_Kept"].Columns["EquipmentID"]};
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Equipment_Keepers"].Columns["EquipmentID"]};
                DataRelation relation = new DataRelation("Linked Allocations", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);

                //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Equipment_Kept"].Columns["EquipmentID"], dsDataSet.Tables["Table_Equipment_Keepers"].Columns["EquipmentID"]);
                //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
             
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Equipment_Keepers DataTables from the end user //
        {
            #region IDataDictionary Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Equipment_Keepers"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }
            #endregion


            
        }

    }
}

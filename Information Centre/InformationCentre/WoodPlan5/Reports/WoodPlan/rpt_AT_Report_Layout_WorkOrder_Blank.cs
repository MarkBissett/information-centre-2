using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_AT_Report_Layout_WorkOrder_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private int intWorkOrderID = 0;
        private string strReportPath = "";

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedPictures = false;
        private bool boolLoadMaps = false;

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_AT_Report_Layout_WorkOrder_Blank()
        {
            InitializeComponent();
        }

        public rpt_AT_Report_Layout_WorkOrder_Blank(BaseObjects.GlobalSettings GlobalSettings, int WorkOrderID, string ReportPath, bool LoadLinkedPictures, bool LoadMaps)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            intWorkOrderID = WorkOrderID;
            strReportPath = ReportPath;
            boolLoadLinkedPictures = LoadLinkedPictures;
            boolLoadMaps = LoadMaps;
            //dsDataSet.Tables.Add("Table");
        }

        private void rpt_AT_Report_Layout_WorkOrder_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                string strPath = "";
                if (boolLoadLinkedPictures)  // Ok to load pictures so get path //
                {
                    try
                    {
                        DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
                    }
                    catch (Exception)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nLinked Pictures may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }

                strMainTableName = "Work Order Header";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp01432_AT_WorkOrders_Print_Header_Details", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@intWorkOrderID", intWorkOrderID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Actions) //
                cmd = null;
                cmd = new SqlCommand("sp01433_AT_WorkOrder_Print_1_To_Many_Actions", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@intWorkOrderID", intWorkOrderID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                DataRelation relation = new DataRelation("Linked Actions", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Actions"].Columns["WorkOrderID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Maps) //
                cmd = null;
                cmd = new SqlCommand("sp01431_AT_WorkOrder_Print_1_To_Many_Maps", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@strPath", strReportPath));
                if (boolLoadMaps) // Ok to load maps so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                }
                else // No maps so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
                relation = new DataRelation("Linked Maps", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Maps"].Columns["MapWorkOrderID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Third Relation (Linked Documents [pictures]) //
                cmd = null;
                cmd = new SqlCommand("sp01446_AT_WorkOrder_Linked_Pictures", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@RecordID", intWorkOrderID));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@RecordID", -999));
                }
                int intRecordType = 7;  // 7 = Work Orders // 
                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                relation = new DataRelation("Linked Picture", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                
                //this.DataSource = dsDataSet;
                //this.DataAdapter = sdaDataAdpter;
                //conn.Close();

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Work Order";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_AT_Report_Layout_WorkOrder_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            string strPath = "";
            if (boolLoadLinkedPictures)  // Ok to load pictures so get path //
            {
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nLinked Pictures may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            strMainTableName = "Work Order Header";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp01432_AT_WorkOrders_Print_Header_Details", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@intWorkOrderID", intWorkOrderID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Actions) //
            cmd = null;
            cmd = new SqlCommand("sp01433_AT_WorkOrder_Print_1_To_Many_Actions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@intWorkOrderID", intWorkOrderID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
            DataRelation relation = new DataRelation("Linked Actions", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Actions"].Columns["WorkOrderID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Maps) //
            cmd = null;
            cmd = new SqlCommand("sp01431_AT_WorkOrder_Print_1_To_Many_Maps", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@strPath", strReportPath));
            if (boolLoadMaps) // Ok to load maps so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            }
            else // No maps so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
            relation = new DataRelation("Linked Maps", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Maps"].Columns["MapWorkOrderID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Third Relation (Linked Documents [pictures]) //
            cmd = null;
            cmd = new SqlCommand("sp01446_AT_WorkOrder_Linked_Pictures", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@RecordID", intWorkOrderID));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@RecordID", -999));
            }
            int intRecordType = 7;  // 7 = Work Orders // 
            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
            relation = new DataRelation("Linked Picture", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            //this.DataSource = dsDataSet;
            //this.DataAdapter = sdaDataAdpter;
            //conn.Close(); 

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        /*private void OnPreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting.
            this.Detail.SortFields.Clear();
            if (currentSortCell != null)
            {
                currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
            }

            // Create a new field to sort.
            GroupField grField = new GroupField();
            grField.FieldName = ((XRControl)sender).Tag.ToString();
            if (currentSortCell != null)
            {
                if (currentSortCell.Text == ((XRLabel)sender).Text)
                {
                    grField.SortOrder = XRColumnSortOrder.Descending;
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                }
            }
            else
            {
                grField.SortOrder = XRColumnSortOrder.Ascending;
            }

            // Add sorting.
            this.Detail.SortFields.Add(grField);
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
            currentSortCell = (XRTableCell)sender;

            // Recreate the report document.
            this.CreateDocument();
        }
        */


        public class MyBS : BindingSource, IDisplayNameProvider  // For Tree Reports: Used to Hide Table_Inspections and Table_Actions DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Actions") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Maps") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

 
    }
}

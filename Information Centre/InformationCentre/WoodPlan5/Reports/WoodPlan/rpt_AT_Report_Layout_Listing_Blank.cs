using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_AT_Report_Layout_Listing_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strRecordIDs = "";
        private int intReportType = 0;
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("Report Data Source");

        private int intSecondParameter = 0;
        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedData = false;
        private bool boolLoadLinkedPictures = false;

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_AT_Report_Layout_Listing_Blank()
        {
            InitializeComponent();
        }

        public rpt_AT_Report_Layout_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, int ReportType, bool LoadLinkedData, bool LoadLinkedPictures)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            intReportType = ReportType;
            boolLoadLinkedData = LoadLinkedData;
            boolLoadLinkedPictures = LoadLinkedPictures;
            //dsDataSet.Tables.Add("Table");
        }

        public rpt_AT_Report_Layout_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, int ReportType, bool LoadLinkedData, bool LoadLinkedPictures, int SecondParameter)
        {
            // Used when called by MailMerge - Incidents //
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            intReportType = ReportType;
            boolLoadLinkedData = LoadLinkedData;
            boolLoadLinkedPictures = LoadLinkedPictures;
            intSecondParameter = SecondParameter;
            //dsDataSet.Tables.Add("Table");
        }

        private void rpt_AT_Report_Layout_Listing_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                string strPath = "";
                if (boolLoadLinkedPictures)  // Ok to load pictures so get path //
                {
                    try
                    {
                        DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
                    }
                    catch (Exception)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nLinked Pictures may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                switch (intReportType)
                {
                    case 2:
                        {
                            strMainTableName = "Table";
                            cmd = new SqlCommand("sp01221_AT_Reports_Listings_Trees", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                            // Add First Relation (Inspections) //
                            cmd = null;
                            cmd = new SqlCommand("sp01428_AT_Reports_1_To_Many_Inspections", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                            }
                            else // No linked data so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strTreeIDs", ""));
                            }
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Inspections");
                            DataRelation relation = new DataRelation("Linked Inspection", dsDataSet.Tables["Table"].Columns["TreeID"], dsDataSet.Tables["Table_Inspections"].Columns["TreeID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                            // Add Second Relation (Actions) //
                            cmd = null;
                            cmd = new SqlCommand("sp01429_AT_Reports_1_To_Many_Actions_Tree", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                            }
                            else // No linked data so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strTreeIDs", ""));
                            }
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                            relation = new DataRelation("Linked Action", dsDataSet.Tables["Table_Inspections"].Columns["InspectionID"], dsDataSet.Tables["Table_Actions"].Columns["InspectionID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                            // Add Third Relation (Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            int intRecordType = 101;  // 101 = Trees // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                            relation = new DataRelation("Linked Tree Picture", dsDataSet.Tables["Table"].Columns["TreeID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                            // Add Fourth Relation (Inspection Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01447_AT_Reports_1_To_Many_Linked_Pictures_Inspections_To_Trees", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            intRecordType = 102;  // 102 = Inspections // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Inspections");
                            relation = new DataRelation("Linked Inspection Picture", dsDataSet.Tables["Table_Inspections"].Columns["InspectionID"], dsDataSet.Tables["Table_Pictures_Inspections"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                            // Add Fifth Relation (Action Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01448_AT_Reports_1_To_Many_Linked_Pictures_Actions_To_Trees", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            intRecordType = 103;  // 103 = Actions // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Actions");
                            relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures_Actions"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                            break;
                        }
                    case 3:
                        {
                            strMainTableName = "Table";
                            cmd = new SqlCommand("sp01231_AT_Reports_Listings_Inspections", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", strRecordIDs));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                            // Add First Relation (Actions) //
                            cmd = null;
                            cmd = new SqlCommand("sp01430_AT_Reports_1_To_Many_Actions_Inspection", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", strRecordIDs));
                            }
                            else // No linked data so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", ""));
                            }
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                            DataRelation relation = new DataRelation("Linked Action", dsDataSet.Tables["Table"].Columns["InspectionID"], dsDataSet.Tables["Table_Actions"].Columns["InspectionID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                            // Add Second Relation (Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            int intRecordType = 102;  // 102 = Inspections // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                            relation = new DataRelation("Linked Inspection Picture", dsDataSet.Tables["Table"].Columns["InspectionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                            // Add Third Relation (Action Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01449_AT_Reports_1_To_Many_Linked_Pictures_Actions_To_Inspection", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            intRecordType = 103;  // 103 = Actions // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Actions");
                            relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures_Actions"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                            break;
                        }
                    case 4:
                        {
                            strMainTableName = "Table";
                            cmd = new SqlCommand("sp01233_AT_Reports_Listings_Actions", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@strActionIDs", strRecordIDs));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                            // Add First Relation (Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            int intRecordType = 103;  // 103 = Actions // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                            DataRelation relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                            break;
                        }
                    case 8:  // Mail Merge - Incidents //
                        {
                            strMainTableName = "Table";
                            cmd = new SqlCommand("sp01450_Mail_Merge_Incidents_Data", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", strRecordIDs));
                            cmd.Parameters.Add(new SqlParameter("@intCompanyHeaderID", intSecondParameter));

                            
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, strMainTableName);
                            /*
                            // Add First Relation (Linked Documents [pictures]) //
                            cmd = null;
                            cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                            }
                            else // No pictures so pass dummy so nothing is returned //
                            {
                                cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                            }
                            int intRecordType = 5;  // 5 = Actions // 
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                            sdaDataAdapter = new SqlDataAdapter(cmd);
                            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                            DataRelation relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //    
                            */
                            break;
                        }

                    default:
                        break;
                }

                //this.DataSource = dsDataSet.Tables[strMainTableName];
                //this.DataAdapter = sdaDataAdapter;
                //conn.Close();

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Tree Listing Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_AT_Report_Layout_Listing_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            string strPath = "";
            if (boolLoadLinkedPictures)  // Ok to load pictures so get path //
            {
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nLinked Pictures may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            switch (intReportType)
            {
                case 2:
                    {
                        strMainTableName = "Table";
                        cmd = new SqlCommand("sp01221_AT_Reports_Listings_Trees", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                        // Add First Relation (Inspections) //
                        cmd = null;
                        cmd = new SqlCommand("sp01428_AT_Reports_1_To_Many_Inspections", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                        }
                        else // No linked data so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strTreeIDs", ""));
                        }
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Inspections");
                        DataRelation relation = new DataRelation("Linked Inspection", dsDataSet.Tables["Table"].Columns["TreeID"], dsDataSet.Tables["Table_Inspections"].Columns["TreeID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                        // Add Second Relation (Actions) //
                        cmd = null;
                        cmd = new SqlCommand("sp01429_AT_Reports_1_To_Many_Actions_Tree", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strTreeIDs", strRecordIDs));
                        }
                        else // No linked data so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strTreeIDs", ""));
                        }
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                        relation = new DataRelation("Linked Action", dsDataSet.Tables["Table_Inspections"].Columns["InspectionID"], dsDataSet.Tables["Table_Actions"].Columns["InspectionID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                        // Add Third Relation (Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        int intRecordType = 101;  // 101 = Trees // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                        relation = new DataRelation("Linked Tree Picture", dsDataSet.Tables["Table"].Columns["TreeID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                        // Add Fourth Relation (Inspection Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01447_AT_Reports_1_To_Many_Linked_Pictures_Inspections_To_Trees", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        intRecordType = 102;  // 102 = Inspections // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Inspections");
                        relation = new DataRelation("Linked Inspection Picture", dsDataSet.Tables["Table_Inspections"].Columns["InspectionID"], dsDataSet.Tables["Table_Pictures_Inspections"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                        // Add Fifth Relation (Action Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01448_AT_Reports_1_To_Many_Linked_Pictures_Actions_To_Trees", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        intRecordType = 103;  // 103 = Actions // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Actions");
                        relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures_Actions"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                        break;
                    }
                case 3:
                    {
                        strMainTableName = "Table";
                        cmd = new SqlCommand("sp01231_AT_Reports_Listings_Inspections", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", strRecordIDs));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                        // Add First Relation (Actions) //
                        cmd = null;
                        cmd = new SqlCommand("sp01430_AT_Reports_1_To_Many_Actions_Inspection", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", strRecordIDs));
                        }
                        else // No linked data so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@strInspectionIDs", ""));
                        }
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                        DataRelation relation = new DataRelation("Linked Action", dsDataSet.Tables["Table"].Columns["InspectionID"], dsDataSet.Tables["Table_Actions"].Columns["InspectionID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //
                        
                        // Add Second Relation (Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        int intRecordType = 102;  // 102 = Inspections // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                        relation = new DataRelation("Linked Inspection Picture", dsDataSet.Tables["Table"].Columns["InspectionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                        // Add Third Relation (Action Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01449_AT_Reports_1_To_Many_Linked_Pictures_Actions_To_Inspection", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures && boolLoadLinkedData)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        intRecordType = 103;  // 103 = Actions // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Actions");
                        relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures_Actions"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                        break;
                    }
                case 4:
                    {
                        strMainTableName = "Table";
                        cmd = new SqlCommand("sp01233_AT_Reports_Listings_Actions", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@strActionIDs", strRecordIDs));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, strMainTableName);
                        
                        // Add First Relation (Linked Documents [pictures]) //
                        cmd = null;
                        cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                        }
                        else // No pictures so pass dummy so nothing is returned //
                        {
                            cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                        }
                        int intRecordType = 103;  // 103 = Actions // 
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                        cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                        sdaDataAdapter = new SqlDataAdapter(cmd);
                        sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                        DataRelation relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                        dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                        break;
                    }
                case 8: // Mail Merge - Incidents //
                    strMainTableName = "Table";
                    cmd = new SqlCommand("sp01450_Mail_Merge_Incidents_Data", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@strRecordIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@intCompanyHeaderID", intSecondParameter));
                    sdaDataAdapter = new SqlDataAdapter(cmd);
                    sdaDataAdapter.Fill(dsDataSet, strMainTableName);
                    /*
                    // Add First Relation (Linked Documents [pictures]) //
                    cmd = null;
                    cmd = new SqlCommand("sp01445_AT_Reports_1_To_Many_Linked_Pictures", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    if (boolLoadLinkedPictures)  // Ok to load pictures so pass correct value //
                    {
                        cmd.Parameters.Add(new SqlParameter("@RecordIDs", strRecordIDs));
                    }
                    else // No pictures so pass dummy so nothing is returned //
                    {
                        cmd.Parameters.Add(new SqlParameter("@RecordIDs", ""));
                    }
                    int intRecordType = 5;  // 5 = Actions // 
                    cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordType));
                    cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                    sdaDataAdapter = new SqlDataAdapter(cmd);
                    sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                    DataRelation relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table"].Columns["ActionID"], dsDataSet.Tables["Table_Pictures"].Columns["PictureLinkedToRecordID"]);
                    dsDataSet.Relations.Add(relation);  // Assign relation to dataset //
                    */
                    break;


                default:
                    break;
            }
            /*
            this.DataSource = dsDataSet.Tables[strMainTableName];
            this.DataAdapter = sdaDataAdapter;
            conn.Close();
            */
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();

        }

        public class MyBS : BindingSource, IDisplayNameProvider  // For Tree Reports: Used to Hide Table_Inspections and Table_Actions DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Inspections") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Actions") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures_Inspections") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures_Actions"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }

    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_GC_Report_Team_Listing_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strRecordIDs = "";
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("Report Data Source");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedGrittingCallouts = false;
        private bool boolLoadLinkedGrittingCalloutExtraCosts = false;
        private bool boolLoadLinkedGrittingCalloutImages = false;
        private bool boolLoadLinkedSnowClearanceCallouts = false;
        private bool boolLoadLinkedSnowClearanceCalloutExtraCosts = false;
        private bool boolLoadLinkedSnowClearanceCalloutRates = false;
        private DateTime? dtFromDateTime = null;
        private DateTime? dtToDateTime = null;
        private string strCalloutStatuses = null;

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Report_Team_Listing_Blank()
        {
            InitializeComponent();
        }

        public rpt_GC_Report_Team_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, bool LoadLinkedGrittingCallouts, bool LoadLinkedGrittingCalloutExtraCosts, bool LoadLinkedGrittingCalloutImages, bool LoadLinkedSnowClearanceCallouts, bool LoadLinkedSnowClearanceCalloutExtraCosts, bool LoadLinkedSnowClearanceCalloutRates, DateTime? FromDateTime, DateTime? ToDateTime, string CalloutStatuses)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            boolLoadLinkedGrittingCallouts = LoadLinkedGrittingCallouts;
            boolLoadLinkedGrittingCalloutExtraCosts = LoadLinkedGrittingCalloutExtraCosts;
            boolLoadLinkedGrittingCalloutImages = LoadLinkedGrittingCalloutImages;
            boolLoadLinkedSnowClearanceCallouts = LoadLinkedSnowClearanceCallouts;
            boolLoadLinkedSnowClearanceCalloutExtraCosts = LoadLinkedSnowClearanceCalloutExtraCosts;
            boolLoadLinkedSnowClearanceCalloutRates = LoadLinkedSnowClearanceCalloutRates;
            dtFromDateTime = FromDateTime;
            dtToDateTime = ToDateTime;
            strCalloutStatuses = CalloutStatuses;
        }

        private void rpt_GC_Report_Team_Listing_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                string strPath = "";
                if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so get path //
                {
                    try
                    {
                        DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolder").ToString();
                    }
                    catch (Exception)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Images path (from the System Configuration Screen).\n\nLinked Images may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                strMainTableName = "Table";
                cmd = new SqlCommand("sp04309_Reports_Teams_List_For_Report", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@strSubContractorIDs", strRecordIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Gritting Callouts) //
                cmd = null;
                cmd = new SqlCommand("sp04310_GC_Reports_Gritting_Callouts_Linked_To_Teams_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCallouts)  // Ok to load linked gritting callouts so pass correct values //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Callouts");
                DataRelation relation = new DataRelation("Linked Gritting Callouts", dsDataSet.Tables["Table"].Columns["SubContractorID"], dsDataSet.Tables["Table_Gritting_Callouts"].Columns["SubContractorID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Gritting Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp04311_GC_Reports_Extra_Costs_Linked_To_Team_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Gritting_Callouts"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Third Relation (Linked Gritting Images) //
                cmd = null;
                cmd = new SqlCommand("sp04312_GC_Reports_Images_Linked_To_Team_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Images");
                relation = new DataRelation("Linked Gritting Images", dsDataSet.Tables["Table_Gritting_Callouts"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Images"].Columns["GrittingCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Fourth Relation (Linked Snow Clearance Callouts) //
                cmd = null;
                cmd = new SqlCommand("sp04313_GC_Reports_Snow_Callouts_Linked_To_Teams_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCallouts)  // Ok to load linked gritting callouts so pass correct values //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Clearance_Callouts");
                relation = new DataRelation("Linked Snow Clearance Callouts", dsDataSet.Tables["Table"].Columns["SubContractorID"], dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SubContractorID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Fifth Relation (Linked Snow Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp04314_GC_Reports_Extra_Costs_Linked_To_Team_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Extra_Costs");
                relation = new DataRelation("Linked Snow Extra Costs", dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Snow_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                
                // Add Sixth Relation (Linked Rates) //
                cmd = null;
                cmd = new SqlCommand("sp04315_GC_Reports_Rates_Linked_To_Team_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No rates so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Linked_Snow_Rates");
                relation = new DataRelation("Linked Snow Rates", dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Linked_Snow_Rates"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                
                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Site Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Report_Team_Listing_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            string strPath = "";
            if (boolLoadLinkedGrittingCalloutImages || boolLoadLinkedSnowClearanceCalloutRates)  // Ok to load pictures so get path //
            {
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolder").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Images path (from the System Configuration Screen).\n\nLinked Images may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            strMainTableName = "Table";
            cmd = new SqlCommand("sp04309_Reports_Teams_List_For_Report", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@strSubContractorIDs", strRecordIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Gritting Callouts) //
            cmd = null;
            cmd = new SqlCommand("sp04310_GC_Reports_Gritting_Callouts_Linked_To_Teams_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCallouts)  // Ok to load linked gritting callouts so pass correct values //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Callouts");
            DataRelation relation = new DataRelation("Linked Gritting Callouts", dsDataSet.Tables["Table"].Columns["SubContractorID"], dsDataSet.Tables["Table_Gritting_Callouts"].Columns["SubContractorID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Gritting Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp04311_GC_Reports_Extra_Costs_Linked_To_Team_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Gritting_Callouts"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Third Relation (Linked Gritting Images) //
            cmd = null;
            cmd = new SqlCommand("sp04312_GC_Reports_Images_Linked_To_Team_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Images");
            relation = new DataRelation("Linked Gritting Images", dsDataSet.Tables["Table_Gritting_Callouts"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Images"].Columns["GrittingCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Fourth Relation (Linked Snow Clearance Callouts) //
            cmd = null;
            cmd = new SqlCommand("sp04313_GC_Reports_Snow_Callouts_Linked_To_Teams_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCallouts)  // Ok to load linked gritting callouts so pass correct values //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Clearance_Callouts");
            relation = new DataRelation("Linked Snow Clearance Callouts", dsDataSet.Tables["Table"].Columns["SubContractorID"], dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SubContractorID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Fifth Relation (Linked Snow Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp04314_GC_Reports_Extra_Costs_Linked_To_Team_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Extra_Costs");
            relation = new DataRelation("Linked Snow Extra Costs", dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Snow_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Sixth Relation (Linked Rates) //
            cmd = null;
            cmd = new SqlCommand("sp04315_GC_Reports_Rates_Linked_To_Team_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No rates so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SubContractorIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Linked_Snow_Rates");
            relation = new DataRelation("Linked Snow Rates", dsDataSet.Tables["Table_Snow_Clearance_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Linked_Snow_Rates"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();

        }

        public class MyBS : BindingSource, IDisplayNameProvider  // For Site Reports: Used to Hide Table_Gritting_Callouts and Table_Extra_Costs DataTables etc from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Gritting_Callouts") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Gritting_Images") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Snow_Clearance_Callouts") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Snow_Extra_Costs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Linked_Snow_Rates"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }

    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_GC_Report_Snow_Callout_Listing_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strRecordIDs = "";
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("Report Data Source");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedSnowCalloutExtraCosts = false;
        private bool boolLoadLinkedSnowCalloutRates = false;
        private DateTime? dtFromDateTime = null;
        private DateTime? dtToDateTime = null;
        private string strCalloutStatuses = null;
        private string strCompanies = null;
 
        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Report_Snow_Callout_Listing_Blank()
        {
            InitializeComponent();
        }

        public rpt_GC_Report_Snow_Callout_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, bool LoadLinkedSnowCalloutExtraCosts, bool LoadLinkedSnowCalloutRates, DateTime? FromDateTime, DateTime? ToDateTime, string CalloutStatuses, string Companies)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            boolLoadLinkedSnowCalloutExtraCosts = LoadLinkedSnowCalloutExtraCosts;
            boolLoadLinkedSnowCalloutRates = LoadLinkedSnowCalloutRates;
            dtFromDateTime = FromDateTime;
            dtToDateTime = ToDateTime;
            strCalloutStatuses = CalloutStatuses;
            strCompanies = Companies;
        }

        private void rpt_GC_Report_Snow_Callout_Listing_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {               
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                strMainTableName = "Table";
                cmd = new SqlCommand("sp04321_GC_Reports_Snow_Callouts_List_For_Report", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Gritting Callouts) //
                cmd = null;
                cmd = new SqlCommand("sp04322_GC_Reports_Extra_Costs_Linked_To_Snow_Callout_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSnowCalloutExtraCosts)  // Ok to load linked gritting callouts so pass correct values //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Rates) //
                cmd = null;
                cmd = new SqlCommand("sp04323_GC_Reports_Rates_Linked_To_Snow_Callout_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSnowCalloutRates)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
                relation = new DataRelation("Linked Rates", dsDataSet.Tables["Table"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

 
                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Gritting Callout Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Report_Snow_Callout_Listing_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            strMainTableName = "Table";
            cmd = new SqlCommand("sp04321_GC_Reports_Snow_Callouts_List_For_Report", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Gritting Callouts) //
            cmd = null;
            cmd = new SqlCommand("sp04322_GC_Reports_Extra_Costs_Linked_To_Snow_Callout_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSnowCalloutExtraCosts)  // Ok to load linked gritting callouts so pass correct values //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Rates) //
            cmd = null;
            cmd = new SqlCommand("sp04323_GC_Reports_Rates_Linked_To_Snow_Callout_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSnowCalloutRates)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", strRecordIDs));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceCallOutIDs", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
            relation = new DataRelation("Linked Rates", dsDataSet.Tables["Table"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
 
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();

        }

        public class MyBS : BindingSource, IDisplayNameProvider  // For Site Reports: Used to Hide Table_Gritting_Callouts and Table_Extra_Costs DataTables etc from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Rates"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

    }
}

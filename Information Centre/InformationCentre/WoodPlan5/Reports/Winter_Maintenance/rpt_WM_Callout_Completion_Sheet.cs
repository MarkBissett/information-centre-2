﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_WM_Callout_Completion_Sheet : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strGrittingCallOutIDs = "";
        private string strMapImagePath = "";
        private string strSignaturePath = "";
        private string strPicturePath = "";
        private int intShowMaps = 0;
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_WM_Callout_Completion_Sheet()
        {
            InitializeComponent();
        }

        public rpt_WM_Callout_Completion_Sheet(BaseObjects.GlobalSettings GlobalSettings, string GrittingCallOutIDs, string MapImagePath, string SignaturePath, string PicturePath, int ShowMaps)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strGrittingCallOutIDs = GrittingCallOutIDs;
            strMapImagePath = MapImagePath;
            strSignaturePath = SignaturePath;
            strPicturePath = PicturePath;
            intShowMaps = ShowMaps;
        }


        private void rpt_WM_Callout_Completion_Sheet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Callout";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04375_GC_Gritting_Completion_Sheet_Callout_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CalloutIDs", strGrittingCallOutIDs));
                cmd.Parameters.Add(new SqlParameter("@SignaturePath", strSignaturePath));
                cmd.Parameters.Add(new SqlParameter("@MapImagePath", strMapImagePath));
                cmd.Parameters.Add(new SqlParameter("@ShowMaps", intShowMaps));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp04376_GC_Gritting_Completion_Sheet_Picture_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strGrittingCallOutIDs));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Callout"].Columns["GrittingCallOutID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures"].Columns["GrittingCallOutID"] };
                    DataRelation relation = new DataRelation("Linked Pictures", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Gritting Callout Completion Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_WM_Callout_Completion_Sheet_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Callout";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04375_GC_Gritting_Completion_Sheet_Callout_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CalloutIDs", strGrittingCallOutIDs));
                cmd.Parameters.Add(new SqlParameter("@SignaturePath", strSignaturePath));
                cmd.Parameters.Add(new SqlParameter("@MapImagePath", strMapImagePath));
                cmd.Parameters.Add(new SqlParameter("@ShowMaps", intShowMaps));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp04376_GC_Gritting_Completion_Sheet_Picture_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strGrittingCallOutIDs));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Callout"].Columns["GrittingCallOutID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures"].Columns["GrittingCallOutID"] };
                    DataRelation relation = new DataRelation("Linked Pictures", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();
            }
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Next_Of_Kin DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }


    }
}

﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5
{
    public partial class rpt_GC_Snow_Clearance_Team_Purchase_Order : DevExpress.XtraReports.UI.XtraReport
    {
       #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private int intCalloutID = 0;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Snow_Clearance_Team_Purchase_Order()
        {
            InitializeComponent();
        }

        public rpt_GC_Snow_Clearance_Team_Purchase_Order(BaseObjects.GlobalSettings GlobalSettings, int CalloutID)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            intCalloutID = CalloutID;
        }



        private void rpt_GC_Snow_Clearance_Team_Purchase_Order_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "PO_Header";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04190_GC_Snow_Clearance_Team_PO_Header", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Rates) //
                cmd = null;
                cmd = new SqlCommand("sp04191_GC_Snow_Clearance_Team_PO_Rates", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
                DataRelation relation = null;
                try
                {
                    relation = new DataRelation("Linked Rates", dsDataSet.Tables["PO_Header"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
                    dsDataSet.Relations.Add(relation);  // Assign relation to dataset //
                }
                catch (Exception)
                {
                }

                // Add Second Relation (Linked Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp04192_GC_Snow_Clearance_Team_PO_Extra_Costs", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                try
                {
                    relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["PO_Header"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
                    dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                }
                catch (Exception)
                {
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Team Purchase Order";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Snow_Clearance_Team_Purchase_Order_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            strMainTableName = "PO_Header";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp04190_GC_Snow_Clearance_Team_PO_Header", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Rates) //
            cmd = null;
            cmd = new SqlCommand("sp04191_GC_Snow_Clearance_Team_PO_Rates", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
            DataRelation relation = null;
            try
            {
                relation = new DataRelation("Linked Rates", dsDataSet.Tables["PO_Header"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //
            }
            catch (Exception)
            {
            }

            // Add Second Relation (Linked Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp04192_GC_Snow_Clearance_Team_PO_Extra_Costs", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@CalloutID", intCalloutID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            try
            {
                relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["PO_Header"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
            }
            catch (Exception)
            {
            }

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Rates and Table_Extra_Costs DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Rates") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }




    }
}

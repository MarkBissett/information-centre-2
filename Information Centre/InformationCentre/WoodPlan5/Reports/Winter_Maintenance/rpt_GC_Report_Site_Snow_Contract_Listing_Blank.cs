using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_GC_Report_Site_Snow_Contract_Listing_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strRecordIDs = "";
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("Report Data Source");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedSnowCallouts = false;
        private bool boolLoadLinkedSnowCalloutExtraCosts = false;
        private bool boolLoadLinkedSnowCalloutRates = false;
        private DateTime? dtFromDateTime = null;
        private DateTime? dtToDateTime = null;
        private string strCalloutStatuses = null;

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Report_Site_Snow_Contract_Listing_Blank()
        {
            InitializeComponent();
        }

        public rpt_GC_Report_Site_Snow_Contract_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, bool LoadLinkedSnowCallouts, bool LoadLinkedSnowCalloutExtraCosts, bool LoadLinkedSnowCalloutRates, DateTime? FromDateTime, DateTime? ToDateTime, string CalloutStatuses)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            boolLoadLinkedSnowCallouts = LoadLinkedSnowCallouts;
            boolLoadLinkedSnowCalloutExtraCosts = LoadLinkedSnowCalloutExtraCosts;
            boolLoadLinkedSnowCalloutRates = LoadLinkedSnowCalloutRates;
            dtFromDateTime = FromDateTime;
            dtToDateTime = ToDateTime;
            strCalloutStatuses = CalloutStatuses;
        }

        private void rpt_GC_Report_Site_Snow_Contract_Listing_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {               
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                strMainTableName = "Table";
                cmd = new SqlCommand("sp04304_GC_Reports_Snow_Clearance_Contract_List_For_Report2", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Snow Clearance Callouts) //
                cmd = null;
                cmd = new SqlCommand("sp04305_GC_Reports_Snow_Callouts_Linked_To_Snow_Contract_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSnowCallouts)  // Ok to load linked gritting callouts so pass correct values //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Callouts");
                DataRelation relation = new DataRelation("Linked Snow Callouts", dsDataSet.Tables["Table"].Columns["SnowClearanceSiteContractID"], dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceSiteContractID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Snow Clearance Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp04306_GC_Reports_Extra_Costs_Linked_To_Snow_Contract_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSnowCalloutExtraCosts)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Third Relation (Linked Snow Clearance Rates) //
                cmd = null;
                cmd = new SqlCommand("sp04307_GC_Reports_Rates_Linked_To_Snow_Contract_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSnowCalloutRates)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                    cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
                relation = new DataRelation("Linked Rates", dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

 
                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Site Snow Clearance Contract Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Report_Site_Snow_Contract_Listing_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            strMainTableName = "Table";
            cmd = new SqlCommand("sp04304_GC_Reports_Snow_Clearance_Contract_List_For_Report2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Snow Clearance Callouts) //
            cmd = null;
            cmd = new SqlCommand("sp04305_GC_Reports_Snow_Callouts_Linked_To_Snow_Contract_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSnowCallouts)  // Ok to load linked gritting callouts so pass correct values //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Snow_Callouts");
            DataRelation relation = new DataRelation("Linked Snow Callouts", dsDataSet.Tables["Table"].Columns["SnowClearanceSiteContractID"], dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceSiteContractID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Snow Clearance Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp04306_GC_Reports_Extra_Costs_Linked_To_Snow_Contract_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSnowCalloutExtraCosts)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Third Relation (Linked Snow Clearance Rates) //
            cmd = null;
            cmd = new SqlCommand("sp04307_GC_Reports_Rates_Linked_To_Snow_Contract_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSnowCalloutRates)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtFromDateTime));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtToDateTime));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", strCalloutStatuses));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@SnowClearanceSiteContractIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@FromDate", ""));
                cmd.Parameters.Add(new SqlParameter("@ToDate", ""));
                cmd.Parameters.Add(new SqlParameter("@GrittingCalloutStatuses", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Rates");
            relation = new DataRelation("Linked Rates", dsDataSet.Tables["Table_Snow_Callouts"].Columns["SnowClearanceCallOutID"], dsDataSet.Tables["Table_Rates"].Columns["SnowClearanceCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
 
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();

        }

        public class MyBS : BindingSource, IDisplayNameProvider  // For Site Reports: Used to Hide Table_Gritting_Callouts and Table_Extra_Costs DataTables etc from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Snow_Callouts") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Rates"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

    }
}

﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_GC_Client_Job_Breakdown : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strSiteIDs = "";
        private DateTime? dtStartDate;
        private DateTime? dtEndDate;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Client_Job_Breakdown()
        {
            InitializeComponent();
        }

        public rpt_GC_Client_Job_Breakdown(BaseObjects.GlobalSettings GlobalSettings, string SiteIDs, DateTime? StartDate, DateTime? EndDate)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strSiteIDs = SiteIDs;
            dtStartDate = StartDate;
            dtEndDate = EndDate;
        }


        private void rpt_GC_Client_Job_Breakdown_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Callouts";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp04239_GC_Report_Client_Job_Breakdown", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SiteIDs", strSiteIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Extra Costs) //
                cmd = null;
                cmd = new SqlCommand("sp04240_GC_Report_Client_Job_Breakdown_Extra_Costs", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SiteIDs", strSiteIDs));
                cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
                cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Callouts"].Columns["RecordID"], dsDataSet.Tables["Table_Callouts"].Columns["JobDescription"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCalloutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["JobDescription"] };
                    DataRelation relation = new DataRelation("Linked Extra Costs", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);

                    //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Callouts"].Columns["RecordID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCalloutID"]);
                    //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                }
                catch (Exception)
                {
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Client Job Breakdown";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Client_Job_Breakdown_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            strMainTableName = "Table_Callouts";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp04239_GC_Report_Client_Job_Breakdown", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SiteIDs", strSiteIDs));
            cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Extra Costs) //
            cmd = null;
            cmd = new SqlCommand("sp04240_GC_Report_Client_Job_Breakdown_Extra_Costs", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SiteIDs", strSiteIDs));
            cmd.Parameters.Add(new SqlParameter("@FromDate", dtStartDate));
            cmd.Parameters.Add(new SqlParameter("@ToDate", dtEndDate));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Callouts"].Columns["RecordID"], dsDataSet.Tables["Table_Callouts"].Columns["JobDescription"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCalloutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["JobDescription"] };
                DataRelation relation = new DataRelation("Linked Extra Costs", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);

                //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Callouts"].Columns["RecordID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCalloutID"]);
                //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
            }
            catch (Exception)
            {
            }
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Extra_Costs DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }

    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_GC_Report_Gritting_Callout_Listing_Blank : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strRecordIDs = "";
        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("Report Data Source");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        private bool boolLoadLinkedGrittingCalloutExtraCosts = false;
        private bool boolLoadLinkedGrittingCalloutImages = false;
        private bool boolLoadLinkedSiteServiceAreas = false;
        private bool boolLoadAllServiceAreas = false;
        private DateTime? dtFromDateTime = null;
        private DateTime? dtToDateTime = null;
        private string strCalloutStatuses = null;
        private string strCompanies = null;
 
        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_GC_Report_Gritting_Callout_Listing_Blank()
        {
            InitializeComponent();
        }

        public rpt_GC_Report_Gritting_Callout_Listing_Blank(BaseObjects.GlobalSettings GlobalSettings, string RecordIDs, bool LoadLinkedGrittingCalloutExtraCosts, bool LoadLinkedGrittingCalloutImages, bool LoadLinkedSiteServiceAreas, bool LoadAllServiceAreas, DateTime? FromDateTime, DateTime? ToDateTime, string CalloutStatuses, string Companies)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strRecordIDs = RecordIDs;
            boolLoadLinkedGrittingCalloutExtraCosts = LoadLinkedGrittingCalloutExtraCosts;
            boolLoadLinkedGrittingCalloutImages = LoadLinkedGrittingCalloutImages;
            boolLoadLinkedSiteServiceAreas = LoadLinkedSiteServiceAreas;
            boolLoadAllServiceAreas = LoadAllServiceAreas;
            dtFromDateTime = FromDateTime;
            dtToDateTime = ToDateTime;
            strCalloutStatuses = CalloutStatuses;
            strCompanies = Companies;
        }

        private void rpt_GC_Report_Gritting_Callout_Listing_Blank_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                string strPath = "";
                if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so get path //
                {
                    try
                    {
                        DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolder").ToString();
                    }
                    catch (Exception)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Images path (from the System Configuration Screen).\n\nLinked Images may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                strMainTableName = "Table";
                cmd = new SqlCommand("sp04317_GC_Reports_Gritting_Callouts_List_For_Report", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Gritting Callouts) //
                cmd = null;
                cmd = new SqlCommand("sp04318_GC_Reports_Extra_Costs_Linked_To_Gritting_Callouts_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load linked gritting callouts so pass correct values //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
                }
                else // No linked data so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
                DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Images) //
                cmd = null;
                cmd = new SqlCommand("sp04319_GC_Reports_Images_Linked_To_Gritting_Callouts_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
                }
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Images");
                relation = new DataRelation("Linked Gritting Images", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Images"].Columns["GrittingCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Third Relation (Linked Site Service Areas) //
                cmd = null;
                cmd = new SqlCommand("sp04367_GC_Reports_Site_Service_Areas_Linked_To_Gritting_Callouts_List", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadLinkedSiteServiceAreas)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
                    cmd.Parameters.Add(new SqlParameter("@LoadAllSiteServiceAreas", (boolLoadAllServiceAreas ? 1 : 0)));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
                    cmd.Parameters.Add(new SqlParameter("@LoadAllSiteServiceAreas", 0));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Site_Service_Areas");
                relation = new DataRelation("Linked Site Service Areas", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Site_Service_Areas"].Columns["GrittingCallOutID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Gritting Callout Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_GC_Report_Gritting_Callout_Listing_Blank_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            string strPath = "";
            if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so get path //
            {
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strPath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingCalloutPictureFilesFolder").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Images path (from the System Configuration Screen).\n\nLinked Images may not display correctly. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            strMainTableName = "Table";
            cmd = new SqlCommand("sp04317_GC_Reports_Gritting_Callouts_List_For_Report", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Gritting Callouts) //
            cmd = null;
            cmd = new SqlCommand("sp04318_GC_Reports_Extra_Costs_Linked_To_Gritting_Callouts_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutExtraCosts)  // Ok to load linked gritting callouts so pass correct values //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
            }
            else // No linked data so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Extra_Costs");
            DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Extra_Costs"].Columns["GrittingCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Images) //
            cmd = null;
            cmd = new SqlCommand("sp04319_GC_Reports_Images_Linked_To_Gritting_Callouts_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedGrittingCalloutImages)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
            }
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Images");
            relation = new DataRelation("Linked Gritting Images", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Images"].Columns["GrittingCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Third Relation (Linked Site Service Areas) //
            cmd = null;
            cmd = new SqlCommand("sp04367_GC_Reports_Site_Service_Areas_Linked_To_Gritting_Callouts_List", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadLinkedSiteServiceAreas)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", strRecordIDs));
                cmd.Parameters.Add(new SqlParameter("@LoadAllSiteServiceAreas", (boolLoadAllServiceAreas ? 1 : 0)));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@GrittingCallOutIDs", ""));
                cmd.Parameters.Add(new SqlParameter("@LoadAllSiteServiceAreas", 0));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Gritting_Site_Service_Areas");
            relation = new DataRelation("Linked Site Service Areas", dsDataSet.Tables["Table"].Columns["GrittingCallOutID"], dsDataSet.Tables["Table_Gritting_Site_Service_Areas"].Columns["GrittingCallOutID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
            
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();

        }

        public class MyBS : BindingSource, IDisplayNameProvider  // For Site Reports: Used to Hide Table_Gritting_Callouts and Table_Extra_Costs DataTables etc from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Extra_Costs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Gritting_Images") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Gritting_Site_Service_Areas"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

    }
}

﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_OM_Team_Self_Billing_Invoice : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strSelfBillingInvoiceIDs = "";

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_OM_Team_Self_Billing_Invoice()
        {
            InitializeComponent();
        }

        public rpt_OM_Team_Self_Billing_Invoice(BaseObjects.GlobalSettings GlobalSettings, string SelfBillingInvoiceIDs)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strSelfBillingInvoiceIDs = SelfBillingInvoiceIDs;
         }


        private void rpt_OM_Team_Self_Billing_Invoice_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Invoice_Headers";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp06460_OM_Self_Billing_Report_Header", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceIDs", strSelfBillingInvoiceIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Visits) //
                cmd = null;
                cmd = new SqlCommand("sp06461_OM_Self_Billing_Report_Items", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceIDs", strSelfBillingInvoiceIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Invoice_Visits");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Invoice_Headers"].Columns["SelfBillingInvoiceHeaderID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Invoice_Visits"].Columns["SelfBillingInvoiceHeaderID"] };
                    DataRelation relation = new DataRelation("Linked Visits", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }
               
                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Team Self-Billing Invoice";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_OM_Team_Self_Billing_Invoice_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            strMainTableName = "Table_Invoice_Headers";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp06460_OM_Self_Billing_Report_Header", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceIDs", strSelfBillingInvoiceIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Visits) //
            cmd = null;
            cmd = new SqlCommand("sp06461_OM_Self_Billing_Report_Items", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceIDs", strSelfBillingInvoiceIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Invoice_Visits");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Invoice_Headers"].Columns["SelfBillingInvoiceHeaderID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Invoice_Visits"].Columns["SelfBillingInvoiceHeaderID"] };
                DataRelation relation = new DataRelation("Linked Visits", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }
                
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide child linked tables DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Invoice_Visits"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }


    }
}

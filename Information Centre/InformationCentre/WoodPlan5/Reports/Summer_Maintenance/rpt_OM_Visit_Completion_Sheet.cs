﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_OM_Visit_Completion_Sheet : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strVisitIDs = "";
        private string strSignaturePath = "";
        private string strPicturePath = "";
        private string strHeaderImagePath = "";
        private int intTeamID = 0;
        private int intLabourTypeID = 0;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_OM_Visit_Completion_Sheet()
        {
            InitializeComponent();
        }

        public rpt_OM_Visit_Completion_Sheet(BaseObjects.GlobalSettings GlobalSettings, string VisitIDs, string SignaturePath, string PicturePath, int TeamID, int LabourTypeID)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strVisitIDs = VisitIDs;
            strSignaturePath = SignaturePath;
            strPicturePath = PicturePath;
            strHeaderImagePath = Application.StartupPath;
            intTeamID = TeamID;
            intLabourTypeID = LabourTypeID;
        }


        private void rpt_OM_Visit_Completion_Sheet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Visit";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp06376_OM_Completion_Sheet_Report_Visit_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@SignaturePath", strSignaturePath));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Labour) //
                cmd = null;
                cmd = new SqlCommand("sp06378_OM_Completion_Sheet_Report_Labour_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Labour");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Labour"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Labour", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Add Second Relation (Linked Waste) //
                cmd = null;
                cmd = new SqlCommand("sp06380_OM_Completion_Sheet_Report_Visit_Waste_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Waste");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Waste"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Waste", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                int intPictureType = 1;
                // Add Third Relation (Linked Before Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp06381_OM_Completion_Sheet_Report_Picture_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                cmd.Parameters.Add(new SqlParameter("@PictureType", intPictureType));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Before");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_Before"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Pictures Before", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                intPictureType = 2;
                // Add Forth Relation (Linked After Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp06381_OM_Completion_Sheet_Report_Picture_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                cmd.Parameters.Add(new SqlParameter("@PictureType", intPictureType));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_After");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_After"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Pictures After", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Add Fifth Relation (Permits To Work \ Contractor Attendance Records - [Dummy Jobs with pictures]) //
                cmd = null;
                cmd = new SqlCommand("sp06486_OM_Completion_Sheet_Report_Picture_Info_Authorisations", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Authorisations");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_Authorisations"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Authorisation Pictures", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Add Sixth Relation (Linked Jobs) //
                cmd = null;
                cmd = new SqlCommand("sp06377_OM_Completion_Sheet_Report_Job_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Jobs");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Jobs"].Columns["VisitID"] };
                    DataRelation relation = new DataRelation("Linked Jobs", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }

                // Add Seventh Relation (Linked Job Extra Attributes) //
                cmd = null;
                cmd = new SqlCommand("sp06379_OM_Completion_Sheet_Report_Job_Extra_Info_Info", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
                cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
                cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Job_Attributes");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Jobs"].Columns["JobID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Job_Attributes"].Columns["JobID"] };
                    DataRelation relation = new DataRelation("Linked Job Attributes", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);
                }
                catch (Exception)
                {
                }
                
                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Maintenance Completion Report";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_OM_Visit_Completion_Sheet_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            strMainTableName = "Table_Visit";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp06376_OM_Completion_Sheet_Report_Visit_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@SignaturePath", strSignaturePath));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Labour) //
            cmd = null;
            cmd = new SqlCommand("sp06378_OM_Completion_Sheet_Report_Labour_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Labour");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Labour"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Labour", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            // Add Second Relation (Linked Waste) //
            cmd = null;
            cmd = new SqlCommand("sp06380_OM_Completion_Sheet_Report_Visit_Waste_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Waste");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Waste"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Waste", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            int intPictureType = 1;
            // Add Third Relation (Linked Before Pictures) //
            cmd = null;
            cmd = new SqlCommand("sp06381_OM_Completion_Sheet_Report_Picture_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
            cmd.Parameters.Add(new SqlParameter("@PictureType", intPictureType));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Before");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_Before"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Pictures Before", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            intPictureType = 2;
            // Add Forth Relation (Linked After Pictures) //
            cmd = null;
            cmd = new SqlCommand("sp06381_OM_Completion_Sheet_Report_Picture_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
            cmd.Parameters.Add(new SqlParameter("@PictureType", intPictureType));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_After");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_After"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Pictures After", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            // Add Fifth Relation (Permits To Work \ Contractor Attendance Records - [Dummy Jobs with pictures]) //
            cmd = null;
            cmd = new SqlCommand("sp06486_OM_Completion_Sheet_Report_Picture_Info_Authorisations", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures_Authorisations");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Pictures_Authorisations"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Authorisation Pictures", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            // Add Sixth Relation (Linked Jobs) //
            cmd = null;
            cmd = new SqlCommand("sp06377_OM_Completion_Sheet_Report_Job_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Jobs");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Visit"].Columns["VisitID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Jobs"].Columns["VisitID"] };
                DataRelation relation = new DataRelation("Linked Jobs", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }

            // Add Seventh Relation (Linked Job Extra Attributes) //
            cmd = null;
            cmd = new SqlCommand("sp06379_OM_Completion_Sheet_Report_Job_Extra_Info_Info", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@VisitIDs", strVisitIDs));
            cmd.Parameters.Add(new SqlParameter("@TeamID", intTeamID));
            cmd.Parameters.Add(new SqlParameter("@LabourTypeID", intLabourTypeID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Job_Attributes");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Jobs"].Columns["JobID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Job_Attributes"].Columns["JobID"] };
                DataRelation relation = new DataRelation("Linked Job Attributes", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);
            }
            catch (Exception)
            {
            }
                
            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Next_Of_Kin DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Jobs") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Labour") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Job_Attributes") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Waste") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures_Before") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures_After") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures_Authorisations"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }


    }
}

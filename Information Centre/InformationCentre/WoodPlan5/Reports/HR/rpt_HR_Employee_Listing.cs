﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_HR_Employee_Listing : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private string strEmployeeIDs = "";
        private DateTime? dtStartDate;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_HR_Employee_Listing()
        {
            InitializeComponent();
        }

        public rpt_HR_Employee_Listing(BaseObjects.GlobalSettings GlobalSettings, string EmployeeIDs, DateTime? StartDate)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            strEmployeeIDs = EmployeeIDs;
            dtStartDate = StartDate;
        }


        private void rpt_HR_Employee_Listing_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                strMainTableName = "Table_Employees";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp_HR_00240_Reports_Employee_Personal_Details_Verification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EmployeeIDs", strEmployeeIDs));
                cmd.Parameters.Add(new SqlParameter("@ReturnDate", dtStartDate));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Next Of Kin) //
                cmd = null;
                cmd = new SqlCommand("sp_HR_00241_Reports_Employee_Personal_Details_Verification_Next_Of_Kin", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@EmployeeIDs", strEmployeeIDs));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Next_Of_Kin");
                try
                {
                    DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Employees"].Columns["EmployeeID"] };
                    DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Next_Of_Kin"].Columns["EmployeeID"] };
                    DataRelation relation = new DataRelation("Linked Next Of Kin", parentColumns, childColumns);
                    dsDataSet.Relations.Add(relation);

                    //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Employees"].Columns["RecordID"], dsDataSet.Tables["Table_Next_Of_Kin"].Columns["GrittingCalloutID"]);
                    //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
                }
                catch (Exception)
                {
                }

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Employee Personal Details";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_HR_Employee_Listing_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            strMainTableName = "Table_Employees";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp_HR_00240_Reports_Employee_Personal_Details_Verification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EmployeeIDs", strEmployeeIDs));
            cmd.Parameters.Add(new SqlParameter("@ReturnDate", dtStartDate));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Next Of Kin) //
            cmd = null;
            cmd = new SqlCommand("sp_HR_00241_Reports_Employee_Personal_Details_Verification_Next_Of_Kin", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@EmployeeIDs", strEmployeeIDs));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Next_Of_Kin");
            try
            {
                DataColumn[] parentColumns = new DataColumn[] { dsDataSet.Tables["Table_Employees"].Columns["EmployeeID"] };
                DataColumn[] childColumns = new DataColumn[] { dsDataSet.Tables["Table_Next_Of_Kin"].Columns["EmployeeID"] };
                DataRelation relation = new DataRelation("Linked Next Of Kin", parentColumns, childColumns);
                dsDataSet.Relations.Add(relation);

                //DataRelation relation = new DataRelation("Linked Extra Costs", dsDataSet.Tables["Table_Employees"].Columns["RecordID"], dsDataSet.Tables["Table_Next_Of_Kin"].Columns["GrittingCalloutID"]);
                //dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  
            }
            catch (Exception)
            {
            }

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        public class MyBS : BindingSource, IDisplayNameProvider  // For Reports: Used to Hide Table_Next_Of_Kin DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Next_Of_Kin"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }

            #endregion
        }


    }
}

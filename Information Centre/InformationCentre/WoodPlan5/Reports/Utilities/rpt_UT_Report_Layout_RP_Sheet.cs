using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_UT_Report_Layout_RP_Sheet : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private int intCompletionSheetID = 0;

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_UT_Report_Layout_RP_Sheet()
        {
            InitializeComponent();
        }

        public rpt_UT_Report_Layout_RP_Sheet(BaseObjects.GlobalSettings GlobalSettings, int CompletionSheetID)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            intCompletionSheetID = CompletionSheetID;
        }

        private void rpt_UT_Report_Layout_RP_Sheet_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                strMainTableName = "Completion Sheet";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp07314_UT_RP_Sheet_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Team Members) //
                cmd = null;
                cmd = new SqlCommand("sp07315_UT_RP_Sheet_Print_Team_Members_On_Site", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Team_Members");
                DataRelation relation = new DataRelation("Linked Team Members", dsDataSet.Tables["Completion Sheet"].Columns["CompletionSheetID"], dsDataSet.Tables["Table_Team_Members"].Columns["CompletionSheetID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Questions) //
                cmd = null;
                cmd = new SqlCommand("sp07316_UT_RP_Sheet_Print_Questions", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Questions");
                relation = new DataRelation("Linked Questions", dsDataSet.Tables["Completion Sheet"].Columns["CompletionSheetID"], dsDataSet.Tables["Table_Questions"].Columns["CompletionSheetID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                //this.DataSource = dsDataSet;
                //this.DataAdapter = sdaDataAdpter;
                //conn.Close();

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Completion Sheet";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_UT_Report_Layout_RP_Sheet_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            strMainTableName = "Completion Sheet";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp07314_UT_RP_Sheet_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Team Members) //
            cmd = null;
            cmd = new SqlCommand("sp07315_UT_RP_Sheet_Print_Team_Members_On_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Team_Members");
            DataRelation relation = new DataRelation("Linked Team Members", dsDataSet.Tables["Completion Sheet"].Columns["CompletionSheetID"], dsDataSet.Tables["Table_Team_Members"].Columns["CompletionSheetID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Questions) //
            cmd = null;
            cmd = new SqlCommand("sp07316_UT_RP_Sheet_Print_Questions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@CompletionSheetID", intCompletionSheetID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Questions");
            relation = new DataRelation("Linked Questions", dsDataSet.Tables["Completion Sheet"].Columns["CompletionSheetID"], dsDataSet.Tables["Table_Questions"].Columns["CompletionSheetID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            //this.DataSource = dsDataSet;
            //this.DataAdapter = sdaDataAdpter;
            //conn.Close(); 

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        /*private void OnPreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting.
            this.Detail.SortFields.Clear();
            if (currentSortCell != null)
            {
                currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
            }

            // Create a new field to sort.
            GroupField grField = new GroupField();
            grField.FieldName = ((XRControl)sender).Tag.ToString();
            if (currentSortCell != null)
            {
                if (currentSortCell.Text == ((XRLabel)sender).Text)
                {
                    grField.SortOrder = XRColumnSortOrder.Descending;
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                }
            }
            else
            {
                grField.SortOrder = XRColumnSortOrder.Ascending;
            }

            // Add sorting.
            this.Detail.SortFields.Add(grField);
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
            currentSortCell = (XRTableCell)sender;

            // Recreate the report document.
            this.CreateDocument();
        }
        */


        public class MyBS : BindingSource, IDisplayNameProvider  // For Tree Reports: Used to Hide Table_Inspections and Table_Actions DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Team_Members") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Questions"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }
    }
}

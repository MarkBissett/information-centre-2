using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_UT_Report_Layout_Permission_Form : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private int intPermissionDocumentID = 0;
        private string strSignaturePath = "";
        private string strPicturePath = "";
        private string strMapPath = "";
        private string strHeaderImagePath = "";

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";

        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_UT_Report_Layout_Permission_Form()
        {
            InitializeComponent();
        }

        public rpt_UT_Report_Layout_Permission_Form(BaseObjects.GlobalSettings GlobalSettings, int PermissionDocumentID, string SignaturePath, string PicturePath, string MapPath)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            intPermissionDocumentID = PermissionDocumentID;
            strSignaturePath = SignaturePath;
            strPicturePath = PicturePath;
            strMapPath = MapPath;
            strHeaderImagePath = Application.StartupPath;
        }

        private void rpt_UT_Report_Layout_Permission_Form_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                strMainTableName = "Permission Document";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp07210_UT_Permission_Document_Header_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strSignaturePath));
                cmd.Parameters.Add(new SqlParameter("@PassedInHeaderImagePath", strHeaderImagePath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Work) //
                cmd = null;
                cmd = new SqlCommand("sp07211_UT_Permission_Document_Work_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Work");
                DataRelation relation = new DataRelation("Linked Work", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Work"].Columns["PermissionDocumentID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp07212_UT_Permission_Document_Pictures_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
                relation = new DataRelation("Linked Pictures", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Pictures"].Columns["PermissionDocumentID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Third Relation (Linked Maps) //
                cmd = null;
                cmd = new SqlCommand("sp07213_UT_Permission_Document_Maps_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
                relation = new DataRelation("Linked Maps", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Maps"].Columns["PermissionDocumentID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Fourth Relation (Linked Equipment) //
                cmd = null;
                cmd = new SqlCommand("sp07214_UT_Permission_Document_Equipment_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Equipment");
                relation = new DataRelation("Linked Equipment", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Equipment"].Columns["PermissionDocumentID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Fifth Relation (Linked Access Maps) //
                cmd = null;
                cmd = new SqlCommand("sp07215_UT_Permission_Document_Access_Maps_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Access_Maps");
                relation = new DataRelation("Linked Access Maps", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Access_Maps"].Columns["PermissionDocumentID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                //this.DataSource = dsDataSet;
                //this.DataAdapter = sdaDataAdpter;
                //conn.Close();

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Permission Document";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_UT_Report_Layout_Permission_Form_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            strMainTableName = "Permission Document";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp07210_UT_Permission_Document_Header_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strSignaturePath));
            cmd.Parameters.Add(new SqlParameter("@PassedInHeaderImagePath", strHeaderImagePath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Actions) //
            cmd = null;
            cmd = new SqlCommand("sp07211_UT_Permission_Document_Work_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Work");
            DataRelation relation = new DataRelation("Linked Work", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Work"].Columns["PermissionDocumentID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Pictures) //
            cmd = null;
            cmd = new SqlCommand("sp07212_UT_Permission_Document_Pictures_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Pictures");
            relation = new DataRelation("Linked Pictures", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Pictures"].Columns["PermissionDocumentID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Third Relation (Linked Maps) //
            cmd = null;
            cmd = new SqlCommand("sp07213_UT_Permission_Document_Maps_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
            relation = new DataRelation("Linked Maps", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Maps"].Columns["PermissionDocumentID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Fourth Relation (Linked Equipment) //
            cmd = null;
            cmd = new SqlCommand("sp07214_UT_Permission_Document_Equipment_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Equipment");
            relation = new DataRelation("Linked Equipment", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Equipment"].Columns["PermissionDocumentID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Fifth Relation (Linked Access Maps) //
            cmd = null;
            cmd = new SqlCommand("sp07215_UT_Permission_Document_Access_Maps_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@PermissionDocumentID", intPermissionDocumentID));
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Access_Maps");
            relation = new DataRelation("Linked Access Maps", dsDataSet.Tables["Permission Document"].Columns["PermissionDocumentID"], dsDataSet.Tables["Table_Access_Maps"].Columns["PermissionDocumentID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            //this.DataSource = dsDataSet;
            //this.DataAdapter = sdaDataAdpter;
            //conn.Close(); 

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        /*private void OnPreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting.
            this.Detail.SortFields.Clear();
            if (currentSortCell != null)
            {
                currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
            }

            // Create a new field to sort.
            GroupField grField = new GroupField();
            grField.FieldName = ((XRControl)sender).Tag.ToString();
            if (currentSortCell != null)
            {
                if (currentSortCell.Text == ((XRLabel)sender).Text)
                {
                    grField.SortOrder = XRColumnSortOrder.Descending;
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                }
            }
            else
            {
                grField.SortOrder = XRColumnSortOrder.Ascending;
            }

            // Add sorting.
            this.Detail.SortFields.Add(grField);
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
            currentSortCell = (XRTableCell)sender;

            // Recreate the report document.
            this.CreateDocument();
        }
        */


        public class MyBS : BindingSource, IDisplayNameProvider  // For Tree Reports: Used to Hide Table_Inspections and Table_Actions DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Work") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Pictures") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Maps") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Equipment") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Access_Maps"))  
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

 
    }
}

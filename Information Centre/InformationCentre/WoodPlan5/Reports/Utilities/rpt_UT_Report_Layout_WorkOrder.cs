using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Data;

namespace WoodPlan5.Reports
{
    public partial class rpt_UT_Report_Layout_WorkOrder : DevExpress.XtraReports.UI.XtraReport
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        private int intWorkOrderID = 0;
        private string strMapPath = "";
        private string strPicturePath = "";

        private SqlDataAdapter sdaDataAdapter = new SqlDataAdapter();
        private DataSet dsDataSet = new DataSet("NewDataSet");

        //private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private string strMainTableName = "";
        private bool boolLoadMaps = false;
        private bool boolLoadPictures = false;
        private bool boolLoadEquipment = false;
        private bool boolLoadMaterials = false;
        #endregion

        // ***** Note: The Constructor for this report is overloaded with the parameterized version below. Both versions are required ***** //
        public rpt_UT_Report_Layout_WorkOrder()
        {
            InitializeComponent();
        }

        public rpt_UT_Report_Layout_WorkOrder(BaseObjects.GlobalSettings GlobalSettings, int WorkOrderID, bool LoadMaps, bool LoadEquipment, bool LoadMaterials, bool LoadPictures, string MapPath, string PicturePath)
        {
            InitializeComponent();
            strConnectionString = GlobalSettings.ConnectionString;
            intWorkOrderID = WorkOrderID;
            boolLoadMaps = LoadMaps;
            boolLoadPictures = LoadPictures;
            boolLoadEquipment = LoadEquipment;
            boolLoadMaterials = LoadMaterials;
            strMapPath = MapPath;
            strPicturePath = PicturePath;
            //dsDataSet.Tables.Add("Table");
        }

        private void rpt_UT_Report_Layout_WorkOrder_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (this.DataSource == null) // Prevent code re-firing [causes rows to be duplicated] if Custom Sorting is calling this via it's CreateDocument call //
            {
                // Fired when Running Report //
                strMainTableName = "Work Order Header";
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp07281_UT_Work_Order_Header_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, strMainTableName);

                // Add First Relation (Linked Actions) //
                cmd = null;
                cmd = new SqlCommand("sp07282_UT_Work_Order_Actions_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
                DataRelation relation = new DataRelation("Linked Actions", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Actions"].Columns["WorkOrderID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Second Relation (Linked Teams) //
                cmd = null;
                cmd = new SqlCommand("sp07283_UT_Work_Order_Teams_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Teams");
                relation = new DataRelation("Linked Teams", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Teams"].Columns["WorkOrderID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Third Relation (Linked Maps) //
                cmd = null;
                cmd = new SqlCommand("sp07284_UT_Work_Order_Maps_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadMaps) // Ok to load maps so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                }
                else // No maps so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
                }
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
                relation = new DataRelation("Linked Maps", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Maps"].Columns["WorkOrderID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

                // Add Fourth Relation (Linked Action Pictures) //
                cmd = null;
                cmd = new SqlCommand("sp07285_UT_Work_Order_Action_Pictures_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadPictures)  // Ok to load pictures so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                }
                else // No pictures so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
                }
                cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Action_Pictures");
                relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Pictures"].Columns["ActionID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Fifth Relation (Linked Action Equipment) //
                cmd = null;
                cmd = new SqlCommand("sp07286_UT_Work_Order_Action_Equipment_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadEquipment)  // Ok to load equipment so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                }
                else // No equipment so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Action_Equipment");
                relation = new DataRelation("Linked Action Equipment", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Equipment"].Columns["ActionID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                // Add Sixth Relation (Linked Action Materials) //
                cmd = null;
                cmd = new SqlCommand("sp07287_UT_Work_Order_Action_Materials_Print", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                if (boolLoadMaterials)  // Ok to load equipment so pass correct value //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
                }
                else // No equipment so pass dummy so nothing is returned //
                {
                    cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
                }
                sdaDataAdapter = new SqlDataAdapter(cmd);
                sdaDataAdapter.Fill(dsDataSet, "Table_Action_Materials");
                relation = new DataRelation("Linked Action Materials", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Materials"].Columns["ActionID"]);
                dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

                //this.DataSource = dsDataSet;
                //this.DataAdapter = sdaDataAdpter;
                //conn.Close();

                // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
                MyBS bs = new MyBS();
                bs.DataSource = dsDataSet;
                this.DataSource = bs;
                this.DataAdapter = dsDataSet.Tables[strMainTableName];
                this.DataMember = strMainTableName;
                conn.Close();

                this.Report.Name = "Work Order";  // Set Document Root Node to meaningful text //
            }
        }

        private void rpt_UT_Report_Layout_WorkOrder_DesignerLoaded(object sender, DevExpress.XtraReports.UserDesigner.DesignerLoadedEventArgs e)
        {
            // Fired when Designing Report //
            strMainTableName = "Work Order Header";
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp07281_UT_Work_Order_Header_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, strMainTableName);

            // Add First Relation (Linked Actions) //
            cmd = null;
            cmd = new SqlCommand("sp07282_UT_Work_Order_Actions_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Actions");
            DataRelation relation = new DataRelation("Linked Actions", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Actions"].Columns["WorkOrderID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Second Relation (Linked Teams) //
            cmd = null;
            cmd = new SqlCommand("sp07283_UT_Work_Order_Teams_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Teams");
            relation = new DataRelation("Linked Teams", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Teams"].Columns["WorkOrderID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Third Relation (Linked Maps) //
            cmd = null;
            cmd = new SqlCommand("sp07284_UT_Work_Order_Maps_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadMaps) // Ok to load maps so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            }
            else // No maps so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
            }
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strMapPath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Maps");
            relation = new DataRelation("Linked Maps", dsDataSet.Tables["Work Order Header"].Columns["WorkOrderID"], dsDataSet.Tables["Table_Maps"].Columns["WorkOrderID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //

            // Add Fourth Relation (Linked Action Pictures) //
            cmd = null;
            cmd = new SqlCommand("sp07285_UT_Work_Order_Action_Pictures_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadPictures)  // Ok to load pictures so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            }
            else // No pictures so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
            }
            cmd.Parameters.Add(new SqlParameter("@PassedInPath", strPicturePath));
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Action_Pictures");
            relation = new DataRelation("Linked Action Picture", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Pictures"].Columns["ActionID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Fifth Relation (Linked Action Equipment) //
            cmd = null;
            cmd = new SqlCommand("sp07286_UT_Work_Order_Action_Equipment_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadEquipment)  // Ok to load equipment so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            }
            else // No equipment so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Action_Equipment");
            relation = new DataRelation("Linked Action Equipment", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Equipment"].Columns["ActionID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            // Add Sixth Relation (Linked Action Materials) //
            cmd = null;
            cmd = new SqlCommand("sp07287_UT_Work_Order_Action_Materials_Print", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            if (boolLoadMaterials)  // Ok to load equipment so pass correct value //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", intWorkOrderID));
            }
            else // No equipment so pass dummy so nothing is returned //
            {
                cmd.Parameters.Add(new SqlParameter("@WorkOrderID", -999));
            }
            sdaDataAdapter = new SqlDataAdapter(cmd);
            sdaDataAdapter.Fill(dsDataSet, "Table_Action_Materials");
            relation = new DataRelation("Linked Action Materials", dsDataSet.Tables["Table_Actions"].Columns["ActionID"], dsDataSet.Tables["Table_Action_Materials"].Columns["ActionID"]);
            dsDataSet.Relations.Add(relation);  // Assign relation to dataset //                  

            //this.DataSource = dsDataSet;
            //this.DataAdapter = sdaDataAdpter;
            //conn.Close(); 

            // Link Data to report and remove any unneccessary data tables so the end-user can't see them (in GetDataSourceDisplayName Method of MyBS - below) //
            MyBS bs = new MyBS();
            bs.DataSource = dsDataSet;
            this.DataSource = bs;
            this.DataAdapter = dsDataSet.Tables[strMainTableName];
            this.DataMember = strMainTableName;
            conn.Close();
        }


        /*private void OnPreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting.
            this.Detail.SortFields.Clear();
            if (currentSortCell != null)
            {
                currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
            }

            // Create a new field to sort.
            GroupField grField = new GroupField();
            grField.FieldName = ((XRControl)sender).Tag.ToString();
            if (currentSortCell != null)
            {
                if (currentSortCell.Text == ((XRLabel)sender).Text)
                {
                    grField.SortOrder = XRColumnSortOrder.Descending;
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                }
            }
            else
            {
                grField.SortOrder = XRColumnSortOrder.Ascending;
            }

            // Add sorting.
            this.Detail.SortFields.Add(grField);
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
            currentSortCell = (XRTableCell)sender;

            // Recreate the report document.
            this.CreateDocument();
        }
        */


        public class MyBS : BindingSource, IDisplayNameProvider  // For Tree Reports: Used to Hide Table_Inspections and Table_Actions DataTables from the end user //
        {
            #region IDisplayNameProvider Members

            public string GetDataSourceDisplayName()
            {
                return "Report Data Source";
            }

            /*
            public string GetObjectDisplayName(string dataMember)
            {
                DataSet ds = ((DataSet)this.DataSource);
                bool istable = false;
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].TableName == dataMember)
                        istable = true;

                if (istable)
                {
                    bool isParentTable = false;
                    for (int i = 0; i < ds.Relations.Count; i++)
                        if (ds.Relations[i].ParentTable.TableName == dataMember)
                            isParentTable = true;
                    if (!isParentTable)
                        return "";
                    else
                        return dataMember;


                }
                else
                    return dataMember;
            }*/

            public string GetFieldDisplayName(string[] fieldAccessors)
            {
                if (fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Actions") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Teams") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Maps") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Action_Pictures") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Action_Equipment") || fieldAccessors[fieldAccessors.Length - 1].StartsWith("Table_Action_Materials"))
                {
                    return null;
                }
                else
                {
                    return fieldAccessors[fieldAccessors.Length - 1];
                }
            }


            #endregion
        }

 
    }
}

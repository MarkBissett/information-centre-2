﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data;

namespace WoodPlan5.Reports.Utilities
{
    public partial class rpt_UT_Report_Quote : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_UT_Report_Quote()
        {
            InitializeComponent();
            sp07457_UT_Quote_ParameterTableAdapter1.Fill(this.dataSet_UT_Quote_Report1.sp07457_UT_Quote_Parameter);
        }

        private void rpt_UT_Report_Quote_ParametersRequestSubmit(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {
            RefreshData();
        }

        public void RefreshData()
        {

            try
            {
                sp07455_UT_Quote_RptTableAdapter.Fill(this.dataSet_UT_Quote_Report1.sp07455_UT_Quote_Rpt, intQuoteID.Value.ToString());
                sp07456_UT_Quote_Item_RptTableAdapter1.Fill(this.dataSet_UT_Quote_Report1.sp07456_UT_Quote_Item_Rpt, intQuoteID.Value.ToString());
            }
            catch (ConstraintException)
            {
                LoadData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("An error occurred while loading quote \n[{0}]!\n\nTry loading again - if the problem persists, contact Technical Support.", ex.Message), "Loading Utilities Quote", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void LoadData()
        {
            try
            {
                sp07455_UT_Quote_RptTableAdapter.Fill(this.dataSet_UT_Quote_Report1.sp07455_UT_Quote_Rpt, intQuoteID.Value.ToString());
                sp07456_UT_Quote_Item_RptTableAdapter1.Fill(this.dataSet_UT_Quote_Report1.sp07456_UT_Quote_Item_Rpt, intQuoteID.Value.ToString());
            }
            catch (Exception)
            {
            }
        }

    }
}

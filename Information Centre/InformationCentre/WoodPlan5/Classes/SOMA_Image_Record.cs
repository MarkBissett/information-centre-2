﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5
{
    class SOMA_Image_Record
    {
        public string token { get; set; }
        public int dataType { get; set; }
        public string jobId { get; set; }
        public string visitId { get; set; }
        public string CalloutID { get; set; }
        public int pictureType { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public DateTime? dateTime { get; set; }
        public string filename { get; set; }
        public string GCM_ID { get; set; }
    }
}

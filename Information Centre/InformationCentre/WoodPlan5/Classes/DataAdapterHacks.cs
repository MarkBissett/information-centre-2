using System;
using System.ComponentModel;
using System.Reflection;
using System.Data.SqlClient;

// Need one for each DataSet //
namespace WoodPlan5.DataSet_ATTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AT_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AT_DataTransferTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AT_IncidentsTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AT_TreePickerTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AT_WorkOrdersTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_GroupsTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.WoodPlanDataSetTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_EPTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_EP_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_GC_CoreTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
         public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_Snow_CoreTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_Snow_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
         public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_ReportsTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_Summer_CoreTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_GC_Summer_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_UTTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_EditTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_MappingTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_DataTransferTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_WorkOrderTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_ReportingTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_UT_QuoteTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_HR_CoreTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_HR_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_TR_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

namespace WoodPlan5.DataSet_AS_DataEntryTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_OM_ContractTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}

namespace WoodPlan5.DataSet_OM_VisitTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}

namespace WoodPlan5.DataSet_OM_JobTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}

namespace WoodPlan5.DataSet_OM_Client_POTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}

namespace WoodPlan5.DataSet_OM_CoreTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}


namespace WoodPlan5.DataSet_OM_BillingTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
                s.CommandTimeout = 3600;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
        public void ChangeTableAdapterTimeout(Component component, int timeout)
        {
            if (!component.GetType().Name.Contains("TableAdapter")) return;

            PropertyInfo adapterProp = component.GetType().GetProperty("CommandCollection", BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.Instance);
            if (adapterProp == null) return;

            SqlCommand[] command = adapterProp.GetValue(component, null) as SqlCommand[];
            if (command == null) return;

            foreach (SqlCommand sc in command) 
            { 
                sc.CommandTimeout = timeout; 
            }
        }
    }

}

namespace WoodPlan5.DataSet_OM_TenderTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
        public void ChangeCommandTimeout(string strCommandName, int intTimeout)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                if (s.CommandText == strCommandName)
                {
                    s.CommandTimeout = intTimeout;
                    return;
                }
            }
        }
    }
}


namespace WoodPlan5.DataSet_AccidentTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


namespace WoodPlan5.DataSet_Common_FunctionalityTableAdapters
{
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}


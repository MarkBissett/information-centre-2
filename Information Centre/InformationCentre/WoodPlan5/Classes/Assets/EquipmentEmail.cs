﻿using BaseObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WoodPlan5.Classes.Assets
{
    public struct EquipmentEmail
    {


        public static void Build(
            string strConnectionString,
            string strEquipmentIDs,
            string strSubject)
        {

            DataSet_AS_DataEntryTableAdapters.sp_AS_11179_Keeper_EmailsTableAdapter sp_AS_11179_Keeper_EmailsTableAdapter;

            DataSet_AS_DataEntry.sp_AS_11179_Keeper_EmailsDataTable sp_AS_11179_Keeper_Emails; 

            sp_AS_11179_Keeper_EmailsTableAdapter = new DataSet_AS_DataEntryTableAdapters.sp_AS_11179_Keeper_EmailsTableAdapter();
            sp_AS_11179_Keeper_EmailsTableAdapter.Connection.ConnectionString = strConnectionString;

            //string strSubject = "Fleet : ";
            //string strCCToEmailAddress = "";

            try
            {
                sp_AS_11179_Keeper_Emails = new DataSet_AS_DataEntry.sp_AS_11179_Keeper_EmailsDataTable();

                sp_AS_11179_Keeper_EmailsTableAdapter.Fill(sp_AS_11179_Keeper_Emails, strEquipmentIDs);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Email addresses for the current Keepers.\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Current Keeper Emails", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (sp_AS_11179_Keeper_Emails.Rows.Count <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Email addresses exist for the current keepers of the selected items.\n\n", "Email current Keepers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            var fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Preparing Email...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();

            try
            {
                string strBody = "";
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                oMailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;

                //
                Outlook.Recipient oFrom = oRecips.Add("fleet@ground-control.co.uk");
                oFrom.Type = (int)Outlook.OlMailRecipientType.olOriginator;
                oFrom.Resolve();

                //Add To
                Outlook.Recipient oToRecip = oRecips.Add("fleet@ground-control.co.uk");
                oToRecip.Type = (int)Outlook.OlMailRecipientType.olTo;
                oToRecip.Resolve();

                //Add Recipients as Blind Copies
                foreach (DataRow dr in sp_AS_11179_Keeper_Emails.Rows)
                {
                    string email = dr["Email"].ToString();
                    if (email.Length > 0)
                    {
                        Outlook.Recipient oBCCRecip = (Outlook.Recipient)oRecips.Add(email);
                        oBCCRecip.Type = (int)Outlook.OlMailRecipientType.olBCC;
                        oBCCRecip.Resolve();
                    }
                }

                /**
                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                {
                    //Add CC
                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                    oCCRecip.Resolve();
                }
                **/

                // create message body text //
                strBody = "<tr></tr><tr></tr><tr></tr>";  // Put a few blank lines in so the user can put whatever text in they require //

                if (fProgress != null)
                {
                    fProgress.SetProgressValue(100);
                    fProgress.Close();
                    fProgress = null;
                }
                //message.ShowDialog();  // Display Email Window to User //
                oMailItem.Subject = strSubject;
                //oMailItem.Body = strBody;
                oMailItem.HTMLBody = strBody;
                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Current Keepers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }


    }
}

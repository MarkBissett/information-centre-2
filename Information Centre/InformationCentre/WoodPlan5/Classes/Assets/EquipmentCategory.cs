﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.Assets
{
    public struct EquipmentCategory
    {
        // The List of Equipment Categories is based on the AS_Equipment_Category table contents

        public const int Vehicle = 1;
        public const int Plant = 2;
        public const int Gadget = 3;
        public const int Hardware = 4;
        public const int Software = 5;
        public const int Office = 6;
        public const int Rental = 7;


        public static bool Is_P11D_Required(int EquipmentCategory)
        {
            return(EquipmentCategory == Vehicle || EquipmentCategory == Rental);
        }

    }
}

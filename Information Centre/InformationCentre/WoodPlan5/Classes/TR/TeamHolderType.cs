﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.TR
{
    public struct TeamHolderType
    {
        // The List of Team Holder Types is based on the TR_Pick_List_Item list for HeaderID '10'

        public const int NotRequired = 0;
        public const int All = 1;
        public const int SubjectToMinimum = 2;

    }
}

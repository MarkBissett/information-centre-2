﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.TR
{
    public struct RequirementType
    {
        // The List of Requirement Types is based on the TR_Pick_List_Item list for HeaderID '7'

        public const int Mandatory = 1;
        public const int Recommended = 2;
        public const int Elective = 3;
        public const int Developmental = 4;
    }
}

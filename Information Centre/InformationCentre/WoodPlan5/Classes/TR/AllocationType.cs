﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.TR
{
    public struct AllocationType
    {
        // The List of Allocation Types is based on the TR_Pick_List_Item list for HeaderID '6'

        public const int GenericJobTitle = 1;
        public const int Client = 2;
        public const int Site = 3;
        public const int Machinery = 4;
        public const int WorkType = 5;
        public const int BusinessArea = 6;
        public const int Teams = 7;
        public const int TeamByWorkType = 8;

    }
}

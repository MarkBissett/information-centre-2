﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WoodPlan5
{
    using System.Drawing;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;
    using GMap.NET;
 
    class GMapTextPolygon : GMapPolygon
    {
        public GMapTextPolygon(List<PointLatLng> points, string name) : base(points, name)
        {

        }
        public override void OnRender(System.Drawing.Graphics g)
        {
            g.DrawString("Custom Text", new Font("Arial", 10, GraphicsUnit.Pixel), new SolidBrush(Color.Red), 0, 0);
        }
    }
}
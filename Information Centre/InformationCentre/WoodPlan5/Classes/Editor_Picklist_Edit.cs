using System;
using System.Windows.Forms;
using BaseObjects;

namespace WoodPlan5
{
    class Editor_Picklist_Edit
    {

        public static int Edit_picklist(Form frmCallingForm, GlobalSettings globalSettings, int intPicklistHeaderID, string strPickListHeaderName)
        {
            // Opens a picklist from an editor screen for data editing //
            // Note: The user will need to click the reload button on the editor to see any changes made to the data from the picklist screen //
            
            System.Reflection.MethodInfo method = null;

            switch (intPicklistHeaderID)
            {
                case 900:  // Budgets \ Ownerships \ Cost Centres //
                    {
                        frm_AT_Budget_Manager fBudgets = new frm_AT_Budget_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fBudgets.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fBudgets.MdiParent = frmCallingForm.MdiParent;
                        }
                        fBudgets.GlobalSettings = globalSettings;
                        fBudgets.strCaller = frmCallingForm.GetType().FullName;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(fBudgets.MdiParent, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fBudgets.splashScreenManager = splashScreenManager1;
                        fBudgets.splashScreenManager.ShowWaitForm();
                        fBudgets.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fBudgets, new object[] { null });
                    }
                    return 1;
                case 902:  // Jobs \ Job Rates //
                    {
                        var fJobs = new frm_Core_Master_Job_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fJobs.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fJobs.MdiParent = frmCallingForm.MdiParent;
                        }
                        fJobs.GlobalSettings = globalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(fJobs.MdiParent, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fJobs.splashScreenManager = splashScreenManager1;
                        fJobs.splashScreenManager.ShowWaitForm();
                        fJobs.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fJobs, new object[] { null });
                    }
                    return 1;
                case 9:  // Sequences //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Sequence_Manager fSequence = new frm_Core_Sequence_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fSequence.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fSequence.MdiParent = frmCallingForm.MdiParent;
                        }
                        fSequence.GlobalSettings = globalSettings;
                        fSequence.strCaller = frmCallingForm.GetType().FullName;
                        fSequence.fProgress = fProgress;
                        fSequence.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fSequence, new object[] { null });
                    }
                    return 1;
                case 2:  // Species //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Species_Manager fSpecies = new frm_Core_Species_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fSpecies.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fSpecies.MdiParent = frmCallingForm.MdiParent;
                        }
                        fSpecies.GlobalSettings = globalSettings;
                        fSpecies.strCaller = frmCallingForm.GetType().FullName;
                        fSpecies.fProgress = fProgress;
                        fSpecies.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fSpecies, new object[] { null });
                    }
                    return 1;
                case 1:  // Contractors //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Contractor_Manager fContractors = new frm_Core_Contractor_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fContractors.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fContractors.MdiParent = frmCallingForm.MdiParent;
                        }
                        fContractors.GlobalSettings = globalSettings;
                        fContractors.strCaller = frmCallingForm.GetType().FullName;
                        fContractors.fProgress = fProgress;
                        fContractors.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fContractors, new object[] { null });
                    }
                    return 1;
                case 8:  // Company Headers //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Company_Header_Manager fCompanyHeaders = new frm_Core_Company_Header_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fCompanyHeaders.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fCompanyHeaders.MdiParent = frmCallingForm.MdiParent;
                        }
                        fCompanyHeaders.GlobalSettings = globalSettings;
                        fCompanyHeaders.fProgress = fProgress;
                        fCompanyHeaders.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fCompanyHeaders, new object[] { null });
                    }
                    return 1;
                case 102:  // Staff Names And Addresses //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Staff_Manager fStaff = new frm_Core_Staff_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fStaff.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fStaff.MdiParent = frmCallingForm.MdiParent;
                        }
                        fStaff.GlobalSettings = globalSettings;
                        fStaff.fProgress = fProgress;
                        fStaff.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fStaff, new object[] { null });
                    }
                    return 1;

                case 3001:  // EstatePlan Clients //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Client_Manager fClient = new frm_Core_Client_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fClient.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fClient.MdiParent = frmCallingForm.MdiParent;
                        }
                        fClient.GlobalSettings = globalSettings;
                        fClient.fProgress = fProgress;
                        fClient.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fClient, new object[] { null });
                    }
                    return 1;

                case 3002:  // EstatePlan Sites //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        var fSite = new frm_Core_Site_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fSite.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fSite.MdiParent = frmCallingForm.MdiParent;
                        }
                        fSite.GlobalSettings = globalSettings;
                        fSite.fProgress = fProgress;
                        fSite.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fSite, new object[] { null });
                    }
                    return 1;

                case 9060:  // EstatePlan Asset Type, Sub-Types, Conditions, Master Job Types //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_EP_Asset_Types_Manager fAssetType = new frm_EP_Asset_Types_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fAssetType.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fAssetType.MdiParent = frmCallingForm.MdiParent;
                        }
                        fAssetType.GlobalSettings = globalSettings;
                        fAssetType.fProgress = fProgress;
                        fAssetType.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fAssetType, new object[] { null });
                    }
                    return 1;

                case 4003:  // GC Companies //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_GC_Company_Manager fCompany = new frm_GC_Company_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fCompany.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fCompany.MdiParent = frmCallingForm.MdiParent;
                        }
                        fCompany.GlobalSettings = globalSettings;
                        fCompany.fProgress = fProgress;
                        fCompany.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fCompany, new object[] { null });
                    }
                    return 1;

               case 9907:  // HR Salary Bandings //
                    {
                        frm_HR_Master_Salary_Banding_Manager fScreen = new frm_HR_Master_Salary_Banding_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fScreen.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fScreen.MdiParent = frmCallingForm.MdiParent;
                        }
                        fScreen.GlobalSettings = globalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(fScreen.MdiParent, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fScreen.splashScreenManager = splashScreenManager1;
                        fScreen.splashScreenManager.ShowWaitForm();

                        fScreen.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fScreen, new object[] { null });
                    }
                    return 1;

               case 9908:  // HR MAster Vetting Types //
                    {
                        frm_HR_Master_Vetting_Types_Manager fScreen = new frm_HR_Master_Vetting_Types_Manager();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fScreen.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fScreen.MdiParent = frmCallingForm.MdiParent;
                        }
                        fScreen.GlobalSettings = globalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(fScreen.MdiParent, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fScreen.splashScreenManager = splashScreenManager1;
                        fScreen.splashScreenManager.ShowWaitForm();

                        fScreen.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fScreen, new object[] { null });
                    }
                    return 1;

                default:  // All other picklists //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_Core_Picklist_Edit fChildForm = new frm_Core_Picklist_Edit();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            fChildForm.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            fChildForm.MdiParent = frmCallingForm.MdiParent;
                        }
                        fChildForm.GlobalSettings = globalSettings;
                        fChildForm.intPicklistHeaderID = intPicklistHeaderID;
                        fChildForm.strPickListHeaderName = strPickListHeaderName;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = frmCallingForm.GetType().FullName;
                        fChildForm.intRecordCount = 0;
                        fChildForm.iBool_AllowAdd = true;
                        fChildForm.iBool_AllowEdit = true;
                        fChildForm.iBool_AllowDelete = true;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    return 1;
            }


        }


    }
}

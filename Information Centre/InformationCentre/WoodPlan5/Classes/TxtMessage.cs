﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Collections;
using System.Net;
using System.Windows.Forms;

// ***** THIS CLASS IS REQUIRED TO SEND THE TEXT MESSAGES ***** //
namespace WoodPlan5
{
    public class TxtMessage
    {
        #region constructor
        public TxtMessage(string _target, string _body, int _id, string _UserName, string _UserPassword)
        {
            Target = _target;
            Body = _body;
            id = _id;
            status = EnmStatus.NotTried;
            UserName = _UserName;
            UserPassword = _UserPassword;
        }
        #endregion

        #region sending
        public void SendAsync()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(Send));
        }
        public void Send(object args)
        {
            try
            {
                if (status == EnmStatus.NotTried)
                {
                    net.textapp.www.Service mySms = new net.textapp.www.Service();
                    string result = mySms.SendSMS(true, // return CSV
                                                username,  // Client_ID //
                                                userpassword,  // Client_Pass //
                                                "Automatic for Gritting",
                                                id.ToString(),  // ClientMessageRef - Unique ID [this is the SentTextMessageID from the Sent_Text_Message record created already for this text message //
                                                 "", // Orginater - not used since we want replies //
                                                target,  // Destination //
                                                body,  // Body //
                                                (long)5,  // Validitity - how long to keep trying to send the message for //
                                                2,  // Character Set ID - 2 = GSM 03.38 character set //
                                                3,  // Reply MethodID //
                                                "",  // ReplyData //
                                                "");  // StatusNotificationURL //
                    string[] results = result.Split('\n');
                    status = EnmStatus.Ok;
                    if (results.Length < 3)
                    {
                        status = EnmStatus.Fail;
                    }
                    else if (!results[1].EndsWith(":1"))
                    {
                        status = EnmStatus.Fail;
                    }
                }
            }
            catch (WebException we)
            {
                status = EnmStatus.Fail;
                MessageBox.Show(we.Message, "Network problem", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region pk tracking
        public void AddPk(int pk)
        {
            pksToUpdate.Add(pk);
        }
        public List<Int64> PksToUpdate()
        {
            return pksToUpdate;
        }
        #endregion

        #region accessors & public members
        public string Team;
        public int id = 0;

        public string Target
        {
            get
            {
                return target;
            }
            set
            {
                if (value.Length < 8)
                    status = EnmStatus.Fail;
                target = value;
                if (target.StartsWith("0"))
                {
                    target = target.Substring(1);
                    target = "+44" + target;
                }
                target = target.Replace(" ", "");
                foreach (char c in target)
                    if ((c != '+') && (!char.IsDigit(c)))
                        status = EnmStatus.Fail;

            }
        }

        public string Body
        {
            get { return body; }
            set
            {
                body = value;
                if (body.Length > 160)
                    body = body.Remove(160);
            }
        }

        public string UserName
        {
            get { return username; }
            set
            {
                username = value;
            }
        }

        public string UserPassword
        {
            get { return userpassword; }
            set
            {
                userpassword = value;
            }
        }


        public EnmStatus Status
        {
            get { return status; }
        }
        #endregion

        #region members

        private string target = string.Empty;
        private string body;
        private string username;
        private string userpassword;
        EnmStatus status;
        List<Int64> pksToUpdate = new List<Int64>();
        #endregion
    }

    public enum EnmStatus
    {
        NotTried,
        Ok,
        Fail
    }
}

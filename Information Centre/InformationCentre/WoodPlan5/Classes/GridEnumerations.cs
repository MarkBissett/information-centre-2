﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5
{
    public class GridEnumerations
    {
        public enum enmFocusedGrid
        {
            MasterJobType = 1,
            MasterJobSubType = 2,
            MasterJobSubTypeCompetency = 3,
            MasterJobSubTypeAttribute = 4,
            MasterJobTypeDefaultMaterial = 5,
            MasterJobTypeDefaultEquipment = 6,
            MasterJobTypeReferenceFile = 7,
            MasterJobTypeVisitType = 8,
            MasterJobTypeRate = 9,
            MasterJobTypeRateCriteria = 10
        }
    }
}

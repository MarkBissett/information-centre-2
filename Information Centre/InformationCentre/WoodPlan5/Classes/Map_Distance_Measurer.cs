using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using MapInfo.Tools;
using MapInfo.Geometry;
using MapInfo.Windows.Controls;

namespace WoodPlan5
{
    /// <summary>
    /// Provides event arguments for the PSGDistanceTool event handlers.
    /// </summary>
    public class PSGDistanceToolEventArgs : EventArgs
    {
        //Internal list of coordinates to keep track of the points submitted by the user.
        private MapInfo.Geometry.DPoint[] _linePoints;
        //Internal logical to determine if drawing is finished
        private bool _isFinished;
        //Internal variable to keep track of the coordsys used for the _linePoints
        private MapInfo.Geometry.CoordSys _coordSys;

        public PSGDistanceToolEventArgs(bool isFinished, MapInfo.Geometry.DPoint[] dPoints, MapInfo.Geometry.CoordSys coordSys)
        {
            _isFinished = isFinished;
            _linePoints = dPoints;
            _coordSys = coordSys;
        }

        public bool IsFinished
        {
            get
            {
                return _isFinished;
            }
        }

        public MapInfo.Geometry.MultiCurve GetCurrentLine()
        {
            //return the feature
            return new MultiCurve(_coordSys, MapInfo.Geometry.CurveSegmentType.Linear, _linePoints);
        }

        public double GetLastSegmentLength(MapInfo.Geometry.DistanceUnit distanceUnit, MapInfo.Geometry.DistanceType distanceType)
        {
            //Create a curve object and return its length
            if (_linePoints.Length > 1 && !_isFinished)
            {
                DPoint[] _lastSegmentPoints = new DPoint[2];
                _lastSegmentPoints[0] = _linePoints[_linePoints.Length - 2];
                _lastSegmentPoints[1] = _linePoints[_linePoints.Length - 1];
                Curve curve = new Curve(_coordSys, MapInfo.Geometry.CurveSegmentType.Linear, _lastSegmentPoints);
                return curve.Length(distanceUnit, distanceType);
            }
            return 0;
        }

        public double GetCurrentLineLength(MapInfo.Geometry.DistanceUnit distanceUnit, MapInfo.Geometry.DistanceType distanceType)
        {
            //Create a curve object and return its length
            Curve curve = new Curve(_coordSys, MapInfo.Geometry.CurveSegmentType.Linear, _linePoints);
            return curve.Length(distanceUnit, distanceType);
        }
    }

    /// <summary>
    /// Represents a method that will be used with points that are added.
    /// </summary>
    public delegate void PSGDistanceToolPointAddedEventHandler(object sender, PSGDistanceToolEventArgs e);

    /// <summary>
    /// Sample distance tool to demonstrate creating a tool with custom functionality and events.
    /// </summary>
    public class PSGDistanceTool : CustomPolylineMapTool
    {
        //event for when points are added to the list
        public event PSGDistanceToolPointAddedEventHandler PSGDistanceToolPointAdded;
        //event for when the tool mouse moves over the map
        public event PSGDistanceToolPointAddedEventHandler PSGDistanceToolMove;

        //Internal list of coordinates to keep track of the points submitted by the user.
        private ArrayList _linePoints;
        //Internal logical to determine when to reset the point list
        private bool _isFinished;
        //Internal handle to grab a reference to the MapControl
        private int _handle;

        public PSGDistanceTool(bool alterobject, bool callcomplete, bool drawrubberobject, MapInfo.Mapping.FeatureViewer featureViewer, int hwnd, MapInfo.Tools.MapTools maptools, MapInfo.Tools.IMouseToolProperties iMouseToolProperties, MapInfo.Tools.IMapToolProperties iMapToolProperties)
            : base(alterobject, callcomplete, drawrubberobject, featureViewer, hwnd, maptools, iMouseToolProperties, iMapToolProperties)
        {
            //initialize internal variables
            _linePoints = new ArrayList();
            _isFinished = true;
            _handle = hwnd;
        }

        public override void OnMouseDown(object sender, MouseEventArgs mea)
        {
            //add a point to the list
            AddPoint(mea.X, mea.Y);
            if (mea.Clicks == 2)
            {
                //End of the Line - reset internal DPoints
                _isFinished = true;
            }
            //raise event if the PointAdded event has a subscription
            if (PSGDistanceToolPointAdded != null)
            {
                PSGDistanceToolEventArgs e = new PSGDistanceToolEventArgs(_isFinished, (DPoint[])_linePoints.ToArray(typeof(DPoint)), base.FeatureViewer.GetDisplayCoordSys());
                PSGDistanceToolPointAdded(this, e);
            }
            //call the base method
            base.OnMouseDown(sender, mea);
        }

        public override void OnMouseMove(object sender, MouseEventArgs mea)
        {
            //temporarily add a point to the list and raise event if the ToolMove event has a subscription
            if (_linePoints.Count > 0 && PSGDistanceToolMove != null)
            {
                //add the point to the list
                AddPoint(mea.X, mea.Y);
                PSGDistanceToolEventArgs e = new PSGDistanceToolEventArgs(_isFinished, (DPoint[])_linePoints.ToArray(typeof(DPoint)), base.FeatureViewer.GetDisplayCoordSys());
                PSGDistanceToolMove(this, e);
                //remove the point from the list
                _linePoints.RemoveAt(_linePoints.Count - 1);
            }
            //call the base method
            base.OnMouseMove(sender, mea);
        }

        private void AddPoint(int mouseX, int mouseY)
        {
            if (_isFinished == true)
            {
                _linePoints.Clear();
            }
            _isFinished = false;
            MapInfo.Geometry.DPoint newPoint = new DPoint(0, 0);
            base.FeatureViewer.DisplayTransform.FromDisplay(new PointF(mouseX, mouseY), out newPoint);
            _linePoints.Add(newPoint);
        }

        public override void OnKeyDown(object sender, KeyEventArgs key)
        {
            switch (key.KeyCode)
            {
                case Keys.Enter:
                    //get the mouse position in relation to the map.
                    MapControl mapControl = System.Windows.Forms.Control.FromHandle(new System.IntPtr(_handle)) as MapControl;
                    System.Drawing.Point ptCursor = new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y);
                    System.Drawing.Point ptMap = mapControl.PointToClient(ptCursor);
                    AddPoint(ptMap.X, ptMap.Y);
                    //End of the Line - reset internal DPoints
                    _isFinished = true;
                    //raise event if the PointAdded event has a subscription
                    if (PSGDistanceToolPointAdded != null)
                    {
                        try
                        {
                            PSGDistanceToolEventArgs e = new PSGDistanceToolEventArgs(_isFinished, (DPoint[])_linePoints.ToArray(typeof(DPoint[])), base.FeatureViewer.GetDisplayCoordSys());
                            PSGDistanceToolPointAdded(this, e);
                        }
                        catch
                        {
                        }
                    }
                    break;
                case Keys.M:
                    //string strTest = "";
                    break;

                case Keys.Escape:
                    //TODO: Throw cancelled event?
                    //Cancelled Line - reset internal DPoints
                    _isFinished = true;
                    break;
                default:
                    break;
            }
            base.OnKeyDown(sender, key);
        }
        
    }
}
﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WoodPlan5.Classes.Utilities
{
    [DataContract]
    class PurchaseQuote : IDisposable
    {

        #region Declarations

        [DataMember]
        public string ResultCode { get; set; }
        [DataMember]
        public string ErrorTitle { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public int RecordsAffected { get; set; }
        [DataMember]
        public Transaction Transaction { get; set; }
        [DataMember]
        public string thJobCode { get; set; }
        [DataMember]
        public string thAnalysisCode { get; set; }
        [DataMember]
        public string thUserField4 { get; set; }

        [DataMember]
        public string CompanyID { get; set; }
        [DataMember]
        public string thOurRef { get; set; }
        [DataMember]
        public string thAcCode { get; set; }
        [DataMember]
        public string thTransDate { get; set; }
        [DataMember]
        public string thYourRef { get; set; }
        public string thDelAddress1 { get; set; }
        public string thDelAddress2 { get; set; }
        public string thDelAddress3 { get; set; }
        public string thDelAddress4 { get; set; }
        public string thDelAddress5 { get; set; }
        public string thDeliveryPostcode { get; set; }
        public string thUserField1 { get; set; }
        public string thUserField2 { get; set; }
        public string thUserField3 { get; set; }
        //public string thUserField4 { get; set; }
        [DataMember]
        public POLine[] thLines { get; set; }

        private DataSet_UT_Quote.sp07448_UT_POHeaderTransactionDataTable dt_POTransaction;
        private DataSet_UT_Quote.sp07454_UT_POLineDataTable dt_POLine;

        #endregion

        PurchaseQuote()
        { }
        public PurchaseQuote(string strResultCode, Transaction pqTransaction, int intRecordsAffected)
        {
            ResultCode = strResultCode;
            Transaction = pqTransaction;
            RecordsAffected = intRecordsAffected;
        }

        public PurchaseQuote(DataSet_UT_Quote.sp07448_UT_POHeaderTransactionDataTable sp07448_UT_POHeaderTransactionDataTable, DataSet_UT_Quote.sp07454_UT_POLineDataTable sp07454_UT_POLineDataTable)
        {
            loadData(sp07448_UT_POHeaderTransactionDataTable, sp07454_UT_POLineDataTable);
        }


        #region Methods

        private void loadData(DataSet_UT_Quote.sp07448_UT_POHeaderTransactionDataTable sp07448_UT_POHeaderTransactionDataTable, DataSet_UT_Quote.sp07454_UT_POLineDataTable sp07454_UT_POLineDataTable)
        {
            this.dt_POTransaction = sp07448_UT_POHeaderTransactionDataTable;
            this.dt_POLine = sp07454_UT_POLineDataTable;
            DataRow[] drQuoteList = this.dt_POTransaction.Select();

            foreach (DataSet_UT_Quote.sp07448_UT_POHeaderTransactionRow dataRowQuote in drQuoteList)
            {
                //populate quote
                CompanyID = dataRowQuote.CompanyID;
                thOurRef = dataRowQuote.thOurRef;
                thJobCode = dataRowQuote.thJobCode;
                thAnalysisCode = dataRowQuote.thAnalysisCode;
                thUserField4 = dataRowQuote.thUserField4;
                thAcCode = dataRowQuote.thAcCode;
                thTransDate = dataRowQuote.thTransDate;
                thYourRef = dataRowQuote.thYourRef.ToString();

                DataRow[] drQuoteItemList = this.dt_POLine.Select("QuoteID = " + dataRowQuote.QuoteID);

                int i = 0;

                POLine[] poLines = new POLine[drQuoteItemList.Count()];
                foreach (DataSet_UT_Quote.sp07454_UT_POLineRow dataRowQuoteItems in drQuoteItemList)
                {
                    poLines[i] = new POLine()
                    {
                        tlLineNo = i + 1,
                        tlStockCode = dataRowQuoteItems.tlStockCode,
                        tlDescr = dataRowQuoteItems.tlDescr,
                        tlLocation = dataRowQuoteItems.tlLocation,
                        tlQty = dataRowQuoteItems.tlQty,
                        tlNetValue = (float)(dataRowQuoteItems.tlNetValue),
                        tlVATCode = dataRowQuoteItems.tlVATCode,
                        tlJobCode = dataRowQuoteItems.tlJobCode,
                        tlAnalysisCode = dataRowQuoteItems.tlAnalysisCode,
                        tlCostCentre = dataRowQuoteItems.tlCostCentre,
                        tlDepartment = dataRowQuoteItems.tlDepartment,
                        tlGLCode = Convert.ToInt32(dataRowQuoteItems.tlGLcode)
                    };

                    i++;
                }
               thLines = poLines;
            }

        }

        internal void UpdateStoreResults()
        {            
            MessageBox.Show("Purchase Quote ID is : " + this.thOurRef);
        }

        internal void displayDeleteErrorMessage()
        {
            XtraMessageBox.Show("Quote has not been deleted\n" + this.ErrorMessage, this.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal void displayErrorMessage()
        {
            XtraMessageBox.Show("Quote has not been posted\n" + this.ErrorMessage, this.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal void displayGetNotFoundMessage()
        {
            XtraMessageBox.Show("Quote does not exist on Exchequer", "Record Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        internal void deleteSuccessMessage()
        {
            if (this.RecordsAffected > 0)
            {
                XtraMessageBox.Show(this.RecordsAffected + " record(s) successfully deleted\n ", "Quote Succesfully Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                XtraMessageBox.Show(this.RecordsAffected + " record(s) affected\nQuote could not be found on Exchequer.", "Quote does not exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        internal void displayStoreSuccessMessage()
        {
            XtraMessageBox.Show("Quote has been successfully posted and waiting for approval\n PQU is :" + this.Transaction.thOurRef, "Quote Succesfully Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        internal void displayGetSuccessMessage()
        {
            XtraMessageBox.Show("Quote Exists\n\n PQU :" + this.Transaction.thOurRef
                + "\n\nAcCode :" + this.Transaction.thAcCode
                + "\n\nacCompany :" + this.Transaction.acCompany
                + "\n\nacEmailAddr :" + this.Transaction.acEmailAddr
                + "\n\nacVATCode :" + this.Transaction.acVATCode
                + "\n\nthTransDate :" + this.Transaction.thTransDate
                + "\n\nthDueDate :" + this.Transaction.thDueDate
                + "\n\nthPeriod :" + this.Transaction.thPeriod
                + "\n\nTotal Quote Item Lines :" + this.Transaction.thLines.Length
                , "Quote Available", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Dispose()
        {
            if (dt_POTransaction != null)
            {
                dt_POTransaction.Dispose();
                dt_POTransaction = null;
            }
            if (dt_POLine != null)
            {
                dt_POLine.Dispose();
                dt_POLine = null;
            }
        }
        
        #endregion

    }
        #region Classes

        [DataContract]
        public class POLine
    {
        [DataMember]
        public int tlABSLineNo { get; set; }
        [DataMember]
        public string tlAnalysisCode { get; set; }
        [DataMember]
        public string tlCostCentre { get; set; }
        [DataMember]
        public string tlDepartment { get; set; }
        [DataMember]
        public string tlDescr { get; set; }
        [DataMember]
        public int tlGLCode { get; set; }
        [DataMember]
        public string tlJobCode { get; set; }
        [DataMember]
        public object tlLineDate { get; set; }
        [DataMember]
        public int tlLineNo { get; set; }
        [DataMember]
        public string tlLocation { get; set; }
        [DataMember]
        public float tlNetValue { get; set; }
        [DataMember]
        public int tlQty { get; set; }
        [DataMember]
        public string tlStockCode { get; set; }
        [DataMember]
        public string tlVATCode { get; set; }

        public int tlFolioNum { get; set; }

        public string[] tlDescrLines { get; set; }

        public float tlTotal0 { get; set; }

        public float tlTotal1 { get; set; }

        public string jrDesc { get; set; }

        public string tlUserField1 { get; set; }

        public string tlUserField2 { get; set; }

        public string tlUserField3 { get; set; }

        public string tlUserField4 { get; set; }
        
    }

        public class Transaction
        {
            [DataMember]
            public string CompanyID { get; set; }

            [DataMember]
            public string thOurRef { get; set; }

            public int thFolioNum { get; set; }

            [DataMember]
            public string thAcCode { get; set; }

            [DataMember]
            public string acCompany { get; set; }

            public string acEmailAddr { get; set; }

            [DataMember]
            public string acVATCode { get; set; }

            [DataMember]
            public string thTransDate { get; set; }

            public string thDueDate { get; set; }

            public int thPeriod { get; set; }

            public int thYear { get; set; }

            [DataMember]
            public string thJobCode { get; set; }
            [DataMember]
            public string thAnalysisCode { get; set; }

            [DataMember]
            public string thUserField4 { get; set; }
            [DataMember]
            public string thYourRef { get; set; }

            public string thLongYourRef { get; set; }

            public string thDelAddress1 { get; set; }

            public string thDelAddress2 { get; set; }

            public string thDelAddress3 { get; set; }

            public string thDelAddress4 { get; set; }

            public string thDelAddress5 { get; set; }

            public string thDeliveryPostcode { get; set; }

            public int thCurrency { get; set; }

            public string CurrencySymbol { get; set; }

            public int thDailyRate { get; set; }

            public int thCompanyRate { get; set; }

            public int thVATDailyRate { get; set; }

            public float thNetValue { get; set; }

            public bool thManualVAT { get; set; }

            public float thTotal0 { get; set; }

            public float thTotal1 { get; set; }

            public float thTotal5 { get; set; }

            public float thTotal6 { get; set; }

            public float thTotal8 { get; set; }

            public float thTotal9 { get; set; }

            [DataMember]
            public POLine[] thLines { get; set; }

            public object[] thNotes { get; set; }

            public object[] thLinks { get; set; }

            public string MD5Hash { get; set; }

            public string thUserField1 { get; set; }

            public string thUserField2 { get; set; }

            public string thUserField3 { get; set; }

            //public string thUserField4 { get; set; }
        }


        [DataContract]
        public class TransactionGet
        {
            public TransactionGet()
            { }
            public TransactionGet(string strCompanyID, string strPQU)
            {
                CompanyID = strCompanyID;
                OurRef = strPQU;
            }
            [DataMember]
            public string CompanyID { get; set; }

            [DataMember]
            public string OurRef { get; set; }
        }
        #endregion
}

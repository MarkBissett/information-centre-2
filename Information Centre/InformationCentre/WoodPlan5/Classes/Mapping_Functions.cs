using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Drawing;  // For ScaleBar //
using System.Drawing.Drawing2D;  // For PenAlignmet call on ScaleBar //

using System.Data;  // For Show_Map_From_Screen //
using System.Data.SqlClient;  // For Show_Map_From_Screen //

using DevExpress.Utils;  // For WaitDialogForm //

using WoodPlan5;
using BaseObjects;

using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;


namespace WoodPlan5
{
    class Mapping_Functions
    {
        public int Get_Mapping_Workspace(GlobalSettings globalSettings, ref int intWorkSpaceID, ref int intWorkSpaceOwner, ref string strWorkspaceName)
        {
            // *** Some parameters passed by ref so their value is automatically updated on calling object *** //
            // Check for a default Mapping Workspace to use //
            DataSet_AT_TreePicker dataSet_AT_TreePicker = new DataSet_AT_TreePicker();
            WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter();
            sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter.Connection.ConnectionString = globalSettings.ConnectionString;
            sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter.ClearBeforeFill = true;
            try
            {
                sp01263_AT_Tree_Picker_Get_Default_WorkspaceTableAdapter.Fill(dataSet_AT_TreePicker.sp01263_AT_Tree_Picker_Get_Default_Workspace, globalSettings.UserID, globalSettings.PersonType);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load mapping - an error occurred while attempting to retrieve the default Mapping Workspace.\n\nThe Mapping screen will now close. Try opening again - if the problem persists, contact Technical Support.", "Get Default Mapping Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
            if (dataSet_AT_TreePicker.sp01263_AT_Tree_Picker_Get_Default_Workspace.Rows.Count >= 1)
            {
                // Load first found default //
                intWorkSpaceID = Convert.ToInt32(dataSet_AT_TreePicker.sp01263_AT_Tree_Picker_Get_Default_Workspace.Rows[0]["WorkspaceID"]);
                intWorkSpaceOwner = Convert.ToInt32(dataSet_AT_TreePicker.sp01263_AT_Tree_Picker_Get_Default_Workspace.Rows[0]["CreatedByID"]);
                strWorkspaceName = Convert.ToString(dataSet_AT_TreePicker.sp01263_AT_Tree_Picker_Get_Default_Workspace.Rows[0]["WorkSpaceName"]);
                return intWorkSpaceID;  
            }
            else
            {
                // No Default Mapping Workspace found, so either none marked as default or no workspaces available to user so open WorkSpace Manager screen modally //
                frm_AT_Mapping_Workspace_Select fm_AT_Mapping_Workspace_Select = new frm_AT_Mapping_Workspace_Select();
                fm_AT_Mapping_Workspace_Select.GlobalSettings = globalSettings;
                if (fm_AT_Mapping_Workspace_Select.ShowDialog() == DialogResult.Cancel)
                {
                    return 0;
                }
                else
                {
                    intWorkSpaceID = fm_AT_Mapping_Workspace_Select.intWorkspaceID;
                    intWorkSpaceOwner = fm_AT_Mapping_Workspace_Select.intWorkspaceOwner;
                    strWorkspaceName = fm_AT_Mapping_Workspace_Select.strWorkspaceName;
                    return fm_AT_Mapping_Workspace_Select.intWorkspaceID;
                }
            }
        }

        public int Show_Map_From_Screen(GlobalSettings globalSettings, string strConnectionString, Form frmCallingForm, string strPassedIDs, string strPassedIDType)
        {
            bool boolFireGazetteerCode = false;
            string strAddressDescription = "";
            decimal decDistanceRange = (decimal)0.00;
            decimal decX = (decimal)0.00;
            decimal decY = (decimal)0.00;
            decimal decRangeAdjusted = (decimal)0.00;
            int intWorkSpaceID = 0;
            int intWorkspaceOwner = 0;
            string strWorkspaceName = "";
            Get_Mapping_Workspace(globalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
            if (intWorkSpaceID == 0) return 0;
            frm_AT_Mapping_Tree_Picker fm_AT_Mapping_Tree_Picker = new frm_AT_Mapping_Tree_Picker(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            WaitDialogForm loading = new WaitDialogForm("Preparing Mapping, Please Wait...", "WoodPlan Mapping");
            loading.Show();

            // Get Highlight Colour //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intHighlightColour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTreeHighlightColour"));

            // Get Show Other Trees Status //
            string strShowOtherTrees = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees").ToString();
            string strTreeIDsToShow = "";
            string strTreeIDsToHighlight = "";
            int intCentreX = 0;
            int intCentreY = 0;
            string strClientIDsToExpandOnMap = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;

            // Check if we need to get list of clients and trees to pre-populate with or if the map centre X and Y and client have been passed instead of tree IDs...//
            char[] delimiters = new char[] { ';' };
            string[] strArray;
            strArray = strPassedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray[0].ToString() == "XYclient" && strArray.Length == 4)  // X and Y and client have been passed in //
            {
                intCentreX = Convert.ToInt32(strArray[1]);
                intCentreY = Convert.ToInt32(strArray[2]);
                strClientIDsToExpandOnMap = strArray[3].ToString() + ",";
                // Get List of Trees to be shown on the map (from all trees matching the client ID) //
                SQlConn = new SqlConnection(strConnectionString);
                cmd = null;
                cmd = new SqlCommand("sp01317_AT_Get_All_Trees_For_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strClientIDsToExpandOnMap));
                cmd.Parameters.Add(new SqlParameter("@IDType", "client"));
                SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
                DataSet dsTrees = new DataSet("NewDataSet");
                sdaTrees.Fill(dsTrees, "Table");
                foreach (DataRow dr in dsTrees.Tables[0].Rows)
                {
                    strTreeIDsToShow += dr["TreeID"].ToString() + ",";
                }
            }
            else if (strArray[0].ToString() == "XYincident" && strArray.Length == 5)  // X and Y and Distance Range and Address have been passed in //
            {
                intCentreX = Convert.ToInt32(strArray[1]);
                intCentreY = Convert.ToInt32(strArray[2]);
                decDistanceRange = Convert.ToDecimal(strArray[3]);
                strAddressDescription = strArray[4].ToString();
                decX = Convert.ToDecimal(intCentreX);
                decY = Convert.ToDecimal(intCentreY);
                //decRangeAdjusted = (decDistanceRange * (decimal)100) / (decimal)113.5;  // Gets round size of circle drawn (always 13.5% too big) //
                decRangeAdjusted = decDistanceRange;

                if (decX != (decimal)0.00 || decY != (decimal)0.00)
                {
                    boolFireGazetteerCode = true;
                    if (decRangeAdjusted > (decimal)0.00)
                    {
                        loading.SetCaption("Checking For Nearby Map Objects...");
                        Application.DoEvents();
                        SQlConn = new SqlConnection(strConnectionString);
                        cmd = null;
                        cmd = new SqlCommand("sp01320_AT_Gazetteer_Get_Map_Objects_In_Range", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@decX", decX));
                        cmd.Parameters.Add(new SqlParameter("@decY", decY));
                        cmd.Parameters.Add(new SqlParameter("@decRange", decDistanceRange));
                        SqlDataAdapter sdaTreesToShow = new SqlDataAdapter(cmd);
                        DataSet dsTreesToShow = new DataSet("NewDataSet");
                        try
                        {
                            sdaTreesToShow.Fill(dsTreesToShow, "Table");
                        }
                        catch (Exception ex)
                        {
                            loading.Close();
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map objects within " + decDistanceRange.ToString() + (decDistanceRange == (decimal)1.00 ? " metre" : " metres") + " of the current address.\n\nError: " + ex.Message, "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return 0;

                        }
                        if (dsTreesToShow.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsTreesToShow.Tables[0].Rows)
                            {
                                strTreeIDsToShow += dr["TreeID"].ToString() + ",";
                            }
                        }
                        //strTreeIDsToHighlight = strTreeIDsToShow;  // Commented out because we don't want to highlight the objects, just show them. //
                        loading.SetCaption("Preparing Mapping, Please Wait...");
                        Application.DoEvents();
                    }
                }
            }

            else  // List of Tree IDs to be highlighted has been passed in //
            {
                // Get list of parent clients for the work order to pass to mapping //
                cmd = new SqlCommand("sp01316_AT_Get_ClientIDs_From_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                SqlDataAdapter sdaClients = new SqlDataAdapter(cmd);
                DataSet dsClients = new DataSet("NewDataSet");
                sdaClients.Fill(dsClients, "Table");
                foreach (DataRow dr in dsClients.Tables[0].Rows)
                {
                    strClientIDsToExpandOnMap += dr["ClientID"].ToString() + ",";
                }

                // Get list of work order trees to highlight on the map //
                if (strPassedIDType.ToLower() == "tree")
                {
                    strTreeIDsToHighlight = strPassedIDs;
                }
                else // Retrieve the TreeIDs from the Database //
                {

                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp01317_AT_Get_All_Trees_For_IDs", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                    cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                    SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
                    DataSet dsTrees = new DataSet("NewDataSet");
                    sdaTrees.Fill(dsTrees, "Table");
                    foreach (DataRow dr in dsTrees.Tables[0].Rows)
                    {
                        strTreeIDsToHighlight += dr["TreeID"].ToString() + ",";
                    }
                }

                // Get list of trees to be shown on the map //
                if (strShowOtherTrees.ToLower() != "show")
                {
                    strTreeIDsToShow = strTreeIDsToHighlight;  // Other trees not shown so just set to the same trees to be highlighted //
                }
                else
                {
                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp01310_AT_WorkOrder_Print_Mapping_TreeIDs_Shown", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@TreeIDs", strTreeIDsToHighlight));
                    SqlDataAdapter sdaTreesShow = new SqlDataAdapter(cmd);
                    DataSet dsTreesShow = new DataSet("NewDataSet");
                    sdaTreesShow.Fill(dsTreesShow, "Table");
                    foreach (DataRow dr in dsTreesShow.Tables[0].Rows)
                    {
                        strTreeIDsToShow += dr["TreeID"].ToString() + ",";
                    }
                }
            }

            // Check if Mapping form (intFormID = 2004) is already open. If yes, activate it and reload contents otherwise open it //
            try
            {
                frmMain2 frmMDI;
                if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                {
                    frmMDI = (frmMain2)frmCallingForm;
                }
                else  // We don't have the MDI form so get the current form's parent //
                {
                    frmMDI = (frmMain2)frmCallingForm.MdiParent;
                }
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 2004)
                    {
                        frmChild.Activate();
                        frm_AT_Mapping_Tree_Picker frmTreePicker = (frm_AT_Mapping_Tree_Picker)frmChild;
                        if (frmTreePicker.btnGPS_Start.Text == "Stop GPS") frmTreePicker.btnGPS_Start.PerformClick();  // Stop GPS by simulating Click on GPS Button //
                        frmTreePicker.i_intCallingModule = 1;  // Amenity Trees //
                        frmTreePicker.i_str_selected_client_ids = strClientIDsToExpandOnMap;
                        frmTreePicker.strVisibleIDs = strTreeIDsToShow;
                        frmTreePicker.strHighlightedIDs = strTreeIDsToHighlight;
                        frmTreePicker.intInitialHighlightColour = intHighlightColour;
                        frmTreePicker.intPassedMapX = intCentreX;
                        frmTreePicker.intPassedMapY = intCentreY;
                        //frmTreePicker.DeselectAllHighlightedMapObjects();
                        frmTreePicker.Pre_Select_Clients();
                        frmTreePicker.Load_Map_Objects_Grid();
                        frmTreePicker.Load_Layer_Manager();
                        if (boolFireGazetteerCode) frmTreePicker.Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decDistanceRange, strAddressDescription, decRangeAdjusted);
                        loading.Close();
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
            }
            loading.Close();
            // If we are here then the form was not already open, so open it //
            frmProgress fProgress = new frmProgress(10);
            frmCallingForm.AddOwnedForm(fProgress);

            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();
            fm_AT_Mapping_Tree_Picker.fProgress = fProgress;
            if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
            {
                fm_AT_Mapping_Tree_Picker.MdiParent = frmCallingForm;
            }
            else  // We don't have the MDI form so get the current form's parent //
            {
                fm_AT_Mapping_Tree_Picker.MdiParent = frmCallingForm.MdiParent;
            }
            fm_AT_Mapping_Tree_Picker.GlobalSettings = globalSettings;

            // Other Data Configuration Parameters for Map Below... //
            fm_AT_Mapping_Tree_Picker.i_str_selected_client_ids = strClientIDsToExpandOnMap;
            fm_AT_Mapping_Tree_Picker.strVisibleIDs = strTreeIDsToShow;
            fm_AT_Mapping_Tree_Picker.strHighlightedIDs = strTreeIDsToHighlight;
            fm_AT_Mapping_Tree_Picker.intInitialHighlightColour = intHighlightColour;
            fm_AT_Mapping_Tree_Picker.intPassedMapX = intCentreX;
            fm_AT_Mapping_Tree_Picker.intPassedMapY = intCentreY;
            fm_AT_Mapping_Tree_Picker.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fm_AT_Mapping_Tree_Picker, new object[] { null });
            if (boolFireGazetteerCode) fm_AT_Mapping_Tree_Picker.Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decDistanceRange, strAddressDescription, decRangeAdjusted);

            return 1;
        }

        public int Show_Map_From_Assets_Screen(GlobalSettings globalSettings, string strConnectionString, Form frmCallingForm, string strPassedIDs, string strPassedIDType)
        {
            bool boolFireGazetteerCode = false;
            string strAddressDescription = "";
            decimal decDistanceRange = (decimal)0.00;
            decimal decX = (decimal)0.00;
            decimal decY = (decimal)0.00;
            decimal decRangeAdjusted = (decimal)0.00;
            int intWorkSpaceID = 0;
            int intWorkspaceOwner = 0;
            string strWorkspaceName = "";
            int intCentreX = 0;
            int intCentreY = 0;

            char[] delimiters = new char[] { ';' };
            string[] strArray;
            strArray = strPassedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray[0].ToString() == "XYsite" && strArray.Length == 4)  // X and Y and client have been passed in //
            {
                strPassedIDs = strArray[3].ToString() + ",";
            }

            // Try and get correct Workspace from passed in Record Type //
            // Get Amenity Trees Updated Objects //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp03071_Tree_Picker_Get_Assets_Workspace", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@strRecordType", strPassedIDType));
            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", strPassedIDs));
            SqlDataAdapter sdaWorkspaces = new SqlDataAdapter();
            DataSet dsWorkspaces = new DataSet("NewDataSet");
            dsWorkspaces.Clear();  // Remove any old values first //
            sdaWorkspaces = new SqlDataAdapter(cmd);
            sdaWorkspaces.Fill(dsWorkspaces, "Table");
            cmd = null;

            if (dsWorkspaces.Tables[0].Rows.Count <= 0) // No Workspace, so see if there is a default one for the current user //
            {
                Get_Mapping_Workspace(globalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
            }
            else  // Mapping Workspace found so get details from it... //
            {
                DataRow dr = dsWorkspaces.Tables[0].Rows[0];
                intWorkSpaceID = Convert.ToInt32(dr["WorkspaceID"]);
                intWorkspaceOwner = Convert.ToInt32(dr["CreatedByID"]);
                strWorkspaceName = Convert.ToString(dr["WorkspaceName"]);
            }
            if (intWorkSpaceID == 0) return 0;
            frm_AT_Mapping_Tree_Picker fm_AT_Mapping_Tree_Picker = new frm_AT_Mapping_Tree_Picker(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 2);

            WaitDialogForm loading = new WaitDialogForm("Preparing Mapping, Please Wait...", "WoodPlan Mapping");
            loading.Show();

            // Get Highlight Colour //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intHighlightColour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTreeHighlightColour"));

            // Get Show Other Asset Objects Status    ***** CURRENTLY USING AMENITY TREES SETTING ***** //
            string strShowOtherAssets = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees").ToString();
            string strAssetIDsToShow = "";
            string strAssetIDsToHighlight = "";
            string strSiteIDsToExpandOnMap = "";
            cmd = null;

            // Check if we need to get list of sites and assets to pre-populate with or if the map centre X and Y and client have been passed instead of asset IDs...//
            if (strArray[0].ToString() == "XYsite" && strArray.Length == 4)  // X and Y and client have been passed in //
            {
                intCentreX = Convert.ToInt32(strArray[1]);
                intCentreY = Convert.ToInt32(strArray[2]);
                strSiteIDsToExpandOnMap = strArray[3].ToString() + ",";
                // Get List of Assets to be shown on the map (from all assets matching the site ID) //
                SQlConn = new SqlConnection(strConnectionString);
                cmd = null;
                cmd = new SqlCommand("sp03072_EP_Get_All_Assets_For_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strSiteIDsToExpandOnMap));
                cmd.Parameters.Add(new SqlParameter("@IDType", "client"));
                SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
                DataSet dsTrees = new DataSet("NewDataSet");
                sdaTrees.Fill(dsTrees, "Table");
                foreach (DataRow dr in dsTrees.Tables[0].Rows)
                {
                    strAssetIDsToShow += dr["AssetID"].ToString() + ",";
                }
            }

            else  // List of Tree IDs to be highlighted has been passed in //
            {
                // Get list of parent clients for the work order to pass to mapping //
                cmd = new SqlCommand("sp03073_AT_Get_SiteIDs_From_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                SqlDataAdapter sdaClients = new SqlDataAdapter(cmd);
                DataSet dsClients = new DataSet("NewDataSet");
                sdaClients.Fill(dsClients, "Table");
                foreach (DataRow dr in dsClients.Tables[0].Rows)
                {
                    strSiteIDsToExpandOnMap += dr["SiteID"].ToString() + ",";
                }

                // Get list of assets to highlight on the map //
                if (strPassedIDType.ToLower() == "asset")
                {
                    strAssetIDsToHighlight = strPassedIDs;
                }
                else // Retrieve the AssetIDs from the Database //
                {

                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp03072_EP_Get_All_Assets_For_IDs", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                    cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                    SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
                    DataSet dsTrees = new DataSet("NewDataSet");
                    sdaTrees.Fill(dsTrees, "Table");
                    foreach (DataRow dr in dsTrees.Tables[0].Rows)
                    {
                        strAssetIDsToHighlight += dr["AssetID"].ToString() + ",";
                    }
                }

                // Get list of trees to be shown on the map //
                if (strShowOtherAssets.ToLower() != "show")
                {
                    strAssetIDsToShow = strAssetIDsToHighlight;  // Other trees not shown so just set to the same trees to be highlighted //
                }
                else
                {
                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp03074_AT_Get_All_Assets_For_IDs", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AssetIDs", strAssetIDsToHighlight));
                    SqlDataAdapter sdaTreesShow = new SqlDataAdapter(cmd);
                    DataSet dsTreesShow = new DataSet("NewDataSet");
                    sdaTreesShow.Fill(dsTreesShow, "Table");
                    foreach (DataRow dr in dsTreesShow.Tables[0].Rows)
                    {
                        strAssetIDsToShow += dr["AssetID"].ToString() + ",";
                    }
                }
            }

            // Check if Mapping form (intFormID = 2004) is already open. If yes, activate it and reload contents otherwise open it //
            try
            {
                frmMain2 frmMDI;
                if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                {
                    frmMDI = (frmMain2)frmCallingForm;
                }
                else  // We don't have the MDI form so get the current form's parent //
                {
                    frmMDI = (frmMain2)frmCallingForm.MdiParent;
                }
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 2004)
                    {
                        frmChild.Activate();
                        frm_AT_Mapping_Tree_Picker frmTreePicker = (frm_AT_Mapping_Tree_Picker)frmChild;
                        if (frmTreePicker.btnGPS_Start.Text == "Stop GPS") frmTreePicker.btnGPS_Start.PerformClick();  // Stop GPS by simulating Click on GPS Button //
                        frmTreePicker.i_intCallingModule = 2;  // Assets //
                        frmTreePicker.i_str_selected_site_ids2 = strSiteIDsToExpandOnMap;
                        frmTreePicker.strVisibleAssetIDs = strAssetIDsToShow;
                        frmTreePicker.strHighlightedAssetIDs = strAssetIDsToHighlight;
                        frmTreePicker.intInitialHighlightColour = intHighlightColour;
                        frmTreePicker.intPassedMapX = intCentreX;
                        frmTreePicker.intPassedMapY = intCentreY;
                        //frmTreePicker.DeselectAllHighlightedMapObjects();
                        frmTreePicker.Pre_Select_Sites();
                        frmTreePicker.Load_Map_Objects_Grid_Assets();
                        frmTreePicker.Load_Layer_Manager();
                        if (boolFireGazetteerCode) frmTreePicker.Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decDistanceRange, strAddressDescription, decRangeAdjusted);
                        loading.Close();
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
            }
            loading.Close();
            // If we are here then the form was not already open, so open it //
            frmProgress fProgress = new frmProgress(10);
            frmCallingForm.AddOwnedForm(fProgress);

            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();
            fm_AT_Mapping_Tree_Picker.fProgress = fProgress;
            if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
            {
                fm_AT_Mapping_Tree_Picker.MdiParent = frmCallingForm;
            }
            else  // We don't have the MDI form so get the current form's parent //
            {
                fm_AT_Mapping_Tree_Picker.MdiParent = frmCallingForm.MdiParent;
            }
            fm_AT_Mapping_Tree_Picker.GlobalSettings = globalSettings;

            // Other Data Configuration Parameters for Map Below... //
            fm_AT_Mapping_Tree_Picker.i_str_selected_site_ids2 = strSiteIDsToExpandOnMap;
            fm_AT_Mapping_Tree_Picker.strVisibleAssetIDs = strAssetIDsToShow;
            fm_AT_Mapping_Tree_Picker.strHighlightedAssetIDs = strAssetIDsToHighlight;
            fm_AT_Mapping_Tree_Picker.intInitialHighlightColour = intHighlightColour;
            fm_AT_Mapping_Tree_Picker.intPassedMapX = intCentreX;
            fm_AT_Mapping_Tree_Picker.intPassedMapY = intCentreY;
            fm_AT_Mapping_Tree_Picker.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fm_AT_Mapping_Tree_Picker, new object[] { null });
            if (boolFireGazetteerCode) fm_AT_Mapping_Tree_Picker.Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decDistanceRange, strAddressDescription, decRangeAdjusted);

            return 1;
        }


        public int Get_Mapping_Workspace_Owner(GlobalSettings globalSettings, int intWorkSpaceID)
        {
            int intWorkSpaceOwner = 0;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter getOwner = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            getOwner.ChangeConnectionString(globalSettings.ConnectionString);
            try
            {
                intWorkSpaceOwner = Convert.ToInt32(getOwner.sp07109_UT_Mapping_Get_Workspace_Owner(intWorkSpaceID));
            }
            catch (Exception)
            {
            }
            return intWorkSpaceOwner;
        }


        public MapInfo.Geometry.FeatureGeometry CreateLatLongFeatureFromNonLatLongCoords(float dNorthing, float dEasting, string strPolygonXY)
        {
            MapInfo.Geometry.FeatureGeometry ftr = null;
            if (!string.IsNullOrEmpty(strPolygonXY))  // polygon or polyline //
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray;
                // Check there is an even number of points //
                strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length % 2 != 0)  // Modulus (%) //
                {
                    return ftr;  // Unable to do anything with it as it has an invalid number of coordinates (odd number)
                }
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                int iPoint = 0;
                for (int j = 0; j < strArray.Length - 1; j++)
                {
                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                    iPoint++;
                }

                MapInfo.Geometry.CoordSys coord1 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys("mapinfo:coordsys 8,79,7,-2,49,0.9996012717,400000,-100000");

                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                {
                    MapInfo.Geometry.MultiPolygon g = new MapInfo.Geometry.MultiPolygon(coord1, MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                    MapInfo.Geometry.MultiPolygon gConverted = Convert_Polygon_Between_Projections(g);
                    return (MapInfo.Geometry.FeatureGeometry)gConverted;
                }
                else
                {
                    MapInfo.Geometry.MultiCurve g = new MapInfo.Geometry.MultiCurve(coord1, MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                    MapInfo.Geometry.MultiCurve gConverted = Convert_Polyline_Between_Projections(g);
                    return (MapInfo.Geometry.FeatureGeometry)gConverted;
                }
            }
            else
            {
                MapInfo.Geometry.Point gConverted = Convert_Point_Between_Projections(dNorthing, dEasting);
                return (MapInfo.Geometry.FeatureGeometry)gConverted;
            }


        }

        public void GetLatLongCoordsFromLatLongFeature(MapInfo.Geometry.FeatureGeometry ftr, ref float flLatLongX, ref float flLatLongY, ref string strLatLongPolygonXY)
        {
            float flPreviousX = (float)-99999;
            float flPreviousY = (float)-99999;
            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area / length calculation ///

            strLatLongPolygonXY = "";
            switch (ftr.Type)
            {
                case GeometryType.MultiPolygon:
                    {
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        foreach (MapInfo.Geometry.Polygon poly in (MapInfo.Geometry.MultiPolygon)ftr)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flLatLongX = (float)pt.x;
                                    flLatLongY = (float)pt.y;
                                    if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                                    {
                                        strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                        flPreviousX = flLatLongX;
                                        flPreviousY = flLatLongY;
                                    }
                                }
                            }
                        }
                        flLatLongX = (float)ftr.Centroid.x;
                        flLatLongY = (float)ftr.Centroid.y;
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    {
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)ftr;
                        MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flLatLongX = (float)dptArr[i].x;
                            flLatLongY = (float)dptArr[i].y;
                            if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                            {
                                strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                flPreviousX = flLatLongX;
                                flPreviousY = flLatLongY;
                            }
                        }
                        flLatLongX = (float)ftr.Centroid.x;
                        flLatLongY = (float)ftr.Centroid.y;
                    }
                    break;
                case GeometryType.Point:
                    {
                        flLatLongX = (float)ftr.Centroid.x;
                        flLatLongY = (float)ftr.Centroid.y;
                        strLatLongPolygonXY = "";
                    }
                    break;
           }
           return;         
        }



        public int View_Object_In_Google_Maps(Form frmCallingForm, Double dNorthing, Double dEasting, string strPolygonXY)
        {
            if (dNorthing == (double)0 && dEasting == (double)0)  // No X and Y so attempt to calculate one (may be a polygon or polyline with no stored centroid //
            {
                if (string.IsNullOrEmpty(strPolygonXY))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display object - object has no mapping coordinates associated with it.", "View Object with Google Maps", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return 0;  // Unable to do anything so abort //
                }
                char[] delimiters = new char[] { ';' };
                string[] strArray;
                // Check there is an even number of points //
                strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length % 2 != 0)  // Modulus (%) //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display object - object has invalid mapping coordinates associated with it.", "View Object with Google Maps", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                int iPoint = 0;
                for (int j = 0; j < strArray.Length - 1; j++)
                {
                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                    iPoint++;
                }

                MapInfo.Geometry.CoordSys coord1 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys("mapinfo:coordsys 8,79,7,-2,49,0.9996012717,400000,-100000");

                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                {
                    MapInfo.Geometry.MultiPolygon g = new MapInfo.Geometry.MultiPolygon(coord1, MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                    MapInfo.Geometry.MultiPolygon gConverted = Convert_Polygon_Between_Projections(g);
                    frm_AT_Google_Maps frmChild = new frm_AT_Google_Maps(gConverted.Centroid.y.ToString(), gConverted.Centroid.x.ToString());
                    frmChild.Show(frmCallingForm);
                }
                else
                {
                    MapInfo.Geometry.MultiCurve g = new MapInfo.Geometry.MultiCurve(coord1, MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                    MapInfo.Geometry.MultiCurve gConverted = Convert_Polyline_Between_Projections(g);
                    frm_AT_Google_Maps frmChild = new frm_AT_Google_Maps(gConverted.Centroid.y.ToString(), gConverted.Centroid.x.ToString());
                    frmChild.Show(frmCallingForm);
                }
            }
            else
            {
                MapInfo.Geometry.Point gConverted = Convert_Point_Between_Projections(dNorthing, dEasting);
                frm_AT_Google_Maps frmChild = new frm_AT_Google_Maps(gConverted.Y.ToString(), gConverted.X.ToString());
                frmChild.Show(frmCallingForm);
            }

            return 1;
        }

        public int View_Object_In_Internet_Mapping(GlobalSettings globalSettings, string strConnectionString, Form frmCallingForm, List<Mapping_Objects> mapObjectsList)
        {
            frm_Core_Generic_Mapping fChildForm = new frm_Core_Generic_Mapping();
            if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
            {
                fChildForm.MdiParent = frmCallingForm;
            }
            else  // We don't have the MDI form so get the current form's parent //
            {
                fChildForm.MdiParent = frmCallingForm.MdiParent;
            }
            fChildForm.GlobalSettings = globalSettings;
            fChildForm.strCaller = frmCallingForm.Name;
            fChildForm.ListPassedInObjects = mapObjectsList;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });

            return 1;
        }

        public MapInfo.Geometry.Point Convert_Point_Between_Projections(double doubleX, double doubleY)
        {
            //DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            //GetSetting.ChangeConnectionString(strConnectionString);
            //string strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            //if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";
            
            MapInfo.Geometry.CoordSys coord1 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys("mapinfo:coordsys 8,79,7,-2,49,0.9996012717,400000,-100000");
            
            CoordSysFactory factory = new CoordSysFactory();
            Datum datum = factory.CreateDatum(DatumID.WGS84);
            MapInfo.Geometry.CoordSys coord2 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys(CoordSysType.LongLat, datum, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null);
            
            MapInfo.Geometry.Point pt1 = new MapInfo.Geometry.Point(coord1, doubleX, doubleY);
            MapInfo.Geometry.Point pt2 = pt1.Copy(coord2) as MapInfo.Geometry.Point;
            return pt2;
        }

        public MapInfo.Geometry.MultiPolygon Convert_Polygon_Between_Projections(MapInfo.Geometry.MultiPolygon poly)
        {
            CoordSysFactory factory = new CoordSysFactory();
            Datum datum = factory.CreateDatum(DatumID.WGS84);
            MapInfo.Geometry.CoordSys coord2 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys(CoordSysType.LongLat, datum, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null);

            MapInfo.Geometry.MultiPolygon poly2 = poly.Copy(coord2) as MapInfo.Geometry.MultiPolygon;
            return poly2;
        }

        public MapInfo.Geometry.MultiCurve Convert_Polyline_Between_Projections(MapInfo.Geometry.MultiCurve poly)
        {
            CoordSysFactory factory = new CoordSysFactory();
            Datum datum = factory.CreateDatum(DatumID.WGS84);
            MapInfo.Geometry.CoordSys coord2 = MapInfo.Engine.Session.Current.CoordSysFactory.CreateCoordSys(CoordSysType.LongLat, datum, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null);

            MapInfo.Geometry.MultiCurve poly2 = poly.Copy(coord2) as MapInfo.Geometry.MultiCurve;
            return poly2;
        }



        public int Gazetteer_Map_Object_Search(GlobalSettings globalSettings, string strConnectionString, Form frmCallingForm, string strPassedIDs, string strPassedIDType, string strPassedAssetIDs, string strPassedAssetIDType)
        {

            WaitDialogForm loading = new WaitDialogForm("Preparing Mapping, Please Wait...", "WoodPlan Mapping");
            loading.Show();

            // Get Highlight Colour //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intHighlightColour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTreeHighlightColour"));

            // Get Show Other Trees Status //
            string strShowOtherTrees = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderPrintShowDetailsOfOtherTrees").ToString();
            string strTreeIDsToShow = "";
            string strTreeIDsToHighlight = "";
            string strAssetIDsToShow = "";
            string strAssetIDsToHighlight = "";
            int intCentreX = 0;
            int intCentreY = 0;
            string strClientIDsToExpandOnMap = "";
            string strSiteIDsToExpandOnMap = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;

            if (!string.IsNullOrEmpty(strPassedIDs))  // Get Amenity Trees //
            {
                // Get list of parent clients for the work order to pass to mapping //
                cmd = new SqlCommand("sp01316_AT_Get_ClientIDs_From_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                SqlDataAdapter sdaClients = new SqlDataAdapter(cmd);
                DataSet dsClients = new DataSet("NewDataSet");
                sdaClients.Fill(dsClients, "Table");
                foreach (DataRow dr in dsClients.Tables[0].Rows)
                {
                    strClientIDsToExpandOnMap += dr["ClientID"].ToString() + ",";
                }

                // Get list of trees to highlight on the map //
                if (strPassedIDType.ToLower() == "tree")
                {
                    strTreeIDsToHighlight = strPassedIDs;
                }
                else // Retrieve the TreeIDs from the Database //
                {

                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp01317_AT_Get_All_Trees_For_IDs", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IDs", strPassedIDs));
                    cmd.Parameters.Add(new SqlParameter("@IDType", strPassedIDType));
                    SqlDataAdapter sdaTrees = new SqlDataAdapter(cmd);
                    DataSet dsTrees = new DataSet("NewDataSet");
                    sdaTrees.Fill(dsTrees, "Table");
                    foreach (DataRow dr in dsTrees.Tables[0].Rows)
                    {
                        strTreeIDsToHighlight += dr["TreeID"].ToString() + ",";
                    }
                }
                strTreeIDsToShow = strTreeIDsToHighlight;  // Other trees not shown so just set to the same trees to be highlighted //
            }
            if (!string.IsNullOrEmpty(strPassedAssetIDs))  // Get Assets //
            {
                // Get list of parent sites for the work order to pass to mapping //
                cmd = null;
                cmd = new SqlCommand("sp03073_AT_Get_SiteIDs_From_IDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IDs", strPassedAssetIDs));
                cmd.Parameters.Add(new SqlParameter("@IDType", strPassedAssetIDType));
                SqlDataAdapter sdaSites = new SqlDataAdapter(cmd);
                DataSet dsSites = new DataSet("NewDataSet");
                sdaSites.Fill(dsSites, "Table");
                foreach (DataRow dr in dsSites.Tables[0].Rows)
                {
                    strSiteIDsToExpandOnMap += dr["SiteID"].ToString() + ",";
                }

                // Get list of trees to highlight on the map //
                if (strPassedAssetIDType.ToLower() == "asset")
                {
                    strAssetIDsToHighlight = strPassedAssetIDs;
                }
                else // Retrieve the AssetIDs from the Database //
                {

                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = null;
                    cmd = new SqlCommand("sp03072_EP_Get_All_Assets_For_IDs", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IDs", strPassedAssetIDs));
                    cmd.Parameters.Add(new SqlParameter("@IDType", strPassedAssetIDType));
                    SqlDataAdapter sdaAssets = new SqlDataAdapter(cmd);
                    DataSet dsAssets = new DataSet("NewDataSet");
                    sdaAssets.Fill(dsAssets, "Table");
                    foreach (DataRow dr in dsAssets.Tables[0].Rows)
                    {
                        strAssetIDsToHighlight += dr["AssetID"].ToString() + ",";
                    }
                }
                strAssetIDsToShow = strAssetIDsToHighlight;  // Other trees not shown so just set to the same trees to be highlighted //
            }

            // Activate Mapping form and reload contents //
            try
            {
                frmMain2 frmMDI;
                if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                {
                    frmMDI = (frmMain2)frmCallingForm;
                }
                else  // We don't have the MDI form so get the current form's parent //
                {
                    frmMDI = (frmMain2)frmCallingForm.MdiParent;
                }
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 2004)
                    {
                        frmChild.Activate();
                        frm_AT_Mapping_Tree_Picker frmTreePicker = (frm_AT_Mapping_Tree_Picker)frmChild;
                        frmTreePicker.i_str_selected_client_ids = strClientIDsToExpandOnMap;
                        frmTreePicker.strVisibleIDs = strTreeIDsToShow;
                        frmTreePicker.strHighlightedIDs = strTreeIDsToHighlight;
                        frmTreePicker.intInitialHighlightColour = intHighlightColour;
                        frmTreePicker.intPassedMapX = intCentreX;
                        frmTreePicker.intPassedMapY = intCentreY;
                        //frmTreePicker.DeselectAllHighlightedMapObjects();
                        frmTreePicker.Pre_Select_Clients();

                        frmTreePicker.i_str_selected_site_ids2 = strSiteIDsToExpandOnMap;
                        frmTreePicker.strAssetTypeIDs = "";  // Clear any Asset Type Filter //
                        frmTreePicker.strVisibleAssetIDs = strAssetIDsToShow;
                        frmTreePicker.strHighlightedAssetIDs = strAssetIDsToHighlight;
                        frmTreePicker.Pre_Select_Sites();

                        frmTreePicker.Load_Map_Objects_Grid();
                        frmTreePicker.Load_Map_Objects_Grid_Assets();
                        //frmTreePicker.Load_Layer_Manager();  // This has been commented out as it re-loads the map layers and everthing else - replaced with following 2 lines...//
                        frmTreePicker.Update_Map_Object_Grid_Visible_And_Highlighted();
                        frmTreePicker.UpdateMapObjectsDisplayed(false);
                        
                        loading.Close();
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
            }
            loading.Close();
            return 1;
        }

        public Image Create_Scale_Bar(int ParentWidth, int ParentHeight, int MapScale)
        {
            Image imageScaleBar = null;
            int intStartX = 5;
            int intStartY = 16;// 20;
            Brush brush1;
            Brush brush2;
            Brush textBrush = new SolidBrush(Color.Black);

            if (ParentWidth < 10 || MapScale <= 9)
            {
                // Create Bitmap //
                imageScaleBar = new Bitmap((ParentWidth > 55 ? ParentWidth : 55), (ParentHeight > 55 ? ParentHeight : 55));
                using (Graphics G = Graphics.FromImage(imageScaleBar))
                {

                    DrawText(G, "Scale Bar Unavailable", intStartX, -1, textBrush, (float)8.25, FontStyle.Regular);
                    return imageScaleBar;
                }
            }

            string strUnitDesc = "";
            decimal increment = 0;
            decimal incrementDisplayed = 0;
            if (MapScale >= 0 && MapScale <= 30)
            {
                strUnitDesc = "millimetres";
                incrementDisplayed = 250;
                increment = (decimal)0.25;
            }
            else if (MapScale >= 31 && MapScale <= 49)
            {
                strUnitDesc = "millimetres";
                incrementDisplayed = 500;
                increment = (decimal)0.50;
            }
            else if (MapScale >= 50 && MapScale <= 125)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 1;
                increment = 1;
            }
            else if (MapScale >= 126 && MapScale <= 300)
            {
                strUnitDesc = "metres";
                incrementDisplayed = (decimal)2.5;
                increment = (decimal)2.5;
            }
            else if (MapScale >= 301 && MapScale <= 500)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 5;
                increment = 5;
            }
            else if (MapScale >= 501 && MapScale <= 1250)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 10;
                increment = 10;
            }
            else if (MapScale >= 1251 && MapScale <= 3000)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 25;
                increment = 25;
            }
            else if (MapScale >= 3001 && MapScale <= 5000)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 50;
                increment = 50;
            }
            else if (MapScale >= 5001 && MapScale <= 12500)
            {
                strUnitDesc = "metres";
                incrementDisplayed = 100;
                increment = 100;
            }
            else if (MapScale >= 12501 && MapScale <= 30000)
            {
                strUnitDesc = "kilometres";
                incrementDisplayed = (decimal)0.25;
                increment = 250;
            }
            else if (MapScale >= 30001 && MapScale <= 50000)
            {
                strUnitDesc = "kilometres";
                incrementDisplayed = (decimal)0.5;
                increment = 500;
            }
            else if (MapScale >= 50000)
            {
                strUnitDesc = "kilometres";
                incrementDisplayed = 1;
                increment = 1;
            }

            int intWidth = 0;
            decimal decScaleText = 0;

            // Create Bitmap //
            imageScaleBar = new Bitmap((ParentWidth > 55 ? ParentWidth : 55), (ParentHeight > 55 ? ParentHeight : 55));

            bool boolBrush1LastUsed = false;
            SizeF fs;

            using (Graphics G = Graphics.FromImage(imageScaleBar))
            {
                //float floatFudgeFactor = ((float)increment / (float)MapScale == (float)0.01 ? (float)1 : (float)1.005);
                //intWidth = Convert.ToInt32((((((float)increment * 1000) / (float)25.4) * G.DpiX) / (float)MapScale) * floatFudgeFactor);

                intWidth = Convert.ToInt32(Math.Round((((((float)increment * 1000) / (float)25.4) * G.DpiX) / (float)MapScale), 2, MidpointRounding.ToEven));

                if (intWidth <= 0)
                {
                    DrawText(G, "Scale Bar Unavailable", intStartX, 1, textBrush, (float)8.25, FontStyle.Regular);
                    return imageScaleBar;
                }

                brush1 = new SolidBrush(Color.Red);
                brush2 = new SolidBrush(Color.White);

                G.DrawLine(new Pen(Color.Black, 1), new System.Drawing.Point(intStartX, intStartY - 5), new System.Drawing.Point(intStartX, intStartY));
                fs = G.MeasureString(decScaleText.ToString(), new System.Drawing.Font("Tahoma", (float)8.25, FontStyle.Regular), 200);
                DrawText(G, decScaleText.ToString(), intStartX - (Convert.ToInt32(fs.Width) / 2), -1, textBrush, (float)8.25, FontStyle.Regular);

                while ((intStartX + intWidth + 5) <= ParentWidth)
                {
                    if (boolBrush1LastUsed)
                    {
                        DrawShape(G, "Rectangle", intStartX, intStartY, Color.Black, brush1, intWidth);
                    }
                    else
                    {
                        DrawShape(G, "Rectangle", intStartX, intStartY, Color.Black, brush2, intWidth);
                    }
                    G.DrawLine(new Pen(Color.Black, 1), new System.Drawing.Point(intStartX, intStartY - 5), new System.Drawing.Point(intStartX, intStartY));
                    boolBrush1LastUsed = !boolBrush1LastUsed;
                    intStartX += intWidth;
                    decScaleText += incrementDisplayed;

                    fs = G.MeasureString(decScaleText.ToString(), new System.Drawing.Font("Tahoma", (float)8.25, FontStyle.Regular), 200);
                    DrawText(G, decScaleText.ToString(), intStartX - (Convert.ToInt32(fs.Width) / 2), -1, textBrush, (float)8.25, FontStyle.Regular);
                }
                G.DrawLine(new Pen(Color.Black, 1), new System.Drawing.Point(intStartX, intStartY - 5), new System.Drawing.Point(intStartX, intStartY));
                DrawText(G, "1:" + MapScale.ToString(), 5, 26, textBrush, (float)8.25, FontStyle.Regular);

                fs = G.MeasureString(strUnitDesc, new System.Drawing.Font("Tahoma", (float)8.25, FontStyle.Regular), 200);
                DrawText(G, strUnitDesc, intStartX - Convert.ToInt32(fs.Width), 26, textBrush, (float)8.25, FontStyle.Regular);


                brush1.Dispose();
                brush2.Dispose();
            }
            return imageScaleBar;
        }

        private void DrawText(Graphics G, string strText, int intStartX, int intStartY, Brush BrushStyle, float floatSize, FontStyle fontStyle)
        {
            G.DrawString(strText, new System.Drawing.Font("Tahoma", floatSize, fontStyle), BrushStyle, (float)intStartX, (float)intStartY);
        }

        private void DrawShape(Graphics G, string strShapeName, int intStartX, int intStartY, Color BorderColor, Brush BrushStyle, int intWidth)
        {
            G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
            Pen pen = new Pen(BorderColor, 1);
            pen.Alignment = PenAlignment.Inset;

            switch (strShapeName)
            {
                case "Rectangle":
                    System.Drawing.Rectangle rec = new System.Drawing.Rectangle(intStartX, intStartY, intWidth, 10);
                    G.DrawRectangle(pen, rec);
                    rec.Inflate(-1, -1);  // Takes 1 from all sides (-2 from width and -2 from height) //
                    rec.Width = rec.Width + 1;
                    rec.Height = rec.Height + 1;
                    G.FillRectangle(BrushStyle, rec);
                    break;
                default:
                    break;
            }
            pen.Dispose();
        }

    }
}



using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Security;
using System.Security.Policy;
using System.Security.Permissions;
using DevExpress.UserSkins;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;

namespace WoodPlan5
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
//        [LoaderOptimizationAttribute(LoaderOptimization.MultiDomain)]
        [LoaderOptimizationAttribute(LoaderOptimization.MultiDomainHost)]
        static void Main()
        {
            Boolean blProcessRunning = false;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Process[] pName = Process.GetProcessesByName("WP5");
            Process pCurrent = Process.GetCurrentProcess();

            for (int i = 0; i < pName.Length; i++)
            {
                //string strTest = "";
                //strTest = "SID: " + pName[i].SessionId.ToString() + " Current SID: " + pCurrent.SessionId.ToString();
                //strTest += "\n\n PID: " + pName[i].Id + " Current PID: " + pCurrent.Id;

                //MessageBox.Show(strTest);

                if (pName[i].SessionId == pCurrent.SessionId && pName[i].Id != pCurrent.Id)
                {
                    blProcessRunning = true;
                    break;
                }
            }

            //MessageBox.Show("Here?");

            if(blProcessRunning)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("This application is already running!", "Information Centre Running", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Thread t = new Thread(ShowSplashScreen);
                t.Start();

                string[] strCommands = Environment.GetCommandLineArgs();
                string strDatabase = "";

                if (strCommands.Length > 1)
                {
                    strDatabase = strCommands[1].ToUpper();
                }

                DevExpress.UserSkins.BonusSkins.Register();
                DevExpress.Skins.SkinManager.EnableFormSkins();

                //DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = DevExpress.XtraEditors.ScrollUIMode.Touch;
                //DevExpress.XtraEditors.WindowsFormsSettings.ForceDirectXPaint();
                DevExpress.XtraEditors.WindowsFormsSettings.AllowDefaultSvgImages = DevExpress.Utils.DefaultBoolean.False;  // Use non-SVG graphic on the Dev Express Grid right-click Menu, Report Builder etc //

                if (strDatabase == "")
                {
                    Application.Run(new frmMain2(t));
                }
                else
                {
                    Application.Run(new frmMain2(t, strDatabase));
                }
            }
        }

        static void ShowSplashScreen()
        {
            frmInitialScreen fisScreen = new frmInitialScreen();
            fisScreen.TopMost = true;
            fisScreen.ShowDialog();
        }
    }
}
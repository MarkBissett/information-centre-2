﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5
{
    public class GoogleCloudMessage
    {
        public string[] registration_ids { get; set; }
        public GoogleCloudMessageData data { get; set; }
    }

    public class GoogleCloudMessageData
    {
        public DateTime DATE { get; set; }
        public string TITLE { get; set; }
        public string MESSAGE { get; set; }
        public GoogleCloudMessageSubData DATA { get; set; }
    }

    public class GoogleCloudMessageSubData
    {
        public int commandId { get; set; }
        public int dataTypeID { get; set; }
        public List<Record> records { get; set; }
    }

    public class Record
    {
        public string id { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    class Activate_All_TabPages
    {
        public void Activate(System.Collections.IEnumerable controls)
        {
            // Activates every tab page and expands then collapses any collapsed groups to force the hosted child controls to initialise their data bindings //
            foreach (Control item in controls)
            {
                if (item is DevExpress.XtraLayout.LayoutControl)
                {
                    DevExpress.XtraLayout.LayoutControl item2 = (DevExpress.XtraLayout.LayoutControl)item;
                    foreach (object obj in item2.Items)
                    {
                        if (obj is DevExpress.XtraLayout.TabbedControlGroup)
                        {
                            DevExpress.XtraLayout.TabbedControlGroup TabbedControlGroup = (DevExpress.XtraLayout.TabbedControlGroup)obj;
                            int intOriginalSelectedPage = TabbedControlGroup.SelectedTabPageIndex;
                            for (int i = 0; i < TabbedControlGroup.TabPages.Count; i++)
                            {
                                TabbedControlGroup.SelectedTabPageIndex = i;
                            }
                            TabbedControlGroup.SelectedTabPageIndex = intOriginalSelectedPage;
                        }
                        else if (obj is DevExpress.XtraLayout.LayoutControlGroup)
                        {
                            DevExpress.XtraLayout.LayoutControlGroup lcg = (DevExpress.XtraLayout.LayoutControlGroup)obj;
                            if (!lcg.Expanded)
                            {
                                lcg.Expanded = true;
                                lcg.Expanded = false;
                            }
                        }
                    }
                }
                if (item is ContainerControl) Activate(item.Controls);
            }
        }
    }
}

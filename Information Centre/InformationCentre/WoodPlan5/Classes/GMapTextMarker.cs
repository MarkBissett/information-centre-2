﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WoodPlan5
{
    using System.Drawing;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;
    using GMap.NET;

    public class GMapTextMarker : GMapMarker 
    {
        protected Image _MarkerImage = Properties.Resources.map_point_blue;
        protected string _Text = "";
        protected Brush _TextBrush = Brushes.Black;
        //protected Color backColor = Color.FromArgb(128, Color.Wheat);
        Brush BackColorBrush = new SolidBrush(Color.FromArgb(128, Color.Wheat));
        //Pen BackPen = new Pen(Color.Black);
        Pen BackPen = new Pen(Color.FromArgb(140, Color.Navy));
        protected Font Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);
    
        public Image Image 
        {
            get 
            {
                return _MarkerImage;
            }
            set 
            {
                _MarkerImage = value;
            }
        }
    
        public string Text 
        {
            get 
            {
                return _Text;
            }
            set 
            {
                _Text = value;
            }
        }

        public Brush TextBrush
        {
            get
            {
                return _TextBrush;
            }
            set
            {
                _TextBrush = value;
            }
        }

        //public System.Nullable Bearing;
    
        public GMapTextMarker(PointLatLng p) : base(p) 
        {
            //  This call is required by the designer.
            //InitializeComponent();
            //  Add any initialization after the InitializeComponent() call.
            Size = new  System.Drawing.Size(_MarkerImage.Width, _MarkerImage.Height);
        }
    
        public override void OnRender(Graphics g) 
        {
            if (_MarkerImage != null) 
            {
                 g.DrawImage(_MarkerImage, this.LocalPosition);
            }
            // Make some adjustments to the position here depending on your image
            //AddLabel(g, _Text, (this.LocalPosition.X + _MarkerImage.Width), (this.LocalPosition.Y + _MarkerImage.Height));
            CenterTextAt(g, _Text, (this.LocalPosition.X + _MarkerImage.Width), (this.LocalPosition.Y + _MarkerImage.Height));
        }
    
        public void CenterTextAt(Graphics gr, string txt, float x, float y) {
            //  Make a StringFormat object that centers.
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            //  Draw the text.
            //TextFormatFlags flags = TextFormatFlags.Bottom | TextFormatFlags.EndEllipsis;
            //TextRenderer.DrawText(gr, txt, Font, new Rectangle((int)x, (int)y, (int)50, (int)20), SystemColors.ControlText, SystemColors.ControlDark, flags);

            SizeF size = gr.MeasureString(txt, Font);
            Rectangle rect = new Rectangle((int)x-12, (int)y-6, (int)size.Width, (int)size.Height);
            gr.DrawRectangle(BackPen, rect);
            gr.FillRectangle(BackColorBrush,rect);

            //gr.DrawString(txt, Font, _TextBrush, x, y, sf);

            using (GraphicsPath objGP = new GraphicsPath())
            {
                objGP.AddLine(rect.X + 2 * Radius, rect.Y + rect.Height, rect.X + Radius, rect.Y + rect.Height + Radius);
                objGP.AddLine(rect.X + Radius, rect.Y + rect.Height + Radius, rect.X + Radius, rect.Y + rect.Height);

                objGP.AddArc(rect.X, rect.Y + rect.Height - (Radius * 2), Radius * 2, Radius * 2, 90, 90);
                objGP.AddLine(rect.X, rect.Y + rect.Height - (Radius * 2), rect.X, rect.Y + Radius);
                objGP.AddArc(rect.X, rect.Y, Radius * 2, Radius * 2, 180, 90);
                objGP.AddLine(rect.X + Radius, rect.Y, rect.X + rect.Width - (Radius * 2), rect.Y);
                objGP.AddArc(rect.X + rect.Width - (Radius * 2), rect.Y, Radius * 2, Radius * 2, 270, 90);
                objGP.AddLine(rect.X + rect.Width, rect.Y + Radius, rect.X + rect.Width, rect.Y + rect.Height - (Radius * 2));
                objGP.AddArc(rect.X + rect.Width - (Radius * 2), rect.Y + rect.Height - (Radius * 2), Radius * 2, Radius * 2, 0, 90); // Corner

                objGP.CloseFigure();

                gr.FillPath(BackColorBrush, objGP);
                gr.DrawPath(Stroke, objGP);
                gr.DrawString(txt, Font, _TextBrush, x, y, sf);
            }

            sf.Dispose();
        }

        public float Radius = 10f;
        Pen Stroke = new Pen(Color.FromArgb(140, Color.Navy));
       /* 
        public void AddLabel(Graphics g, string txt, float x, float y)
        {
            Stroke.Width = 3;
            this.Stroke.LineJoin = LineJoin.Round;
            this.Stroke.StartCap = LineCap.RoundAnchor;
            Brush Fill = BackColorBrush;

            SizeF size = g.MeasureString(txt, Font);
            Rectangle rect = new Rectangle((int)x - 12, (int)y - 6, (int)size.Width, (int)size.Height);
            rect.Offset(Offset.X, Offset.Y);

            using (GraphicsPath objGP = new GraphicsPath())
            {
                objGP.AddLine(rect.X + 2 * Radius, rect.Y + rect.Height, rect.X + Radius, rect.Y + rect.Height + Radius);
                objGP.AddLine(rect.X + Radius, rect.Y + rect.Height + Radius, rect.X + Radius, rect.Y + rect.Height);

                objGP.AddArc(rect.X, rect.Y + rect.Height - (Radius * 2), Radius * 2, Radius * 2, 90, 90);
                objGP.AddLine(rect.X, rect.Y + rect.Height - (Radius * 2), rect.X, rect.Y + Radius);
                objGP.AddArc(rect.X, rect.Y, Radius * 2, Radius * 2, 180, 90);
                objGP.AddLine(rect.X + Radius, rect.Y, rect.X + rect.Width - (Radius * 2), rect.Y);
                objGP.AddArc(rect.X + rect.Width - (Radius * 2), rect.Y, Radius * 2, Radius * 2, 270, 90);
                objGP.AddLine(rect.X + rect.Width, rect.Y + Radius, rect.X + rect.Width, rect.Y + rect.Height - (Radius * 2));
                objGP.AddArc(rect.X + rect.Width - (Radius * 2), rect.Y + rect.Height - (Radius * 2), Radius * 2, Radius * 2, 0, 90); // Corner

                objGP.CloseFigure();

                g.FillPath(Fill, objGP);
                g.DrawPath(Stroke, objGP);
             }
        }*/
    }
}




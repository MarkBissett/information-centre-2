﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5
{
    public class UpdatedIDsEventArgs : EventArgs
    {
        private string updatedIDs = "";

        private UpdatedIDsEventArgs()
        { }

        public UpdatedIDsEventArgs(string updatedIDs)
        {
            this.updatedIDs = updatedIDs;
        }

        public string UpdatedIDs
        { get { return updatedIDs; } }

    }
}

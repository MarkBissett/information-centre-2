﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5
{
    using System.Drawing;
    using GMap.NET;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;

    public class GMapMarkerWithSquare : GMapMarkerCustom
    {

        public GMapMarkerWithSquare(PointLatLng p)
            : base(p)
        {
            Icon = Properties.Resources.square;
            this.Size = new Size(8, 8);
        }
    }

}

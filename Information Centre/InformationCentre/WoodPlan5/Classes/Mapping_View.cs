using System;
using System.Collections.Generic;
using System.Text;

namespace WoodPlan5
{
    public class Mapping_View
    {
        private string strViewDescription;
        public string StrViewDescription
        {
            get
            {
                return strViewDescription;
            }
            set
            {
                if (strViewDescription == value)
                    return;
                strViewDescription = value;
            }
        }

        private double dViewScale;
        public double DViewScale
        {
            get
            {
                return dViewScale;
            }
            set
            {
                if (dViewScale == value) return;
                dViewScale = value;
            }
        }

        private double dMapCentreX;
        public double DMapCentreX
        {
            get
            {
                return dMapCentreX;
            }
            set
            {
                if (dMapCentreX == value) return;
                dMapCentreX = value;
            }
        }

        private double dMapCentreY;
        public double DMapCentreY
        {
            get
            {
                return dMapCentreY;
            }
            set
            {
                if (dMapCentreY == value) return;
                dMapCentreY = value;
            }
        }

        private DateTime dtDateTime;
        public DateTime DtDateTime
        {
            get
            {
                return dtDateTime;
            }
            set
            {
                if (dtDateTime == value) return;
                dtDateTime = value;
            }
        }


    }
 
}



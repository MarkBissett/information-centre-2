using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5;
using BaseObjects;

namespace WoodPlan5
{
    class SequenceNumberGetNext
    {

        public string GetNextNumberInSequence(string strConnectionString, string strFieldName, string strTableName, string strSequence)
        {
            DataSet_ATTableAdapters.QueriesTableAdapter GetValue = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetValue.ChangeConnectionString(strConnectionString);
            string strNextNumber = "";
            int intAdditionalIncrement = 0;
            try
            {
                strNextNumber = GetValue.sp01248_AT_Sequence_Get_Next_Sequence(strTableName, strFieldName, strSequence, intAdditionalIncrement).ToString();
                if (String.IsNullOrEmpty(strNextNumber))
                {
                    XtraMessageBox.Show("Unable to calculate the next sequence number - the input sequence prefix is unrecognised!\n\nTry selecting a different sequence prefix first or clear the sequence prefix to calculate a sequence without a prefix.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "";  // Abort - System was unable to return a sequence [may have had an invalid first part of the sequence passed through] //
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //
                if (strSequence.Length + strNextNumber.Length > 20)
                {
                    XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "";
                }
            }
            catch (Exception Ex)
            {
                XtraMessageBox.Show("Unable to calculate the next sequence number - an error ocurred [" + Ex.Message + "]!\n\nYou may try again - if the error persists, contact Technical Support.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            return strNextNumber;
        }
    }
}

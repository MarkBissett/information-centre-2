using System;
using System.Collections.Generic;
using System.Text;

namespace WoodPlan5
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class GIROGlobals
    {
        private int intUserID = 0;
        private string strUserName = "";
        private int intUserType = 0;
        private int intCurrentUserID = 0;
        private string strCurrentUserName = "";
        private int intCurrentUserType = 0;
        private string strCurrentFirstName = "";
        private string strCurrentSurname = "";
        private int intViewedSystemPeriodID = 0;
        private DateTime dtViewedSystemPeriodStart;
        private DateTime dtViewedSystemPeriodEnd;
        private int intLiveSystemPeriodID = 0;
        private DateTime dtLiveSystemPeriodStart;
        private DateTime dtLiveSystemPeriodEnd;
        private int intViewedAcademicPeriodID = 0;
        private DateTime dtViewedAcademicPeriodStart;
        private DateTime dtViewedAcademicPeriodEnd;
        private int intLiveAcademicPeriodID = 0;
        private DateTime dtLiveAcademicPeriodStart;
        private DateTime dtLiveAcademicPeriodEnd;

        /// <summary>
        /// 
        /// </summary>
        public int UserID
        {
            get { return intUserID; }
            set { intUserID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            get { return strUserName; }
            set { strUserName = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int UserType
        {
            get { return intUserType; }
            set { intUserType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CurrentUserID
        {
            get { return intCurrentUserID; }
            set { intCurrentUserID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentUserName
        {
            get { return strCurrentUserName; }
            set { strCurrentUserName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CurrentUserType
        {
            get { return intCurrentUserType; }
            set { intCurrentUserType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentFirstName
        {
            get { return strCurrentFirstName; }
            set { strCurrentFirstName = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string CurrentSurname
        {
            get { return strCurrentSurname; }
            set { strCurrentSurname = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ViewedSystemPeriodID
        {
            get { return intViewedSystemPeriodID; }
            set { intViewedSystemPeriodID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ViewedSystemPeriodStart
        {
            get { return dtViewedSystemPeriodStart; }
            set { dtViewedSystemPeriodStart = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ViewedSystemPeriodEnd
        {
            get { return dtViewedSystemPeriodEnd; }
            set { dtViewedSystemPeriodEnd = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LiveSystemPeriodID
        {
            get { return intLiveSystemPeriodID; }
            set { intLiveSystemPeriodID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LiveSystemPeriodStart
        {
            get { return dtLiveSystemPeriodStart; }
            set { dtLiveSystemPeriodStart = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LiveSystemPeriodEnd
        {
            get { return dtLiveSystemPeriodEnd; }
            set { dtLiveSystemPeriodEnd = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ViewedAcademicPeriodID
        {
            get { return intViewedAcademicPeriodID; }
            set { intViewedAcademicPeriodID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ViewedAcademicPeriodStart
        {
            get { return dtViewedAcademicPeriodStart; }
            set { dtViewedAcademicPeriodStart = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ViewedAcademicPeriodEnd
        {
            get { return dtViewedAcademicPeriodEnd; }
            set { dtViewedAcademicPeriodEnd = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LiveAcademicPeriodID
        {
            get { return intLiveAcademicPeriodID; }
            set { intLiveAcademicPeriodID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LiveAcademicPeriodStart
        {
            get { return dtLiveAcademicPeriodStart; }
            set { dtLiveAcademicPeriodStart = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LiveAcademicPeriodEnd
        {
            get { return dtLiveAcademicPeriodEnd; }
            set { dtLiveAcademicPeriodEnd = value; }
        }

    }
}

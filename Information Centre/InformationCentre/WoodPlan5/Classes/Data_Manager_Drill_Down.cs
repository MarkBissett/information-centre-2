using System;
using System.Windows.Forms;
using BaseObjects;

namespace WoodPlan5
{
    class Data_Manager_Drill_Down
    {

        public static int Drill_Down(Form frmCallingForm, GlobalSettings globalSettings, string strRecordIDs, string strDrillDownLevel)
        {
            System.Reflection.MethodInfo method = null;
            frmProgress fProgress = null;
            switch (strDrillDownLevel.ToLower())
            {
                case "site_trees":
                    {
                        frm_AT_Tree_Manager frmChild = new frm_AT_Tree_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "tree":
                case "incident_inspections":
                    {
                        var frmChild = new frm_AT_Inspection_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "inspection":
                case "workorder":
                case "costcentre":
                case "incident_actions":
                    {
                        var frmChild = new frm_AT_Action_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        frmChild.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;

                
                // ***** Asset Management Code ***** --
                case "client":
                    {
                        var frmChild = new frm_Core_Site_Manager();
                       // fProgress = new frmProgress(10);
                        //frmCallingForm.AddOwnedForm(fProgress);

                        //fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        //Application.DoEvents();
                        //frmChild.fProgress = fProgress;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmChild.MdiParent, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();

                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                        break;
                    }
                case "site":
                    {
                        var frmChild = new frm_EP_Asset_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        frmChild.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "asset":
                case "site_inspection":
                    {
                        var frmChild = new frm_EP_Action_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        frmChild.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;


                // ***** Winter Maintenance - Gritting ***** //
                case "site_gritting_callouts":
                case "client_invoicing_gritting":
                    {
                        var f_child = new frm_GC_Callout_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        f_child.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            f_child.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            f_child.MdiParent = frmCallingForm.MdiParent;
                        }
                        f_child.GlobalSettings = globalSettings;
                        f_child.strPassedInDrillDownIDs = strRecordIDs;
                        f_child.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(f_child, new object[] { null });
                        break;
                    }
                case "site_snow_clearance_callouts":
                case "client_invoicing_snow_clearance":
                    {
                        var f_child = new frm_GC_Snow_Callout_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        f_child.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            f_child.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            f_child.MdiParent = frmCallingForm.MdiParent;
                        }
                        f_child.GlobalSettings = globalSettings;
                        f_child.strPassedInDrillDownIDs = strRecordIDs;
                        f_child.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(f_child, new object[] { null });
                        break;
                    }
                case "salt_transfers":
                    {
                        var f_child = new frm_GC_Salt_Transfer_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        f_child.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            f_child.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            f_child.MdiParent = frmCallingForm.MdiParent;
                        }
                        f_child.GlobalSettings = globalSettings;
                        f_child.strPassedInDrillDownIDs = strRecordIDs;
                        f_child.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(f_child, new object[] { null });
                        break;
                    }
                case "salt_orders":
                    {
                        var f_child = new frm_GC_Salt_Order_Manager();
                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        f_child.fProgress = fProgress;
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            f_child.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            f_child.MdiParent = frmCallingForm.MdiParent;
                        }
                        f_child.GlobalSettings = globalSettings;
                        f_child.strPassedInDrillDownIDs = strRecordIDs;
                        f_child.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(f_child, new object[] { null });
                        break;
                    }
                

                /* ***** Utilities Code **** */
                case "ut_region":
                    {
                        var frmChild = new frm_UT_SubArea_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInSubAreaIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_subarea":
                    {
                        var frmChild = new frm_UT_Primary_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInPrimaryIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_primary":
                    {
                        var frmChild = new frm_UT_Feeder_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInFeederIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_pole":
                    {
                        var frmChild = new frm_UT_Pole_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInPoleIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_surveyed_pole":
                    {
                        var frmChild = new frm_UT_Surveyed_Pole_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInSurveyedPoleIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_action":
                    {
                        var frmChild = new frm_UT_Action_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInActionIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_action_revisits":
                    {
                        var frmChild = new frm_UT_Action_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInActionIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "ut_permission_manager":
                    {
                        var frmChild = new frm_UT_Permission_Document_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInPermissionDocumentIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                

                /* ***** HR Code **** */
                case "hr_absence":
                    {
                        var frmChild = new frm_HR_Holiday_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInAbsenceIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;


                /* ***** Operations Manager Code **** */
                case "operations_visit":
                    {
                        var frmChild = new frm_OM_Visit_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "operations_job":
                    {
                        var frmChild = new frm_OM_Job_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "operations_job_labour":
                    {
                        var frmChild = new frm_OM_Job_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild._boolSelectAllJobsOnOpen = true;
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "operations_client_contracts":
                    {
                        var frmChild = new frm_OM_Client_Contract_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "operations_site_contracts":
                    {
                        var frmChild = new frm_OM_Site_Contract_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "operations_tender":
                    {
                        var frmChild = new frm_OM_Tender_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;

                case "operations_tender_group":
                    {
                        var frmChild = new frm_OM_Tender_Group_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;   
                    
                case "waste_document":
                    {
                        var frmChild = new frm_OM_Waste_Document_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;

                case "contractors":
                    {
                        var frmChild = new frm_Core_Contractor_Manager();

                        fProgress = new frmProgress(10);
                        frmCallingForm.AddOwnedForm(fProgress);

                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        frmChild.fProgress = fProgress;

                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInDrillDownIDs = strRecordIDs;
                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;


                default:
                    break;
            }

            return 1;
        }

        public static int Drill_Down(Form frmCallingForm, GlobalSettings globalSettings, string strRecordIDs, string strDrillDownLevel, int intRecordType, int intRecordSubType, int intLinkedToRecordID, string strLinkedToRecord)
        {
            System.Reflection.MethodInfo method = null;
            switch (strDrillDownLevel.ToLower())
            {
                case "hr_linked_documents":
                    {
                        var frmChild = new frm_HR_Linked_Document_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInLinkedDocumentIDs = strRecordIDs;
                        frmChild.intPassedInRecordTypeID = intRecordType;
                        frmChild.intPassedInRecordSubTypeID = intRecordSubType;
                        frmChild.intPassedInLinkedToRecordID = intLinkedToRecordID;
                        frmChild.strPassedInLinkedToRecord = strLinkedToRecord;

                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;
                case "om_linked_documents":
                    {
                        var frmChild = new frm_OM_Linked_Document_Manager();
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(frmCallingForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmChild.splashScreenManager = splashScreenManager1;
                        frmChild.splashScreenManager.ShowWaitForm();
                        if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                        {
                            frmChild.MdiParent = frmCallingForm;
                        }
                        else  // We don't have the MDI form so get the current form's parent //
                        {
                            frmChild.MdiParent = frmCallingForm.MdiParent;
                        }
                        frmChild.GlobalSettings = globalSettings;
                        frmChild.strPassedInLinkedDocumentIDs = strRecordIDs;
                        frmChild.intPassedInRecordTypeID = intRecordType;
                        frmChild.intPassedInRecordSubTypeID = intRecordSubType;
                        frmChild.intPassedInLinkedToRecordID = intLinkedToRecordID;
                        frmChild.strPassedInLinkedToRecord = strLinkedToRecord;

                        frmChild.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmChild, new object[] { null });
                    }
                    break;

                default:
                    break;
            }

            return 1;
        }

    }
}

using System;
using System.Collections.Generic;
using System.Windows.Forms;

using WoodPlan5;

namespace WoodPlan5
{
    class Broadcast_Data_Refresh
    {

        public int Inspection_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_AT_Tree_Manager":
                        if (strCaller != "frm_AT_Tree_Manager")
                        {
                            frm_AT_Tree_Manager fParentForm;
                            fParentForm = (frm_AT_Tree_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_AT_Inspection_Manager":
                        if (strCaller != "frm_AT_Inspection_Manager")
                        {
                            frm_AT_Inspection_Manager fParentForm;
                            fParentForm = (frm_AT_Inspection_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "", "");
                        }
                        break;
                    case "frm_AT_Tree_Edit":
                        if (strCaller != "frm_AT_Tree_Edit")
                        {
                            frm_AT_Tree_Edit fParentForm;
                            fParentForm = (frm_AT_Tree_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_AT_Mapping_Tree_Picker":
                        if (strCaller != "frm_AT_Mapping_Tree_Picker")
                        {
                            frm_AT_Mapping_Tree_Picker fParentForm;
                            fParentForm = (frm_AT_Mapping_Tree_Picker)frmChild;
                            if (!string.IsNullOrEmpty(strUpdateIDs2))
                            {
                                fParentForm.RefreshMapObjects(strUpdateIDs2, 1);  // Redraw map, refreshing loaded objects - passing Tree IDs //
                            }
                            if (!string.IsNullOrEmpty(strUpdateIDs))
                            {
                                fParentForm.UpdateFormRefreshStatus(1, "", strUpdateIDs, "");  // Set Flag on form to reload linked inspections on Activate //
                            }

                        }
                        break;
                    default:
                        break;
                }          
            }
            return 1;
        }

        public int Action_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_AT_Action_Manager":
                        if (strCaller != "frm_AT_Action_Manager")
                        {
                            frm_AT_Action_Manager fParentForm;
                            fParentForm = (frm_AT_Action_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_AT_Inspection_Manager":
                        if (strCaller != "frm_AT_Inspection_Manager")
                        {
                            frm_AT_Inspection_Manager fParentForm;
                            fParentForm = (frm_AT_Inspection_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_AT_Tree_Edit":
                        if (strCaller != "frm_AT_Tree_Edit")
                        {
                            frm_AT_Tree_Edit fParentForm;
                            fParentForm = (frm_AT_Tree_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_AT_Inspection_Edit":
                        if (strCaller != "frm_AT_Inspection_Edit")
                        {
                            frm_AT_Inspection_Edit fParentForm;
                            fParentForm = (frm_AT_Inspection_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_AT_Mapping_Tree_Picker":
                        if (strCaller != "frm_AT_Mapping_Tree_Picker")
                        {
                            frm_AT_Mapping_Tree_Picker fParentForm;
                            fParentForm = (frm_AT_Mapping_Tree_Picker)frmChild;
                            if (!string.IsNullOrEmpty(strUpdateIDs2))
                            {
                                fParentForm.RefreshMapObjects(strUpdateIDs2, 0);  // Redraw map, refreshing loaded objects //
                            }
                            else
                            {
                                fParentForm.UpdateFormRefreshStatus(2, "", "", strUpdateIDs);  // Set Flag on form to reload linked inspections on Activate //
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int WorkOrder_Refresh(Form frm_main, string strCaller, string strUpdateIDs)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_AT_WorkOrder_Manager":
                        if (strCaller == "frm_AT_WorkOrder_Edit")
                        {
                            frm_AT_WorkOrder_Manager fParentForm;
                            fParentForm = (frm_AT_WorkOrder_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs, "", "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int LinkedDocument_Refresh(Form frm_main, string strCaller, string strUpdateIDs)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_AT_Tree_Manager":
                        if (strCaller != "frm_AT_Tree_Manager")
                        {
                            frm_AT_Tree_Manager fParentForm;
                            fParentForm = (frm_AT_Tree_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_AT_Tree_Edit":
                        if (strCaller != "frm_AT_Tree_Edit")
                        {
                            frm_AT_Tree_Edit fParentForm;
                            fParentForm = (frm_AT_Tree_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_AT_Inspection_Manager":
                        if (strCaller != "frm_AT_Inspection_Manager")
                        {
                            frm_AT_Inspection_Manager fParentForm;
                            fParentForm = (frm_AT_Inspection_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_AT_Inspection_Edit":
                        if (strCaller != "frm_AT_Inspection_Edit")
                        {
                            frm_AT_Inspection_Edit fParentForm;
                            fParentForm = (frm_AT_Inspection_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;


                    case "frm_EP_Asset_Manager":
                        if (strCaller != "frm_EP_Asset_Manager")
                        {
                            frm_EP_Asset_Manager fParentForm;
                            fParentForm = (frm_EP_Asset_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_EP_Action_Manager":
                        if (strCaller != "frm_EP_Action_Manager")
                        {
                            frm_EP_Action_Manager fParentForm;
                            fParentForm = (frm_EP_Action_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_EP_Action_Edit":
                        if (strCaller != "frm_EP_Action_Edit")
                        {
                            frm_EP_Action_Edit fParentForm;
                            fParentForm = (frm_EP_Action_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_EP_Site_Inspection_Manager":
                        if (strCaller != "frm_EP_Site_Inspection_Manager")
                        {
                            frm_EP_Site_Inspection_Manager fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(3, "", "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;


                    default:
                        break;
                }
            }
            return 1;
        }


        public int Asset_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                     case "frm_EP_Asset_Manager":
                        if (strCaller != "frm_EP_Asset_Manager")
                        {
                            frm_EP_Asset_Manager fParentForm;
                            fParentForm = (frm_EP_Asset_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "", "");
                        }
                        break;
                    case "frm_Core_Site_Edit":
                        if (strCaller != "frm_Core_Site_Edit")
                        {
                            frm_Core_Site_Edit fParentForm;
                            fParentForm = (frm_Core_Site_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs));
                        }
                        break;
                    case "frm_AT_Mapping_Tree_Picker":
                        if (frmChild.Name == "frm_AT_Mapping_Tree_Picker" && !string.IsNullOrEmpty(strUpdateIDs2))
                        {
                            frm_AT_Mapping_Tree_Picker fParentForm;
                            fParentForm = (frm_AT_Mapping_Tree_Picker)frmChild;
                            fParentForm.RefreshMapObjects(strUpdateIDs2, 0);
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int Asset_Action_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_EP_Action_Manager":
                        if (strCaller != "frm_EP_Action_Manager")
                        {
                            frm_EP_Action_Manager fParentForm;
                            fParentForm = (frm_EP_Action_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_EP_Asset_Manager":
                        if (strCaller != "frm_EP_Asset_Manager")
                        {
                            frm_EP_Asset_Manager fParentForm;
                            fParentForm = (frm_EP_Asset_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_EP_Asset_Edit":
                        if (strCaller != "frm_EP_Asset_Edit")
                        {
                            frm_EP_Asset_Edit fParentForm;
                            fParentForm = (frm_EP_Asset_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    case "frm_EP_Site_Inspection_Edit":
                        if (strCaller != "frm_EP_Site_Inspection_Edit")
                        {
                            frm_EP_Site_Inspection_Edit fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int Site_Inspection_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_EP_Site_Inspection_Manager":
                        if (strCaller != "frm_EP_Site_Inspection_Manager")
                        {
                            frm_EP_Site_Inspection_Manager fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "", "");
                        }
                        break;
                    case "frm_Core_Site_Manager":
                        if (strCaller != "frm_Core_Site_Manager")
                        {
                            frm_Core_Site_Manager fParentForm;
                            fParentForm = (frm_Core_Site_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, "", (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "", "", "", "");
                        }
                        break;
                    case "frm_Core_Site_Edit":
                        if (strCaller != "frm_Core_Site_Edit")
                        {
                            frm_Core_Site_Edit fParentForm;
                            fParentForm = (frm_Core_Site_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int Site_Inspection_Immediate_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data immediatly //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_EP_Site_Inspection_Manager":
                        if (strCaller != "frm_EP_Site_Inspection_Manager")
                        {
                            frm_EP_Site_Inspection_Manager fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Manager)frmChild;
                            fParentForm.External_Call_For_Immediate_Refresh_Linked_Records((string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "", "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int Site_Inspection_Time_Deduction_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data on activating //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_EP_Site_Inspection_Edit":
                        if (strCaller != "frm_EP_Site_Inspection_Edit")
                        {
                            frm_EP_Site_Inspection_Edit fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Edit)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, (string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }

        public int Site_Inspection_Time_Deduction_Immediate_Refresh(Form frm_main, string strCaller, string strUpdateIDs, string strUpdateIDs2)
        {
            // Notify any open Forms which may reference inspection data that they will need to refresh their data immediatly //
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                switch (frmChild.Name)
                {
                    case "frm_EP_Site_Inspection_Edit":
                        if (strCaller != "frm_EP_Site_Inspection_Edit")
                        {
                            frm_EP_Site_Inspection_Edit fParentForm;
                            fParentForm = (frm_EP_Site_Inspection_Edit)frmChild;
                            fParentForm.External_Call_For_Immediate_Refresh_Linked_Records((string.IsNullOrEmpty(strUpdateIDs) ? "" : strUpdateIDs), "");
                        }
                        break;
                    default:
                        break;
                }
            }
            return 1;
        }


    }
}

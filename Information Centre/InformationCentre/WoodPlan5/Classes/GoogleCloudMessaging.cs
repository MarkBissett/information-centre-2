﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;  // Required for Sending to the Google Cloud API //
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;


namespace WoodPlan5
{
    class GoogleCloudMessaging
    {
        public string SendUsingGoogleCloudMessaging(string[] PhoneIDs, string Title, string Message, int MessageID, int DataTypeID, string RecordIDs, string strConnectionString)
        {
            string strAPIKey = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strAPIKey = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "GoogleAPIKey").ToString();
            }
            catch (Exception)
            {
                return "An error occurred while trying to obtain the Google API Key (from the System Configuration).";
            }
            if (string.IsNullOrWhiteSpace(strAPIKey)) return "Missing Google API Key (from the System Configuration).";

            String url = "https://android.googleapis.com/gcm/send";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.MaximumAutomaticRedirections = 4;
            request.MaximumResponseHeadersLength = 100;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/json";
            request.Headers.Add("ContentType", "application/json");
            //request.Headers.Add("Authorization", "key=AIzaSyAW-KZB6dVje8l0yw1ee0PFj62L2az-37A");
            request.Headers.Add("Authorization", "key=" + strAPIKey);
            request.Method = "POST";

            GoogleCloudMessage googleCloudMessage = new GoogleCloudMessage();
            googleCloudMessage.registration_ids = PhoneIDs;
            GoogleCloudMessageData messageData = new GoogleCloudMessageData();
            messageData.DATE = DateTime.Now;
            messageData.TITLE = Title;
            messageData.MESSAGE = Message;

            GoogleCloudMessageSubData messageSubData = new GoogleCloudMessageSubData();
            messageSubData.commandId = MessageID;
            messageSubData.dataTypeID = DataTypeID;
            List<Record> recordList = new List<Record>();

            char[] delimiters = new char[] { ',' };
            Array arrayItems = RecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
            if (arrayItems.Length <= 0)
            {
                Record record = new Record();
                record.id = RecordIDs;
                recordList.Add(record);
            }
            else
            {
                foreach (string strElement in arrayItems)
                {
                    Record record = new Record();
                    record.id = strElement;
                    recordList.Add(record);
                }
            }
            messageSubData.records = recordList;
            messageData.DATA = messageSubData;
            googleCloudMessage.data = messageData;
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(googleCloudMessage);

            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            /*var postData = "delay_while_idle=1";
            postData += "&collapse_key=" + collapse_key;
            postData += "&data.message=" + messageId;
            postData += "&registration_id=" + GCMRegId;
            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }*/
            var response = (HttpWebResponse)request.GetResponse();
            var receiveStream = response.GetResponseStream();
            if (receiveStream == null)
            {
                throw new Exception("Error SendUsingGoogleCloudMessaging.");
            }
            var readStream = new StreamReader(receiveStream, Encoding.UTF8);
            var rspstring = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
            //var data = JsonConvert.DeserializeObject<GoogleReverseGeocodingResponse>(rspstring);
            if (rspstring == null)
            {
                throw new Exception("Error in SendUsingGoogleCloudMessaging.");
            }

            return rspstring;
        }

        public string[] GetPhoneIDs(string strConnectionString, string TeamIDs)
        {
            string strPhoneIDs = "";
            char[] delimiters = new char[] { ',' };
            using (var GetPhoneIDs = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
            {
                GetPhoneIDs.ChangeConnectionString(strConnectionString);
                try
                {
                    strPhoneIDs = GetPhoneIDs.sp06288_OM_Get_Phone_IDs_For_Messaging(TeamIDs).ToString();
                }
                catch (Exception) { }
           }
            List<string> listPhoneIDs = new List<string>();
            Array arrayItems = strPhoneIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
            if (arrayItems.Length > 0)
            {
                foreach (string strElement in arrayItems)
                {
                    listPhoneIDs.Add(strElement);
                }
            }
            string[] GCM_IDs = listPhoneIDs.ToArray();
            return GCM_IDs;
        }




    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WoodPlan5.Classes
{
    // Contains methods to allow format checking of supplied fields 
    public struct FormatCheck
    {

        #region email validation
        // Checks supplied email for compliance with expected format
        public static bool emailIsValid(string email, out string message)
        {
            return emailIsValid(email, false, out message);
        }

        public static bool emailIsValid(string email,bool mandatory,out string message)
        {
            message = "";

            if (email.Trim().Length == 0)
            {
                if (mandatory)
                {
                    message = "This Email address is mandatory. Enter a value";
                    return false;
                }
                else
                { return true; }
            }

            string expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expression))
            {
                if (Regex.Replace(email, expression, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    message = "The Email address contains invalid characters";
                    return false;
                }
            }
            else
            {
                message = "The Email address format is invalid";
                return false;
            }
        }

        #endregion

        #region mobile number validation

        // Checks supplied mobile phone number 

        public static bool mobileIsValid(string mobileNumber, out string message)
        {
            return mobileIsValid(mobileNumber, false, out message);
        }
        
        public static bool mobileIsValid(string mobileNumber, bool mandatory, out string message)
        {
            message = "";

            if (mobileNumber.Trim().Length == 0)
            {
                if (mandatory)
                {
                    message = "This Mobile Number is mandatory. Enter a value";
                    return false;
                }
                else
                { return true; }
            }

            if (!mobileNumber.StartsWith("07"))
            {
                message = "A Mobile Number must begin with '07'";
                return false;
            }

            return true;
        }

        #endregion
    }
}

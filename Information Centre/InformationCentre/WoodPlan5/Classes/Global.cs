using System;

namespace WoodPlan5
{
    struct stcItems
    {
        public Int32 iItemID;
        public string strItemName;
/*      /// <summary>
        /// 
        /// </summary>
        public string strInterfaceName;
        /// <summary>
        /// 
        /// </summary>
        public string strAssemblyPath;
 */
    }

    public struct stcComms
    {
        public byte[] key;
        public byte[] IV;
    }

    public class Mapping_Objects
    {
        public double doubleLat { get; set; }
        public double doubleLong { get; set; }
        public string stringToolTip { get; set; }
        public string stringID { get; set; }
    }

    class Global
    {
        public Global() { } // Constructor
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using BaseObjects;

namespace WoodPlan5.Classes.Operations
{
    class OM_View_Self_Billing_Invoice
    {
        public int View_Self_Billing_Invoice(Form frmCallingForm, GlobalSettings globalSettings, string ConnectionString, string VisitIDs)
        {
            if (string.IsNullOrWhiteSpace(VisitIDs)) return 0;
            string strSavedSelfBillingInvoiceFolder = "";

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(ConnectionString);
                strSavedSelfBillingInvoiceFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TeamSelfBillingInvoicesSavedFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Team Self-Billing Invoices (from the System Configuration Screen).\n\nPlease try again. If the problem persists, contact Technical Support.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
            if (!strSavedSelfBillingInvoiceFolder.EndsWith("\\")) strSavedSelfBillingInvoiceFolder += "\\";  // Add Backslash to end //


            string strPDF = "";
            try
            {
                SqlConnection SQlConn = new SqlConnection(ConnectionString);
                SQlConn.Open();
                SqlDataAdapter sdaData = new SqlDataAdapter();
                DataSet dsData = new DataSet("NewDataSet");

                SqlCommand cmd = new SqlCommand("sp06476_OM_Get_Self_Bliing_Invoice_Files_For_VisitIDs", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@VisitIDs", VisitIDs));
                sdaData = new SqlDataAdapter(cmd);
                sdaData.Fill(dsData, "Table");

                Form frmMDIParent = null;
                if (frmCallingForm.GetType().FullName.EndsWith("frmMain2"))  // We already have the MDI form //
                {
                    frmMDIParent = frmCallingForm;
                }
                else  // We don't have the MDI form so get the current form's parent //
                {
                    frmMDIParent = frmCallingForm.MdiParent;
                }


                if (dsData.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsData.Tables[0].Rows)
                    {
                        strPDF = dr["InvoicePDF"].ToString();
                        if (!string.IsNullOrWhiteSpace(strPDF))
                        {
                            // Open PDF file //
                            if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                            {
                                frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                fChildForm.strPDFFile = Path.Combine(strSavedSelfBillingInvoiceFolder + strPDF);
                                fChildForm.MdiParent = frmMDIParent;
                                fChildForm.GlobalSettings = globalSettings;
                                fChildForm.Show();
                            }
                            else
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strSavedSelfBillingInvoiceFolder + strPDF));
                            }

                        }
                    }
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to View Self-Billing Invoice File - the Self-Billing Invoice may not have had it's PDF file generated yet.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                SQlConn.Close();
                return 1;
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to View Self-Billing Invoice File - an error occurred: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.Operations
{
    public class Utils
    {
        public enum enmFocusedGrid
        {
            Jobs = 1,
            Labour = 2,
            Equipment = 3,
            Materials = 4,
            Pictures = 5,
            HealthAndSafety = 6,
            CRM = 7,
            Comments = 8,
            Waste = 9,
            Spraying = 10,
            ExtraInfo = 11,
            PurchaseOrder = 12,
            LabourTiming = 13,
            WasteJob = 14,
            SprayingJob = 15,
            NoAccess = 16,
            OperativeTiming = 17,
            VisitLabour = 18,
            NoAccessPicture = 19,
            BillingRequirement = 20,
            BillingException = 21,
            Labour2 = 22,
            Invoice = 23,
            FinanceExport = 34,
            FinanceVisitExport = 35,
            Tender = 36,
            History = 37,
            LinkedDocument = 68,

            Contract = 101,
            ContractYear = 102,
            ContractYearProfile = 103,
            PersonResponsibility = 104,
            ClientPOLink = 105,

            Sites = 201,
            SiteEdit = 202,
            SiteYear = 203,
            SiteYearEdit = 204,
            SiteYearBilling = 205,
            PreferredLabour = 206,
            PreferredEquipment = 207,
            PreferredMaterial = 208,
            JobRate = 209,

            Visit = 301,
            VisitStartDate = 302,
            VisitPanelJobs = 303,
            VisitPanelLabour = 304,

            JobMaster = 401,

            Accident = 501,
            AccidentInjury = 502,
            AccidentPicture = 503,
            AccidentComment = 504,
            AccidentSubType = 505,
            AccidentBodyArea = 506,
            AccidentInjuryType = 507,

            NONE = 999
        }


    }
}

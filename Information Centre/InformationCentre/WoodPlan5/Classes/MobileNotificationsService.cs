﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
//using Newtonsoft.Json;
using System.Web;

namespace WoodPlan5
{
    class MobileNotificationsService
    {
        public static string SendUsingGoogleCloudMessaging(string collapse_key, string messageToUser, String[] GCMRegIds, String JSONPayLoad, string strConnectionString)
        {
            string strAPIKey = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strAPIKey = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "GoogleAPIKey").ToString();
            }
            catch (Exception)
            {
                return "An error occurred while trying to obtain the Google API Key (from the System Configuration).";
            }
            if (string.IsNullOrWhiteSpace(strAPIKey)) return "Missing Google API Key (from the System Configuration).";
            
            String url = "https://android.googleapis.com/gcm/send";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.MaximumAutomaticRedirections = 4;
            request.MaximumResponseHeadersLength = 100;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            request.Headers.Add("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            //request.Headers.Add("Authorization", "key=AIzaSyDPkhcR_gohsRii3AEm_hDh5VYGcXg9Rmo");  // Make Dynamic //
            request.Headers.Add("Authorization", "key=" + strAPIKey);
            request.Method = "POST";
            var postData = "delay_while_idle=1";
            postData += "&collapse_key=" + collapse_key;
            postData += "&data.message=" + messageToUser;
            postData += "&data.payload=" + JSONPayLoad;
            postData += "&registration_ids=" + GCMRegIds;
            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var receiveStream = response.GetResponseStream();
            if (receiveStream == null)
                throw new Exception("Error SendUsingGoogleCloudMessaging.");
            var readStream = new StreamReader(receiveStream, Encoding.UTF8);
            var rspstring = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
            //var data = JsonConvert.DeserializeObject<GoogleReverseGeocodingResponse>(rspstring);
            if (rspstring == null)
                throw new Exception("Error in SendUsingGoogleCloudMessaging.");

            return rspstring;
        }


    }
}

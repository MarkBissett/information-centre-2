﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WoodPlan5.Classes.HR
{
    public class Utils
    {
        public enum enmMainGrids
            {
                Employees = 1,
                Addresses = 2,
                Bonus = 3,
                NextOfKin = 4,
                Discipline = 5,
                Eligibility = 6,
                DepartmentsWorked = 7,
                Training = 8,
                WorkPatternHeader = 9,
                WorkPattern = 10,
                Pensions = 11,
                PensionContributions = 12,
                HolidayYear = 13,
                Absences = 14,
                Vetting = 15,
                CRM = 16,
                Reference = 17,
                PayRise = 18,
                Shares = 19,
                Administer = 20,
                Subsistence = 21
            }

            public enum enmBusinessAreaGrids
            {
                BusinessArea=1,
                Department=2,
                DepartmentLocation=3,
                Region=4,
                Location=5,
                Insurance=6
            }

            public enum enmMasterVettingGrids
            {
                MasterVettingType = 1,
                VettingSubType = 2,
                VettingRestriction = 3,
            }

            public enum enmMasterAttributePicklists
            {
                PicklistHeader = 1,
                PicklistItem = 2,
            }

        }
    }

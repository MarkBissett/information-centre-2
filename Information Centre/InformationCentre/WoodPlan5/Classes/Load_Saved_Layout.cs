using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;  // Required for DataRow //
using WoodPlan5;
using BaseObjects;

using System.IO;  // for StreamReader //
using System.Reflection; // MethodInfo call to invoke post view loaded event on base form //

namespace WoodPlan5
{
    class Load_Saved_Layout
    {
        List<string> _StoredItemList = new List<string>();

        public int Load_User_Specified_Screen_Layout(string strConnectionString, frmBase frmToProcess, int LayoutID)
        {
            if (frmToProcess == null || LayoutID <= 0) return -1;  // -1: Missing Form or LayoutID //

            DataSet_Groups dataSet_Groups = new DataSet_Groups();
            WoodPlan5.DataSet_GroupsTableAdapters.sp00090_load_user_screen_layoutTableAdapter sp00090_load_user_screen_layoutTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00090_load_user_screen_layoutTableAdapter();
            WoodPlan5.DataSet_GroupsTableAdapters.sp00091_load_user_screen_components_layoutTableAdapter sp00091_load_user_screen_components_layoutTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00091_load_user_screen_components_layoutTableAdapter();
            sp00090_load_user_screen_layoutTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00090_load_user_screen_layoutTableAdapter.ClearBeforeFill = true;
            sp00091_load_user_screen_components_layoutTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00091_load_user_screen_components_layoutTableAdapter.ClearBeforeFill = true;

            // Load Setting for main Form object //
            sp00090_load_user_screen_layoutTableAdapter.Fill(dataSet_Groups.sp00090_load_user_screen_layout, LayoutID);
            if(dataSet_Groups.sp00090_load_user_screen_layout.Rows.Count <= 0) return -2;  // -2: No Layout Found //

            string strLayoutDescription = Convert.ToString(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["LayoutDescription"]);
            int intCreatedByID = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["CreatedByID"]);
            int intX = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["X"]);
            int intY = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["Y"]);
            int intWidth = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["Width"]);
            int intHeight = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["Height"]);
            int intState = Convert.ToInt32(dataSet_Groups.sp00090_load_user_screen_layout.Rows[0]["State"]);

            frmToProcess.LoadedViewID = LayoutID;
            frmToProcess.LoadedViewCreatedByID = intCreatedByID;
            if (!frmToProcess.IsMdiChild)
            {
                frmToProcess.Left = intX;
                frmToProcess.Top = intY;
                frmToProcess.Width = intWidth;
                frmToProcess.Height = intHeight;
            }
            if (intState == 0)
            {
                frmToProcess.WindowState = FormWindowState.Maximized;
            }
            else if (intState == 1)
            {
                frmToProcess.WindowState = FormWindowState.Minimized;
            }
            else if (intState == 2)
            {
                frmToProcess.WindowState = FormWindowState.Normal;
            }
            else
            {
                frmToProcess.WindowState = FormWindowState.Normal;
            }

            // Load Settings for individual objects on Form //
            sp00091_load_user_screen_components_layoutTableAdapter.Fill(dataSet_Groups.sp00091_load_user_screen_components_layout, LayoutID);
            if (dataSet_Groups.sp00091_load_user_screen_components_layout.Rows.Count <= 0) return 0;  // 0: Main form loaded but no child objects loaded [not neccesarily an error] //

            int intPosition = 0;
            int intCollapsed = 0;
            string strXMLText = "";
            string strObjectName = "";
            string strScreenObjectParentName = "";
            Control ctrlOnForm = null;
            Type tControlType;
           
            EncodingConversions ecTest = new EncodingConversions();
            System.IO.Stream memStream = new System.IO.MemoryStream();
            Byte[] byteArray;
            
            foreach (DataRow row in dataSet_Groups.sp00091_load_user_screen_components_layout.Rows)
            {
                strObjectName = Convert.ToString(row["ScreenObjectName"]);
                if (string.IsNullOrEmpty(strObjectName))
                {
                    continue;
                }
                strScreenObjectParentName = Convert.ToString(row["ScreenObjectParentName"]);
                strXMLText = Convert.ToString(row["XMLText"]);
                intX = Convert.ToInt32(row["X"]);
                intY = Convert.ToInt32(row["Y"]);
                intWidth = Convert.ToInt32(row["Width"]);
                intHeight = Convert.ToInt32(row["Height"]);
                intPosition = Convert.ToInt32(row["Position"]);
                intCollapsed = Convert.ToInt32(row["Collapsed"]);
                Control[] controls = frmToProcess.Controls.Find(strObjectName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm.Dock != DockStyle.Fill) // && (ctrlOnForm.Anchor == AnchorStyles.None || ctrlOnForm.Anchor == AnchorStyles.Top || ctrlOnForm.Anchor == AnchorStyles.Left))
                    {
                        if (!ctrlOnForm.Anchor.ToString().Contains("Left") || !ctrlOnForm.Anchor.ToString().Contains("Right"))
                        {
                            ctrlOnForm.Width = intWidth;
                        }
                        if (!ctrlOnForm.Anchor.ToString().Contains("Top") || !ctrlOnForm.Anchor.ToString().Contains("Bottom"))
                        {
                            ctrlOnForm.Height = intHeight;
                        }
                        if (!ctrlOnForm.Anchor.ToString().Contains("None"))
                        {
                            //ctrlOnForm.Left = intX;
                            //ctrlOnForm.Left = intY;
                        }
                    }
                    tControlType = ctrlOnForm.GetType();

                    switch (tControlType.FullName)
                    {
                        case "DevExpress.XtraEditors.SplitContainerControl":
                            DevExpress.XtraEditors.SplitContainerControl splitcontainerControl = (DevExpress.XtraEditors.SplitContainerControl)ctrlOnForm;
                            splitcontainerControl.SplitterPosition = intPosition;
                            splitcontainerControl.Collapsed = (intCollapsed == 1 ? true : false);
                            break;
                        case "DevExpress.XtraGrid.GridSplitContainer":
                            DevExpress.XtraGrid.GridSplitContainer gridSplitcontainerControl = (DevExpress.XtraGrid.GridSplitContainer)ctrlOnForm;
                            gridSplitcontainerControl.SplitterPosition = intPosition;
                            gridSplitcontainerControl.Initialize();  // Make sure it is initialised (it may not be if the screen was originally opened and the split was never shown) //
                            gridSplitcontainerControl.Grid.ForceInitialize();
                            if (intCollapsed == 1)
                            {
                                gridSplitcontainerControl.HideSplitView();
                            }
                            else 
                            {
                                gridSplitcontainerControl.ShowSplitView();
                            }
                            break;
                        case "DevExpress.XtraEditors.SplitterControl":
                            DevExpress.XtraEditors.SplitterControl splitterControl = (DevExpress.XtraEditors.SplitterControl)ctrlOnForm;
                            splitterControl.SplitPosition = intPosition;
                            break;
                        case "DevExpress.XtraLayout.LayoutControl":
                            DevExpress.XtraLayout.LayoutControl layoutControl = (DevExpress.XtraLayout.LayoutControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                layoutControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraDataLayout.DataLayoutControl":
                            {
                                DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl = (DevExpress.XtraDataLayout.DataLayoutControl)ctrlOnForm;
                                if (strXMLText != "")
                                {
                                    ecTest = new EncodingConversions();
                                    byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                    strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                    System.IO.Stream stream = new System.IO.MemoryStream();
                                    stream.Write(byteArray, 0, byteArray.Length);
                                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                                    dataLayoutControl.RestoreLayoutFromStream(stream);
                                }
                            }
                            break;
                        case "BaseObjects.ExtendedDataLayoutControl":
                            {
                                BaseObjects.ExtendedDataLayoutControl dataLayoutControl = (BaseObjects.ExtendedDataLayoutControl)ctrlOnForm;
                                if (strXMLText != "")
                                {
                                    ecTest = new EncodingConversions();
                                    byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                    strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                    System.IO.Stream stream = new System.IO.MemoryStream();
                                    stream.Write(byteArray, 0, byteArray.Length);
                                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                                    dataLayoutControl.RestoreLayoutFromStream(stream);
                                }
                            }
                            break;
                        case "DevExpress.XtraPivotGrid.PivotGridControl":
                            DevExpress.XtraPivotGrid.PivotGridControl pivotgridControl = (DevExpress.XtraPivotGrid.PivotGridControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                pivotgridControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraCharts.ChartControl":
                            DevExpress.XtraCharts.ChartControl chartControl = (DevExpress.XtraCharts.ChartControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                chartControl.LoadFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraScheduler.SchedulerControl":
                            DevExpress.XtraScheduler.SchedulerControl schedulerControl = (DevExpress.XtraScheduler.SchedulerControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                schedulerControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraGauges.Win.GaugeControl":
                            DevExpress.XtraGauges.Win.GaugeControl gaugeControl = (DevExpress.XtraGauges.Win.GaugeControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                gaugeControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraVerticalGrid.VGridControl":
                            DevExpress.XtraVerticalGrid.VGridControl verticalgridControl = (DevExpress.XtraVerticalGrid.VGridControl)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                verticalgridControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraTreeList.TreeList":
                            DevExpress.XtraTreeList.TreeList treeControl = (DevExpress.XtraTreeList.TreeList)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                treeControl.RestoreLayoutFromStream(stream);
                            }
                            break;
                        case "DevExpress.XtraGrid.Views.Grid.GridView":
                            if (strXMLText != "")
                            {
                                Control[] controls2 = frmToProcess.Controls.Find(strScreenObjectParentName, true);  // Get Parent Grid // 
                                if (controls2.Length == 1)  // Only proceed if just one control was found matching the name // 
                                {
                                    ctrlOnForm = controls2[0] as Control;
                                    tControlType = ctrlOnForm.GetType();
                                    DevExpress.XtraGrid.GridControl gridControl = (DevExpress.XtraGrid.GridControl)ctrlOnForm;
                                    foreach (DevExpress.XtraGrid.Views.Grid.GridView gridView in gridControl.Views)
                                    {
                                        if (gridView.Name == strObjectName)
                                        {
                                            ecTest = new EncodingConversions();
                                            byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                            strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                            System.IO.Stream stream = new System.IO.MemoryStream();
                                            stream.Write(byteArray, 0, byteArray.Length);
                                            stream.Seek(0, System.IO.SeekOrigin.Begin);
                                            gridView.RestoreLayoutFromStream(stream);
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        case "DevExpress.DashboardWin.DashboardDesigner":
                            DevExpress.DashboardWin.DashboardDesigner dashboardControl = (DevExpress.DashboardWin.DashboardDesigner)ctrlOnForm;
                            if (strXMLText != "")
                            {
                                ecTest = new EncodingConversions();
                                byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                System.IO.Stream stream = new System.IO.MemoryStream();
                                stream.Write(byteArray, 0, byteArray.Length);
                                stream.Seek(0, System.IO.SeekOrigin.Begin);
                                dashboardControl.Dashboard.LoadFromXml(stream);
                            }
                            break;
                    }
                }
                else  // No Control, so check to see if it's a grid view and use the parent value from the SP to get the parent grid control //
                {
                    if (strScreenObjectParentName != "" && strXMLText != "")
                    {
                        Control[] controls2 = frmToProcess.Controls.Find(strScreenObjectParentName, true);  // Get Parent Grid // 
                        if (controls2.Length == 1)  // Only proceed if just one control was found matching the name // 
                        {
                            ctrlOnForm = controls2[0] as Control;
                            tControlType = ctrlOnForm.GetType();
                            DevExpress.XtraGrid.GridControl gridControl = (DevExpress.XtraGrid.GridControl)ctrlOnForm;
                            foreach (DevExpress.XtraGrid.Views.Grid.GridView gridView in gridControl.Views)
                            {
                                if (gridView.Name == strObjectName)
                                {
                                    ecTest = new EncodingConversions();
                                    byteArray = ecTest.StringToUTF8ByteArray(strXMLText);
                                    strXMLText = ecTest.UTF8ByteArrayToString(byteArray);
                                    System.IO.Stream stream = new System.IO.MemoryStream();
                                    stream.Write(byteArray, 0, byteArray.Length);
                                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                                    gridView.RestoreLayoutFromStream(stream);
                                    break;
                                }
                            }
                        }
                    }
                    
                }
            }

            // Invoke Post Load View event on Base form //
            MethodInfo method = typeof(frmBase).GetMethod("PostLoadView");
            if (method != null)
            {
                method.Invoke(frmToProcess, new object[] { null });
            }

            return 1;
        }

        public int Load_Default_Screen_Layout(string strConnectionString, frmBase frmToProcess, GlobalSettings globalSettings)
        {
            if (frmToProcess == null) return -1;  // -1: No form passed in //
            int intScreenID = frmToProcess.FormID;
            if (intScreenID <= 0) return -2;  // Missing FormId on passed in form //

            DataSet_Groups dataSet_Groups = new DataSet_Groups();
            WoodPlan5.DataSet_GroupsTableAdapters.sp00092_get_default_user_screen_layoutTableAdapter sp00092_get_default_user_screen_layoutTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00092_get_default_user_screen_layoutTableAdapter();
            sp00092_get_default_user_screen_layoutTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00092_get_default_user_screen_layoutTableAdapter.ClearBeforeFill = true;
            sp00092_get_default_user_screen_layoutTableAdapter.Fill(dataSet_Groups.sp00092_get_default_user_screen_layout, intScreenID, globalSettings.UserID, globalSettings.PersonType);
            if (dataSet_Groups.sp00092_get_default_user_screen_layout.Rows.Count != 1)
            {
                return 0;  // No Default or more than one default so abort //
            }
            else
            {
                int intLayoutID = Convert.ToInt32(dataSet_Groups.sp00092_get_default_user_screen_layout.Rows[0]["LayoutID"]);
                Load_User_Specified_Screen_Layout(strConnectionString, frmToProcess, intLayoutID);
            }


            return 1;
        }

        public void Store_Screen_Object_Settings(string strConnectionString, int intLayoutID, frmBase fbForm, Control ctrlWithChildren)
        {
            System.Windows.Forms.Control.ControlCollection ctrlToIterate;
            Type tControlType;
            System.IO.Stream stream = new System.IO.MemoryStream();
            StreamReader srReader;

            string strChangeType = "insert";
            string strScreenObjectName = "";
            string strScreenObjectParentName = "";
            int intX = 0;
            int intY = 0;
            int intHeight = 0;
            int intWidth = 0;
            int intPosition = 0;
            int intCollapsed = 0;
            string strXML = "";
            DataSet_GroupsTableAdapters.QueriesTableAdapter SaveScreenObjectLayout = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
            SaveScreenObjectLayout.ChangeConnectionString(strConnectionString);

            if (ctrlWithChildren != null)
            {
                ctrlToIterate = (System.Windows.Forms.Control.ControlCollection)ctrlWithChildren.Controls;
            }
            else
            {
                ctrlToIterate = (System.Windows.Forms.Control.ControlCollection)fbForm.Controls;
            }
            foreach (Control ctrlOnForm in ctrlToIterate)
            {
                // Find out type...
                tControlType = ctrlOnForm.GetType();
                switch (tControlType.FullName)
                {
                    case "DevExpress.XtraLayout.LayoutControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraLayout.LayoutControl layoutControl = (DevExpress.XtraLayout.LayoutControl)ctrlOnForm;
                        layoutControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraDataLayout.DataLayoutControl":
                        {
                            strXML = "";
                            stream = new System.IO.MemoryStream();
                            DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl = (DevExpress.XtraDataLayout.DataLayoutControl)ctrlOnForm;
                            dataLayoutControl.SaveLayoutToStream(stream);
                            stream.Seek(0, System.IO.SeekOrigin.Begin);
                            if (stream != null)
                            {
                                srReader = new StreamReader(stream);
                                strXML = srReader.ReadToEnd();
                            }
                            strScreenObjectName = ctrlOnForm.Name;
                            strScreenObjectParentName = "";
                            intX = ctrlOnForm.Bounds.X;
                            intY = ctrlOnForm.Bounds.Y;
                            intHeight = ctrlOnForm.Bounds.Height;
                            intWidth = ctrlOnForm.Bounds.Width;
                            if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                            {
                                SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                                _StoredItemList.Add(strScreenObjectName);
                            }
                            Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        }
                        break;
                    case "BaseObjects.ExtendedDataLayoutControl":
                        {
                            strXML = "";
                            stream = new System.IO.MemoryStream();
                            BaseObjects.ExtendedDataLayoutControl dataLayoutControl = (BaseObjects.ExtendedDataLayoutControl)ctrlOnForm;
                            dataLayoutControl.SaveLayoutToStream(stream);
                            stream.Seek(0, System.IO.SeekOrigin.Begin);
                            if (stream != null)
                            {
                                srReader = new StreamReader(stream);
                                strXML = srReader.ReadToEnd();
                            }
                            strScreenObjectName = ctrlOnForm.Name;
                            strScreenObjectParentName = "";
                            intX = ctrlOnForm.Bounds.X;
                            intY = ctrlOnForm.Bounds.Y;
                            intHeight = ctrlOnForm.Bounds.Height;
                            intWidth = ctrlOnForm.Bounds.Width;
                            if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                            {
                                SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                                _StoredItemList.Add(strScreenObjectName);
                            }
                            Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        }
                        break;
                    case "DevExpress.XtraEditors.SplitterControl":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        intPosition = ((DevExpress.XtraEditors.SplitterControl)ctrlOnForm).SplitPosition;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraEditors.SplitContainerControl":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        intPosition = ((DevExpress.XtraEditors.SplitContainerControl)ctrlOnForm).SplitterPosition;
                        intCollapsed = (((DevExpress.XtraEditors.SplitContainerControl)ctrlOnForm).Collapsed ? 1 : 0);
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraEditors.SplitGroupPanel":
                        // Don't write the details as it doesn't have a name, just process it and any child controls on it //
                        /*strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType);
                        */
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraGrid.GridSplitContainer":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        intPosition = ((DevExpress.XtraGrid.GridSplitContainer)ctrlOnForm).SplitterPosition;
                        intCollapsed = (((DevExpress.XtraGrid.GridSplitContainer)ctrlOnForm).IsSplitViewVisible ? 0 : 1);
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, intCollapsed);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraEditors.PopupContainerControl":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraTab.XtraTabControl":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraTab.XtraTabPage":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraCharts.ChartControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraCharts.ChartControl chartControl = (DevExpress.XtraCharts.ChartControl)ctrlOnForm;
                        chartControl.SaveToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraGauges.Win.GaugeControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraGauges.Win.GaugeControl gaugeControl = (DevExpress.XtraGauges.Win.GaugeControl)ctrlOnForm;
                        gaugeControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraScheduler.SchedulerControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraScheduler.SchedulerControl schedulerControl = (DevExpress.XtraScheduler.SchedulerControl)ctrlOnForm;
                        schedulerControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraEditors.PanelControl":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraBars.Docking.DockPanel":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraBars.Docking.ControlContainer":
                        strXML = "";
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        Store_Screen_Object_Settings(strConnectionString, intLayoutID, fbForm, ctrlOnForm);  // Call recursivly to drill down into any child controls //
                        break;
                    case "DevExpress.XtraPivotGrid.PivotGridControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraPivotGrid.PivotGridControl pivotgridControl = (DevExpress.XtraPivotGrid.PivotGridControl)ctrlOnForm;
                        pivotgridControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraVerticalGrid.VGridControl":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraVerticalGrid.VGridControl verticalgridControl = (DevExpress.XtraVerticalGrid.VGridControl)ctrlOnForm;
                        verticalgridControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraTreeList.TreeList":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.XtraTreeList.TreeList treeControl = (DevExpress.XtraTreeList.TreeList)ctrlOnForm;
                        treeControl.SaveLayoutToStream(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    case "DevExpress.XtraGrid.GridControl":
                        strXML = "";
                        DevExpress.XtraGrid.GridControl gridControl = (DevExpress.XtraGrid.GridControl)ctrlOnForm;
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        // Get each GridView and save it's layout to XML //
                        foreach (DevExpress.XtraGrid.Views.Grid.GridView gvView in gridControl.Views)
                        {
                            strXML = "";
                            stream = new System.IO.MemoryStream();
                            gvView.SaveLayoutToStream(stream);
                            stream.Seek(0, System.IO.SeekOrigin.Begin);
                            if (stream != null)
                            {
                                srReader = new StreamReader(stream);
                                strXML = srReader.ReadToEnd();
                            }
                            strScreenObjectName = gvView.Name;
                            strScreenObjectParentName = ctrlOnForm.Name;  // Store Parent Grid's Name //
                            intX = ctrlOnForm.Bounds.X;
                            intY = ctrlOnForm.Bounds.Y;
                            intHeight = ctrlOnForm.Bounds.Height;
                            intWidth = ctrlOnForm.Bounds.Width;
                            if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                            {
                                SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                                _StoredItemList.Add(strScreenObjectName);
                            }
                        }
                        break;
                    case "DevExpress.DashboardWin.DashboardDesigner":
                        strXML = "";
                        stream = new System.IO.MemoryStream();
                        DevExpress.DashboardWin.DashboardDesigner dashboardControl = (DevExpress.DashboardWin.DashboardDesigner)ctrlOnForm;
                        dashboardControl.Dashboard.SaveToXml(stream);
                        stream.Seek(0, System.IO.SeekOrigin.Begin);
                        if (stream != null)
                        {
                            srReader = new StreamReader(stream);
                            strXML = srReader.ReadToEnd();
                        }
                        strScreenObjectName = ctrlOnForm.Name;
                        strScreenObjectParentName = "";
                        intX = ctrlOnForm.Bounds.X;
                        intY = ctrlOnForm.Bounds.Y;
                        intHeight = ctrlOnForm.Bounds.Height;
                        intWidth = ctrlOnForm.Bounds.Width;
                        if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                        {
                            SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                            _StoredItemList.Add(strScreenObjectName);
                        }
                        break;
                    default:
                        if (ctrlOnForm.Name.Trim() != "")
                        {
                            strXML = "";
                            strScreenObjectName = ctrlOnForm.Name;
                            strScreenObjectParentName = "";
                            intX = ctrlOnForm.Bounds.X;
                            intY = ctrlOnForm.Bounds.Y;
                            intHeight = ctrlOnForm.Bounds.Height;
                            intWidth = ctrlOnForm.Bounds.Width;
                            if (!_StoredItemList.Contains(strScreenObjectName) && !string.IsNullOrEmpty(strScreenObjectName))
                            {
                                SaveScreenObjectLayout.sp00084_Save_User_Screen_Object_Layout(0, intLayoutID, strScreenObjectName, strScreenObjectParentName, intX, intY, intWidth, intHeight, strXML, intPosition, strChangeType, 0);
                                _StoredItemList.Add(strScreenObjectName);
                            }
                        }
                        break;
                }
            }
        }


    }
}

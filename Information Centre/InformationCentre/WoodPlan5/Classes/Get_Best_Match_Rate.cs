using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WoodPlan5;
using BaseObjects;

namespace WoodPlan5
{
    class Get_Best_Match_Rate
    {
        public int GetRate(string strConnectionString, ref string strReturnedRateDescription, ref decimal decReturnedRate, ref int intReturnedRateID, int intOnScheduleOfRates, int intInspectionID, int intActionByID, int intAction)
        {
            // Returns a Best match rate in the passed by reference method parameters //          
            strReturnedRateDescription = "";    // Value returned from Method as passed by reference //
            decReturnedRate = (decimal)0.00;    // Value returned from Method as passed by reference //
            intReturnedRateID = 0;              // Value returned from Method as passed by reference //

            SqlDataAdapter sdaRates = new SqlDataAdapter();
            DataSet dsRates = new DataSet("NewDataSet");

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp01392_AT_Job_Rates_For_Action", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            sdaRates = new SqlDataAdapter(cmd);
            sdaRates.Fill(dsRates, "Table");

            if (dsRates.Tables[0].Rows.Count == 0) return 0;  // No Rates so abort //

            // Get Criteria Used //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intCriteriaType = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesJobRateCriteriaID"));

            DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter GetMatches = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
            string strMatchingJobRates = GetMatches.sp01393_AT_Action_Get_Matching_Job_Rates(intInspectionID, intCriteriaType, intAction, intActionByID).ToString() ?? "";
            if (!string.IsNullOrEmpty(strMatchingJobRates))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strMatchingJobRates.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 1) return 0;  // More than 1 matching best fit rate so abort //

                int intID = Convert.ToInt32(strArray[0]);
                DataRow[] drRows = dsRates.Tables[0].Select("RateID = " + intID.ToString());
                if (drRows.Length != 1) return 0;

                DataRow dr = drRows[0];
                float rate = (string.IsNullOrEmpty(dr["Rate"].ToString()) ? (float)0.00 : (float)Convert.ToDecimal(dr["Rate"]));
                strReturnedRateDescription = dr["RateDesc"].ToString() + " [" + String.Format("{0:C}", rate) + "]";
                decReturnedRate = Convert.ToDecimal(dr["Rate"]);
                intReturnedRateID = Convert.ToInt32(dr["RateID"]);
            }
            return 1;
        }
    }
}

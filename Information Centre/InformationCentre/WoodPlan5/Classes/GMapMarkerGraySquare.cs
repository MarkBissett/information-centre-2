﻿

namespace WoodPlan5
{
    using System.Drawing;
    using GMap.NET;
    using GMap.NET.WindowsForms;
    using GMap.NET.WindowsForms.Markers;

    public class GMapMarkerGraySquare : GMapMarkerCustom
    {

        public GMapMarkerGraySquare(PointLatLng p)
            : base(p)
        {
            Icon = Properties.Resources.transparentsquare;
            this.Size = new Size(8, 8);
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frmUserFeedBack1 : BaseObjects.frmBase
    {
        /// <summary>
        /// 
        /// </summary>
        public frmUserFeedBack1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCaption"></param>
        public void SetCaption1(string strCaption)
        {
            labelControl1.Text = strCaption;
        }

        private void frmUserFeedBack1_Load(object sender, EventArgs e)
        {
            this.Height = 63;
            this.Width = 387;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMilliSeconds"></param>
        public void SetTimer(int intMilliSeconds)
        {
            if (intMilliSeconds > 0)
            {
                timer1.Enabled = true;
                timer1.Interval = intMilliSeconds;
                timer1.Start();
            }
            else
            {
                timer1.Enabled = false;
                timer1.Stop();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intImage"></param>
        public void SetImage(int intImage)
        {
            pictureEdit1.Image = imageList1.Images[intImage];
        }


    }
}


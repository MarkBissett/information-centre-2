﻿// How to allow end users to customize column captions in the GridView.
// 
// This code adds an item to the Column Header Menu, which
// invokes the popup dialog allowing end-users to customize the column's caption.

namespace BaseObjects
{
    partial class frm_Select_Records_From_User_Input_Criteria {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.lookUpEditImportGridColumns = new DevExpress.XtraEditors.LookUpEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.textEditSeparator = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.radioGroupSearchOperator = new DevExpress.XtraEditors.RadioGroup();
            this.checkEditClearPriorSearches = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.radioGroupMatchDirection = new DevExpress.XtraEditors.RadioGroup();
            this.labelMatchDirection = new System.Windows.Forms.Label();
            this.checkEditCaseSensitive = new DevExpress.XtraEditors.CheckEdit();
            this.CleanSearchStringCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditInvert = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.checkEditShowNotFound = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditImportGridColumns.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSeparator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSearchOperator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearPriorSearches.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMatchDirection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCaseSensitive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CleanSearchStringCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditInvert.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowNotFound.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Column to Match:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lookUpEditImportGridColumns
            // 
            this.lookUpEditImportGridColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditImportGridColumns.Location = new System.Drawing.Point(103, 10);
            this.lookUpEditImportGridColumns.Name = "lookUpEditImportGridColumns";
            this.lookUpEditImportGridColumns.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditImportGridColumns.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ColumnName", "Column Name", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ColumnCaption", "Column", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.Default)});
            this.lookUpEditImportGridColumns.Properties.NullText = "";
            this.lookUpEditImportGridColumns.Properties.NullValuePrompt = "Select Column";
            this.lookUpEditImportGridColumns.Size = new System.Drawing.Size(390, 20);
            this.lookUpEditImportGridColumns.TabIndex = 7;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageOptions.Image = global::BaseObjects.Properties.Resources.Ok_16;
            this.btnOK.Location = new System.Drawing.Point(337, 450);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "Find";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageOptions.Image = global::BaseObjects.Properties.Resources.Close_16;
            this.btnCancel.Location = new System.Drawing.Point(418, 450);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit1.Location = new System.Drawing.Point(11, 176);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(482, 260);
            this.memoEdit1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(8, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Value(s) to Search for:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textEditSeparator
            // 
            this.textEditSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditSeparator.Location = new System.Drawing.Point(103, 36);
            this.textEditSeparator.Name = "textEditSeparator";
            this.textEditSeparator.Properties.MaxLength = 100;
            this.textEditSeparator.Size = new System.Drawing.Size(391, 20);
            this.textEditSeparator.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(8, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Separator:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioGroupSearchOperator
            // 
            this.radioGroupSearchOperator.EditValue = 0;
            this.radioGroupSearchOperator.Location = new System.Drawing.Point(98, 62);
            this.radioGroupSearchOperator.Name = "radioGroupSearchOperator";
            this.radioGroupSearchOperator.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupSearchOperator.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupSearchOperator.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroupSearchOperator.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Contains"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Equals")});
            this.radioGroupSearchOperator.Size = new System.Drawing.Size(310, 23);
            this.radioGroupSearchOperator.TabIndex = 15;
            this.radioGroupSearchOperator.EditValueChanged += new System.EventHandler(this.radioGroupSearchOperator_EditValueChanged);
            // 
            // checkEditClearPriorSearches
            // 
            this.checkEditClearPriorSearches.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditClearPriorSearches.EditValue = 1;
            this.checkEditClearPriorSearches.Location = new System.Drawing.Point(12, 453);
            this.checkEditClearPriorSearches.Name = "checkEditClearPriorSearches";
            this.checkEditClearPriorSearches.Properties.Caption = "Clear Prior Search Results";
            this.checkEditClearPriorSearches.Properties.ValueChecked = 1;
            this.checkEditClearPriorSearches.Properties.ValueUnchecked = 0;
            this.checkEditClearPriorSearches.Size = new System.Drawing.Size(150, 19);
            this.checkEditClearPriorSearches.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(9, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Operator:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioGroupMatchDirection
            // 
            this.radioGroupMatchDirection.EditValue = 0;
            this.radioGroupMatchDirection.Location = new System.Drawing.Point(98, 86);
            this.radioGroupMatchDirection.Name = "radioGroupMatchDirection";
            this.radioGroupMatchDirection.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupMatchDirection.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupMatchDirection.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroupMatchDirection.Properties.Columns = 2;
            this.radioGroupMatchDirection.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Grid Value --> Input Value"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Input Value --> Grid Value")});
            this.radioGroupMatchDirection.Size = new System.Drawing.Size(310, 23);
            this.radioGroupMatchDirection.TabIndex = 18;
            // 
            // labelMatchDirection
            // 
            this.labelMatchDirection.AutoSize = true;
            this.labelMatchDirection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMatchDirection.Location = new System.Drawing.Point(10, 91);
            this.labelMatchDirection.Name = "labelMatchDirection";
            this.labelMatchDirection.Size = new System.Drawing.Size(85, 13);
            this.labelMatchDirection.TabIndex = 19;
            this.labelMatchDirection.Text = "Match Direction:";
            this.labelMatchDirection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkEditCaseSensitive
            // 
            this.checkEditCaseSensitive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditCaseSensitive.Location = new System.Drawing.Point(240, 155);
            this.checkEditCaseSensitive.Name = "checkEditCaseSensitive";
            this.checkEditCaseSensitive.Properties.Caption = "Case Sensitive";
            this.checkEditCaseSensitive.Size = new System.Drawing.Size(89, 19);
            this.checkEditCaseSensitive.TabIndex = 20;
            // 
            // CleanSearchStringCheckEdit
            // 
            this.CleanSearchStringCheckEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CleanSearchStringCheckEdit.EditValue = 1;
            this.CleanSearchStringCheckEdit.Location = new System.Drawing.Point(172, 454);
            this.CleanSearchStringCheckEdit.Name = "CleanSearchStringCheckEdit";
            this.CleanSearchStringCheckEdit.Properties.Caption = "Clean Search String";
            this.CleanSearchStringCheckEdit.Properties.ValueChecked = 1;
            this.CleanSearchStringCheckEdit.Properties.ValueUnchecked = 0;
            this.CleanSearchStringCheckEdit.Size = new System.Drawing.Size(118, 19);
            this.CleanSearchStringCheckEdit.TabIndex = 21;
            this.CleanSearchStringCheckEdit.ToolTip = "Tick me to remove Spaces, Tabs and Carriage Returns from the Value to Search For." +
    " On clicking Find, these values will be removed.";
            // 
            // checkEditInvert
            // 
            this.checkEditInvert.Location = new System.Drawing.Point(102, 114);
            this.checkEditInvert.Name = "checkEditInvert";
            this.checkEditInvert.Properties.Caption = "(Invert Selection)";
            this.checkEditInvert.Size = new System.Drawing.Size(107, 19);
            this.checkEditInvert.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(10, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Find Mis-matches:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkEditShowNotFound
            // 
            this.checkEditShowNotFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditShowNotFound.Location = new System.Drawing.Point(361, 154);
            this.checkEditShowNotFound.Name = "checkEditShowNotFound";
            this.checkEditShowNotFound.Properties.Caption = "Show Values Not Found";
            this.checkEditShowNotFound.Size = new System.Drawing.Size(133, 19);
            this.checkEditShowNotFound.TabIndex = 24;
            // 
            // frm_Select_Records_From_User_Input_Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 482);
            this.Controls.Add(this.checkEditShowNotFound);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.checkEditInvert);
            this.Controls.Add(this.CleanSearchStringCheckEdit);
            this.Controls.Add(this.checkEditCaseSensitive);
            this.Controls.Add(this.labelMatchDirection);
            this.Controls.Add(this.radioGroupMatchDirection);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkEditClearPriorSearches);
            this.Controls.Add(this.radioGroupSearchOperator);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textEditSeparator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lookUpEditImportGridColumns);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frm_Select_Records_From_User_Input_Criteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select based on Input Value";
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditImportGridColumns.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSeparator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSearchOperator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearPriorSearches.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMatchDirection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCaseSensitive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CleanSearchStringCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditInvert.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowNotFound.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditImportGridColumns;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textEditSeparator;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.RadioGroup radioGroupSearchOperator;
        private DevExpress.XtraEditors.CheckEdit checkEditClearPriorSearches;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.RadioGroup radioGroupMatchDirection;
        private System.Windows.Forms.Label labelMatchDirection;
        private DevExpress.XtraEditors.CheckEdit checkEditCaseSensitive;
        private DevExpress.XtraEditors.CheckEdit CleanSearchStringCheckEdit;
        private DevExpress.XtraEditors.CheckEdit checkEditInvert;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit checkEditShowNotFound;

    }
}
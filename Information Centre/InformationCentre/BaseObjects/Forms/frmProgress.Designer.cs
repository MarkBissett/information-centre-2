namespace BaseObjects
{
    partial class frmProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState1 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState2 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleLabel scaleLabel1 = new DevExpress.XtraGauges.Core.Model.ScaleLabel();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState3 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState4 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState5 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState6 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState7 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState8 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState9 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState10 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState11 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState12 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState13 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState14 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState15 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState16 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState17 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState18 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState19 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            DevExpress.XtraGauges.Core.Model.ScaleIndicatorState scaleIndicatorState20 = new DevExpress.XtraGauges.Core.Model.ScaleIndicatorState();
            this.rating1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.linearGauge1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearGauge();
            this.linearScaleStateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent();
            this.linearScaleStateIndicatorComponent2 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent3 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent4 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent5 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent6 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent7 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent8 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent9 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.linearScaleStateIndicatorComponent10 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.loadingTimer = new System.Windows.Forms.Timer();
            this.rating1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.linearGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rating1
            // 
            this.rating1.AutoLayout = false;
            this.rating1.BackColor = System.Drawing.Color.Transparent;
            this.rating1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rating1.Controls.Add(this.labelControl1);
            this.rating1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.linearGauge1});
            this.rating1.Location = new System.Drawing.Point(-27, -67);
            this.rating1.MaximumSize = new System.Drawing.Size(250, 400);
            this.rating1.MinimumSize = new System.Drawing.Size(250, 400);
            this.rating1.Name = "rating1";
            this.rating1.Size = new System.Drawing.Size(250, 400);
            this.rating1.TabIndex = 13;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(35, 254);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(182, 36);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Loading Data, Please Wait...";
            // 
            // linearGauge1
            // 
            this.linearGauge1.Bounds = new System.Drawing.Rectangle(15, 15, 220, 299);
            this.linearGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent[] {
            this.linearScaleStateIndicatorComponent1,
            this.linearScaleStateIndicatorComponent2,
            this.linearScaleStateIndicatorComponent3,
            this.linearScaleStateIndicatorComponent4,
            this.linearScaleStateIndicatorComponent5,
            this.linearScaleStateIndicatorComponent6,
            this.linearScaleStateIndicatorComponent7,
            this.linearScaleStateIndicatorComponent8,
            this.linearScaleStateIndicatorComponent9,
            this.linearScaleStateIndicatorComponent10});
            this.linearGauge1.Name = "linearGauge1";
            this.linearGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent[] {
            this.linearScaleComponent1});
            // 
            // linearScaleStateIndicatorComponent1
            // 
            this.linearScaleStateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(15F, 125F);
            this.linearScaleStateIndicatorComponent1.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent1.Name = "Indicator0";
            this.linearScaleStateIndicatorComponent1.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState1.IntervalLength = 100F;
            scaleIndicatorState1.Name = "Colored";
            scaleIndicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState1.StartValue = 0.01F;
            scaleIndicatorState2.IntervalLength = 0F;
            scaleIndicatorState2.Name = "Empty";
            scaleIndicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState1,
            scaleIndicatorState2});
            this.linearScaleStateIndicatorComponent1.ZOrder = 100;
            // 
            // linearScaleComponent1
            // 
            this.linearScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.linearScaleComponent1.EndPoint = new DevExpress.XtraGauges.Core.Base.PointF2D(62.5F, 225F);
            scaleLabel1.AppearanceText.Font = new System.Drawing.Font("Tahoma", 12F);
            scaleLabel1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            scaleLabel1.FormatString = "{0} {2:P0}";
            scaleLabel1.Name = "Label0";
            scaleLabel1.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(62.5F, 125F);
            scaleLabel1.Size = new System.Drawing.SizeF(60F, 30F);
            scaleLabel1.Text = "";
            this.linearScaleComponent1.Labels.AddRange(new DevExpress.XtraGauges.Core.Model.ILabel[] {
            scaleLabel1});
            this.linearScaleComponent1.MajorTickCount = 2;
            this.linearScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.linearScaleComponent1.MajorTickmark.ShapeOffset = -20F;
            this.linearScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Linear_Style1_1;
            this.linearScaleComponent1.MajorTickmark.ShowText = false;
            this.linearScaleComponent1.MajorTickmark.ShowTick = false;
            this.linearScaleComponent1.MajorTickmark.TextOffset = -32F;
            this.linearScaleComponent1.MaxValue = 100F;
            this.linearScaleComponent1.MinorTickCount = 0;
            this.linearScaleComponent1.MinorTickmark.ShapeOffset = -14F;
            this.linearScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Linear_Style1_2;
            this.linearScaleComponent1.MinorTickmark.ShowTick = false;
            this.linearScaleComponent1.Name = "scale1";
            this.linearScaleComponent1.StartPoint = new DevExpress.XtraGauges.Core.Base.PointF2D(62.5F, 25F);
            // 
            // linearScaleStateIndicatorComponent2
            // 
            this.linearScaleStateIndicatorComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(22.05F, 95.61F);
            this.linearScaleStateIndicatorComponent2.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent2.Name = "Indicator1";
            this.linearScaleStateIndicatorComponent2.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState3.IntervalLength = 90F;
            scaleIndicatorState3.Name = "Colored";
            scaleIndicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState3.StartValue = 10.01F;
            scaleIndicatorState4.IntervalLength = 10F;
            scaleIndicatorState4.Name = "Empty";
            scaleIndicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent2.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState3,
            scaleIndicatorState4});
            this.linearScaleStateIndicatorComponent2.ZOrder = 99;
            // 
            // linearScaleStateIndicatorComponent3
            // 
            this.linearScaleStateIndicatorComponent3.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(47.05F, 77.45F);
            this.linearScaleStateIndicatorComponent3.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent3.Name = "Indicator2";
            this.linearScaleStateIndicatorComponent3.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState5.IntervalLength = 80F;
            scaleIndicatorState5.Name = "Colored";
            scaleIndicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState5.StartValue = 20.01F;
            scaleIndicatorState6.IntervalLength = 20F;
            scaleIndicatorState6.Name = "Empty";
            scaleIndicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent3.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState5,
            scaleIndicatorState6});
            this.linearScaleStateIndicatorComponent3.ZOrder = 98;
            // 
            // linearScaleStateIndicatorComponent4
            // 
            this.linearScaleStateIndicatorComponent4.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(77.95F, 77.45F);
            this.linearScaleStateIndicatorComponent4.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent4.Name = "Indicator3";
            this.linearScaleStateIndicatorComponent4.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState7.IntervalLength = 70F;
            scaleIndicatorState7.Name = "Colored";
            scaleIndicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState7.StartValue = 30.01F;
            scaleIndicatorState8.IntervalLength = 30F;
            scaleIndicatorState8.Name = "Empty";
            scaleIndicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent4.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState7,
            scaleIndicatorState8});
            this.linearScaleStateIndicatorComponent4.ZOrder = 97;
            // 
            // linearScaleStateIndicatorComponent5
            // 
            this.linearScaleStateIndicatorComponent5.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(102.95F, 95.61F);
            this.linearScaleStateIndicatorComponent5.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent5.Name = "Indicator4";
            this.linearScaleStateIndicatorComponent5.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState9.IntervalLength = 60F;
            scaleIndicatorState9.Name = "Colored";
            scaleIndicatorState9.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState9.StartValue = 40.01F;
            scaleIndicatorState10.IntervalLength = 40F;
            scaleIndicatorState10.Name = "Empty";
            scaleIndicatorState10.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent5.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState9,
            scaleIndicatorState10});
            this.linearScaleStateIndicatorComponent5.ZOrder = 96;
            // 
            // linearScaleStateIndicatorComponent6
            // 
            this.linearScaleStateIndicatorComponent6.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(110F, 125F);
            this.linearScaleStateIndicatorComponent6.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent6.Name = "Indicator5";
            this.linearScaleStateIndicatorComponent6.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState11.IntervalLength = 50F;
            scaleIndicatorState11.Name = "Colored";
            scaleIndicatorState11.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState11.StartValue = 50.01F;
            scaleIndicatorState12.IntervalLength = 50F;
            scaleIndicatorState12.Name = "Empty";
            scaleIndicatorState12.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent6.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState11,
            scaleIndicatorState12});
            this.linearScaleStateIndicatorComponent6.ZOrder = 95;
            // 
            // linearScaleStateIndicatorComponent7
            // 
            this.linearScaleStateIndicatorComponent7.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(102.92F, 154.39F);
            this.linearScaleStateIndicatorComponent7.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent7.Name = "Indicator6";
            this.linearScaleStateIndicatorComponent7.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState13.IntervalLength = 40F;
            scaleIndicatorState13.Name = "Colored";
            scaleIndicatorState13.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState13.StartValue = 60.01F;
            scaleIndicatorState14.IntervalLength = 60F;
            scaleIndicatorState14.Name = "Empty";
            scaleIndicatorState14.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent7.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState13,
            scaleIndicatorState14});
            this.linearScaleStateIndicatorComponent7.ZOrder = 94;
            // 
            // linearScaleStateIndicatorComponent8
            // 
            this.linearScaleStateIndicatorComponent8.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(77.95F, 172.55F);
            this.linearScaleStateIndicatorComponent8.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent8.Name = "Indicator7";
            this.linearScaleStateIndicatorComponent8.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState15.IntervalLength = 30F;
            scaleIndicatorState15.Name = "Colored";
            scaleIndicatorState15.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState15.StartValue = 70.01F;
            scaleIndicatorState16.IntervalLength = 70F;
            scaleIndicatorState16.Name = "Empty";
            scaleIndicatorState16.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent8.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState15,
            scaleIndicatorState16});
            this.linearScaleStateIndicatorComponent8.ZOrder = 93;
            // 
            // linearScaleStateIndicatorComponent9
            // 
            this.linearScaleStateIndicatorComponent9.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(47.05F, 172.55F);
            this.linearScaleStateIndicatorComponent9.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent9.Name = "Indicator8";
            this.linearScaleStateIndicatorComponent9.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState17.IntervalLength = 20F;
            scaleIndicatorState17.Name = "Colored";
            scaleIndicatorState17.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState17.StartValue = 80.01F;
            scaleIndicatorState18.IntervalLength = 80F;
            scaleIndicatorState18.Name = "Empty";
            scaleIndicatorState18.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent9.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState17,
            scaleIndicatorState18});
            this.linearScaleStateIndicatorComponent9.ZOrder = 92;
            // 
            // linearScaleStateIndicatorComponent10
            // 
            this.linearScaleStateIndicatorComponent10.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(22.05F, 154.39F);
            this.linearScaleStateIndicatorComponent10.IndicatorScale = this.linearScaleComponent1;
            this.linearScaleStateIndicatorComponent10.Name = "Indicator9";
            this.linearScaleStateIndicatorComponent10.Size = new System.Drawing.SizeF(28.94356F, 28.94356F);
            scaleIndicatorState19.IntervalLength = 10F;
            scaleIndicatorState19.Name = "Colored";
            scaleIndicatorState19.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem9;
            scaleIndicatorState19.StartValue = 90.01F;
            scaleIndicatorState20.IntervalLength = 90F;
            scaleIndicatorState20.Name = "Empty";
            scaleIndicatorState20.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ProgressItem5;
            this.linearScaleStateIndicatorComponent10.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            scaleIndicatorState19,
            scaleIndicatorState20});
            this.linearScaleStateIndicatorComponent10.ZOrder = 91;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.rating1);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(198, 226);
            this.panelControl1.TabIndex = 14;
            // 
            // loadingTimer
            // 
            this.loadingTimer.Interval = 500;
            this.loadingTimer.Tick += new System.EventHandler(this.loadingTimer_Tick);
            // 
            // frmProgress
            // 
            this.ClientSize = new System.Drawing.Size(198, 226);
            this.ControlBox = false;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProgress";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Progress";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProgress_FormClosing);
            this.rating1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.linearGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleStateIndicatorComponent10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGauges.Win.GaugeControl rating1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearGauge linearGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent linearScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent3;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent4;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent5;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent6;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent7;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent8;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent9;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleStateIndicatorComponent linearScaleStateIndicatorComponent10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Timer loadingTimer;

    }
}

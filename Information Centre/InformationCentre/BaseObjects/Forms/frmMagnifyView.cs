using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frmMagnifyView : frmBase
    {
        UserActivityHook globalHooks;

        bool zoomActive;
        SpotGrab objSpotGrab;
//        System.Windows.Forms.KeyEventArgs hotkeyArgs;

        /// <summary>
        /// 
        /// </summary>
        public frmMagnifyView()
        {
            InitializeComponent();
        }

        void OnColorSnagLoaded(object obj, EventArgs e)
        {
            System.Drawing.Point tempPnt = new System.Drawing.Point(256, 256);
            MagnifierConfiguration config = new MagnifierConfiguration();
            objSpotGrab = new SpotGrab(tempPnt, MagnifyRect.ClientRectangle.Width, MagnifyRect.ClientRectangle.Height);

            MagnifyRect.Image = objSpotGrab.CurrentSpotBmpSrc;

            objSpotGrab.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(OnSpotChange);

            timer1.Interval = 20;

            globalHooks = new UserActivityHook();
            globalHooks.OnMouseActivity += new System.Windows.Forms.MouseEventHandler(MouseMoveHandler);

            zoomActive = true;

//            hotkeyArgs = new System.Windows.Forms.KeyEventArgs(Keys.ControlKey | Keys.Y);

            trackBarControl1.Value = 2;
        }

        delegate void OnSpotChangeCallback(object sender, System.ComponentModel.PropertyChangedEventArgs args);

        private void OnSpotChange(object sender, System.ComponentModel.PropertyChangedEventArgs args)
        {
            if (zoomActive)
            {
                MagnifyRect.Image = objSpotGrab.CurrentSpotBmpSrc;
            }

            //curColorBrush.Color = objSpotGrab.CurrentCursorColor;
            //CurColorTextBlock.Text = objSpotGrab.CurrentCursorColor.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HotkeyCheck(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            objSpotGrab.CursorPosition = new System.Drawing.Point(MousePosition.X, MousePosition.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MouseMoveHandler(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //MousePosTextBlock.Text = String.Format("x={0}  y={1}", e.X, e.Y);
            objSpotGrab.CursorPosition = new System.Drawing.Point(e.X, e.Y);
        }

        private void frmMagnifyView_Load(object sender, EventArgs e)
        {
            OnColorSnagLoaded(this, new EventArgs());
            timer1.Start();
        }

        private void trackBarControl1_EditValueChanged(object sender, EventArgs e)
        {
            objSpotGrab.Zoom = trackBarControl1.Value;
        }

        private void MagnifyRect_Resize(object sender, EventArgs e)
        {
            if (objSpotGrab != null)
            {
                objSpotGrab.SpotWidth = MagnifyRect.ClientRectangle.Width;
                objSpotGrab.SpotHeight = MagnifyRect.ClientRectangle.Height;
            }
        }

        private void frmMagnifyView_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            globalHooks.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            objSpotGrab.Update();
        }

        void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            objSpotGrab.Update();
        }
    }

    #region SpotGrab
    /// <summary>
    ///     SpotGrab can capture regions of the desktop
    ///     and find the color of the center pixel
    /// </summary>
    /// <remarks>
    ///     Alexander Lih May-June 2006
    ///     http://feebdack.com
    /// </remarks>

    class SpotGrab : INotifyPropertyChanged
    {
        protected int mPosX;
        protected int mPosY;
        protected int spotWidth;
        protected int spotHeight;

        protected int zoom = 2;

        protected int bumpX;
        protected int bumpY;
        protected int originX;
        protected int originY;

        protected System.Drawing.Point cursorPoint;
        protected Bitmap curSpotBmpSrc;
        protected Color curCursorColor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="curMousePos"></param>
        /// <param name="setupWidth"></param>
        /// <param name="setupHeight"></param>
        public SpotGrab(System.Drawing.Point curMousePos, int setupWidth, int setupHeight)
        {
            SpotWidth = setupWidth;
            SpotHeight = setupHeight;
            CursorPosition = curMousePos;
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Drawing.Point CursorPosition
        {
            get { return cursorPoint; }
            set
            {
                if (cursorPoint != value)
                {
                    cursorPoint = value;
                    mPosX = value.X;
                    mPosY = value.Y;
                    originX = value.X - bumpX;
                    originY = value.Y - bumpY;
                    SetSpotInfo();
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public int SpotWidth
        {
            get { return spotWidth; }

            set
            {
                if (spotWidth != value)
                {
                    spotWidth = value;
                    //bumpX = (int)((double)(value) / 2);
                    bumpX = 5;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int SpotHeight
        {
            get { return spotHeight; }

            set
            {
                if (spotHeight != value)
                {
                    spotHeight = value;
                    //bumpY = (int)((double)(value) / 2);
                    bumpY = 5;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Zoom
        {
            get
            {
                return zoom;
            }
            set
            {
                zoom = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Bitmap CurrentSpotBmpSrc
        {
            get 
            { 
                return curSpotBmpSrc; 
            }
            set
            {
                curSpotBmpSrc = value;
                OnPropertyChanged("CurrentSpotBmpSrc");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color CurrentCursorColor
        {
            get { return curCursorColor; }
            set
            {
                curCursorColor = value;
                OnPropertyChanged("CurrentCursorColor");
            }
        }

        #region ///Imported Screen Methods
        class ImportedMethods
        {

            [DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
            public static extern IntPtr DeleteDC(IntPtr hDc);

            [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
            public static extern IntPtr DeleteObject(IntPtr hDc);

            [DllImport("gdi32.dll", EntryPoint = "BitBlt")]
            public static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int
            wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);

            [DllImport("gdi32.dll", EntryPoint = "StretchBlt")]
            public static extern bool StretchBlt(IntPtr hdcDest, int xDest, int yDest, int wDest,
                                                 int hDest, IntPtr hdcSource, int xSrc, int ySrc,
                                                 int wSrc, int hSrc, int RasterOp);
            
            [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

            [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

            [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
            public static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
            [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
            public static extern IntPtr GetDesktopWindow();

            [DllImport("user32.dll", EntryPoint = "GetDC")]
            public static extern IntPtr GetDC(IntPtr ptr);

            [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
            public static extern int GetSystemMetrics(int abc);

            [DllImport("user32.dll", EntryPoint = "GetWindowDC")]
            public static extern IntPtr GetWindowDC(IntPtr ptr);

            [DllImport("user32.dll", EntryPoint = "ReleaseDC")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void SetSpotInfo()
        {
            int intResizeWidth = 0;
            int intResizeHeight = 0;

            IntPtr hwnd = ImportedMethods.GetDesktopWindow();
            IntPtr dc = ImportedMethods.GetWindowDC(hwnd);
            IntPtr memDC = ImportedMethods.CreateCompatibleDC(dc);
            IntPtr hbm = ImportedMethods.CreateCompatibleBitmap(dc, spotWidth, spotHeight);
            IntPtr oldbm = ImportedMethods.SelectObject(memDC, hbm);
            intResizeHeight = spotHeight * zoom;
            intResizeWidth = spotWidth * zoom;
            Image img = System.Drawing.Image.FromHbitmap(hbm);
            bool b = ImportedMethods.StretchBlt(memDC, 0, 0, intResizeWidth, intResizeHeight, dc, originX, originY, img.Width, img.Height, 0x40CC0020);
            CurrentSpotBmpSrc = System.Drawing.Image.FromHbitmap(hbm); // new Bitmap(img, new Size(intResizeWidth, intResizeHeight));

            ImportedMethods.SelectObject(memDC, oldbm);
            ImportedMethods.DeleteObject(hbm);
            ImportedMethods.DeleteDC(memDC);
            ImportedMethods.ReleaseDC(hwnd, dc);
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


        internal void Update()
        {
            SetSpotInfo();
        }
    }
    #endregion

    #region UserActivityHook
    /// <summary>
    /// This class allows you to tap keyboard and mouse and / or to detect their activity even when an 
    /// application runes in background or does not have any user interface at all. This class raises 
    /// common .NET events with KeyEventArgs and MouseEventArgs so you can easily retrive any information you need.
    /// </summary>
    /// <remarks>
    /// 	created by - Georgi
    /// 	created on - 22.05.2004 13:08:01
    /// 
    ///     Found at:  http://www.codeproject.com/csharp/globalhook.asp
    /// </remarks>
    public class UserActivityHook : object
    {

        /// <summary>
        /// Default constructor - starts hooks automatically
        /// </summary>
        public UserActivityHook()
        {
            Start();
        }

        /// <summary>
        /// 
        /// </summary>
        ~UserActivityHook()
        {
            Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        public event MouseEventHandler OnMouseActivity;
        /// <summary>
        /// 
        /// </summary>
        public event KeyEventHandler KeyDown;
        /// <summary>
        /// 
        /// </summary>
        public event KeyPressEventHandler KeyPress;
        /// <summary>
        /// 
        /// </summary>
        public event KeyEventHandler KeyUp;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        public delegate int HookProc(int nCode, Int32 wParam, IntPtr lParam);

        static int hMouseHook = 0; //Declare mouse hook handle as int.
        static int hKeyboardHook = 0; //Declare keyboard hook handle as int.

        //values from Winuser.h in Microsoft SDK.
        /// <summary>
        /// 
        /// </summary>
        public const int WH_MOUSE_LL = 14;	//mouse hook constant
        /// <summary>
        /// 
        /// </summary>
        public const int WH_KEYBOARD_LL = 13;	//keyboard hook constant	

        HookProc MouseHookProcedure; //Declare MouseHookProcedure as HookProc type.
//        HookProc KeyboardHookProcedure; //Declare KeyboardHookProcedure as HookProc type.

        
        //Declare wrapper managed POINT class.
        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class POINT
        {
            /// <summary>
            /// 
            /// </summary>
            public int x;
            /// <summary>
            /// 
            /// </summary>
            public int y;
        }

        //Declare wrapper managed MouseHookStruct class.
        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class MouseHookStruct
        {
            /// <summary>
            /// 
            /// </summary>
            public POINT pt;
            /// <summary>
            /// 
            /// </summary>
            public int hwnd;
            /// <summary>
            /// 
            /// </summary>
            public int wHitTestCode;
            /// <summary>
            /// 
            /// </summary>
            public int dwExtraInfo;
        }

        //Declare wrapper managed KeyboardHookStruct class.
        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public class KeyboardHookStruct
        {
            /// <summary>
            /// 
            /// </summary>
            public int vkCode;	//Specifies a virtual-key code. The code must be a value in the range 1 to 254. 
            /// <summary>
            /// 
            /// </summary>
            public int scanCode; // Specifies a hardware scan code for the key. 
            /// <summary>
            /// 
            /// </summary>
            public int flags;  // Specifies the extended-key flag, event-injected flag, context code, and transition-state flag.
            /// <summary>
            /// 
            /// </summary>
            public int time; // Specifies the time stamp for this message.
            /// <summary>
            /// 
            /// </summary>
            public int dwExtraInfo; // Specifies extra information associated with the message. 
        }


        //Import for SetWindowsHookEx function.
        //Use this function to install a hook.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idHook"></param>
        /// <param name="lpfn"></param>
        /// <param name="hInstance"></param>
        /// <param name="threadId"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);

        //Import for UnhookWindowsHookEx.
        //Call this function to uninstall the hook.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idHook"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        //Import for CallNextHookEx.
        //Use this function to pass the hook information to next hook procedure in chain.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idHook"></param>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode, Int32 wParam, IntPtr lParam);

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            // install Mouse hook 
            if (hMouseHook == 0)
            {
                // Create an instance of HookProc.
                MouseHookProcedure = new HookProc(MouseHookProc);

                hMouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProcedure, Marshal.GetHINSTANCE( Assembly.GetExecutingAssembly().GetModules(false)[0]), 0);

                //If SetWindowsHookEx fails.
                if (hMouseHook == 0)
                {
                    Stop();
                    throw new Exception("SetWindowsHookEx failed.");
                }
            }

            // install Keyboard hook 
            //if (hKeyboardHook == 0)
            //{
            //    KeyboardHookProcedure = new HookProc(KeyboardHookProc);
            //    hKeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL,
            //        KeyboardHookProcedure,
            //        Marshal.GetHINSTANCE(
            //        Assembly.GetExecutingAssembly().GetModules()[0]),
            //        0);

            //    //If SetWindowsHookEx fails.
            //    if (hKeyboardHook == 0)
            //    {
            //        Stop();
            //        throw new Exception("SetWindowsHookEx ist failed.");
            //    }
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            bool retMouse = true;
            bool retKeyboard = true;
//            Int32 lLastError = 0;

            if (hMouseHook != 0)
            {
                retMouse = UnhookWindowsHookEx(hMouseHook);
                hMouseHook = 0;
            }

//            lLastError = (Int32)GetLastError();

            //if (hKeyboardHook != 0)
            //{
            //    retKeyboard = UnhookWindowsHookEx(hKeyboardHook);
            //    hKeyboardHook = 0;
            //}

            try
            {
                //If UnhookWindowsHookEx fails.
                if (!(retMouse && retKeyboard))
                {
                    throw new Exception("UnhookWindowsHookEx failed.");
                }
            }
            catch (Exception ex)
            {
                // Do nothing...
                Console.WriteLine(ex.Message);
            }
        }
        
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDBLCLK = 0x209;

        private int MouseHookProc(int nCode, Int32 wParam, IntPtr lParam)
        {
            // if ok and someone listens to our events
            if ((nCode >= 0) && (OnMouseActivity != null))
            {

                MouseButtons button = MouseButtons.None;
                switch (wParam)
                {
                    case WM_LBUTTONDOWN:
                        //case WM_LBUTTONUP: 
                        //case WM_LBUTTONDBLCLK: 
                        button = MouseButtons.Left;
                        break;
                    case WM_RBUTTONDOWN:
                        //case WM_RBUTTONUP: 
                        //case WM_RBUTTONDBLCLK: 
                        button = MouseButtons.Right;
                        break;
                }
                int clickCount = 0;
                if (button != MouseButtons.None)
                    if (wParam == WM_LBUTTONDBLCLK || wParam == WM_RBUTTONDBLCLK) clickCount = 2;
                    else clickCount = 1;

                //Marshall the data from callback.
                MouseHookStruct MyMouseHookStruct = (MouseHookStruct)Marshal.PtrToStructure(lParam, typeof(MouseHookStruct));
                MouseEventArgs e = new MouseEventArgs(
                                                    button,
                                                    clickCount,
                                                    MyMouseHookStruct.pt.x,
                                                    MyMouseHookStruct.pt.y,
                                                    0);
                OnMouseActivity(this, e);
            }
            return CallNextHookEx(hMouseHook, nCode, wParam, lParam);
        }


        //The ToAscii function translates the specified virtual-key code and keyboard state to the corresponding character or characters. The function translates the code using the input language and physical keyboard layout identified by the keyboard layout handle.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uVirtKey"></param>
        /// <param name="uScanCode"></param>
        /// <param name="lpbKeyState"></param>
        /// <param name="lpwTransKey"></param>
        /// <param name="fuState"></param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int ToAscii(int uVirtKey, //[in] Specifies the virtual-key code to be translated. 
                                         int uScanCode, // [in] Specifies the hardware scan code of the key to be translated. The high-order bit of this value is set if the key is up (not pressed). 
                                         byte[] lpbKeyState, // [in] Pointer to a 256-byte array that contains the current keyboard state. Each element (byte) in the array contains the state of one key. If the high-order bit of a byte is set, the key is down (pressed). The low bit, if set, indicates that the key is toggled on. In this function, only the toggle bit of the CAPS LOCK key is relevant. The toggle state of the NUM LOCK and SCROLL LOCK keys is ignored.
                                         byte[] lpwTransKey, // [out] Pointer to the buffer that receives the translated character or characters. 
                                         int fuState); // [in] Specifies whether a menu is active. This parameter must be 1 if a menu is active, or 0 otherwise. 

        //The GetKeyboardState function copies the status of the 256 virtual keys to the specified buffer. 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pbKeyState"></param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int GetKeyboardState(byte[] pbKeyState);

        private const int WM_KEYDOWN = 0x100;
        private const int WM_KEYUP = 0x101;
        private const int WM_SYSKEYDOWN = 0x104;
        private const int WM_SYSKEYUP = 0x105;

        private int KeyboardHookProc(int nCode, Int32 wParam, IntPtr lParam)
        {
            // it was ok and someone listens to events
            if ((nCode >= 0) && (KeyDown != null || KeyUp != null || KeyPress != null))
            {
                KeyboardHookStruct MyKeyboardHookStruct = (KeyboardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyboardHookStruct));
                // raise KeyDown
                if (KeyDown != null && (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN))
                {
                    Keys keyData = (Keys)MyKeyboardHookStruct.vkCode;
                    KeyEventArgs e = new KeyEventArgs(keyData);
                    KeyDown(this, e);
                }

                // raise KeyPress
                if (KeyPress != null && wParam == WM_KEYDOWN)
                {
                    byte[] keyState = new byte[256];
                    GetKeyboardState(keyState);

                    byte[] inBuffer = new byte[2];
                    if (ToAscii(MyKeyboardHookStruct.vkCode,
                            MyKeyboardHookStruct.scanCode,
                            keyState,
                            inBuffer,
                            MyKeyboardHookStruct.flags) == 1)
                    {
                        KeyPressEventArgs e = new KeyPressEventArgs((char)inBuffer[0]);
                        KeyPress(this, e);
                    }
                }

                // raise KeyUp
                if (KeyUp != null && (wParam == WM_KEYUP || wParam == WM_SYSKEYUP))
                {
                    Keys keyData = (Keys)MyKeyboardHookStruct.vkCode;
                    KeyEventArgs e = new KeyEventArgs(keyData);
                    KeyUp(this, e);
                }

            }
            return CallNextHookEx(hKeyboardHook, nCode, wParam, lParam);
        }
    }
    #endregion 
}
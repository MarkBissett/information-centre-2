﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.Utils.Drawing;
using System.Linq;
using LocusEffects;

namespace BaseObjects
{
    public partial class frm_Select_Records_From_User_Input_Criteria : DevExpress.XtraEditors.XtraForm
    {
        private GridView gvView;  
        private ArrayList arrayListImportGridColumns = new ArrayList();  // Holds data for Column list //
        private GridColumn gcColumn;

        public frm_Select_Records_From_User_Input_Criteria() 
        {
            InitializeComponent();
        }

        public frm_Select_Records_From_User_Input_Criteria(GridView view, GridColumn column) : this() 
        {
            gvView = view;
            gcColumn = column;
            memoEdit1.EditValue = "";
            textEditSeparator.EditValue = ",";

            arrayListImportGridColumns.Add(new LookUpDataOBJ("-- All Columns --", "-- All Columns --"));

            foreach (GridColumn col in gvView.VisibleColumns)
            {
                //arrayListImportGridColumns.Add(new LookUpDataOBJ(col.FieldName, col.Caption));
                arrayListImportGridColumns.Add(new LookUpDataOBJ(col.FieldName, col.GetTextCaption()));
                
            }
            lookUpEditImportGridColumns.Properties.DataSource = arrayListImportGridColumns;
            lookUpEditImportGridColumns.Properties.DisplayMember = "ColumnCaption";
            lookUpEditImportGridColumns.Properties.ValueMember = "ColumnName";
            lookUpEditImportGridColumns.Properties.SortColumnIndex = 1;
            lookUpEditImportGridColumns.Properties.Columns[1].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;

            if (gcColumn != null)
            {
                lookUpEditImportGridColumns.EditValue = gcColumn.FieldName;
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (CleanSearchStringCheckEdit.Checked) memoEdit1.EditValue = memoEdit1.EditValue.ToString().Replace(System.Environment.NewLine, string.Empty).Replace(" ", string.Empty).Replace("\t", string.Empty);
            
            string strTextToSearchFor = memoEdit1.EditValue.ToString();
            string strcolumnToSearch = lookUpEditImportGridColumns.EditValue.ToString();
            string strSeparator = textEditSeparator.EditValue.ToString();
            bool boolCaseSensitive = checkEditCaseSensitive.Checked;
            if (!boolCaseSensitive) strTextToSearchFor = strTextToSearchFor.ToLower();

            if (string.IsNullOrWhiteSpace(strTextToSearchFor) || string.IsNullOrWhiteSpace(strcolumnToSearch))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Column to search and enter the text to be searched for before proceeding.", "Select Records From User Input Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intSearchOperator = Convert.ToInt32(radioGroupSearchOperator.EditValue);
            int intMatchDirection = Convert.ToInt32(radioGroupMatchDirection.EditValue);
            int intRowCount = gvView.DataRowCount;
            int intSelectedCount = 0;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            int intUpdateProgressThreshhold = gvView.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            List<string> listMissingValues = new List<string>();
            if (!string.IsNullOrWhiteSpace(strSeparator))
            {
                string[] delimiters = new string[] { strSeparator };
                string[] strArray = strTextToSearchFor.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                listMissingValues.AddRange(strArray);
                if (strcolumnToSearch != "-- All Columns --")
                {
                    gvView.BeginUpdate();
                    gvView.BeginSelection();
                    if (checkEditClearPriorSearches.Checked) gvView.ClearSelection();
                    for (int i = 0; i < intRowCount; i++)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                        foreach (string x in strArray)
                        {
                            if (intSearchOperator == 0)  // Contains //
                            {
                                if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                {
                                    string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    if (!boolCaseSensitive) strValue = strValue.ToLower();
                                    if (x.Contains(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                        continue;
                                    }
                                }
                                else  // Input Value --> Grid Value //
                                {
                                    string strValue = x;
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                    if (strGridValue.Contains(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                        continue;
                                    }
                                }
                            }
                            else  // Equals //
                            {
                                if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                {
                                    string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    if (!boolCaseSensitive) strValue = strValue.ToLower();
                                    if (x.Equals(Convert.ToString(strValue)))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                        continue;
                                    }
                                }
                                else  // Input Value --> Grid Value //
                                {
                                    string strValue = x;
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                    if (strGridValue.Equals(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    if (checkEditInvert.Checked)
                    {
                        intSelectedCount = Invert_Selection();
                    }
                    gvView.EndSelection();
                    gvView.EndUpdate();
                }
                else  // Search all columns //
                {
                    gvView.BeginUpdate();
                    gvView.BeginSelection();
                    if (checkEditClearPriorSearches.Checked) gvView.ClearSelection();
                    for (int i = 0; i < intRowCount; i++)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                        bool boolMatchFound = false;
                        foreach (string x in strArray)
                        {
                            foreach (GridColumn col in gvView.VisibleColumns)
                            {
                                if (boolMatchFound) continue;
                                strcolumnToSearch = col.FieldName;

                                if (intSearchOperator == 0)  // Contains //
                                {
                                    if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                    {
                                        string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                        if (string.IsNullOrWhiteSpace(strValue)) continue;
                                        if (!boolCaseSensitive) strValue = strValue.ToLower();
                                        if (x.Contains(strValue))
                                        {
                                            gvView.SelectRow(i);
                                            gvView.MakeRowVisible(i, false);
                                            intSelectedCount++;
                                            boolMatchFound = true;
                                            if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                            continue;
                                        }
                                    }
                                    else  // Input Value --> Grid Value //
                                    {
                                        string strValue = x;
                                        if (string.IsNullOrWhiteSpace(strValue)) continue;
                                        string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                        if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                        if (strGridValue.Contains(strValue))
                                        {
                                            gvView.SelectRow(i);
                                            gvView.MakeRowVisible(i, false);
                                            intSelectedCount++;
                                            boolMatchFound = true;
                                            if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                            continue;
                                        }
                                    }
                                }
                                else  // Equals //
                                {
                                    if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                    {
                                        string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                        if (string.IsNullOrWhiteSpace(strValue)) continue;
                                        if (!boolCaseSensitive) strValue = strValue.ToLower();
                                        if (x.Equals(Convert.ToString(strValue)))
                                        {
                                            gvView.SelectRow(i);
                                            gvView.MakeRowVisible(i, false);
                                            intSelectedCount++;
                                            boolMatchFound = true;
                                            if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                            continue;
                                        }
                                    }
                                    else  // Input Value --> Grid Value //
                                    {
                                        string strValue = x;
                                        if (string.IsNullOrWhiteSpace(strValue)) continue;
                                        string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                        if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                        if (strGridValue.Equals(strValue))
                                        {
                                            gvView.SelectRow(i);
                                            gvView.MakeRowVisible(i, false);
                                            intSelectedCount++;
                                            boolMatchFound = true;
                                            if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (checkEditInvert.Checked)
                    {
                        intSelectedCount = Invert_Selection();
                    }
                    gvView.EndSelection();
                    gvView.EndUpdate();
                }
            }
            else  // Just searching for one condition - no separator //
            {
                listMissingValues.Add(strTextToSearchFor);  // Just the one value //
                if (strcolumnToSearch != "-- All Columns --")
                {
                    gvView.BeginUpdate();
                    gvView.BeginSelection();
                    if (checkEditClearPriorSearches.Checked) gvView.ClearSelection();
                    for (int i = 0; i < intRowCount; i++)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                        if (intSearchOperator == 0)  // Contains //
                        {
                            if (intMatchDirection == 0)  // Grid Value --> Input Value //
                            {
                                string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                if (string.IsNullOrWhiteSpace(strValue)) continue;
                                if (!boolCaseSensitive) strValue = strValue.ToLower();
                                if (strTextToSearchFor.Contains(strValue))
                                {
                                    gvView.SelectRow(i);
                                    gvView.MakeRowVisible(i, false);
                                    intSelectedCount++;
                                    if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                    continue;
                                }
                            }
                            else  // Input Value --> Grid Value //
                            {
                                string strValue = strTextToSearchFor;
                                if (string.IsNullOrWhiteSpace(strValue)) continue;
                                string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                if (strGridValue.Contains(strValue))
                                {
                                    gvView.SelectRow(i);
                                    gvView.MakeRowVisible(i, false);
                                    intSelectedCount++;
                                    if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                    continue;
                                }
                            }
                        }
                        else  // Equals //
                        {
                            if (intMatchDirection == 0)  // Grid Value --> Input Value //
                            {
                                string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                if (string.IsNullOrWhiteSpace(strValue)) continue;
                                if (!boolCaseSensitive) strValue = strValue.ToLower();
                                if (strTextToSearchFor.Equals(strValue))
                                {
                                    gvView.SelectRow(i);
                                    gvView.MakeRowVisible(i, false);
                                    intSelectedCount++;
                                    if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                    continue;
                                }
                            }
                            else  // Input Value --> Grid Value //
                            {
                                string strValue = strTextToSearchFor;
                                if (string.IsNullOrWhiteSpace(strValue)) continue;
                                string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                if (strGridValue.Equals(strValue))
                                {
                                    gvView.SelectRow(i);
                                    gvView.MakeRowVisible(i, false);
                                    intSelectedCount++;
                                    if (checkEditShowNotFound.Checked) listMissingValues.Remove(strValue);
                                    continue;
                                }
                            }
                        }
                    }
                    if (checkEditInvert.Checked)
                    {
                        intSelectedCount = Invert_Selection();
                    }
                    gvView.EndSelection();
                    gvView.EndUpdate();
                }
                else  // Search all columns //
                {
                    gvView.BeginUpdate();
                    gvView.BeginSelection();
                    if (checkEditClearPriorSearches.Checked) gvView.ClearSelection();
                    for (int i = 0; i < intRowCount; i++)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                        bool boolMatchFound = false;
                        foreach (GridColumn col in gvView.VisibleColumns)
                        {
                            if (boolMatchFound) continue;
                            strcolumnToSearch = col.FieldName;

                            if (intSearchOperator == 0)  // Contains //
                            {
                                if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                {
                                    string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    if (!boolCaseSensitive) strValue = strValue.ToLower();
                                    if (strTextToSearchFor.Contains(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        boolMatchFound = true;
                                        continue;
                                    }
                                }
                                else  // Input Value --> Grid Value //
                                {
                                    string strValue = strTextToSearchFor;
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                    if (strGridValue.Contains(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        boolMatchFound = true;
                                        continue;
                                    }
                                }
                            }
                            else  // Equals //
                            {
                                if (intMatchDirection == 0)  // Grid Value --> Input Value //
                                {
                                    string strValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    if (!boolCaseSensitive) strValue = strValue.ToLower();
                                    if (strTextToSearchFor.Equals(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        boolMatchFound = true;
                                        continue;
                                    }
                                }
                                else  // Input Value --> Grid Value //
                                {
                                    string strValue = strTextToSearchFor;
                                    if (string.IsNullOrWhiteSpace(strValue)) continue;
                                    string strGridValue = Convert.ToString(gvView.GetRowCellValue(i, strcolumnToSearch));
                                    if (!boolCaseSensitive) strGridValue = strGridValue.ToLower();
                                    if (strGridValue.Equals(strValue))
                                    {
                                        gvView.SelectRow(i);
                                        gvView.MakeRowVisible(i, false);
                                        intSelectedCount++;
                                        boolMatchFound = true;
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    if (checkEditInvert.Checked)
                    {
                        intSelectedCount = Invert_Selection();
                    }
                    gvView.EndSelection();
                    gvView.EndUpdate();
                }
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            string strMessage = intSelectedCount.ToString() + " Record(s) Selected.";
            if (checkEditShowNotFound.Checked)
            {
                strMessage += "\n\n";
                if (listMissingValues.Count == 0)
                { 
                    strMessage += "All search value(s) found.";
                }
                else
                {
                    strMessage += "The following search values were not found...\n";
                    strMessage += String.Join(strSeparator, listMissingValues);
                }
            }
            DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Select Records From User Input Criteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioGroupSearchOperator_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.RadioGroup rg = (DevExpress.XtraEditors.RadioGroup)sender;
            if (Convert.ToInt32(rg.EditValue) == 0)
            {
                labelMatchDirection.Visible = true;
                radioGroupMatchDirection.Visible = true;
                radioGroupMatchDirection.Enabled = true;
            }
            else
            {
                labelMatchDirection.Visible = false;
                radioGroupMatchDirection.Visible = false;
                radioGroupMatchDirection.Enabled = false;
            }

        }

        private int Invert_Selection()
        {
            int intSelectedCount = 0;
            int intRowCount = gvView.DataRowCount;
            for (int i = 0; i < intRowCount; i++)
            {
                if (gvView.IsRowSelected(i))
                {
                    gvView.UnselectRow(i);
                }
                else
                {
                    gvView.SelectRow(i);
                    intSelectedCount++;
                }
                gvView.MakeRowVisible(i, false);     
            }
            return intSelectedCount;
        }


 
    }

}
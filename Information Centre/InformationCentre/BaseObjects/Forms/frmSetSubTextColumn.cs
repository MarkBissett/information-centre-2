﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils.Drawing;

namespace BaseObjects
{
    public partial class frmSetSubTextColumn : DevExpress.XtraEditors.XtraForm
    {
        private GridView gvView;  
        private Point lastPoint;  // Used to move form without titlebar //
        private bool mouseDown;  // Used to move form without titlebar //
        private ArrayList arrayListImportGridColumns = new ArrayList();  // Holds data for GoTo Column list on Import Page //

        public frmSetSubTextColumn() 
        {
            InitializeComponent();
        }

        public frmSetSubTextColumn(GridView view, Point position) : this() 
        {
            gvView = view;
            Location = position;

            arrayListImportGridColumns.Add(new LookUpDataOBJ2("-- NONE --", "-- NONE --"));

            foreach (GridColumn col in gvView.Columns)
            {
                arrayListImportGridColumns.Add(new LookUpDataOBJ2(col.FieldName, col.GetTextCaption()));
            }
            lookUpEditImportGridColumns.Properties.DataSource = arrayListImportGridColumns;
            lookUpEditImportGridColumns.Properties.DisplayMember = "ColumnCaption";
            lookUpEditImportGridColumns.Properties.ValueMember = "ColumnName";
            lookUpEditImportGridColumns.Properties.SortColumnIndex = 1;
            lookUpEditImportGridColumns.Properties.Columns[1].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            lookUpEditImportGridColumns.EditValue = gvView.PreviewFieldName;
        }

        private void SetSubTextColumn()
        {
            if (lookUpEditImportGridColumns.EditValue.ToString() == "-- NONE --")
            {
                gvView.BeginUpdate();
                gvView.OptionsView.ShowPreview = false;
                gvView.OptionsView.AutoCalcPreviewLineCount = false;
                gvView.PreviewFieldName = "";
                gvView.EndUpdate();
            }
            else
            {
                gvView.BeginUpdate();
                gvView.OptionsView.AutoCalcPreviewLineCount = true;
                gvView.PreviewFieldName = lookUpEditImportGridColumns.EditValue.ToString();
                gvView.OptionsView.ShowPreview = true;
                gvView.EndUpdate();
            }
        }

        private void buttonEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetSubTextColumn();
                Close();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void lookUpEditImportGridColumns_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.OK)
            {
                SetSubTextColumn();
                Close();
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Close)
            {
                Close();
            }
        }


        // Code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mouseDown = true;
            this.lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.mouseDown) this.Location = new Point(this.Left - (this.lastPoint.X - e.X), this.Top - (this.lastPoint.Y - e.Y));
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            this.mouseDown = false;
        }
        // End of Code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //
    }

    public class LookUpDataOBJ2
    {
        private string columnName;
        private string columnCaption;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
            set
            {
                columnName = value;
            }
        }

        public string ColumnCaption
        {
            get
            {
                return columnCaption;
            }
            set
            {
                columnCaption = value;
            }
        }

        public LookUpDataOBJ2(string columnName, string columnCaption)
        {
            this.ColumnName = columnName;
            this.ColumnCaption = columnCaption;
        }
    }

}
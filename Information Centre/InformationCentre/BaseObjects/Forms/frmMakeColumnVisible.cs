﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.Utils.Drawing;
using LocusEffects;

namespace BaseObjects
{
    public partial class frmMakeColumnVisible : DevExpress.XtraEditors.XtraForm
    {
        private GridView gvView;  
        private Point lastPoint;  // Used to move form without titlebar //
        private bool mouseDown;  // Used to move form without titlebar //
        private ArrayList arrayListImportGridColumns = new ArrayList();  // Holds data for Column list //
        private GridColumn flashingColumn = null;
        private ObjectState state = ObjectState.Normal;
        private int FlashCount = 0;
        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private BeaconLocusEffect m_customBeaconLocusEffect1 = null;

        /// <summary>
        /// 
        /// </summary>
        public frmMakeColumnVisible() 
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <param name="position"></param>
        public frmMakeColumnVisible(GridView view, Point position) : this() 
        {
            gvView = view;
            Location = position;

            foreach (GridColumn col in gvView.VisibleColumns)
            {
                //arrayListImportGridColumns.Add(new LookUpDataOBJ(col.FieldName, col.Caption));
                arrayListImportGridColumns.Add(new LookUpDataOBJ(col.FieldName, col.GetTextCaption()));
                
            }
            lookUpEditImportGridColumns.Properties.DataSource = arrayListImportGridColumns;
            lookUpEditImportGridColumns.Properties.DisplayMember = "ColumnCaption";
            lookUpEditImportGridColumns.Properties.ValueMember = "ColumnName";
            lookUpEditImportGridColumns.Properties.SortColumnIndex = 1;
            lookUpEditImportGridColumns.Properties.Columns[1].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            gvView.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(gvView_CustomDrawColumnHeader);

            // Prepare LocusEffects and add custom effect //
            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;
            m_customBeaconLocusEffect1 = new BeaconLocusEffect();
            m_customBeaconLocusEffect1.Name = "CustomBeacon1";
            m_customBeaconLocusEffect1.InitialSize = new Size(150, 150);
            m_customBeaconLocusEffect1.AnimationTime = 1500;
            m_customBeaconLocusEffect1.LeadInTime = 0;
            m_customBeaconLocusEffect1.LeadOutTime = 0;
            m_customBeaconLocusEffect1.AnimationStartColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationEndColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationOuterColor = Color.Salmon;
            m_customBeaconLocusEffect1.RingWidth = 6; // 6;
            m_customBeaconLocusEffect1.OuterRingWidth = 3;// 3;
            m_customBeaconLocusEffect1.BodyFadeOut = true;
            m_customBeaconLocusEffect1.Style = BeaconEffectStyles.Shrink;//.HeartBeat;
            locusEffectsProvider1.AddLocusEffect(m_customBeaconLocusEffect1);
        }

        private void MakeColumnVisible()
        {
            if (lookUpEditImportGridColumns.EditValue == null) return;
            gvView.MakeColumnVisible(gvView.Columns[lookUpEditImportGridColumns.EditValue.ToString()]);

            flashingColumn = gvView.Columns[lookUpEditImportGridColumns.EditValue.ToString()];
            timer1.Start();

            // Get Position of Column within Grid //
            GridViewInfo info = gvView.GetViewInfo() as GridViewInfo;
            GridColumnsInfo colInfo = info.ColumnsInfo;
            GridColumnInfoArgs args = colInfo[gvView.Columns[lookUpEditImportGridColumns.EditValue.ToString()]];
            Rectangle rect = args.Bounds;

            // Get Position of Grid on Screen //
            GridControl gc = gvView.GridControl;
            Rectangle rect2 = gc.RectangleToScreen(gc.Bounds);

            //System.Drawing.Point point = new System.Drawing.Point(rect.X + (rect.Width / 2) + rect2.X, rect.Y + (rect.Height / 2) + rect2.Y);
            GridColumn gColumn = gvView.Columns[lookUpEditImportGridColumns.EditValue.ToString()];
            Point point = (((GridViewInfo)gvView.GetViewInfo()).ColumnsInfo[gColumn] == null ? new Point(0, 0) : new Point(((GridViewInfo)gvView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Left + (rect.Width / 2), ((GridViewInfo)gvView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Bottom - (rect.Height / 2)));
            point = gvView.GridControl.PointToScreen(point);

            locusEffectsProvider1.ShowLocusEffect(this, point, m_customBeaconLocusEffect1.Name);
        }

        private void buttonEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MakeColumnVisible();
                Close();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void lookUpEditImportGridColumns_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                StopColumnFlashing();
                MakeColumnVisible();
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Close)
            {
                StopColumnFlashing();
                Close();
            }
        }


        // Code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mouseDown = true;
            this.lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.mouseDown) this.Location = new Point(this.Left - (this.lastPoint.X - e.X), this.Top - (this.lastPoint.Y - e.Y));
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            this.mouseDown = false;
        }
        // End of Code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //


        private void timer1_Tick(object sender, EventArgs e)
        {
            state = state == ObjectState.Normal ? ObjectState.Hot : ObjectState.Normal;
            gvView.InvalidateColumnHeader(flashingColumn);
            FlashCount++;
            if (FlashCount >= 12)
            {
                StopColumnFlashing();
            }
        }

        private void StopColumnFlashing()
        {
            FlashCount = 0;
            timer1.Stop();
            state = ObjectState.Normal;
            if (flashingColumn != null)
            {
                gvView.InvalidateColumnHeader(flashingColumn);
                flashingColumn = null;
            }
        }

        private void gvView_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == flashingColumn) e.Info.State = state;
        }

        private void frmMakeColumnVisible_FormClosing(object sender, FormClosingEventArgs e)
        {
            gvView.CustomDrawColumnHeader -= new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(gvView_CustomDrawColumnHeader);
        }

    }

}
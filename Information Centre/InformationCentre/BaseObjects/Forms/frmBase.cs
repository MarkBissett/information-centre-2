using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Views.Card.ViewInfo;

using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.ViewInfo;

using DevExpress.Skins;

namespace BaseObjects
{
    public struct stcFormPermissions
    {
        public int intFormID;
        public int intSubPartID;
        public Boolean blCreate;
        public Boolean blRead;
        public Boolean blUpdate;
        public Boolean blDelete;
    }

    public struct stcGridFilters
    {
        public int intFilterID;

        public int intFormID;

        public string strGridName;

        public string strFilterText;

        public string strFormName;
    }

    public struct stcSharedLayouts
    {
        public frmBase fbBase;
        public int intUserID;
        public int intFormID;
    }


    public partial class frmBase : DevExpress.XtraEditors.XtraForm
    {
        private int intFormID = 0;
        private int intStaffID = 0;
        private int intSystemID = 0;
        private int intLoadedViewID = 0;
        private int intLoadedViewCreatedByID = 0;
        private GlobalSettings gsSettings = new GlobalSettings();
        private LockWindow lwWindow;
        private int intCreate = 0; // 0 = No, 1 = Yes //
        private int intRead = 0; // 0 = No, 1 = Yes //
        private int intUpdate = 0; // 0 = No, 1 = Yes //
        private int intDelete = 0; // 0 = No, 1 = Yes //
        private int intReadOnly = 0; // 0 = No, 1 = Yes //
        private ArrayList alFormPermissions = new ArrayList();
        private Control ctrlOriginator;

        public frmProgress fProgress;

        public DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager;
        public Boolean _KeepWaitFormOpen = true;

        public object objObject;

        public string strFormMode = "";

        public delegate void AddEventHandler(object sender, EventArgs e);

        public frmBase()
        {
            // Required for Windows Form Designer support
            InitializeComponent();

            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);

            DevExpress.XtraSpellChecker.SpellCheckerISpellDictionary scdDictionary = new DevExpress.XtraSpellChecker.SpellCheckerISpellDictionary();
            BaseObjects.ExtendedCustomDictionary sccCustom = new BaseObjects.ExtendedCustomDictionary();

            scdDictionary.AlphabetPath = Environment.CurrentDirectory + "\\EnglishAlphabet.txt";
            scdDictionary.CaseSensitive = false;
            scdDictionary.Culture = System.Globalization.CultureInfo.GetCultureInfo("en-GB");
            scdDictionary.DictionaryPath = Environment.CurrentDirectory + "\\british.xlg";
            scdDictionary.Encoding = Encoding.GetEncoding(437);
            scdDictionary.GrammarPath = Environment.CurrentDirectory + "\\english.aff";

            sccCustom.AlphabetPath = Environment.CurrentDirectory + "\\EnglishAlphabet.txt";
            sccCustom.CaseSensitive = false;
            sccCustom.Culture = System.Globalization.CultureInfo.GetCultureInfo("en-GB");
            //sccCustom.DictionaryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\CustomEnglish.dic";
            sccCustom.DictionaryPath = Environment.CurrentDirectory + "\\CustomEnglish.dic";
            sccCustom.Encoding = Encoding.GetEncoding(437);

            scSpellChecker.Dictionaries.Add(scdDictionary);
            scSpellChecker.Dictionaries.Add(sccCustom);
            scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;

            lwWindow = new LockWindow();
        }

        public ArrayList FormPermissions
        {
            get
            {
                return alFormPermissions;
            }
            set
            {
                alFormPermissions = value;
            }
        }

        public void LockThisWindow()
        {
            LockWindow.LockUpdatingWindow(this.Handle, true);
        }

        public void UnlockThisWindow()
        {
            LockWindow.LockUpdatingWindow(IntPtr.Zero, false);
        }

        public GlobalSettings GlobalSettings
        {
            get
            {
                return gsSettings;
            }
            set
            {
                gsSettings = value;
            }
        }

        public int LoadedViewID
        {
            get
            {
                return intLoadedViewID;
            }
            set
            {
                intLoadedViewID = value;
            }
        }

        public int LoadedViewCreatedByID
        {
            get
            {
                return intLoadedViewCreatedByID;
            }
            set
            {
                intLoadedViewCreatedByID = value;
            }
        }

        public int FormID
        {
            get
            {
                return intFormID;
            }
            set
            {
                intFormID = value;
            }
        }

        public int StaffID
        {
            get
            {
                return intStaffID;
            }
            set
            {
                intStaffID = value;
            }
        }

        public int SystemID
        {
            get
            {
                return intSystemID;
            }
            set
            {
                intSystemID = value;
            }
        }

        public int Create
        {
            get
            {
                return intCreate;
            }
            set
            {
                intCreate = value;
            }
        }

        public int Read
        {
            get
            {
                return intRead;
            }
            set
            {
                intRead = value;
            }
        }

        public new int Update
        {
            get
            {
                return intUpdate;
            }
            set
            {
                intUpdate = value;
            }
        }

        public int Delete
        {
            get
            {
                return intDelete;
            }
            set
            {
                intDelete = value;
            }
        }

        public int ReadOnly
        {
            get
            {
                return intReadOnly;
            }
            set
            {
                intReadOnly = value;
            }
        }

        #region Methods to handle events...

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAddEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBlockAddEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnEditEvent(object sender, EventArgs e)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBlockEditEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDeleteEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveEvent(object sender, EventArgs e)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCreateFilterEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCutEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCopyEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPasteEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnUndoEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnRedoEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnClearEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSelectAllEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSelectGroupEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFindEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnReplaceEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPreviewEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPrintEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPrintSetupEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBoldEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnItalicEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnUnderlineEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignLeftEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignCentreEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignRightEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBulletsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnImageEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontColorEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSpellCheckEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnGrammarCheckEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnMyOptionsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveLayoutEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveLayoutAsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSystemOptionsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExportEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExitEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnScratchPadEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnClipboardEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontSizeEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFilterManagerEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDefaultViewEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnToDoListEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLinkRecordToDoListEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCommentBankEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCommentBankCodesEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLivePeriodEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnViewedPeriodEvent(object sender, EventArgs e, GlobalSettings strucGlobalSettings)
        {
            GlobalSettings = strucGlobalSettings; // Update so that updated viewed and live period info is available to form //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnEditSystemPeriodsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnRemotePCUtilityEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLoadFilterEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual object OnSaveFilterEvent(object sender, EventArgs e)
        {
            stcGridFilters sgfFilter = new stcGridFilters();
            return sgfFilter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDatasetCreateEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnShowMapEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnShowGoogleMapEvent(object sender, EventArgs e)
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnViewAuditTrail(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCopyToClipboard(object sender, EventArgs e)
        {
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLayoutRotateEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLayoutFlipEvent(object sender, EventArgs e)
        {
        }

        #endregion

        public void frmBase_Enter(object sender, EventArgs e)
        {
            AppMessenger am = new AppMessenger();
            am.SendMessage(this.FormID, 0, false);
        }
        public void frmBase_Leave(object sender, EventArgs e)
        {
            AppMessenger am = new AppMessenger();
            am.SendMessage(this.FormID, 0, true);
        }
        public virtual void bbiBlockAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnBlockAddEvent(ctrlOriginator, ev);
        }
        public virtual void bbiBlockEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnBlockEditEvent(ctrlOriginator, ev);
        }
        public virtual void bbiClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            DevExpress.XtraEditors.TextBoxMaskBox mtbBox = (DevExpress.XtraEditors.TextBoxMaskBox)ctrlOriginator;
            mtbBox.Clear();
            OnClearEvent(ctrlOriginator, ev);
        }
        public virtual void bbiCopy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            DevExpress.XtraEditors.TextBoxMaskBox mtbBox = (DevExpress.XtraEditors.TextBoxMaskBox)ctrlOriginator;

            if (mtbBox.MaskBoxSelectedText != "")
            {
                Clipboard.SetText(mtbBox.MaskBoxSelectedText);
            }            

            OnCopyEvent(ctrlOriginator, ev);
        }
        public virtual void bbiCut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            DevExpress.XtraEditors.TextBoxMaskBox mtbBox = (DevExpress.XtraEditors.TextBoxMaskBox)ctrlOriginator;

            if (mtbBox.MaskBoxSelectedText != "")
            {
                Clipboard.SetText(mtbBox.MaskBoxSelectedText);
            }

            mtbBox.MaskBoxSelectedText = "";

            OnCutEvent(ctrlOriginator, ev);
        }
        public virtual void bbiDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnDeleteEvent(ctrlOriginator, ev);
        }
        public virtual void bbiPaste_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            DevExpress.XtraEditors.TextBoxMaskBox mtbBox = (DevExpress.XtraEditors.TextBoxMaskBox)ctrlOriginator;

            mtbBox.MaskBoxSelectedText = Clipboard.GetText();

            OnPasteEvent(ctrlOriginator, ev);
        }
        public void bbiRedo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnRedoEvent(ctrlOriginator, ev);
        }
        public virtual void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnSaveEvent(ctrlOriginator, ev);
        }
        public virtual void bbiSelectAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            DevExpress.XtraEditors.TextBoxMaskBox mtbBox = (DevExpress.XtraEditors.TextBoxMaskBox)ctrlOriginator;

            mtbBox.MaskBoxSelectionStart = 0;
            mtbBox.MaskBoxSelectionLength = mtbBox.MaskBoxText.Length;

            OnSelectAllEvent(ctrlOriginator, ev);
        }
        public virtual void bbiSingleAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnAddEvent(ctrlOriginator, ev);
        }
        public virtual void bbiSingleEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnEditEvent(ctrlOriginator, ev);
        }
        public void bbiSpellChecker_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnSpellCheckEvent(ctrlOriginator, ev);
        }
        public void bbiUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnUndoEvent(ctrlOriginator, ev);
        }
        public void bsiAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //EventArgs ev = new EventArgs();

            //OnAddEvent(ctrlOriginator, ev);
        }
        public void bsiEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //EventArgs ev = new EventArgs();

            //OnEditEvent(ctrlOriginator, ev);
        }
        public void barManager1_QueryShowPopupMenu(object sender, DevExpress.XtraBars.QueryShowPopupMenuEventArgs e)
        {
            DevExpress.XtraGrid.GridControl gcControl;
            DevExpress.XtraGrid.Views.Grid.GridView gvCurrent;
            DevExpress.XtraGrid.Views.Card.CardView cvCurrent;
            GridHitInfo gridInfo;
            CardHitInfo cardInfo;
            Point pt;

            if (e.Control.GetType().Name == "GridControl")
            {
                gcControl = (DevExpress.XtraGrid.GridControl)e.Control;

                gcControl = (DevExpress.XtraGrid.GridControl)e.Control; 
                pt = gcControl.PointToClient(Control.MousePosition);

                if (gcControl.FocusedView.GetType().Name == "CardView")
                {
                    cvCurrent = (DevExpress.XtraGrid.Views.Card.CardView)gcControl.FocusedView;
                    cardInfo = cvCurrent.CalcHitInfo(pt);

                    if (!cardInfo.InField)
                    {
                        e.Cancel = true;
                    }
                }
                else if (gcControl.FocusedView.GetType().Name == "GridView")
                {
                    gvCurrent = (DevExpress.XtraGrid.Views.Grid.GridView)gcControl.FocusedView;
                    gridInfo = gvCurrent.CalcHitInfo(pt);

                    if (gridInfo.InColumn)
                    {
                        e.Cancel = true;
                    }
                }
            }
            else
            {
                ctrlOriginator = e.Control;
            }
        }

        public virtual void OnProcessActiveFormAction(int intActionID, int intRecordID)
        {
        }

        public virtual void PostViewClick(object sender, EventArgs e, int row)
        {
        }

        public virtual void RectangleMoved(object sender, int DifferenceX, int DifferenceY)
        {
        }

        //private void frmBase_KeyDown(object sender, KeyEventArgs e)
        //{
        //    MessageBox.Show("Key Down!");
        //}

        //private void frmBase_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    MessageBox.Show("Key Press!");
        //}

        //private void frmBase_KeyUp(object sender, KeyEventArgs e)
        //{
        //    MessageBox.Show("Key Up!");
        //}

        //private void frmBase_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        //{
        //    MessageBox.Show("Preview Key Down!");
        //}

        public virtual void PasteTextToControl(object sender, object objTextToPaste)
        {
            if (this.ActiveControl != null)
            {
                if (this.ActiveControl.GetType().Name == "TextBoxMaskBox")
                {
                    this.ActiveControl.Text = this.ActiveControl.Text.Insert(this.ActiveControl.Text.Length, objTextToPaste.ToString());
                }
            }
        }
     


        protected virtual void ImagePreview(Image PassedImage, string PassedSize, string PassedShowScrollbars)
        {
            MethodInfo method = null;
            if (this.ParentForm != null || this.Owner != null)
            {
                if (this.ParentForm != null)
                {
                    method = this.ParentForm.GetType().GetMethod("ShowImageInImagePanel");
                    if (method != null)
                    {
                        method.Invoke(this.ParentForm, new object[] { this, PassedImage, PassedSize, PassedShowScrollbars });
                    }
                }
                else
                {
                    method = this.Owner.GetType().GetMethod("ShowImageInImagePanel");
                    if (method != null)
                    {
                        method.Invoke(this.Owner, new object[] { PassedImage, PassedSize, PassedShowScrollbars });
                    }
                }
            }
        }

        protected virtual void ImagePreviewClear()
        {
            MethodInfo method = null;
            if (this.ParentForm != null || this.Owner != null)
            {
                if (this.ParentForm != null)
                {
                    method = this.ParentForm.GetType().GetMethod("ClearImageInImagePanel");
                    if (method != null)
                    {
                        method.Invoke(this.ParentForm, new object[] { "" });
                    }
                }
                else
                {
                    method = this.Owner.GetType().GetMethod("ClearImageInImagePanel");
                    if (method != null)
                    {
                        method.Invoke(this.Owner, new object[] { "" });
                    }
                }

            }
        }


        public virtual void PostOpen(object objParameter)
        {
        }

        public virtual void PostLoadView(object objParameter)
        {
        }

        public virtual void CustomColumnButtonClick(object objParameter, string strColumnName)
        {
        }


        private void bbiGrammarCheck_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            OnGrammarCheckEvent(ctrlOriginator, ev);
        }

        private void bbiDatasetSelection_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnDatasetSelectionEvent(ctrlOriginator, ev);
        }

        private void bbiDatasetCreate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnDatasetCreateEvent(ctrlOriginator, ev);
        }

        private void bbiDatasetManager_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnDatasetManagerEvent(ctrlOriginator, ev);
        }

        private void bbiDatasetSelectionInverted_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnDatasetSelectionInvertedEvent(ctrlOriginator, ev);
        }

        private void bbiShowMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnShowMapEvent(ctrlOriginator, ev);
        }

        private void bbiShowGoogleMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnShowGoogleMapEvent(ctrlOriginator, ev);
        }

        private void bbiViewAuditTrail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();
            OnViewAuditTrail(ctrlOriginator, ev);
        }


        public int i_intCopyRowHandle = -1;
        public string i_strCopyColumnName = "";
        public GridView i_GridViewFocused = null;
        private void bbiCopyToClipboard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //EventArgs ev = new EventArgs();
            //OnCopyToClipboard(ctrlOriginator, ev);
            try
            {
                GridView view = i_GridViewFocused;
                if (view != null && i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName))
                {
                    string strTextToCopy = view.GetRowCellValue(i_intCopyRowHandle, i_strCopyColumnName).ToString();
                    if (string.IsNullOrWhiteSpace(strTextToCopy)) return;
                    Clipboard.SetText(strTextToCopy);
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Apply alpha blending to selected row and focused row of all grids on form to allow background colours to show through //
        /// </summary>
        /// <param name="controls"></param>
        public void Set_Grid_Highlighter_Transparent(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                if (item is DevExpress.XtraGrid.GridControl)
                {
                    GridControl gridControl = (GridControl)item;
                    Update_Grid_Highlighter_Colour(gridControl);
                }
                else if (item is DevExpress.XtraGrid.GridSplitContainer)
                {
                    GridSplitContainer gridSplitContainer = (GridSplitContainer)item;
                    GridControl gridControl = gridSplitContainer.Grid;
                    Update_Grid_Highlighter_Colour(gridControl);
                }
                else if (item is DevExpress.XtraVerticalGrid.VGridControl)
                {
                    VGridControl vGridControl = (VGridControl)item;
                    Update_VGrid_Highlighter_Colour(vGridControl);
                }

                if (item.HasChildren) Set_Grid_Highlighter_Transparent(item.Controls);
            }
        }
        private void Update_Grid_Highlighter_Colour(GridControl gridControl)
        {
            if (gridControl == null) return;
            gridControl.ForceInitialize();
            GridView gridView = (GridView)gridControl.MainView;

            Skin currentSkin = DevExpress.Skins.CommonSkins.GetSkin(gridControl.LookAndFeel.ActiveLookAndFeel);
            Color color = currentSkin.Colors["Highlight"];
            gridView.Appearance.SelectedRow.BackColor = Color.FromArgb(100, color.R, color.G, color.B);
            gridView.Appearance.FocusedRow.BackColor = Color.FromArgb(125, color.R, color.G, color.B);
            gridView.Appearance.HideSelectionRow.BackColor = Color.FromArgb(75, color.R, color.G, color.B);
        }
        private void Update_VGrid_Highlighter_Colour(VGridControl vGridControl)
        {
            if (vGridControl == null) return;
            vGridControl.ForceInitialize();

            Skin currentSkin = DevExpress.Skins.CommonSkins.GetSkin(vGridControl.LookAndFeel.ActiveLookAndFeel);
            Color color = currentSkin.Colors["Highlight"];
            vGridControl.Appearance.SelectedRow.BackColor = Color.FromArgb(100, color.R, color.G, color.B);
            vGridControl.Appearance.FocusedRow.BackColor = Color.FromArgb(125, color.R, color.G, color.B);
            vGridControl.Appearance.HideSelectionRow.BackColor = Color.FromArgb(75, color.R, color.G, color.B);
        }



    }
}

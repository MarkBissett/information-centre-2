﻿using System;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraPivotGrid;

namespace BaseObjects
{
    public partial class frmChangePivotGridCaptionDialog : DevExpress.XtraEditors.XtraForm
    {
        private PivotGridField column;
        private Point lastPoint;  // Used to move form without titlebar //
        private bool mouseDown;  // Used to move form without titlebar //

        public frmChangePivotGridCaptionDialog() 
        {
            InitializeComponent();
        }

        public frmChangePivotGridCaptionDialog(PivotGridField column, Point position) : this() 
        {
            this.column = column;
            buttonEdit1.Text = column.Caption;
            Location = position;
        }

        private void OnApplyClick(object sender, EventArgs e) 
        {
            column.Caption = buttonEdit1.Text;
            Close();
        }

        private void OnCancelClick(object sender, EventArgs e) 
        {
            Close();
        }

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.OK)
            {
                column.Caption = buttonEdit1.Text;
                Close();
            }
            else
            {
                Close();
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            column.Caption = buttonEdit1.Text;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                column.Caption = buttonEdit1.Text;
                Close();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }


        // Code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mouseDown = true;
            this.lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.mouseDown) this.Location = new Point(this.Left - (this.lastPoint.X - e.X), this.Top - (this.lastPoint.Y - e.Y));
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            this.mouseDown = false;
        }
        // End of code to Handle moving a form without a title bar [fired from Panel events as the panel takes up the whole form] //
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGauges.Win.Base;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frmProgress : DevExpress.XtraEditors.XtraForm
    {
        public Boolean boolRestartOnExceeding100Percent = false;

        /// <summary>
        /// 
        /// </summary>
        public frmProgress(float floatStartingPosition)
        {
            InitializeComponent();
            loadingTimer.Interval = 300;
            linearScaleComponent1.Value = floatStartingPosition;
            //loadingTimer.Start();
        }

        private void frmProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            loadingTimer.Stop();
        }
        
        int loadingCount = 0;
        private void loadingTimer_Tick(object sender, EventArgs e)
        {
            ((ILinearGauge)rating1.Gauges[0]).Scales[0].Value = (float)(++loadingCount % 10) * 10f + (float)DateTime.Now.Millisecond * 0.01f;
            Application.DoEvents();   // Allow Form time to repaint itself //

        }

        /// <summary>
        /// 
        /// </summary>
        public void StartTimer()
        {
            loadingTimer.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public void EndTimer()
        {
            loadingTimer.Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intInterval"></param>
        public void SetTimerInterval(int intInterval)
        {
            if (intInterval <= 0) return;
            loadingTimer.Interval = intInterval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="floatIncrement"></param>
        public void UpdateProgress(float floatIncrement)
        {
            // Best to pass in 10 as the increment as it scales in incrments of 10, but any value [1-100] is appropriate //
            if (floatIncrement <= 0) return;
            if (floatIncrement > 100) floatIncrement = 100;
            float floatCurrentValue = linearScaleComponent1.Value;

            if (boolRestartOnExceeding100Percent)
            {
                if (floatCurrentValue + floatIncrement > 100) floatCurrentValue = 0;
                floatCurrentValue += floatIncrement;
            }
            else if (floatCurrentValue + floatIncrement > 100)
            {
                floatCurrentValue = 100;
            }
            else
            {
                floatCurrentValue += floatIncrement;
            }
            linearScaleComponent1.Value = floatCurrentValue;
            Application.DoEvents();   // Allow Form time to repaint itself //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="floatValue"></param>
        public void SetProgressValue(float floatValue)
        {
            // Any value [0-100] is appropriate //
            if (floatValue < 0) floatValue = 0;
            if (floatValue > 100) floatValue = 100;
            linearScaleComponent1.Value = floatValue;
            Application.DoEvents();  // Allow Form time to repaint itself //
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strValue"></param>
        public void UpdateCaption(string strValue)
        {
            labelControl1.Text = strValue;
        }
    }
}



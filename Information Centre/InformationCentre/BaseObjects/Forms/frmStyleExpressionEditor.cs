using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Design;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using BaseObjects.Controls;

namespace BaseObjects
{
    public partial class frmStyleExpressionEditor : DevExpress.XtraEditors.XtraForm
    {
        public GridView currentView = null;
        bool init = false;
        BaseObjects.Controls.XtraPropertyGrid propertyGrid1;
        private ArrayList arrayListImportGridColumns = new ArrayList();  // Holds data for Column list //

        public frmStyleExpressionEditor(GridView view)
        {
            InitializeComponent();
            currentView = view;

            propertyGrid1 = new BaseObjects.Controls.XtraPropertyGrid();
            propertyGrid1.Location = new Point(0, 194);
            propertyGrid1.Size = new System.Drawing.Size(294, 315);
            propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            Controls.Add(propertyGrid1);
            propertyGrid1.ShowButtons = propertyGrid1.ShowCategories = propertyGrid1.ShowDescription = false;

            InitConditions();
            InitColumns();
            if(FormatItemList.Items.Count > 0) FormatItemList.SelectedIndex = 0;
            

        }

        void InitColumns() 
        {
            if (currentView == null) return;
            foreach (GridColumn col in currentView.Columns)
            {
                imageComboBoxEdit1.Properties.Items.Add(new ImageComboBoxItem(col.GetTextCaption(), col, -1));
            }
        }

        void InitConditions() 
        {
            if(currentView == null) return;
            FormatItemList.BeginUpdate();
            try 
            {
                FormatItemList.Items.Clear();
                foreach(StyleFormatCondition condition in currentView.FormatConditions) {
                    ItemExpressionCondition eCondition = new ItemExpressionCondition(condition);
                    if(eCondition.IsExpressionCondition) {
                        FormatItemList.Items.Add(eCondition);
                    }
                }
            }
            finally 
            {
                FormatItemList.EndUpdate();
            }
        }

        StyleFormatCondition CurrentCondition
        {
            get
            {
                if (FormatItemList.SelectedItem != null) return ((ItemExpressionCondition)FormatItemList.SelectedItem).Condition;
                return null;
            }
        }

        void ShowEditor(StyleFormatCondition condition)
        {
            using (ExpressionEditorForm form = new ConditionExpressionEditorForm(condition, null))
            {
                form.StartPosition = FormStartPosition.CenterParent;
                if (form.ShowDialog(this) == DialogResult.OK)
                {
                    condition.Expression = form.Expression;
                }
            }
        }

        void ShowEditor()
        {
            if (CurrentCondition == null) return;
            ShowEditor(CurrentCondition);
            FormatItemList.Refresh();
        }

        private void frmStyleExpressionEditor_Load(object sender, EventArgs e)
        {

        }

        private void FormatItemList_DoubleClick(object sender, EventArgs e)
        {
            ShowEditor();
        }
      
        void SelectObjectUpdate() 
        {
            if(update) return;
            EnableButtons();
            init = true;
            if(CurrentCondition == null) 
            {
                propertyGrid1.Enabled = false;
                propertyGrid1.PropertyGrid.SelectedObject = null;
                panelControl1.Visible = false;

            }
            else 
            {
                propertyGrid1.Enabled = true;
                propertyGrid1.PropertyGrid.SelectedObject = CurrentCondition.Appearance;
                panelControl1.Visible = true;
                checkEdit1.Checked = CurrentCondition.ApplyToRow;
                imageComboBoxEdit1.EditValue = CurrentCondition.Column;
            }
            init = false;
        }

        private void FormatItemList_SelectedIndexChanged(object sender, EventArgs e) 
        {
            SelectObjectUpdate();
        }

        private void EnableButtons() 
        {
            barButtonItem2.Enabled = CurrentCondition != null;
            barButtonItem3.Visibility = CurrentCondition == null ? BarItemVisibility.Never : BarItemVisibility.Always;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Condition = FormatConditionEnum.Expression;
            currentView.FormatConditions.Add(condition);
            int index = FormatItemList.Items.Count;
            InitConditions();
            FormatItemList.SelectedIndex = index;
            ShowEditor();
        }

        bool update = false;
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            update = true;
            if(CurrentCondition == null) return;
            currentView.FormatConditions.Remove(CurrentCondition);
            FormatItemList.Items.RemoveAt(FormatItemList.SelectedIndex);
            update = false;
            SelectObjectUpdate();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowEditor();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (CurrentCondition == null || init) return;
            CurrentCondition.ApplyToRow = ce.Checked;
        }

        private void ceTranslucentColors_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                currentView.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);
            }
            else
            {
                currentView.Appearance.FocusedRow.Reset();
            }
        }
  
        private void imageComboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentCondition == null || init) return;
            GridColumn col = imageComboBoxEdit1.EditValue as GridColumn;
            CurrentCondition.Column = col;
        }
  
    }


    public class ItemCondition
    {
        public static string ConditionsName = "(Format Conditions)";
        StyleFormatCondition fcondition;
        public ItemCondition(StyleFormatCondition fcondition)
        {
            this.fcondition = fcondition;
        }
        public override string ToString()
        {
            if (fcondition.Condition != FormatConditionEnum.None)
                if (fcondition.Condition == FormatConditionEnum.Between || fcondition.Condition == FormatConditionEnum.NotBetween)
                {
                    return string.Format("{0} {1}, {2}", fcondition.Condition, fcondition.Value1, fcondition.Value2);
                }
                else
                    return string.Format("{0} {1}", fcondition.Condition, fcondition.Value1);
            return string.Format("Condition Item - Index {0}", Index);
        }
        public int Index { get { return fcondition.Column.View.FormatConditions.IndexOf(fcondition); } }
        public StyleFormatCondition Condition { get { return fcondition; } }
    }

    public class ItemExpressionCondition
    {
        StyleFormatCondition fcondition;
        public ItemExpressionCondition(StyleFormatCondition fcondition)
        {
            this.fcondition = fcondition;
        }
        public bool IsExpressionCondition
        {
            get
            {
                return fcondition.Condition == FormatConditionEnum.Expression;
            }
        }
        public override string ToString()
        {
            if (fcondition.Expression == string.Empty)
                return string.Format("Empty Condition {0}", Index);
            return fcondition.Expression;
        }
        public int Index { get { return fcondition.Collection.IndexOf(fcondition); } }
        public StyleFormatCondition Condition { get { return fcondition; } }
    }
}
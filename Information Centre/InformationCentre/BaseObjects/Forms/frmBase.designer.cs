using System;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            try
            {
                base.Dispose(disposing);
            }
            catch (Exception)
            {
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBase));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.pmDataContextMenu = new DevExpress.XtraBars.PopupMenu();
            this.bsiAdd = new DevExpress.XtraBars.BarSubItem();
            this.bbiSingleAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bsiEdit = new DevExpress.XtraBars.BarSubItem();
            this.bbiSingleEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyToClipboard = new DevExpress.XtraBars.BarButtonItem();
            this.bsiDataset = new DevExpress.XtraBars.BarSubItem();
            this.bbiDatasetSelection = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDatasetSelectionInverted = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDatasetCreate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDatasetManager = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowGoogleMap = new DevExpress.XtraBars.BarButtonItem();
            this.bsiAuditTrail = new DevExpress.XtraBars.BarSubItem();
            this.bbiViewAuditTrail = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiUndo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRedo = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.bbiCut = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopy = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPaste = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSpellChecker = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGrammarCheck = new DevExpress.XtraBars.BarButtonItem();
            this.pmEditContextMenu = new DevExpress.XtraBars.PopupMenu();
            this.scSpellChecker = new BaseObjects.ExtendedSpellChecker();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyToClipboard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiDataset, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowGoogleMap),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.bsiAuditTrail, "Audit Trail...", true)});
            this.pmDataContextMenu.Manager = this.barManager1;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            this.pmDataContextMenu.MenuCaption = "Data Entry Menu";
            this.pmDataContextMenu.Name = "pmDataContextMenu";
            this.pmDataContextMenu.ShowCaption = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.Caption = "Add...";
            this.bsiAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiAdd.Glyph")));
            this.bsiAdd.Id = 0;
            this.bsiAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSingleAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAdd)});
            this.bsiAdd.Name = "bsiAdd";
            this.bsiAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiAdd_ItemClick);
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.Caption = "Add";
            this.bbiSingleAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.Glyph")));
            this.bbiSingleAdd.Id = 1;
            this.bbiSingleAdd.Name = "bbiSingleAdd";
            this.bbiSingleAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSingleAdd_ItemClick);
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiBlockAdd.Caption = "<b>Block</b> Add";
            this.bbiBlockAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.Glyph")));
            this.bbiBlockAdd.Id = 2;
            this.bbiBlockAdd.Name = "bbiBlockAdd";
            this.bbiBlockAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAdd_ItemClick);
            // 
            // bsiEdit
            // 
            this.bsiEdit.Caption = "Edit...";
            this.bsiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiEdit.Glyph")));
            this.bsiEdit.Id = 3;
            this.bsiEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSingleEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEdit)});
            this.bsiEdit.Name = "bsiEdit";
            this.bsiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiEdit_ItemClick);
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.Caption = "Edit";
            this.bbiSingleEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.Glyph")));
            this.bbiSingleEdit.Id = 4;
            this.bbiSingleEdit.Name = "bbiSingleEdit";
            this.bbiSingleEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSingleEdit_ItemClick);
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiBlockEdit.Caption = "<b>Block</b> Edit";
            this.bbiBlockEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.Glyph")));
            this.bbiBlockEdit.Id = 5;
            this.bbiBlockEdit.Name = "bbiBlockEdit";
            this.bbiBlockEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEdit_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Delete";
            this.bbiDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.Glyph")));
            this.bbiDelete.Id = 6;
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Save";
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 7;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.Caption = "Copy To Clipboard";
            this.bbiCopyToClipboard.Enabled = false;
            this.bbiCopyToClipboard.Glyph = global::BaseObjects.Properties.Resources.report_32x32;
            this.bbiCopyToClipboard.Id = 29;
            this.bbiCopyToClipboard.Name = "bbiCopyToClipboard";
            this.bbiCopyToClipboard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyToClipboard_ItemClick);
            // 
            // bsiDataset
            // 
            this.bsiDataset.Caption = "DataSet";
            this.bsiDataset.Enabled = false;
            this.bsiDataset.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiDataset.Glyph")));
            this.bsiDataset.Id = 19;
            this.bsiDataset.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiDataset.LargeGlyph")));
            this.bsiDataset.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDatasetSelection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDatasetSelectionInverted),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDatasetCreate, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDatasetManager)});
            this.bsiDataset.Name = "bsiDataset";
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDatasetSelection.Caption = "Select Where Records <b>in</b> Dataset";
            this.bbiDatasetSelection.Enabled = false;
            this.bbiDatasetSelection.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.Glyph")));
            this.bbiDatasetSelection.Id = 20;
            this.bbiDatasetSelection.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.LargeGlyph")));
            this.bbiDatasetSelection.Name = "bbiDatasetSelection";
            this.bbiDatasetSelection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDatasetSelection_ItemClick);
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDatasetSelectionInverted.Caption = "Select Where Records <b>NOT</b> in Dataset";
            this.bbiDatasetSelectionInverted.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.Glyph")));
            this.bbiDatasetSelectionInverted.Id = 24;
            this.bbiDatasetSelectionInverted.Name = "bbiDatasetSelectionInverted";
            this.bbiDatasetSelectionInverted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDatasetSelectionInverted_ItemClick);
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDatasetCreate.Caption = "<b>Create</b> Dataset";
            this.bbiDatasetCreate.Enabled = false;
            this.bbiDatasetCreate.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.Glyph")));
            this.bbiDatasetCreate.Id = 21;
            this.bbiDatasetCreate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.LargeGlyph")));
            this.bbiDatasetCreate.Name = "bbiDatasetCreate";
            this.bbiDatasetCreate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDatasetCreate_ItemClick);
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDatasetManager.Caption = "Dataset <b>Manager</b>";
            this.bbiDatasetManager.Enabled = false;
            this.bbiDatasetManager.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.Glyph")));
            this.bbiDatasetManager.Id = 22;
            this.bbiDatasetManager.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.LargeGlyph")));
            this.bbiDatasetManager.Name = "bbiDatasetManager";
            this.bbiDatasetManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDatasetManager_ItemClick);
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.Caption = "Show Map";
            this.bbiShowMap.Enabled = false;
            this.bbiShowMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.Glyph")));
            this.bbiShowMap.Id = 25;
            this.bbiShowMap.Name = "bbiShowMap";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Show Map - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Opens a Map displaying the position of the currently selected record where approp" +
    "riate.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiShowMap.SuperTip = superToolTip1;
            this.bbiShowMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowMap_ItemClick);
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.Caption = "Show using Google Maps";
            this.bbiShowGoogleMap.Enabled = false;
            this.bbiShowGoogleMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.Glyph")));
            this.bbiShowGoogleMap.Id = 26;
            this.bbiShowGoogleMap.Name = "bbiShowGoogleMap";
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Show using Google Maps - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Opens a Google Map displaying the position of the currently selected record where" +
    " appropriate.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiShowGoogleMap.SuperTip = superToolTip2;
            this.bbiShowGoogleMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowGoogleMap_ItemClick);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.Enabled = false;
            this.bsiAuditTrail.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.Glyph")));
            this.bsiAuditTrail.Id = 28;
            this.bsiAuditTrail.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.bbiViewAuditTrail, "<b>View</b> Audit Trail for <b>Selection</b>")});
            this.bsiAuditTrail.Name = "bsiAuditTrail";
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewAuditTrail.Enabled = false;
            this.bbiViewAuditTrail.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.Glyph")));
            this.bbiViewAuditTrail.Id = 27;
            this.bbiViewAuditTrail.Name = "bbiViewAuditTrail";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "View Audit Trail - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Audit Trail Viewer to view a historical list of data changes" +
    " to the selected record(s).";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiViewAuditTrail.SuperTip = superToolTip3;
            this.bbiViewAuditTrail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewAuditTrail_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.HideBarsWhenMerging = false;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiAdd,
            this.bbiSingleAdd,
            this.bbiBlockAdd,
            this.bsiEdit,
            this.bbiSingleEdit,
            this.bbiBlockEdit,
            this.bbiDelete,
            this.bbiSave,
            this.bbiUndo,
            this.bbiRedo,
            this.barLinkContainerItem1,
            this.bbiCut,
            this.bbiCopy,
            this.bbiPaste,
            this.bbiSelectAll,
            this.bbiClear,
            this.bbiSpellChecker,
            this.barButtonItem1,
            this.bbiGrammarCheck,
            this.bsiDataset,
            this.bbiDatasetSelection,
            this.bbiDatasetCreate,
            this.bbiDatasetManager,
            this.bbiDatasetSelectionInverted,
            this.bbiShowMap,
            this.bbiShowGoogleMap,
            this.bbiViewAuditTrail,
            this.bsiAuditTrail,
            this.bbiCopyToClipboard});
            this.barManager1.MaxItemId = 30;
            this.barManager1.QueryShowPopupMenu += new DevExpress.XtraBars.QueryShowPopupMenuEventHandler(this.barManager1_QueryShowPopupMenu);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(628, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 537);
            this.barDockControlBottom.Size = new System.Drawing.Size(628, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 537);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(628, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 537);
            // 
            // bbiUndo
            // 
            this.bbiUndo.Caption = "Undo";
            this.bbiUndo.Glyph = global::BaseObjects.Properties.Resources.undo_16;
            this.bbiUndo.Id = 8;
            this.bbiUndo.Name = "bbiUndo";
            this.bbiUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUndo_ItemClick);
            // 
            // bbiRedo
            // 
            this.bbiRedo.Caption = "Redo";
            this.bbiRedo.Glyph = global::BaseObjects.Properties.Resources.redo_16;
            this.bbiRedo.Id = 9;
            this.bbiRedo.Name = "bbiRedo";
            this.bbiRedo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRedo_ItemClick);
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 10;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // bbiCut
            // 
            this.bbiCut.Caption = "Cut";
            this.bbiCut.Glyph = global::BaseObjects.Properties.Resources.cut_16;
            this.bbiCut.Id = 11;
            this.bbiCut.Name = "bbiCut";
            this.bbiCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCut_ItemClick);
            // 
            // bbiCopy
            // 
            this.bbiCopy.Caption = "Copy";
            this.bbiCopy.Glyph = global::BaseObjects.Properties.Resources.copy_16;
            this.bbiCopy.Id = 12;
            this.bbiCopy.Name = "bbiCopy";
            this.bbiCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopy_ItemClick);
            // 
            // bbiPaste
            // 
            this.bbiPaste.Caption = "Paste";
            this.bbiPaste.Glyph = global::BaseObjects.Properties.Resources.paste_16;
            this.bbiPaste.Id = 13;
            this.bbiPaste.Name = "bbiPaste";
            this.bbiPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPaste_ItemClick);
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.Caption = "Select All";
            this.bbiSelectAll.Glyph = global::BaseObjects.Properties.Resources.select_all_16;
            this.bbiSelectAll.Id = 14;
            this.bbiSelectAll.Name = "bbiSelectAll";
            this.bbiSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectAll_ItemClick);
            // 
            // bbiClear
            // 
            this.bbiClear.Caption = "Clear";
            this.bbiClear.Glyph = global::BaseObjects.Properties.Resources.clear_16;
            this.bbiClear.Id = 15;
            this.bbiClear.Name = "bbiClear";
            this.bbiClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClear_ItemClick);
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.Caption = "SpellChecker";
            this.bbiSpellChecker.Glyph = global::BaseObjects.Properties.Resources.spellcheck_16;
            this.bbiSpellChecker.Id = 16;
            this.bbiSpellChecker.Name = "bbiSpellChecker";
            this.bbiSpellChecker.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSpellChecker_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 17;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.Caption = "GrammarChecker";
            this.bbiGrammarCheck.Enabled = false;
            this.bbiGrammarCheck.Glyph = global::BaseObjects.Properties.Resources.grammercheck_16;
            this.bbiGrammarCheck.Id = 18;
            this.bbiGrammarCheck.Name = "bbiGrammarCheck";
            this.bbiGrammarCheck.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiGrammarCheck.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGrammarCheck_ItemClick);
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpellChecker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGrammarCheck)});
            this.pmEditContextMenu.Manager = this.barManager1;
            this.pmEditContextMenu.MenuCaption = "Edit Menu";
            this.pmEditContextMenu.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.pmEditContextMenu.Name = "pmEditContextMenu";
            this.pmEditContextMenu.ShowCaption = true;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.Culture = new System.Globalization.CultureInfo("en-GB");
            this.scSpellChecker.GetEditControlContents = "";
            this.scSpellChecker.ParentContainer = null;
            this.scSpellChecker.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.AsYouType;
            this.scSpellChecker.SpellingFormType = DevExpress.XtraSpellChecker.SpellingFormType.Word;
            // 
            // frmBase
            // 
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frmBase";
            this.ShowIcon = false;
            this.Text = "XtraForm1";
            this.Enter += new System.EventHandler(this.frmBase_Enter);
            this.Leave += new System.EventHandler(this.frmBase_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu pmDataContextMenu;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarSubItem bsiAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSingleAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiBlockAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarSubItem bsiEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSingleEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiBlockEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiDelete;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSave;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiUndo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiRedo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiCut;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiCopy;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiPaste;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSelectAll;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiClear;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSpellChecker;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu pmEditContextMenu;
        /// <summary>
        /// 
        /// </summary>
        protected ExtendedSpellChecker scSpellChecker;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiGrammarCheck;
        protected DevExpress.XtraBars.BarSubItem bsiDataset;
        protected DevExpress.XtraBars.BarButtonItem bbiDatasetSelection;
        protected DevExpress.XtraBars.BarButtonItem bbiDatasetCreate;
        protected DevExpress.XtraBars.BarButtonItem bbiDatasetManager;
        protected DevExpress.XtraBars.BarButtonItem bbiDatasetSelectionInverted;
        protected DevExpress.XtraBars.BarButtonItem bbiShowMap;
        protected DevExpress.XtraBars.BarButtonItem bbiShowGoogleMap;
        public DevExpress.XtraBars.BarManager barManager1;
        protected DevExpress.XtraBars.BarSubItem bsiAuditTrail;
        protected DevExpress.XtraBars.BarButtonItem bbiViewAuditTrail;
        protected DevExpress.XtraBars.BarButtonItem bbiCopyToClipboard;
    }
}

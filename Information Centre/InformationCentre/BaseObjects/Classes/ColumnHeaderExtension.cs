﻿// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E2793

using System;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using DevExpress.Skins;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Drawing;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraGrid;

namespace BaseObjects
{
    public class ColumnHeaderExtender
    {
        GridView view;
        SkinEditorButtonPainter customButtonPainter;
        EditorButtonObjectInfoArgs args;
        DataTable table = null;

        public ColumnHeaderExtender(GridView view, DataTable table)
        {
            this.view = view;
            this.table = table;
        }

        public void AddCustomButton()
        {
            CreateButtonPainter();
            CreateButtonInfoArgs();
            SubscribeToEvents();
        }

        private void CreateButtonInfoArgs()
        {
            EditorButton btn = new EditorButton(ButtonPredefines.Glyph);
            args = new EditorButtonObjectInfoArgs(btn, new DevExpress.Utils.AppearanceObject());
        }

        private void CreateButtonPainter()
        {
            customButtonPainter = new SkinEditorButtonPainter(view.GridControl.LookAndFeel);
        }

        private void SubscribeToEvents()
        {
            view.CustomDrawColumnHeader += OnCustomDrawColumnHeader;
            view.MouseDown += OnMouseDown;
            view.MouseUp += OnMouseUp;
            view.MouseMove += OnMouseMove;
        }

        void OnMouseUp(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;
            if (IsButtonRect(e.Location, column))
            {
                SetButtonState(column, ObjectState.Normal);
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;

                GridControl grid = view.GridControl;
                PopupContainerControl popup = FindPopupContainerControl(grid);
                if (popup != null)
                {
                    PopupContainerEdit edit = popup.OwnerEdit;
                    if (edit == null) return;
                    frmBase f = edit.FindForm() as frmBase;
                    if (f == null) return;
                    MethodInfo method = typeof(frmBase).GetMethod("CustomColumnButtonClick");
                    method.Invoke(f, new object[] { view, column.Name });
                }
                else
                {
                    Control ctrl = view.GridControl.Parent;
                    frmBase f = ctrl.FindForm() as frmBase;
                    if (f == null) return;
                    MethodInfo method = typeof(frmBase).GetMethod("CustomColumnButtonClick");
                    method.Invoke(f, new object[] { view, column.Name });
                }
            }
        }

        void OnMouseMove(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;
            if (IsButtonRect(e.Location, column))
                SetButtonState(column, ObjectState.Hot);
            else
                SetButtonState(column, ObjectState.Normal);
        }

        void OnMouseDown(object sender, MouseEventArgs e)
        {
            GridHitInfo hitInfo = view.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.Column) return;
            GridColumn column = hitInfo.Column;
            if (IsButtonRect(e.Location, column)) SetButtonState(column, ObjectState.Pressed);
        }

        private void SetButtonState(GridColumn column, ObjectState state)
        {
            column.Tag = state;
            view.InvalidateColumnHeader(column);
        }

        private bool IsButtonRect(Point point, GridColumn column)
        {
            GraphicsInfo info = new GraphicsInfo();
            info.AddGraphics(null);
            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridColumnInfoArgs columnArgs = viewInfo.ColumnsInfo[column];

            foreach (DataRow dr in table.Rows)
            {
                if (dr["ColumnName"].ToString() == column.Name)
                {
                    Rectangle buttonRect = CalcButtonRect(columnArgs, info.Graphics, new Size(Convert.ToInt32(dr["ButtonWidth"]), Convert.ToInt32(dr["ButtonHeight"])), dr["ButtonText"].ToString());
                    info.ReleaseGraphics();
                    return buttonRect.Contains(point);
                }
            }
            info.ReleaseGraphics();
            return false;
        }

        private Rectangle CalcButtonRect(GridColumnInfoArgs columnArgs, Graphics gr, Size buttonSize, string buttonText)
        {
            Rectangle columnRect = columnArgs.Bounds;
            int innerElementsWidth = CalcInnerElementsMinWidth(columnArgs, gr);
            Rectangle buttonRect = new Rectangle(columnRect.Right - innerElementsWidth - buttonSize.Width - 2, columnRect.Y + columnRect.Height / 2 - buttonSize.Height / 2, buttonSize.Width, buttonSize.Height);
            return buttonRect;
        }

        private int CalcInnerElementsMinWidth(GridColumnInfoArgs columnArgs, Graphics gr)
        {
            bool canDrawMode = true;
            return columnArgs.InnerElements.CalcMinSize(gr, ref canDrawMode).Width;
        }

        void OnCustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null) return;
            foreach (DataRow dr in table.Rows)
            {
                if (dr["ColumnName"].ToString() == e.Column.Name)
                {
                    DefaultDrawColumnHeader(e);
                    DrawCustomButton(e, new Size(Convert.ToInt32(dr["ButtonWidth"]), Convert.ToInt32(dr["ButtonHeight"])), dr["ButtonText"].ToString());
                    e.Handled = true;
                }
            }
        }

        private void DrawCustomButton(ColumnHeaderCustomDrawEventArgs e, Size buttonSize, string buttonText)
        {
            SetUpButtonInfoArgs(e, buttonSize, buttonText);
            args.Button.Caption = buttonText;
            customButtonPainter.DrawObject(args);
        }

        private void SetUpButtonInfoArgs(ColumnHeaderCustomDrawEventArgs e, Size buttonSize, string buttonText)
        {
            args.Cache = e.Cache;
            args.Bounds = CalcButtonRect(e.Info, e.Graphics, buttonSize, buttonText);
            ObjectState state = ObjectState.Normal;
            if (e.Column.Tag is ObjectState) state = (ObjectState)e.Column.Tag;
            args.State = state;
        }

        private static void DefaultDrawColumnHeader(ColumnHeaderCustomDrawEventArgs e)
        {
            e.Painter.DrawObject(e.Info);
        }

        private void UnsubscribeFromEvents()
        {
            view.CustomDrawColumnHeader -= OnCustomDrawColumnHeader;
            view.MouseDown -= OnMouseDown;
            view.MouseUp -= OnMouseUp;
            view.MouseMove -= OnMouseMove;
        }

        public void RemoveCustomButton()
        {
            UnsubscribeFromEvents();
        }

        private PopupContainerControl FindPopupContainerControl(Control ctrl)
        {
            if (ctrl is PopupContainerControl) return ctrl as PopupContainerControl;
            if (ctrl.Parent == null) return null;
            return FindPopupContainerControl(ctrl.Parent);
        }

    }
}
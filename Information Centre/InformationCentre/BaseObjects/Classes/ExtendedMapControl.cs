using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraMap;


namespace BaseObjects
{
    public class ExtendedMapControl : MapControl
    {
        public bool RightClickDrag = true;

        // Stops Right Click Dragging (don't want it if we are displaying the popup menu) //
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button != System.Windows.Forms.MouseButtons.Right || RightClickDrag)
            {
                base.OnMouseDown(e);
            }
        }


    }

}



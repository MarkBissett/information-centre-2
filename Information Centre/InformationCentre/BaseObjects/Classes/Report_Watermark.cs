﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;

namespace BaseObjects
{
    public class Report_Watermark
    {
        public void Create_Report_Watermark(XtraReport report, string strConnectionString)
        {
            // Get Default Watermark Settings from DB //
            string WatermarkText = "";
            string WatermarkTextDirection = "";
            string WatermarkTextFont = "";
            int WatermarkTextFontSize = 66;
            int WatermarkTextFontBold = 0;
            int WatermarkTextFontItalic = 0;
            int WatermarkTextColour = -16777216;
            int WatermarkTextTransparency = 128;
            string WatermarkImage = "";
            string WatermarkImageAlign = "";
            int WatermarkImageTiling = 0;
            string WatermarkImageViewMode = "";
            int WatermarkImageTransparency = 128;
            int WatermarkShowBehind = 1;
            string WatermarkTextPageRange = "";
            try
            {
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp00239_Get_Watermark_Settings", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
                DataSet dsSettings = new DataSet("NewDataSet");
                sdaSettings.Fill(dsSettings, "Table");
                conn.Close();
                conn.Dispose();
                if (dsSettings.Tables[0].Rows.Count != 1)
                {
                    return;
                }
                DataRow dr = dsSettings.Tables[0].Rows[0];
                WatermarkText = dr["WatermarkText"].ToString();
                WatermarkTextDirection = dr["WatermarkTextDirection"].ToString();
                WatermarkTextFont = dr["WatermarkTextFont"].ToString();
                WatermarkTextFontSize = Convert.ToInt32(dr["WatermarkTextFontSize"]);
                WatermarkTextFontBold = Convert.ToInt32(dr["WatermarkTextFontBold"]);
                WatermarkTextFontItalic = Convert.ToInt32(dr["WatermarkTextFontItalic"]);
                WatermarkTextColour = Convert.ToInt32(dr["WatermarkTextColour"]);
                WatermarkTextTransparency = Convert.ToInt32(dr["WatermarkTextTransparency"]);
                WatermarkImage = dr["WatermarkImage"].ToString();
                WatermarkImageAlign = dr["WatermarkImageAlign"].ToString();
                WatermarkImageTiling = Convert.ToInt32(dr["WatermarkImageTiling"]);
                WatermarkImageViewMode = dr["WatermarkImageViewMode"].ToString();
                WatermarkImageTransparency = Convert.ToInt32(dr["WatermarkImageTransparency"]);
                WatermarkShowBehind = Convert.ToInt32(dr["WatermarkShowBehind"]);
                WatermarkTextPageRange = dr["WatermarkTextPageRange"].ToString();

                if (string.IsNullOrEmpty(WatermarkText) && string.IsNullOrEmpty(WatermarkImage)) return;

                // Watermark Text //
                report.Watermark.Text = WatermarkText;
                switch (WatermarkTextDirection)
                {
                    case "Horizontal":
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.Horizontal;
                        break;
                    case "Vertical":
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.Vertical;
                        break;
                    case "Backward Diagonal":
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.BackwardDiagonal;
                        break;
                    case "Forward Diagonal":
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        break;
                    default:
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        break;
                }
                if (WatermarkTextFontBold == 1 && WatermarkTextFontItalic == 1)
                {
                    report.Watermark.Font = new Font(WatermarkTextFont, WatermarkTextFontSize, FontStyle.Bold | FontStyle.Italic);
                }
                else if (WatermarkTextFontBold == 1)
                {
                    report.Watermark.Font = new Font(WatermarkTextFont, WatermarkTextFontSize, FontStyle.Bold);
                }
                else if (WatermarkTextFontItalic == 1)
                {
                    report.Watermark.Font = new Font(WatermarkTextFont, WatermarkTextFontSize, FontStyle.Italic);
                }
                else
                {
                    report.Watermark.Font = new Font(WatermarkTextFont, WatermarkTextFontSize, FontStyle.Regular);
                }
                report.Watermark.ForeColor = Color.FromArgb(WatermarkTextColour);
                report.Watermark.TextTransparency = WatermarkTextTransparency;

                // Watermark Image //
                if (!string.IsNullOrEmpty(WatermarkImage))
                {
                    report.Watermark.Image = Bitmap.FromFile(WatermarkImage);
                    switch (WatermarkImageAlign)
                    {
                        case "Bottom Centre":
                            report.Watermark.ImageAlign = ContentAlignment.BottomCenter;
                            break;
                        case "Bottom Left":
                            report.Watermark.ImageAlign = ContentAlignment.BottomLeft;
                            break;
                        case "Bottom Right":
                            report.Watermark.ImageAlign = ContentAlignment.BottomRight;
                            break;
                        case "Middle Centre":
                            report.Watermark.ImageAlign = ContentAlignment.MiddleCenter;
                            break;
                        case "Middle Left":
                            report.Watermark.ImageAlign = ContentAlignment.MiddleLeft;
                            break;
                        case "Middle Right":
                            report.Watermark.ImageAlign = ContentAlignment.MiddleRight;
                            break;
                        case "Top Centre":
                            report.Watermark.ImageAlign = ContentAlignment.TopCenter;
                            break;
                        case "Top Left":
                            report.Watermark.ImageAlign = ContentAlignment.TopLeft;
                            break;
                        case "Top Right":
                            report.Watermark.ImageAlign = ContentAlignment.TopRight;
                            break;
                        default:
                            report.Watermark.ImageAlign = ContentAlignment.MiddleCenter;
                            break;
                    }
                    report.Watermark.ImageTiling = (WatermarkImageTiling == 0 ? false : true);
                    switch (WatermarkImageViewMode)
                    {
                        case "Clip":
                            report.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Clip;
                            break;
                        case "Stretch":
                            report.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Stretch;
                            break;
                        case "Zoom":
                            report.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Zoom;
                            break;
                        default:
                            report.Watermark.ImageViewMode = DevExpress.XtraPrinting.Drawing.ImageViewMode.Clip;
                            break;
                    }
                    report.Watermark.ImageTransparency = WatermarkImageTransparency;
                }
                // Both Watermark Text and Image //
                report.Watermark.ShowBehind = (WatermarkShowBehind == 0 ? false : true);
                report.Watermark.PageRange = WatermarkTextPageRange;
            }
            catch (Exception ex)
            {
                return;
            }
        }

    }
}

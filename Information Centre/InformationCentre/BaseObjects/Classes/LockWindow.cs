using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class LockWindow
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("User32.dll", EntryPoint = "LockWindowUpdate")]
        public static extern IntPtr LockWindowUpdate(IntPtr hWnd);

        /// <summary>
        /// 
        /// </summary>
        public LockWindow()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="blValue"></param>
        public static void LockUpdatingWindow(IntPtr hWnd, Boolean blValue)
        {
            if (blValue)
            {
                LockWindowUpdate(hWnd);
            }
            else
            {
                LockWindowUpdate(IntPtr.Zero);
            }
        }
    }
}

﻿// Add bookmark functionality to a grid...
// 
// The following class implements the bookmark functionality in an XtraGridControl. 
// End-users can use the Ctrl + Shift + 0..9 shortcuts to set a bookmark for the focused record,
// and the Ctrl + 0..9 shortcuts to go to the appropriate bookmark. 
// They can also use the bookmark menu for this purpose. It can be raised by right-clicking on the row indicator. 
// To enable this functionality in a form, you must include the following line of code in the constructor: 
//
//      new GridViewBookmarks(yourGridView, "yourKeyFieldName");
// 
// Based on original artical: http://www.devexpress.com/example=E1267
// Mark Bissett
// 07/07/2009

using System;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils.Menu;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace BaseObjects
{
    public class GridViewBookmarks : IDisposable {
        const string ToggleBookmarkStr = "Toggle Bookmark";
        const string GotoBookmarkStr = "Go to Bookmark";
        const string ClearBookmarkStr = "Clear Bookmarks";
        const string BookmarkStr = "Bookmark ";
        const string SetDisplayColumnStr = "Set Display Column";
        GridView view;
        string keyFieldName;
        List<BookmarkData> bookMarks = new List<BookmarkData>();
        string DisplayColumn;

        public GridViewBookmarks(GridView view, string keyFieldName, string DisplayColumn) 
        {
            this.view = view;
            this.keyFieldName = keyFieldName;
            View.MouseUp += new System.Windows.Forms.MouseEventHandler(View_MouseUp);
            View.KeyDown += new System.Windows.Forms.KeyEventHandler(View_KeyDown);
            View.CustomDrawRowIndicator += new RowIndicatorCustomDrawEventHandler(View_CustomDrawRowIndicator);
            this.DisplayColumn = DisplayColumn; // Blank string allowed //
        }
        public GridView View { get { return view; } }
        public string KeyFieldName { get { return keyFieldName; } }
        
        protected List<BookmarkData> BookMarks { get { return bookMarks; } }

        protected void GotoBookmark(int bookmarkIndex) 
        {
            if (bookmarkIndex >= BookMarks.Count) return;
            if (BookMarks[bookmarkIndex] == null) return;
            View.FocusedRowHandle = GetRowHandle(bookmarkIndex);
        }
        protected void ClearBookmarks()
        {
            for (int i = 0; i < BookMarks.Count; i++)
            {
                BookMarks[i] = null;
            }
            view.Invalidate();
            bookMarks = new List<BookmarkData>();
        }
        protected bool HasBookmark(int rowHandle) 
        {
            return GetBookmarkIndex(rowHandle) > -1;
        }
        protected virtual void DrawBookmark(RowIndicatorCustomDrawEventArgs e) 
        {
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(40, Color.DarkBlue))) 
            {
                RectangleF bounds = new RectangleF();
                bounds.X = e.Info.Bounds.X;
                bounds.Y = e.Info.Bounds.Y;
                bounds.Width = e.Info.Bounds.Width;
                bounds.Height = e.Info.Bounds.Height;
                e.Graphics.FillRectangle(brush, bounds);
            }
        }
        protected int GetRowHandle(int bookmarkIndex) 
        {
            return View.DataController.FindRowByValue(KeyFieldName, BookMarks[bookmarkIndex].Value);
        }

        protected int GetBookmarkIndex(int rowHandle) 
        {
            if (!View.IsDataRow(rowHandle)) return -1;
            object value = GetKeyValue(rowHandle);
            for (int i = 0; i < BookMarks.Count; i++) 
            {
                if (BookMarks[i] == null) continue;
                if (object.Equals(BookMarks[i].Value, value)) return i;
            }
            return -1;
        }
        protected object GetKeyValue(int rowHandle)
        {
            return View.GetRowCellValue(rowHandle, KeyFieldName);
        }
        protected object GetDisplayValue(int rowHandle)
        {
            return View.GetRowCellValue(rowHandle, DisplayColumn);
        }
    
        protected DXPopupMenu CreateMenu() 
        {
            DXPopupMenu menu = new DXPopupMenu();
            menu.Items.Add(CreateToggleBookmarkMenu());
            menu.Items.Add(CreateGotoBookmarkMenu());
            menu.Items.Add(CreateClearBookmarksMenu());
            menu.Items.Add(CreateSetDisplayNameColumn());
            return menu;
        }
        protected DXMenuItem CreateToggleBookmarkMenu()
        {
            DXMenuItem menuItem = new DXMenuItem("Toggle Bookmark", new EventHandler(ToggleBookmarkMenuItem_Click));
            return menuItem;
        }
        protected DXSubMenuItem CreateGotoBookmarkMenu()
        {
            DXSubMenuItem menuItem = new DXSubMenuItem(GotoBookmarkStr);
            EventHandler eventHandler = new EventHandler(GotoBookmarkMenuItem_Click);
            string DisplayText = "";
            int FoundRow = 0;
            for (int i = 0; i < BookMarks.Count; i++)
                {
                if (BookMarks[i] != null)
                {
                    DXMenuItem item = null;
                    if (DisplayColumn != "")
                    {
                        DisplayText = "";
                        FoundRow = view.LocateByValue(0, view.Columns[keyFieldName], bookMarks[i].Value);
                        if (FoundRow != GridControl.InvalidRowHandle) DisplayText = view.GetRowCellValue(FoundRow, view.Columns[DisplayColumn]).ToString();
                        item = new DXMenuItem(BookmarkStr + (i + 1).ToString() + "   [" + view.Columns[DisplayColumn].Caption + ": " + DisplayText + "]", eventHandler);
                    }
                    else
                    {
                        item = new DXMenuItem(BookmarkStr + i.ToString(), eventHandler);
                    }
                    item.Tag = i;
                    menuItem.Items.Add(item);
                }
            }
            return menuItem;
        }
        protected DXMenuItem CreateClearBookmarksMenu()
        {
            DXMenuItem menuItem = new DXMenuItem(ClearBookmarkStr, new EventHandler(ClearBookmarksMenuItem_Click));
            bool boolActiveBookMark = false;
            for (int i = 1; i < BookMarks.Count; i++)
            {
                if (BookMarks[i] != null)
                {
                    boolActiveBookMark = true;
                    break;
                }
            }
            menuItem.Enabled = boolActiveBookMark;
            menuItem.BeginGroup = true;

            return menuItem;
        }
        protected DXSubMenuItem CreateSetDisplayNameColumn()
        {
            DXSubMenuItem menuItem = new DXSubMenuItem(SetDisplayColumnStr);
            EventHandler eventHandler = new EventHandler(SetDisplayNameColumnItem_Click);
            List<LookUpDataOBJ> ListGridColumns = new List<LookUpDataOBJ>(); // Holds column names so thay can be sorted into correct order //
            foreach (GridColumn col in view.Columns)
            {
                ListGridColumns.Add(new LookUpDataOBJ(col.FieldName, col.Caption));
            }

            ListGridColumns.Sort();
            foreach (LookUpDataOBJ LookupObject in ListGridColumns)
            {           
                DXMenuItem item = new DXMenuItem(LookupObject.ColumnCaption, eventHandler);
                item.Tag = LookupObject.ColumnName;
                menuItem.Items.Add(item);
            }
            return menuItem;
        }

        protected BookmarkData CreateBookmarkData(int rowHandle)
        {
            return new BookmarkData(GetKeyValue(rowHandle));
        }

        void GotoBookmarkMenuItem_Click(object sender, EventArgs e)
        {
            DXMenuItem menuItem = sender as DXMenuItem;
            GotoBookmark((int)menuItem.Tag);
        }

        void ClearBookmarksMenuItem_Click(object sender, EventArgs e)
        {
            DXMenuItem menuItem = sender as DXMenuItem;
            ClearBookmarks();
        }

        void SetDisplayNameColumnItem_Click(object sender, EventArgs e)
        {
            DXMenuItem menuItem = sender as DXMenuItem;
            DisplayColumn = (string)menuItem.Tag;
        }  

        void ToggleBookmarkMenuItem_Click(object sender, EventArgs e)
        {
            ToggleBookmark(View.FocusedRowHandle);
        }
        protected void ToggleBookmark(int rowHandle)
        {
            while (View.IsGroupRow(rowHandle))
            {
                rowHandle = View.GetChildRowHandle(rowHandle, 0);
            }
            int intReturnedListPosition = CheckStatus(rowHandle);
            if (intReturnedListPosition == -1)
            {
                BookMarks.Add(CreateBookmarkData(rowHandle));
            }
            else
            {
                bookMarks.Remove(BookMarks[intReturnedListPosition]);
            }
            View.InvalidateRowIndicator(rowHandle);
        }
        protected int CheckStatus(int rowHandle)
        {
            for (int i = 0; i < bookMarks.Count; i++)
            {
                if (BookMarks[i] != null)
                {
                    if (bookMarks[i].Value == GetKeyValue(rowHandle)) return i;
                }
            }
            return -1;
        }

        void View_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (!e.Info.IsRowIndicator) return;
            if (!HasBookmark(e.RowHandle)) return;
            e.Painter.DrawObject(e.Info); // Default drawing
            e.Handled = true;
            DrawBookmark(e);
        }
        
        void View_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            GridHitInfo hitInfo = View.CalcHitInfo(e.Location);
            if (hitInfo.HitTest != GridHitTest.RowIndicator) return;
            if (this.View.GridControl.MenuManager != null)
            {
                this.View.GridControl.MenuManager.ShowPopupMenu(CreateMenu(), View.GridControl, e.Location); // Show Menu with Theme //
            }
            else  // No MenuManager specified for MenuManager setting on grid (whoops!) so show menu without theme //
            {
                StandardMenuManager.Default.ShowPopupMenu(CreateMenu(), View.GridControl, e.Location);
            }

        }
        void View_KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.Control) return;
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) return;
            if (e.Shift)
            {
                ToggleBookmark(View.FocusedRowHandle);
            }
            else
            {
                GotoBookmark(e.KeyCode - Keys.D0);
            }
        }


        #region IDisposable Members
        public void Dispose() 
        {
            if (View != null && !View.GridControl.IsDisposed) 
            {
                View.MouseUp -= new System.Windows.Forms.MouseEventHandler(View_MouseUp);
                View.KeyDown -= new System.Windows.Forms.KeyEventHandler(View_KeyDown);
                View.CustomDrawRowIndicator -= new RowIndicatorCustomDrawEventHandler(View_CustomDrawRowIndicator);
                this.view = null;
            }
        }
        #endregion
        protected class BookmarkData 
        {
            object value;
            public BookmarkData(object value) 
            {
                this.value = value;
            }
            public object Value { get { return value; } }
        }

    }
}
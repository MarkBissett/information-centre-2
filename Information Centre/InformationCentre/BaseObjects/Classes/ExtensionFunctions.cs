using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BaseObjects
{
    public class ExtensionFunctions
    {

        public string ReturnNumberToWords(string strText)
        {
            double valToParse = double.Parse(strText);
            return EnglishFromNumber(valToParse);
        }

        private static string[] onesMapping = new string[] {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", 
                                                            "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", 
                                                            "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
        private static string[] tensMapping = new string[] {"Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", 
                                                            "Eighty", "Ninety"};
        private static string[] groupMapping = new string[] {"Hundred", "Thousand", "Million", "Billion", "Trillion", 
                                                             "Quadrillion", "Quintillion", "Sextillian", "Septillion", 
                                                            "Octillion", "Nonillion", "Decillion", "Undecillion", "Duodecillion", 
                                                             "Tredecillion", "Quattuordecillion", "Quindecillion", "Sexdecillion", 
                                                             "Septendecillion", "Octodecillion", "Novemdecillion", "Vigintillion", 
                                                             "Unvigintillion", "Duovigintillion", "10^72", "10^75", "10^78", "10^81", 
                                                             "10^84", "10^87", "Vigintinonillion", "10^93", "10^96", "Duotrigintillion", 
                                                             "Trestrigintillion"};

        public static string EnglishFromNumber(int number)
        {
            return EnglishFromNumber((long)number);
        }

        private static string EnglishFromNumber(long number)
        {
            return EnglishFromNumber((double)number);
        }

        private static string EnglishFromNumber(double number)
        {
            string sign = null;
            if (number < 0)
            {
                sign = "Negative";
                number = Math.Abs(number);
            }

            int decimalDigits = 0;
            Console.WriteLine(number);
            while (number < 1 || (number - Math.Floor(number) > 1e-10))
            {
                number *= 10;
                decimalDigits++;
            }

            string decimalString = null;
            while (decimalDigits-- > 0)
            {
                int digit = (int)(number % 10); number /= 10;
                decimalString = onesMapping[digit] + " " + decimalString;
            }

            string retVal = null;
            int group = 0;
            if (number < 1)
            {
                retVal = onesMapping[0];
            }
            else
            {
                while (number >= 1)
                {
                    int numberToProcess = (number >= 1e16) ? 0 : (int)(number % 1000);
                    number = number / 1000;

                    string groupDescription = ProcessGroup(numberToProcess);
                    if (groupDescription != null)
                    {
                        if (group > 0)
                        {
                            retVal = groupMapping[group] + " " + retVal;
                        }
                        retVal = groupDescription + " " + retVal;
                    }

                    group++;
                }
            }

            return String.Format("{0}{4}{1}{3}{2}", sign, retVal, decimalString, (decimalString != null) ? " Point " : "", (sign != null) ? " " : "");
        }

        private static string ProcessGroup(int number)
        {
            int tens = number % 100;
            int hundreds = number / 100;

            string retVal = null;
            if (hundreds > 0)
            {
                retVal = onesMapping[hundreds] + " " + groupMapping[0];
            }
            if (tens > 0)
            {
                if (tens < 20)
                {
                    retVal += ((retVal != null) ? " " : "") + onesMapping[tens];
                }
                else
                {
                    int ones = tens % 10;
                    tens = (tens / 10) - 2; // 20's offset

                    retVal += ((retVal != null) ? " " : "") + tensMapping[tens];

                    if (ones > 0)
                    {
                        retVal += ((retVal != null) ? " " : "") + onesMapping[ones];
                    }
                }
            }

            return retVal;
        }

        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        public static string ExtractNumbersFromString(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }

        public string RemoveSpecialCharacters(string str)
        {
            // Returns only A-Z, 0 - 9, "." , "_" , "-", " ",
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static System.Data.SqlDbType ToSqlDbType(Type type)
        {
            System.Data.SqlClient.SqlParameter p1 = new System.Data.SqlClient.SqlParameter();
            System.ComponentModel.TypeConverter tc;
            tc = System.ComponentModel.TypeDescriptor.GetConverter(p1.DbType);

            if (tc.CanConvertFrom(type))
                p1.DbType = (System.Data.DbType)tc.ConvertFrom(type.Name);
            else
            {
                try
                {
                    p1.DbType = (System.Data.DbType)tc.ConvertFrom(type.Name);
                }
                catch (Exception ex)
                {
                }
            }
            switch (p1.SqlDbType)  // Handle nVarchar, nText and nChar - WoodPlan doesn't use these so switch to non-unicode versions... //
            {
                case System.Data.SqlDbType.NChar:
                    p1.SqlDbType = System.Data.SqlDbType.Char;
                    break;
                case System.Data.SqlDbType.NText:
                    p1.SqlDbType = System.Data.SqlDbType.Text;
                    break;
                case System.Data.SqlDbType.NVarChar:
                    p1.SqlDbType = System.Data.SqlDbType.VarChar;
                    break;
                default:
                    break;
            }
            return p1.SqlDbType;
        }

        public static bool IsValidEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Converts input string containing carriage returns into comma separated string and optionally removes duplicate commas //
        /// </summary>
        /// <param name="text"></param>
        /// <param name="removeDuplicateCommas"></param>
        /// <returns></returns>
        public static string FormatStringToCSV(string text, bool removeDuplicateCommas)
        {
            string delimiter = ",";
            if (string.IsNullOrWhiteSpace(delimiter)) delimiter = ",";
            string result = Regex.Replace(text.Replace(System.Environment.NewLine, delimiter).Replace(" ", String.Empty), @"(\r\n?|\n)", delimiter);
            if (removeDuplicateCommas) result = Regex.Replace(result, ",{2,}", ",").Trim(',');
            return result;
        }

        /// <summary>
        /// PageFrame Page Text - Adds passed in record count to the text or removes the record count from the text //
        /// </summary>
        /// <param name="PageName"></param>
        /// <param name="Function"></param>
        /// <param name="RecordCount"></param>
        /// <returns></returns>
        public static string PageFramePageTextRecordCount(string PageText, string Function, int RecordCount)
        {
            if (string.IsNullOrWhiteSpace(PageText) || string.IsNullOrWhiteSpace(Function)) return "";
            if (Function.ToLower() == "add")  // Add Record count to end of page text //
            {
                return PageText + " : " + RecordCount.ToString();
            }
            else  // Remove Record Count from end of page text //
            {
                int indexPosition = PageText.LastIndexOf(" : "); ;
                if (indexPosition == -1) return PageText;
                return PageText.Remove(indexPosition);
            }

        }


    }
}

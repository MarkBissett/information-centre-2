using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraPivotGrid.ViewInfo;
using DevExpress.Utils.Menu;
using BaseObjects;


namespace BaseObjects
{
    public class ExtendedPivotGridMenu
    {
        private PivotGridControl PivotGrid;

        public ExtendedPivotGridMenu(object pivotGrid)
        {
            PivotGrid = (PivotGridControl)pivotGrid;
        }

        public void ShowPivotGridMenu(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == PivotGridMenuType.Header)
            {
                DXMenuItem item = new DXMenuItem("Change Caption", new EventHandler(ChangeCaption));
                //item.Tag = e.HitInfo.Column;
                item.Tag = e.Field;
                e.Menu.Items.Add(item);
                
                HandleHeaderShowMenu(e);  // Toggle Show/Hide Totals (Extra Menu Item) //
            }
        }

        private void ChangeCaption(object sender, EventArgs e)
        {
            PivotGridField pgColumn = (PivotGridField)((DXMenuItem)sender).Tag;

            //Rectangle columnBounds = ((GridViewInfo)PivotGrid.GetViewInfo()).ColumnsInfo[pgColumn].Bounds;
            //Point formLocation = new Point(columnBounds.Left, columnBounds.Bottom);
            //formLocation = gView.GridControl.PointToScreen(formLocation);

            //PivotGridViewInfo viewInfo = (PivotGridViewInfo)PivotGrid.Cells.GetFocusedCellInfo().CellViewInfo.Data.ViewInfo;
            Point formLocation = new Point(PivotGrid.Cells.GetFocusedCellInfo().Bounds.Left, PivotGrid.Cells.GetFocusedCellInfo().Bounds.Bottom);
            formLocation = PivotGrid.PointToScreen(formLocation);
            new frmChangePivotGridCaptionDialog(pgColumn, formLocation).ShowDialog();
        }

        private void HandleHeaderShowMenu(DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            if (!IsLastAreaField(PivotGrid, e.Field))
            {
                CreateToggleTotalsItem(e);
            }
            else
            {
                CreateToggleColumnGrandTotalsItem(e);  // Toggle Show/Hide Column Grand Totals //
                CreateToggleRowGrandTotalsItem(e);  // Toggle Show/Hide Row Grand Totals //
            }
        }

        private bool IsLastAreaField(PivotGridControl pivotGridControl, PivotGridField field)
        {
            for (int i = 0; i < pivotGridControl.Fields.Count; i++)
            {
                if (pivotGridControl.Fields[i].Visible && pivotGridControl.Fields[i].Area == field.Area && pivotGridControl.Fields[i].AreaIndex > field.AreaIndex) return false;
            }
            return true;
        }

        private void CreateToggleTotalsItem(DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            bool isTotalsVisible = e.Field.TotalsVisibility != PivotTotalsVisibility.None;
            DXMenuItem toggleTotalItem = new DXMenuItem(isTotalsVisible ? "Hide Totals" : "Show Totals", ToggleTotalsMenuItemClick);
            toggleTotalItem.BeginGroup = true;
            toggleTotalItem.Tag = e.Field;
            e.Menu.Items.Add(toggleTotalItem);
        }

        private void ToggleTotalsMenuItemClick(object sender, EventArgs e)
        {
            DXMenuItem item = (DXMenuItem)sender;
            PivotGridField field = (PivotGridField)item.Tag;
            if (field.TotalsVisibility == PivotTotalsVisibility.None)
            {
                field.TotalsVisibility = PivotTotalsVisibility.AutomaticTotals;
            }
            else
            {
                field.TotalsVisibility = PivotTotalsVisibility.None;
            }
        }

        private void CreateToggleColumnGrandTotalsItem(DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            bool isTotalsVisible = PivotGrid.OptionsView.ShowColumnGrandTotals;
            DXMenuItem toggleTotalItem = new DXMenuItem(isTotalsVisible ? "Hide Column Grand Totals" : "Show Column Grand Totals", ToggleColumnGrandTotalsMenuItemClick);
            toggleTotalItem.BeginGroup = true;
            e.Menu.Items.Add(toggleTotalItem);
        }

        private void ToggleColumnGrandTotalsMenuItemClick(object sender, EventArgs e)
        {
            if (PivotGrid.OptionsView.ShowColumnGrandTotals)
            {
                PivotGrid.OptionsView.ShowColumnGrandTotals = false;
            }
            else
            {
                PivotGrid.OptionsView.ShowColumnGrandTotals = true;
            }
        }

        private void CreateToggleRowGrandTotalsItem(DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            bool isTotalsVisible = PivotGrid.OptionsView.ShowRowGrandTotals;
            DXMenuItem toggleTotalItem = new DXMenuItem(isTotalsVisible ? "Hide Row Grand Totals" : "Show Row Grand Totals", ToggleRowGrandTotalsMenuItemClick);
            //toggleTotalItem.BeginGroup = true;
            e.Menu.Items.Add(toggleTotalItem);
        }

        private void ToggleRowGrandTotalsMenuItemClick(object sender, EventArgs e)
        {
            if (PivotGrid.OptionsView.ShowRowGrandTotals)
            {
                PivotGrid.OptionsView.ShowRowGrandTotals = false;
            }
            else
            {
                PivotGrid.OptionsView.ShowRowGrandTotals = true;
            }
        }


  
    }
}


using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public struct stcFormAndString
    {
        /// <summary>
        /// 
        /// </summary>
        public string strFormName;
        /// <summary>
        /// 
        /// </summary>
        public string strComment;
        /// <summary>
        /// 
        /// </summary>
        public int intCommentBankDetailID;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct stcFormComponentTag
    {
        /// <summary>
        /// 
        /// </summary>
        public int intUserComponentConfigID;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blToBeSaveWithLayout;

        /// <summary>
        /// 
        /// </summary>
        public int intGenericID;
        /// <summary>
        /// 
        /// </summary>
        public int intGenericInteger;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blGenericBoolean;
        /// <summary>
        /// 
        /// </summary>
        public string strGenericString;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct stcFormLayout
    {
        /// <summary>
        /// 
        /// </summary>
        public int intUserFormConfigID;
        /// <summary>
        /// 
        /// </summary>
        public int intFormID;
        /// <summary>
        /// 
        /// </summary>
        public int intUserID;
        /// <summary>
        /// 
        /// </summary>
        public string strViewDescription;
        /// <summary>
        /// 
        /// </summary>
        public string strRemarks;
        /// <summary>
        /// 
        /// </summary>
        public int intX;
        /// <summary>
        /// 
        /// </summary>
        public int intY;
        /// <summary>
        /// 
        /// </summary>
        public int intWidth;
        /// <summary>
        /// 
        /// </summary>
        public int intHeight;
        /// <summary>
        /// 
        /// </summary>
        public int intState;
        /// <summary>
        /// 
        /// </summary>
        public Boolean bitDefaultView;
        /// <summary>
        /// 
        /// </summary>
        public Boolean bitShared;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct stcComponents
    {
        /// <summary>
        /// 
        /// </summary>
        public int intUserComponentConfigID;
        /// <summary>
        /// 
        /// </summary>
        public int intUserFormConfigID;
        /// <summary>
        /// 
        /// </summary>
        public string strScreenComponentName;
        /// <summary>
        /// 
        /// </summary>
        public int intX;
        /// <summary>
        /// 
        /// </summary>
        public int intY;
        /// <summary>
        /// 
        /// </summary>
        public int intWidth;
        /// <summary>
        /// 
        /// </summary>
        public int intHeight;
        /// <summary>
        /// 
        /// </summary>
        public string strXML;
        /// <summary>
        /// 
        /// </summary>
        public int intPosition;
        /// <summary>
        /// 
        /// </summary>
        public Boolean bitUseScreenComponentName;
    }



    /// <summary>
    /// 
    /// </summary>
    public struct stcActiveForm
    {
        /// <summary>
        /// 
        /// </summary>
        public int intActionID;
        /// <summary>
        /// 
        /// </summary>
        public string strActionText;
        /// <summary>
        /// 
        /// </summary>
        public int intRecordID;

    }

    /// <summary>
    /// 
    /// </summary>
    public struct stcToDoList
    {
        /// <summary>
        /// 
        /// </summary>
        public int intToDoListID;
        /// <summary>
        /// 
        /// </summary>
        public int intModuleID;
        /// <summary>
        /// 
        /// </summary>
        public int intFormID;
        /// <summary>
        /// 
        /// </summary>
        public int intLinkedRecordTypeID;
        /// <summary>
        /// 
        /// </summary>
        public int intLinkedRecordID;
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckingFunctions
    {
        /// <summary>
        /// 
        /// </summary>
        public CheckingFunctions()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chrText"></param>
        /// <returns></returns>
        public static bool IsNumeric(char chrText)
        {
            if (chrText < 0x30 || chrText > 0x39)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strString"></param>
        /// <returns></returns>
        public static bool IsNumeric(string strString)
        {
            Boolean blNumeric = false;
            foreach (char chrText in strString.ToCharArray())
            {
                if (chrText < 0x30 || chrText > 0x39)
                {
                    blNumeric = false;
                    break;
                }
                else
                {
                    blNumeric = true;
                }
            }

            return blNumeric;
        }

        /// <summary>
        /// Checks for Number and Type including currency, decimal, and integer - Mark Bissett (24/05/2007 // 
        /// </summary>
        /// <param name="val"></param>
        /// <param name="NumberStyle"></param>
        /// <returns></returns>
        public static bool IsNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inValue"></param>
        /// <returns></returns>
        public bool IsDate(object inValue)
        {
            bool bValid;
            try
            {
                DateTime myDT = DateTime.Parse(inValue.ToString());
                bValid = true;
            }
            catch (FormatException e)
            {
                bValid = false;
                Console.WriteLine(e.Message);
            }
            
            return bValid;
        }

        public bool IsDateTime(string strValueChecked)
        {
            DateTime idt;
            return DateTime.TryParse(strValueChecked, out idt);
        }




        /// <summary>
        /// Basic grammar check for strings.  This version ensures that the first character
        /// in a new sentence is a capital...
        /// </summary>
        /// <param name="strComment"></param>
        /// <returns></returns>
        public static string GrammarCheck(string strComment)
        {
            // Find end of sentences to begin with and uppercase the first letter that follows on...
            char[] chrCharacter;
            char[] chrCharacter2;
            string strTemp = "";
            string strFirstLetter = "";
            Boolean blFoundSentenceEnd = false;
            Boolean blSpaceFound = false;
            Boolean blPunctuationFound = false;
            int intCharacterCount = 0;

            strFirstLetter = strComment.Substring(0, 1);
            strFirstLetter = strFirstLetter.ToUpper();
            strComment = strFirstLetter + strComment.Substring(1);

            for (intCharacterCount = 0; intCharacterCount <= strComment.Length - 1; intCharacterCount++)
            {
                if (strComment.Substring(intCharacterCount, 1).Contains(".") || strComment.Substring(intCharacterCount, 1).Contains("!") ||
                    strComment.Substring(intCharacterCount, 1).Contains("?"))
                {
                    blFoundSentenceEnd = true;
                }
                else if (strComment.Substring(intCharacterCount, 1).Contains(",") || strComment.Substring(intCharacterCount, 1).Contains(";") ||
                         strComment.Substring(intCharacterCount, 1).Contains("&") || strComment.Substring(intCharacterCount, 1).Contains("\"") ||
                         strComment.Substring(intCharacterCount, 1).Contains("'"))
                {
                    blPunctuationFound = true;
                }

                if (blPunctuationFound == true)
                {
                    chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();

                    if (chrCharacter[0] == ' ')
                    {
                        blSpaceFound = true;
                        strTemp += strComment.Substring(intCharacterCount, 1);
                    }
                    else
                    {
                        if (char.IsLetter(chrCharacter[0]))
                        {
                            if (blSpaceFound == false)
                            {
                                //strTemp += " ";
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blFoundSentenceEnd = false;
                                blPunctuationFound = false;
                                blSpaceFound = false;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blFoundSentenceEnd = false;
                                blPunctuationFound = false;
                                blSpaceFound = false;
                            }
                        }
                        else
                        {
                            if (chrCharacter[0] == '�')
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blSpaceFound = true;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                            }
                        }
                    }

                }
                else if (blFoundSentenceEnd == true)
                {
                    chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();
                    if (intCharacterCount + 1 >= strComment.Length)
                    {
                        chrCharacter2 = " ".ToCharArray();
                    }
                    else
                    {
                        chrCharacter2 = strComment.Substring(intCharacterCount + 1, 1).ToCharArray();
                    }

                    if (chrCharacter[0] == ' ' && chrCharacter2[0] == ' ')
                    {
                        blSpaceFound = true;
                        strTemp += strComment.Substring(intCharacterCount, 1);
                    }
                    else
                    {
                        if (char.IsLetter(chrCharacter[0]))
                        {
                            if (blSpaceFound == false)
                            {
                                //if (chrCharacter[0] == ' ' && chrCharacter2[0] != ' ')
                                //{
                                //    strTemp += " ";
                                //}
                                //else
                                //{
                                //    strTemp += "  ";
                                //}
                                strTemp += strComment.Substring(intCharacterCount, 1).ToUpper();
                                blFoundSentenceEnd = false;
                                blSpaceFound = false;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1).ToUpper();
                                blFoundSentenceEnd = false;
                                blSpaceFound = false;
                            }
                        }
                        else
                        {
                            if (chrCharacter[0] == '�')
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blSpaceFound = true;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                            }
                        }
                    }
                }
                else
                {
                    strTemp += strComment.Substring(intCharacterCount, 1);
                }
            }

            return strTemp;
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strComment"></param>
        /// <returns></returns>
        public static string GrammarCheck2(string strComment)
        {
            if (strComment.Length == 0) return "";
            // Find end of sentences to begin with and uppercase the first letter that follows on...
            Boolean blNewLineAdded = false;
//            Boolean blFullStopAdded = false;
            char[] chrCharacter;
            char[] chrCharacter2;
            char[] chrNewLine;
            string strTemp = "";
            string strFirstLetter = "";
            Boolean blFoundSentenceEnd = false;
            Boolean blSpaceFound = false;
            Boolean blPunctuationFound = false;
            int intCharacterCount = 0;

            // Make first character in text a capital letter...
            chrNewLine = Environment.NewLine.ToCharArray();
            strFirstLetter = strComment.Substring(0, 1);
            strFirstLetter = strFirstLetter.ToUpper();
            strComment = strFirstLetter + strComment.Substring(1);

            // Ensure that last character in text is a full stop...
            if (strComment.Substring(strComment.Length - 1) != ".")
            {
                strComment = strComment.Insert(strComment.Length, ".");
            }

            for (intCharacterCount = 0; intCharacterCount <= strComment.Length - 1; intCharacterCount++)
            {
                // This is used to remove any leading spaces on comment...
                chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();

                if (chrCharacter[0] == ' ')
                {
                    strComment = strComment.Remove(intCharacterCount, 1);
                    intCharacterCount = intCharacterCount - 1;
                }
                else
                {
                    break;
                }
            }

            for (intCharacterCount = 0; intCharacterCount <= strComment.Length - 1; intCharacterCount++)
            {
                blNewLineAdded = false;
                // This is used to check for 2xCR after a paragraph and if the preceeding character ends
                // a sentance!!!!!!!
                if (intCharacterCount + 2 >= strComment.Length)
                {
                    break;
                }

                chrCharacter = strComment.Substring(intCharacterCount, 2).ToCharArray();
                
                if (chrCharacter[0] == chrNewLine[0] && chrCharacter[1] == chrNewLine[1])
                {
                    chrCharacter = strComment.Substring(intCharacterCount + 2, 2).ToCharArray();

                    if ((chrCharacter[0] != chrNewLine[0] && chrCharacter[1] != chrNewLine[1]) && strComment.Substring(intCharacterCount - 2, 2) != Environment.NewLine)
                    {
                        strComment = strComment.Insert(intCharacterCount + 2, Environment.NewLine);

                        intCharacterCount = intCharacterCount + 3;
                        blNewLineAdded = true;
                    }

                    if (blNewLineAdded)
                    {
                        if (!strComment.Substring(intCharacterCount - 5, 1).Contains(".") && !strComment.Substring(intCharacterCount - 5, 1).Contains("!") &&
                            !strComment.Substring(intCharacterCount - 5, 1).Contains("?") && !strComment.Substring(intCharacterCount - 5, 1).Contains(":") &&
                            !strComment.Substring(intCharacterCount - 5, 1).Contains(" ") && !strComment.Substring(intCharacterCount - 1, 1).Contains("\r") &&
                            !strComment.Substring(intCharacterCount - 1, 1).Contains("\n"))
                        {
                            strComment = strComment.Insert(intCharacterCount - 5, ".");
                            intCharacterCount = intCharacterCount + 6;
                        }
                    }
                    else
                    {
                        if (!strComment.Substring(intCharacterCount - 1, 1).Contains(".") && !strComment.Substring(intCharacterCount - 1, 1).Contains("!") &&
                            !strComment.Substring(intCharacterCount - 1, 1).Contains("?") && !strComment.Substring(intCharacterCount - 1, 1).Contains(":") &&
                            !strComment.Substring(intCharacterCount - 1, 1).Contains(" ") && !strComment.Substring(intCharacterCount - 1, 1).Contains("\r") &&
                            !strComment.Substring(intCharacterCount - 1, 1).Contains("\n"))
                        {
                            strComment = strComment.Insert(intCharacterCount - 1, ".");
                            intCharacterCount = intCharacterCount + 3;
                        }
                    }
                }

                // This is used to check if the 's is being used properly!
                if (strComment.Substring(intCharacterCount, 2).Contains("'s"))
                {
                    if (strComment.Substring(intCharacterCount - 1, 1).Contains("s"))
                    {
                        strComment = strComment.Remove(intCharacterCount + 1, 1);
                    }
                }
            }

            for (intCharacterCount = 0; intCharacterCount <= strComment.Length - 1; intCharacterCount++)
            {
                if (intCharacterCount + 4 >= strComment.Length)
                {
                    break;
                }

                if (strComment.Substring(intCharacterCount, 4) == Environment.NewLine + Environment.NewLine)
                {
                    intCharacterCount = intCharacterCount + 4;

                    // This is used to remove any leading spaces before a new paragraph...
                    chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();

                    if (chrCharacter[0] == ' ')
                    {
                        strComment = strComment.Remove(intCharacterCount, 1);
                        intCharacterCount = 0;
                    }
                    else
                    {
                        //break;
                    }
                }
            }

            for (intCharacterCount = 0; intCharacterCount <= strComment.Length - 1; intCharacterCount++)
            {
                if (strComment.Substring(intCharacterCount, 1).Contains(".") || strComment.Substring(intCharacterCount, 1).Contains("!") ||
                    strComment.Substring(intCharacterCount, 1).Contains("?") || strComment.Substring(intCharacterCount, 1).Contains(":"))
                {
                    blFoundSentenceEnd = true;
                }
                else if (strComment.Substring(intCharacterCount, 1).Contains(",") || strComment.Substring(intCharacterCount, 1).Contains(";") ||
                         strComment.Substring(intCharacterCount, 1).Contains("&") || strComment.Substring(intCharacterCount, 1).Contains("\""))
                {
                    blPunctuationFound = true;
                }

                if (blPunctuationFound == true)
                {
                    chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();

                    if (chrCharacter[0] == ' ')
                    {
                        blSpaceFound = true;
                        strTemp += strComment.Substring(intCharacterCount, 1);
                    }
                    else
                    {
                        if (char.IsLetter(chrCharacter[0]))
                        {
                            if (blSpaceFound == false)
                            {
                                strTemp += " ";
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blFoundSentenceEnd = false;
                                blPunctuationFound = false;
                                blSpaceFound = false;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blFoundSentenceEnd = false;
                                blPunctuationFound = false;
                                blSpaceFound = false;
                            }
                        }
                        else
                        {
                            if (chrCharacter[0] == '�')
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blSpaceFound = true;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                            }
                        }
                    }
                }
                else if (blFoundSentenceEnd == true)
                {
                    chrCharacter = strComment.Substring(intCharacterCount, 1).ToCharArray();
                    if (intCharacterCount + 1 >= strComment.Length)
                    {
                        chrCharacter2 = " ".ToCharArray();
                    }
                    else
                    {
                        chrCharacter2 = strComment.Substring(intCharacterCount + 1, 1).ToCharArray();
                    }

                    if (chrCharacter[0] == chrNewLine[0] || chrCharacter[0] == chrNewLine[1])
                    {
                        strTemp += strComment.Substring(intCharacterCount, 1);
                        blSpaceFound = true;
                    }
                    else if (chrCharacter[0] == ' ' && chrCharacter2[0] == ' ')
                    {
                        blSpaceFound = true;
                        strTemp += strComment.Substring(intCharacterCount, 1);
                    }
                    else
                    {
                        if (char.IsLetter(chrCharacter[0]))
                        {
                            if (blSpaceFound == false)
                            {
                                if (chrCharacter[0] == ' ' && chrCharacter2[0] != ' ')
                                {
                                    strTemp += " ";
                                }
                                else
                                {
                                    strTemp += "  ";
                                }
                                strTemp += strComment.Substring(intCharacterCount, 1).ToUpper();
                                blFoundSentenceEnd = false;
                                blSpaceFound = false;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1).ToUpper();
                                blFoundSentenceEnd = false;
                                blSpaceFound = false;
                            }
                        }
                        else
                        {
                            if (chrCharacter[0] == '�')
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                                blSpaceFound = true;
                            }
                            else
                            {
                                strTemp += strComment.Substring(intCharacterCount, 1);
                            }
                        }
                    }
                }
                else
                {
                    strTemp += strComment.Substring(intCharacterCount, 1);
                }
            }

            return strTemp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringInput"></param>
        /// <returns></returns>
        public static string ProperCase(string stringInput)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool fEmptyBefore = true;
            foreach (char ch in stringInput)
            {
                char chThis = ch;
                if (Char.IsWhiteSpace(chThis))
                {
                    fEmptyBefore = true;
                }
                else
                {
                    if (Char.IsLetter(chThis) && fEmptyBefore)
                    {
                        chThis = Char.ToUpper(chThis);
                    }
                    else
                    {
                        chThis = Char.ToLower(chThis);
                    }
                    fEmptyBefore = false;
                }
                sb.Append(chThis);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public string StrReverse(string expression)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 1; i <= expression.Length; i++)
            {
                result.Append(expression[expression.Length - i]);
            }

            return result.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AppMessenger
    {
        //Pick any number at random. This number will be used to uniquely identify the message
        private static int _messageID = -1163005939;
        
        /// <summary>
        /// This message will used to send the commandline to the previous instance of the app
        /// </summary>
        public const int WM_RANDOM = 0x9f;
        //API call to send WM_COPYDATA to the previous instance of the app
        [DllImport("user32", EntryPoint = "SendMessageA")]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, int wParam, COPYDATASTRUCT lParam);

        [DllImport("user32", EntryPoint = "PostMessageA")]
        private static extern int PostMessage(IntPtr hWnd, int wMsg, int wParam, COPYDATASTRUCT lParam);
        
        /// <summary>
        /// For blasting a message across the system.  This will ensure that our top window will pick up... 
        /// </summary>
        public const int HWND_BROADCAST = 0xffff;
        private const int WM_ACTIVATE = 6; 
        private const int WA_INACTIVE = 0;
        private const int WA_ACTIVE = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InactivehWnd"></param>
        /// <param name="ActiveHwnd"></param>
        public void SendMessage(IntPtr InactivehWnd, IntPtr ActiveHwnd)
        {
            // Replace these with PostMessages instead if the application starts hanging...
            SendMessage(InactivehWnd, WM_ACTIVATE, WA_INACTIVE, new COPYDATASTRUCT(ActiveHwnd));
            SendMessage(ActiveHwnd, WM_ACTIVATE, WM_ACTIVATE, new COPYDATASTRUCT(ActiveHwnd));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intFormID"></param>
        /// <param name="intPanelID"></param>
        /// <param name="blLoseFocus"></param>
        public void SendMessage(int intFormID, int intPanelID, Boolean blLoseFocus)
        {
            PostMessage((System.IntPtr)HWND_BROADCAST, WM_RANDOM, _messageID, new COPYDATASTRUCT(intFormID, intPanelID, blLoseFocus));
        }

        /// <summary>
        /// Processes WM_COPYDATA message sent to the main window of this app
        /// </summary>
        /// <param name="m">Message sent to the main window of this app</param>
        /// <returns>Message received or null if messageID is not valid</returns>
        public static COPYDATASTRUCT ProcessWM_RANDOM(System.Windows.Forms.Message m)
        {
            if (m.WParam.ToInt32() == _messageID)
            {
                COPYDATASTRUCT st = (COPYDATASTRUCT)Marshal.PtrToStructure(m.LParam, typeof(COPYDATASTRUCT));
                return st;
            }
            return null;
        }
    }

    /// <summary>
    /// Structure required to be sent with the WM_COPYDATA message
    /// This structure is used to contain the CommandLine
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class COPYDATASTRUCT
    {
        /// <summary>
        /// 
        /// </summary>
        public int dwData = 0;
        /// <summary>
        /// 
        /// </summary>
        public Boolean cbData = false;
        /// <summary>
        /// 
        /// </summary>
        public IntPtr lpData;
        /// <summary>
        /// 
        /// </summary>
        public int intPanelID = 0;
        /// <summary>
        /// 
        /// </summary>
        public int intFormID = 0;

        /// <summary>
        /// 
        /// </summary>
        public COPYDATASTRUCT()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intFormID"></param>
        /// <param name="intPanelID"></param>
        /// <param name="blData"></param>
        public COPYDATASTRUCT(int intFormID, int intPanelID, Boolean blData)
        {
            this.intFormID = intFormID;
            this.intPanelID = intPanelID;
            cbData = blData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        public COPYDATASTRUCT(IntPtr hWnd)
        {
            lpData = hWnd;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace BaseObjects
{
    public class Mapping_Conversions
    {
        private static double deg2rad = Math.PI / 180;
        private static double rad2deg = 180.0 / Math.PI;

        public void NEtoLL(double east, double north, ref double latitude, ref double longitude)
        {
            // converts NGR easting and nothing to lat, lon.  Reverse formula present in NMEA class under GPS Project //
            // input metres, output radians
            double nX = north;
            double eX = east;
            double a = 6377563.396;       // OSGB semi-major
            double b = 6356256.91;        // OSGB semi-minor
            double e0 = 400000;           // OSGB easting of false origin
            double n0 = -100000;          // OSGB northing of false origin
            double f0 = 0.9996012717;     // OSGB scale factor on central meridian
            double e2 = 0.0066705397616;  // OSGB eccentricity squared
            double lam0 = -0.034906585039886591;  // OSGB false east
            double phi0 = 0.85521133347722145;    // OSGB false north
            double af0 = a * f0;
            double bf0 = b * f0;
            double n = (af0 - bf0) / (af0 + bf0);
            double Et = east - e0;
            double phid = InitialLat(north, n0, af0, phi0, n, bf0);
            double nu = af0 / (Math.Sqrt(1 - (e2 * (Math.Sin(phid) * Math.Sin(phid)))));
            double rho = (nu * (1 - e2)) / (1 - (e2 * (Math.Sin(phid)) * (Math.Sin(phid))));
            double eta2 = (nu / rho) - 1;
            double tlat2 = Math.Tan(phid) * Math.Tan(phid);
            double tlat4 = Math.Pow(Math.Tan(phid), 4);
            double tlat6 = Math.Pow(Math.Tan(phid), 6);
            double clatm1 = Math.Pow(Math.Cos(phid), -1);
            double VII = Math.Tan(phid) / (2 * rho * nu);
            double VIII = (Math.Tan(phid) / (24 * rho * (nu * nu * nu))) * (5 + (3 * tlat2) + eta2 - (9 * eta2 * tlat2));
            double IX = ((Math.Tan(phid)) / (720 * rho * Math.Pow(nu, 5))) * (61 + (90 * tlat2) + (45 * Math.Pow(Math.Tan(phid), 4)));
            double phip = (phid - ((Et * Et) * VII) + (Math.Pow(Et, 4) * VIII) - (Math.Pow(Et, 6) * IX));
            double X = Math.Pow(Math.Cos(phid), -1) / nu;
            double XI = (clatm1 / (6 * (nu * nu * nu))) * ((nu / rho) + (2 * (tlat2)));
            double XII = (clatm1 / (120 * Math.Pow(nu, 5))) * (5 + (28 * tlat2) + (24 * tlat4));
            double XIIA = clatm1 / (5040 * Math.Pow(nu, 7)) * (61 + (662 * tlat2) + (1320 * tlat4) + (720 * tlat6));
            double lambdap = (lam0 + (Et * X) - ((Et * Et * Et) * XI) + (Math.Pow(Et, 5) * XII) - (Math.Pow(Et, 7) * XIIA));
            //double geo = { latitude: phip, longitude: lambdap };
            //return(geo);
            latitude = phip * rad2deg;
            longitude = lambdap * rad2deg;
            return;
        }

        private double Marc(double bf0, double n, double phi0, double phi)
        {
            return bf0 * (((1 + n + ((5 / 4) * (n * n)) + ((5 / 4) * (n * n * n))) * (phi - phi0))
               - (((3 * n) + (3 * (n * n)) + ((21 / 8) * (n * n * n))) * (Math.Sin(phi - phi0)) * (Math.Cos(phi + phi0)))
               + ((((15 / 8) * (n * n)) + ((15 / 8) * (n * n * n))) * (Math.Sin(2 * (phi - phi0))) * (Math.Cos(2 * (phi + phi0))))
               - (((35 / 24) * (n * n * n)) * (Math.Sin(3 * (phi - phi0))) * (Math.Cos(3 * (phi + phi0)))));
        }


        private double InitialLat(double north, double n0, double af0, double phi0, double n, double bf0)
        {
            double phi1 = ((north - n0) / af0) + phi0;
            double M = Marc(bf0, n, phi0, phi1);
            double phi2 = ((north - n0 - M) / af0) + phi1;
            double ind = 0;
            while ((Math.Abs(north - n0 - M) > 0.00001) && (ind < 20))  // max 20 iterations in case of error
            {
                ind = ind + 1;
                phi2 = ((north - n0 - M) / af0) + phi1;
                M = Marc(bf0, n, phi0, phi2);
                phi1 = phi2;
            }
            return (phi2);
        }

        // IMPORTANT NOTE: The above conversion gets the lat long into OSGB36. To be of use in Google and Bing Maps, the lat long need to be converted to WGS84 lat long //
        // This extra conversion is not yet handled! //


        //var geo = transform(phip, lambdap, OSGB_AXIS, OSGB_ECCENTRIC, height, WGS84_AXIS, WGS84_ECCENTRIC, 446.448, -125.157, 542.06, 0.1502, 0.247, 0.8421, -20.4894);






        /////////////////


        ////////////////////


    }
}

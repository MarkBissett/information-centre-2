using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Reflection;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    public class GridCheckMarksSelection
    {
        protected GridView view;
        protected ArrayList selection;
        private GridColumn column;
        public RepositoryItemCheckEdit edit;  // Used for drawing check boxes on Group Headers and Individual items //
        public RepositoryItemCheckEdit editColumnHeader;  // Used for drawing check box on column header - Seperate so Alignment and Caption etc can be changed without affecting all the other check boxes. //
        
        public GridCheckMarksSelection() : base()
        {
            selection = new ArrayList();
        }

        public GridCheckMarksSelection(GridView view) : this()
        {
            View = view;
        }

        struct structColumns
        {
            public string strColumnID;
            public int intIndex;
        }

        protected virtual void Attach(GridView view)
        {
            int intColumnCount = 0;
            string strColumnName = "";
            ArrayList alColumns = new ArrayList();

            if (view == null) return;
            selection.Clear();
            this.view = view;
            edit = view.GridControl.RepositoryItems.Add("CheckEdit") as RepositoryItemCheckEdit;
            editColumnHeader = view.GridControl.RepositoryItems.Add("CheckEdit") as RepositoryItemCheckEdit;
            edit.EditValueChanged += new EventHandler(edit_EditValueChanged);
            editColumnHeader.EditValueChanged += new EventHandler(edit_EditValueChanged);
            for (int i = 0; i < view.Columns.Count; i++)
            {
                if (view.Columns[i].VisibleIndex > -1)
                {
                    if (view.Columns[i].VisibleIndex == 0)
                    {
                        strColumnName = view.Columns[i].FieldName;
                    }
                    intColumnCount++;
                }
            }

            foreach (GridColumn clmn in view.Columns)
            {
                if (clmn.VisibleIndex > -1)
                {
                    structColumns stcColumns = new structColumns();
                    stcColumns.strColumnID = clmn.FieldName;
                    stcColumns.intIndex = clmn.VisibleIndex;

                    alColumns.Add(stcColumns);
                }
            }
            
            column = view.Columns.Add();
            column.Width = 35;
            column.OptionsColumn.AllowSize = false;
            column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            column.VisibleIndex = 0;
            column.FieldName = "CheckMarkSelection";
            column.Caption = "Mark";
            column.OptionsColumn.ShowCaption = false;
            column.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            column.ColumnEdit = edit;

            foreach (structColumns stcclmn in alColumns)
            {
                view.Columns[stcclmn.strColumnID].VisibleIndex = stcclmn.intIndex + 1;
            }

            view.Click += new EventHandler(View_Click);
            view.CustomDrawColumnHeader += new ColumnHeaderCustomDrawEventHandler(View_CustomDrawColumnHeader);
            view.CustomDrawGroupRow += new RowObjectCustomDrawEventHandler(View_CustomDrawGroupRow);
            view.CustomUnboundColumnData += new CustomColumnDataEventHandler(view_CustomUnboundColumnData);
            // view.RowStyle += new RowStyleEventHandler(view_RowStyle);
            //view.OptionsView.EnableAppearanceOddRow = true;
        }

        protected virtual void Detach()
        {
            if (view == null) return;
            if (column != null)
                column.Dispose();
            if (edit != null)
            {
                view.GridControl.RepositoryItems.Remove(edit);
                edit.Dispose();
            }
            if (editColumnHeader != null)
            {
                view.GridControl.RepositoryItems.Remove(editColumnHeader);
                editColumnHeader.Dispose();
            }
            view.Click -= new EventHandler(View_Click);
            view.CustomDrawColumnHeader -= new ColumnHeaderCustomDrawEventHandler(View_CustomDrawColumnHeader);
            view.CustomDrawGroupRow -= new RowObjectCustomDrawEventHandler(View_CustomDrawGroupRow);
            view.CustomUnboundColumnData -= new CustomColumnDataEventHandler(view_CustomUnboundColumnData);
            view.RowStyle -= new RowStyleEventHandler(view_RowStyle);

            view = null;
        }

        protected void DrawCheckBox(Graphics g, Rectangle r, bool Checked, bool Grayed)
        {
            DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
            DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
            DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
            info = edit.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
            painter = edit.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;
            if (Grayed)
                info.EditValue = edit.ValueGrayed;
            else
                info.EditValue = Checked;
            info.Bounds = r;
            info.CalcViewInfo(g);
            args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

       protected void DrawHeaderCheckBox(Graphics g, Rectangle r, bool Checked, bool Grayed)
        {
            DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
            DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
            DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
            info = editColumnHeader.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
            painter = editColumnHeader.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;
            if (Grayed)
                info.EditValue = editColumnHeader.ValueGrayed;
            else
                info.EditValue = Checked;
            info.Bounds = r;
            info.CalcViewInfo(g);
            args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
            painter.Draw(args);
            args.Cache.Dispose();
        }

        public void View_Click(object sender, EventArgs e)
        {
            object objItem;
            int intRow = 0;
            GridHitInfo info;
            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            info = view.CalcHitInfo(pt);
            if (info.InColumn && info.Column == column)
            {
                if (SelectedCount == view.DataRowCount)
                {
                    ClearSelection();
                }
                else
                {
                    SelectAll();
                }
                intRow = -99999999;
            }
            if (info.InRow && view.IsGroupRow(info.RowHandle) && info.HitTest != GridHitTest.RowGroupButton)
            {
                bool selected = IsGroupRowSelected(info.RowHandle);
                SelectGroup(info.RowHandle, !selected);
                intRow = view.FocusedRowHandle;
            }


            // *** Following replaced by section of code at end ***//
            //MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
            //if (method != null)
            //{
                /*
                objItem = view.GridControl.Parent;
                
                // See if object is on a form or a popup container control so we can fire the PostView Click Method //
                while (objItem != null)
                {
                    if (objItem.GetType().BaseType.Name == "frmBase") break;
                    objItem = ((Control)objItem).Parent;
                }
                if (objItem == null) return;
                method = typeof(frmBase).GetMethod("PostViewClick");
                int intRow = view.FocusedRowHandle;
                method.Invoke(objItem, new object[] { view, e, intRow });
                */
                /*
                   if (Convert.ToString(objItem).Contains("PopupContainerControl")) return;  // Added by MB 25/10/2007 to stop potential crash //
                   if (Convert.ToString(objItem).Contains("LayoutControl")) return;  // Added by MB 01/12/2009 to stop potential crash //
                   while (objItem.GetType().BaseType.Name != "frmBase")
                   {
                       objItem = ((Control) objItem).Parent;
                       if (objItem == null)
                       {
                           objItem = view.GridControl.Parent; 
                           while (objItem.GetType().BaseType.Name != "ctrlBase")
                           {
                               objItem = ((Control)objItem).Parent;
                           }
                           method = typeof(ctrlBase).GetMethod("PostViewClick");
                           break;
                       }
                   }
                   int intRow = -1;  // -1 passed as group or viewed header tick box is clicked. Individual group rows handled in edit_EditValueChanged event. //
                   method.Invoke(objItem, new object[] { view, e, intRow });
                   */
            //}
            GridControl grid = view.GridControl;
            PopupContainerControl popup = FindPopupContainerControl(grid);
            if (popup != null)
            {
                PopupContainerEdit edit = popup.OwnerEdit;
                if (edit == null) return;
                frmBase f = edit.FindForm() as frmBase;
                if (f == null) return;
                MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
                method.Invoke(f, new object[] { view, e, intRow });
            }
            else
            {
                Control ctrl = view.GridControl.Parent;
                frmBase f = ctrl.FindForm() as frmBase;
                if (f == null) return;
                MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
                method.Invoke(f, new object[] { view, e, intRow });
            }  
        }

        private void View_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == column)
            {
                e.Info.InnerElements.Clear();
                e.Painter.DrawObject(e.Info);
                bool gray = SelectedCount > 0 && SelectedCount < view.DataRowCount;
                //DrawCheckBox(e.Graphics, e.Bounds, SelectedCount == view.DataRowCount, gray);
                DrawHeaderCheckBox(e.Graphics, e.Bounds, SelectedCount == view.DataRowCount, gray);
                e.Handled = true;
            }
        }

        private void View_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info;
            info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;

            info.GroupText = "         " + info.GroupText.TrimStart();
            e.Info.Paint.FillRectangle(e.Graphics, e.Appearance.GetBackBrush(e.Cache), e.Bounds);
            e.Painter.DrawObject(e.Info);

            Rectangle r = info.ButtonBounds;
            r.Offset(r.Width * 2, -5);  //0);  // Had to change this as it was 0 before but on upgrading to DevEx 2013.1 they appear 5 pixels too low //
            int g = GroupRowSelectionStatus(e.RowHandle);
            DrawCheckBox(e.Graphics, r, g > 0, g < 0);
            e.Handled = true;
        }

        private void view_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (IsRowSelected(e.RowHandle))
            {
                e.Appearance.BackColor = SystemColors.Highlight;
                e.Appearance.ForeColor = SystemColors.HighlightText;
            }
        }

        public GridView View
        {
            get
            {
                return view;
            }
            set
            {
                if (view != value)
                {
                    Detach();
                    Attach(value);
                }
            }
        }

        public GridColumn CheckMarkColumn
        {
            get
            {
                return column;
            }
        }

        public int SelectedCount
        {
            get
            {
                return selection.Count;
            }
        }

        public object GetSelectedRow(int index)
        {
            return selection[index];
        }

        public int GetSelectedIndex(object row)
        {
            return selection.IndexOf(row);
        }

        public void ClearSelection()
        {
            selection.Clear();
            Invalidate();
        }

        private void Invalidate()
        {
            view.BeginUpdate();
            view.EndUpdate();
        }

        public void SelectAll()
        {
            selection.Clear();
            if (view.DataSource is ICollection)
                selection.AddRange(((ICollection)view.DataSource));  // fast
            else
                for (int i = 0; i < view.DataRowCount; i++)  // slow
                    selection.Add(view.GetRow(i));
            Invalidate();
        }

        public void SelectGroup(int rowHandle, bool select)
        {
            if (IsGroupRowSelected(rowHandle) && select) return;
            for (int i = 0; i < view.GetChildRowCount(rowHandle); i++)
            {
                int childRowHandle = view.GetChildRowHandle(rowHandle, i);
                if (view.IsGroupRow(childRowHandle))
                    SelectGroup(childRowHandle, select);
                else
                    SelectRow(childRowHandle, select, false);
            }
            Invalidate();
        }

        public void SelectRow(int rowHandle, bool select)
        {
            SelectRow(rowHandle, select, true);
        }

        private void SelectRow(int rowHandle, bool select, bool invalidate)
        {
            if (IsRowSelected(rowHandle) == select) return;
            object row = view.GetRow(rowHandle);
            if (select)
                selection.Add(row);
            else
                selection.Remove(row);
            if (invalidate)
            {
                Invalidate();
            }
        }

        public int GroupRowSelectionStatus(int rowHandle)
        {
            int count = 0;
            for (int i = 0; i < view.GetChildRowCount(rowHandle); i++)
            {
                int row = view.GetChildRowHandle(rowHandle, i);
                if (view.IsGroupRow(row))
                {
                    int g = GroupRowSelectionStatus(row);
                    if (g < 0) return g;
                    if (g > 0) count++;
                }
                else
                {
                    if (IsRowSelected(row)) count++;
                }
            }
            if (count == 0) return 0;
            if (count == view.GetChildRowCount(rowHandle)) return 1;
            return -1;
        }

        public bool IsGroupRowSelected(int rowHandle)
        {
            for (int i = 0; i < view.GetChildRowCount(rowHandle); i++)
            {
                int row = view.GetChildRowHandle(rowHandle, i);
                if (view.IsGroupRow(row))
                {
                    if (!IsGroupRowSelected(row)) return false;
                }
                else
                    if (!IsRowSelected(row)) return false;
            }
            return true;
        }

        public bool IsRowSelected(int rowHandle)
        {
            if (view.IsGroupRow(rowHandle))
                return IsGroupRowSelected(rowHandle);

            object row = view.GetRow(rowHandle);
            return GetSelectedIndex(row) != -1;
        }

        private void view_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column == CheckMarkColumn)
            {
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (e.IsGetData)
                    e.Value = IsRowSelected(rHandle);
                else
                    SelectRow(rHandle, (bool)e.Value);
            }
        }

        private void edit_EditValueChanged(object sender, EventArgs e)
        {
            view.PostEditor();
            int intRow = view.FocusedRowHandle;
            // *** Following replaced by section of code at end ***//
            //object objItem;
            //MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
            //if (method != null)
            //{
                /*
                objItem = view.GridControl.Parent;

                // See if object is on a form or a popup container control so we can fire the PostView Click Method //
                while (objItem != null)
                {
                    if (objItem.GetType().BaseType.Name == "frmBase") break;
                    objItem = ((Control)objItem).Parent;
                }
                if (objItem == null) return;
                method = typeof(frmBase).GetMethod("PostViewClick");
                int intRow = view.FocusedRowHandle;
                method.Invoke(objItem, new object[] { view, e, intRow });
                */

                /*                            
                if (Convert.ToString(objItem).Contains("PopupContainerControl")) return;  // Added by MB 25/10/2007 to stop potential crash //
                if (Convert.ToString(objItem).Contains("LayoutControl")) return;  // Added by MB 01/12/2009 to stop potential crash //
                while (objItem.GetType().BaseType.Name != "frmBase")
                {
                    objItem = ((Control)objItem).Parent;
                    if (objItem == null)
                    {
                        objItem = view.GridControl.Parent;
                        while (objItem.GetType().BaseType.Name != "ctrlBase")
                        {
                            objItem = ((Control)objItem).Parent;
                        }
                        method = typeof(ctrlBase).GetMethod("PostViewClick");
                        break;
                    }
                }
                int intRow = view.FocusedRowHandle;
                method.Invoke(objItem, new object[] { view, e, intRow });
                */
            //}

            GridControl grid = view.GridControl;
            PopupContainerControl popup = FindPopupContainerControl(grid);
            if (popup != null)
            {
                PopupContainerEdit edit = popup.OwnerEdit;
                if (edit == null) return;
                frmBase f = edit.FindForm() as frmBase;
                if (f == null) return;
                MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
                method.Invoke(f, new object[] { view, e, intRow });
            }
            else
            {
                Control ctrl = view.GridControl.Parent;
                frmBase f = ctrl.FindForm() as frmBase;
                if (f == null) return;
                MethodInfo method = typeof(frmBase).GetMethod("PostViewClick");
                method.Invoke(f, new object[] { view, e, intRow });
            }
        }

        private PopupContainerControl FindPopupContainerControl(Control ctrl)
        {
            if (ctrl is PopupContainerControl) return ctrl as PopupContainerControl;
            if (ctrl.Parent == null) return null;
            return FindPopupContainerControl(ctrl.Parent);
        }

    }
}

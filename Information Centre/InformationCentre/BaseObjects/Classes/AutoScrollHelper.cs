// Developer Express Code Central Example:
// How to automatically scroll the grid during drag-and-drop
// 
// This example demonstrates how to scroll grid rows and columns when dragging an
// object near the grid's edge. See also:
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E1475

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace BaseObjects {
    public class AutoScrollHelper {
        public AutoScrollHelper(GridView view) {
            fGrid = view.GridControl;
            fScrollInfo = new ScrollInfo(this, view);
        }

        GridControl fGrid;
        ScrollInfo fScrollInfo;
        public int ScrollThreshold = 20;
        public int HorizontalScrollStep = 10;
        public int ScrollTimerInterval {
            get {
                return fScrollInfo.scrollTimer.Interval;
            }
            set {
                fScrollInfo.scrollTimer.Interval = value;
            }
        }

        public void ScrollIfNeeded(GridHitInfo info) {
            if(info == null) return;
            fScrollInfo.GoLeft = (info.HitPoint.X < ScrollThreshold);
            fScrollInfo.GoUp = (info.HitPoint.Y < ScrollThreshold);
            fScrollInfo.GoRight = (( fGrid.Width - info.HitPoint.X) < ScrollThreshold);
            fScrollInfo.GoDown = ((fGrid.Height - info.HitPoint.Y) < ScrollThreshold);
        }

        internal class ScrollInfo {
            internal Timer scrollTimer;
            GridView view = null;
            bool left, right, up, down;

            AutoScrollHelper owner;
            public ScrollInfo(AutoScrollHelper owner, GridView view) {
                this.owner = owner;
                this.view = view;
                this.scrollTimer = new Timer();
                this.scrollTimer.Tick += new EventHandler(scrollTimer_Tick);
            }
            public bool GoLeft {
                get { return left; }
                set {
                    if(left != value) {
                        left = value;
                        CalcInfo();
                    }
                }
            }
            public bool GoRight {
                get { return right; }
                set {
                    if(right != value) {
                        right = value;
                        CalcInfo();
                    }
                }
            }
            public bool GoUp {
                get { return up; }
                set {
                    if(up != value) {
                        up = value;
                        CalcInfo();
                    }
                }
            }
            public bool GoDown {
                get { return down; }
                set {
                    if(down != value) {
                        down = value;
                        CalcInfo();
                    }
                }
            }
            private void scrollTimer_Tick(object sender, System.EventArgs e) {
                if(GoDown) 
                    view.TopRowIndex++;
                if(GoUp) 
                    view.TopRowIndex--;
                if(GoLeft) 
                    view.LeftCoord -= owner.HorizontalScrollStep;
                if(GoRight) 
                    view.LeftCoord += owner.HorizontalScrollStep;

                if((Control.MouseButtons & MouseButtons.Left) == MouseButtons.None)
                    scrollTimer.Stop();
            }
            void CalcInfo() {
                if(!(GoDown && GoLeft && GoRight && GoUp)) 
                    scrollTimer.Stop();

                if(GoDown || GoLeft || GoRight || GoUp) 
                    scrollTimer.Start();
            }
        }
    }
}

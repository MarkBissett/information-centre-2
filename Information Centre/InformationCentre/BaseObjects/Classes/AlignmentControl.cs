using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.LookAndFeel;
using DevExpress.LookAndFeel.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace BaseObjects
{
    public class AlignmentControl : DevExpress.XtraEditors.XtraUserControl
    {
        private System.ComponentModel.Container components = null;
        ContentAlignment fAlignment = ContentAlignment.MiddleCenter;
        public event EventHandler AlignmentChanged;
        public AlignmentControl()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlConstants.DoubleBuffer, true);
            this.TabStop = true;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        [DefaultValue(ContentAlignment.MiddleCenter)]
        public ContentAlignment Alignment
        {
            get { return fAlignment; }
            set
            {
                fAlignment = value;
                Info.CalcLocation();
            }
        }
        AlignmentControlViewInfo fInfo;
        public AlignmentControlViewInfo Info
        {
            get
            {
                if (fInfo == null) fInfo = new AlignmentControlViewInfo(this);
                return fInfo;
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Info.Draw(new GraphicsCache(e), LookAndFeel);
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Info.CalcBounds();
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Info.CalcHotTrack(e);
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            Info.HotTrackIndex = -1;
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            Info.CalcPressed(e);
        }
        public void RaiseAlignmentChanged(ContentAlignment alignment)
        {
            Alignment = alignment;
            if (AlignmentChanged != null)
                AlignmentChanged(this, EventArgs.Empty);
        }
        public class AlignmentControlViewInfo
        {
            AlignmentControl control;
            ArrayList args = new ArrayList();
            int fHotTrackIndex = -1;
            ContentAlignment[] alignments = new ContentAlignment[] { ContentAlignment.TopLeft, ContentAlignment.TopCenter, ContentAlignment.TopRight, ContentAlignment.MiddleLeft, ContentAlignment.MiddleCenter, ContentAlignment.MiddleRight, ContentAlignment.BottomLeft, ContentAlignment.BottomCenter, ContentAlignment.BottomRight };
            public AlignmentControlViewInfo(AlignmentControl control)
            {
                this.control = control;
                for (int i = 0; i < alignments.Length; i++)
                    args.Add(new StyleObjectInfoArgs());
                CalcBounds();
                CalcLocation();
            }
            StyleObjectInfoArgs ObjectInfoByID(int index)
            {
                return args[index] as StyleObjectInfoArgs;
            }
            public void Draw(GraphicsCache cache, UserLookAndFeel lookAndFeel)
            {
                for (int i = 0; i < args.Count; i++)
                {
                    ObjectInfoByID(i).Cache = cache;
                    lookAndFeel.Painter.Button.DrawObject(ObjectInfoByID(i));
                }
            }
            int dx = 8;
            int ElementWidht { get { return (control.Width - dx * 2) / 3; } }
            int ElementHeight { get { return (control.Height - dx * 2) / 3; } }
            public void CalcBounds()
            {
                for (int i = 0; i < 9; i++)
                    ObjectInfoByID(i).Bounds = new Rectangle(
                        (ElementWidht + dx) * (i % 3),
                        (ElementHeight + dx) * (i / 3),
                        ElementWidht, ElementHeight);
                control.Refresh();
            }
            int LocationIndex
            {
                get
                {
                    if (control == null) return -1;
                    for (int i = 0; i < alignments.Length; i++)
                        if (control.Alignment.Equals(alignments[i])) return i;
                    return -1;
                }
            }
            public void CalcLocation()
            {
                int index = LocationIndex;
                for (int i = 0; i < args.Count; i++)
                    ObjectInfoByID(i).State = index == i ? ObjectState.Pressed : ObjectState.Normal;
                if (HotTrackIndex > -1 && HotTrackIndex != index) ObjectInfoByID(HotTrackIndex).State = ObjectState.Hot;
                if (control == null)
                    for (int i = 0; i < args.Count; i++) ObjectInfoByID(i).State = ObjectState.Disabled;
                control.Refresh();
            }
            public int HotTrackIndex
            {
                get { return fHotTrackIndex; }
                set
                {
                    if (fHotTrackIndex == value) return;
                    fHotTrackIndex = value;
                    CalcLocation();
                }
            }
            int IndexByPoint(Point p)
            {
                for (int i = 0; i < args.Count; i++)
                    if (ObjectInfoByID(i).Bounds.Contains(p)) return i;
                return -1;
            }
            public void CalcHotTrack(MouseEventArgs e)
            {
                Point p = new Point(e.X, e.Y);
                HotTrackIndex = IndexByPoint(p);
            }
            public void CalcPressed(MouseEventArgs e)
            {
                Point p = new Point(e.X, e.Y);
                int i = IndexByPoint(p);
                if (i > -1)
                {
                    control.RaiseAlignmentChanged(alignments[i]);
                    CalcLocation();
                }
            }
        }
    }
}

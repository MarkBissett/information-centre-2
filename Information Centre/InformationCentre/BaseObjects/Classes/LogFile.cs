using System;
using System.Collections.Generic;
using System.Text;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class LogFile
    {
        private string strLogFilePath = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strLogFilePath"></param>
        public LogFile(string strLogFilePath)
        {
            this.strLogFilePath = strLogFilePath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        public void LogMessageToFile(string msg)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(strLogFilePath);
            try
            {
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraDataLayout;
using DevExpress.XtraLayout;


namespace BaseObjects
{
    public class ExtendedDataLayoutControl : DataLayoutControl
    {
        public ExtendedDataLayoutControl()
        {
            OptionsCustomizationForm.ShowPropertyGrid = true;
            OptionsView.DrawAdornerLayer = DevExpress.Utils.DefaultBoolean.True;  // Show Adorner Layer so Items with Locked sizes show a locked badge when in customisation mode //
            OptionsView.AllowLockSizeIndicators = DevExpress.Utils.DefaultBoolean.True;
            OptionsView.AllowItemSkinning = true;
        }

        public override LayoutGroup CreateLayoutGroup(LayoutGroup parent)
        {
            LayoutGroup group = base.CreateLayoutGroup(parent);
            group.ExpandButtonVisible = true;
            group.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            return group;
        }
    }


}



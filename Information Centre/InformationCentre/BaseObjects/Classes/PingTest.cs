﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public class PingTest
    {
        public static bool Ping(string strSite)
        {
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            System.Net.NetworkInformation.PingReply pingStatus = ping.Send(strSite, 2000);
            return pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success;
        }

    }
}

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace BaseObjects
{
    class UserRect_ExtendedGraphics
    {
        #region Unmanaged
        
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, DeviceCapsIndex index);

        private enum DeviceCapsIndex
        {
            PhysicalOffsetX = 112,
            PhysicalOffsetY = 113,
            PhysicalHeight = 111 ,
            PhysicalWidth = 110
        }

        #endregion


        private System.Drawing.Graphics _graphics;

        public UserRect_ExtendedGraphics(System.Drawing.Graphics g)
        {
            _graphics = g;
        }


        public void PaintRoundRect(Pen p, Brush fillBrush, int x, int y, int width, int height, int radius)
        {

            GraphicsPath gp = new GraphicsPath();


            gp.AddLine(x + radius, y, x + width - radius, y);

            gp.AddArc(x + width - radius, y, radius, radius, 270, 90);

            gp.AddLine(x + width, y + radius, x + width, y + height - radius);

            gp.AddArc(x + width - radius, y + height - radius, radius, radius, 0, 90);

            gp.AddLine(x + width - radius, y + height, x + radius, y + height);

            gp.AddArc(x, y + height - radius, radius, radius, 90, 90);

            gp.AddLine(x, y + height - radius, x, y + radius);

            gp.AddArc(x, y, radius, radius, 180, 90);

            gp.CloseFigure();

            if (fillBrush != null)
                _graphics.FillPath(fillBrush, gp);

            if (p != null)
                _graphics.DrawPath(p, gp);

            gp.Dispose();
        }


        public void PaintPoint(Color color, Point xy, int width)
        {
            if ((color != Color.Transparent) && (width > 0))
            {
                SolidBrush brush = new SolidBrush(color);
                _graphics.FillEllipse(brush, xy.X - (width / 2), xy.Y - (width / 2), width, width);
                brush.Dispose();
            }
        }

        public void PaintPoint(Color color, PointF xy, float width)
        {
            if ((color != Color.Transparent) && (width > 0))
            {
                SolidBrush brush = new SolidBrush(color);
                _graphics.FillEllipse(brush, xy.X - (width / 2), xy.Y - (width / 2), width, width);
                brush.Dispose();
            }
        }

        public void PaintEllipse(Pen pen, Brush brush, Rectangle bounds)
        {

            if (brush != null)
            {
                _graphics.FillEllipse(brush, bounds);
            }

            if ((pen != null) && (pen.Color != Color.Transparent))
            {
                _graphics.DrawEllipse(pen, bounds);
            }

        }

        public void PaintPolygon(Pen pen, Brush brush, Point[] points)
        {

            if (brush != null)
            {
                _graphics.FillPolygon(brush, points);
            }

            if ((pen != null) && (pen.Color != Color.Transparent))
            {
                _graphics.DrawPolygon(pen, points);
            }

        }


        public void PaintRectangle(Pen pen, Brush brush, int x, int y, int width, int height)
        {

            if (brush != null)
            {
                _graphics.FillRectangle(brush, x, y, width, height);
            }

            if ((pen != null) && (pen.Color != Color.Transparent))
            {
                _graphics.DrawRectangle(pen, x, y, width, height);
            }

        }
        public static void DrawRectangle(Graphics g, Color c, Rectangle r)
        {
            Pen p = new Pen(c);
            g.DrawRectangle(p, r);
            p.Dispose();
        }

        private double GetAngle(double x1, double y1, double x2, double y2)
        {
            double rInv = 180 / Math.PI;

            if (x2 == x1)
            {
                if (y1 > y2)
                {
                    return -90;
                }
                else if (y1 < y2)
                {
                    return 90;
                }
                else
                {
                    return 0;
                }
            }

            //'atg (Y/X) se X>0 e Y>0  (I quadrante)
            //'atg (Y/X) + pigreco se X<0 e Y>0 (II quadrante)
            //'atg (Y/X) + pigreco se X<0 e Y<0 (III quadrante)
            //'atg (Y/X) +2pigreco se X>0 e Y <0 (III quadrante)

            if (y2 > y1)
            {
                if (x2 > x1)
                {
                    return Math.Atan((y2 - y1) / (x2 - x1)) * rInv;
                }
                else
                {
                    return (Math.Atan((y2 - y1) / (x2 - x1)) + Math.PI) * rInv;
                }
            }
            else
            {
                if (x2 > x1)
                {
                    return (Math.Atan((y2 - y1) / (x2 - x1)) + (2 * Math.PI)) * rInv;
                }
                else
                {
                    return (Math.Atan((y2 - y1) / (x2 - x1)) + Math.PI) * rInv;
                }
            }

        }

        private Point GetPointOnEllipse(double orgX, double orgY, double radiusX, double radiusY, double angle)
        {
            double r = Math.PI / 180;
            int destX = (int)(orgX - (Math.Sin(angle * r) * radiusX));
            int destY = (int)(orgY - (Math.Sin((90 - angle) * r) * radiusY));
            return new Point(destX, destY);
        }

        private Point RotatePoint(double orgX, double orgY, double ptX, double ptY, double angle)
        {
            double r;
            double cr;
            double sr;

            if (angle != 0)
            {

                r = -(Math.PI * angle / 180); //o -(pi_ * Angle / 180#)?

                double dummyX = ptX - orgX;
                double dummyY = ptY - orgY;

                cr = Math.Cos(r);
                sr = Math.Sin(r);

                return new Point((int)((dummyX * cr) - (dummyY * sr) + orgX),
                                 (int)((dummyX * sr) + (dummyY * cr) + orgY));

            }
            else
            {
                return new Point((int)ptX, (int)ptY);
            }
        }


    }

}

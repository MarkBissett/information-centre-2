using System;
using System.Collections.Generic;
using System.Text;

namespace BaseObjects
{
    public class LookUpDataOBJ : IComparable
    {
        private string columnName;
        private string columnCaption;

        public string ColumnName
        {
            get
            {
                return columnName;
            }
            set
            {
                columnName = value;
            }
        }

        public string ColumnCaption
        {
            get
            {
                return columnCaption;
            }
            set
            {
                columnCaption = value;
            }
        }
        public LookUpDataOBJ(string columnName, string columnCaption)
        {
            this.ColumnName = columnName;
            this.ColumnCaption = columnCaption;
        }
        public int CompareTo(object obj)  // Required for sorting of the structure to work //
        {
            int result = 1;
            if (obj != null && obj is LookUpDataOBJ)
            {
                LookUpDataOBJ lookUpDataOBJ = (LookUpDataOBJ)obj;
                result = this.columnCaption.CompareTo(lookUpDataOBJ.ColumnCaption);
            }
            return result;
        }

    }



}

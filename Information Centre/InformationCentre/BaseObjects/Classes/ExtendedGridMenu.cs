using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using BaseObjects.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors.Drawing;
using DevExpress.Skins;
using DevExpress.XtraLayout;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Localization;  // Required to Find items on Grid menus //

using System.IO;
using System.Threading;
using System.Reflection;

namespace BaseObjects
{
    public class ExtendedGridMenu
    {
        private GridView gvView;
        private GridColumn gcColumn;

        public ExtendedGridMenu(object gridView)
        {
            gvView = (GridView)gridView;
        }

        public void PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            // If column menu about to be shown, add Footer option for showing / hiding the grid's Footer bar //
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Column) return;
            GridView view = (GridView)sender;
            DevExpress.XtraGrid.Menu.GridViewColumnMenu columnMenu = e.Menu as GridViewColumnMenu;

            gcColumn = columnMenu.Column;
            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Footer_16;
                DevExpress.Utils.Menu.DXMenuCheckItem menuItem = new DXMenuCheckItem("Footer", gvView.OptionsView.ShowFooter, image1, new EventHandler(FooterMenuItem));
                menuItem.Tag = e.Menu;
                columnMenu.Items.Insert(5, menuItem);
            }
            catch (Exception) { }

            int intMenuPositionMakeColumnVisible = 0;  // Used for dynamic positioning of new items depending on if fired from a column header or not //
            int intMenuPositionSetSubText = 0;  // Used for dynamic positioning of new items depending on if fired from a column header or not //
            int intMenuPositionChangeCaption = 0;  // Used for dynamic positioning of new items depending on if fired from a column header or not //

            // Add Column freezing options to menu //
            GridHitInfo info;
            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            info = view.CalcHitInfo(pt);
            if (info.InColumn)  // If Menu fired from a column header band //
            {
                try
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Fixed_None_16;
                    DXMenuItem menuItem = CreateCheckItem("Not Fixed", columnMenu.Column, FixedStyle.None, image1);
                    menuItem.BeginGroup = true;  // Put menu seperator in before menu item //
                    columnMenu.Items.Insert(6, menuItem);
                }
                catch (Exception) { }

                try
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Fixed_Left_16;
                    columnMenu.Items.Insert(7, CreateCheckItem("Fixed Left", columnMenu.Column, FixedStyle.Left, image1));
                }
                catch (Exception) { }

                try
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Fixed_Right_16;
                    columnMenu.Items.Insert(8, CreateCheckItem("Fixed Right", columnMenu.Column, FixedStyle.Right, image1));
                }
                catch (Exception) { }

            }
            else
            {
                if (info.HitTest == GridHitTest.ColumnPanel)  // In column bar but not in column (on overlap at start or end) //
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Footer_16;
                    DevExpress.Utils.Menu.DXMenuCheckItem menuItem = new DXMenuCheckItem("Footer", gvView.OptionsView.ShowFooter, image1, new EventHandler(FooterMenuItem));
                    menuItem.Tag = e.Menu;
                    columnMenu.Items.Insert(2, menuItem);
                }
            }

            // Get offset to position new item within the existing menu items as opposed to adding to end of menu items //
            DXMenuItem mItem = GetItemByStringId(e.Menu, GridStringId.MenuColumnColumnCustomization);
            //if (menuItem != null) menuItem.Caption = "Column chooser";
            if (mItem != null)
            {
                int intIndex = columnMenu.Items.IndexOf(mItem);
                if (intIndex >= 0)
                {
                    intMenuPositionMakeColumnVisible = intIndex + 1;
                }
            }
            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Make_Visible_16;
                DXMenuItem menuItem = new DXMenuItem("Make Column Visible...", new EventHandler(MakeColumnVisible), image1);
                menuItem.Tag = e.HitInfo.Column;
                //e.Menu.Items.Add(menuItem);
                e.Menu.Items.Insert(intMenuPositionMakeColumnVisible, menuItem);
            }
            catch (Exception) { }


            // Get offsets to position new items within the existing menu items as opposed to adding to end of menu items //
            mItem = GetItemByStringId(e.Menu, GridStringId.MenuColumnBestFitAllColumns);
            if (mItem != null)
            {
                int intIndex = columnMenu.Items.IndexOf(mItem);
                if (intIndex >= 0)
                {
                    intMenuPositionSetSubText = intIndex + 1;
                    intMenuPositionChangeCaption = intIndex + 2;
                }
            }
            else  // Best Fit ALL doesn't exist so get Best Fit item //
            {
                mItem = GetItemByStringId(e.Menu, GridStringId.MenuColumnBestFit);
                if (mItem != null)
                {
                    int intIndex = columnMenu.Items.IndexOf(mItem);
                    if (intIndex >= 0)
                    {
                        intMenuPositionSetSubText = intIndex + 1;
                        intMenuPositionChangeCaption = intIndex + 2;
                    }
                }
            }
            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Sub_Text_16;
                DXMenuItem menuItem = new DXMenuItem("Set Sub-Text Column...", new EventHandler(SetSubTextColumn), image1);
                menuItem.Tag = e.HitInfo.Column;
                //e.Menu.Items.Add(menuItem);
                e.Menu.Items.Insert(intMenuPositionSetSubText, menuItem);
            }
            catch (Exception) { }

            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Change_Caption_16;
                DXMenuItem menuItem = new DXMenuItem("Change Caption...", new EventHandler(ChangeCaption), image1);
                menuItem.Tag = e.HitInfo.Column;
                //e.Menu.Items.Add(menuItem);
                e.Menu.Items.Insert(intMenuPositionChangeCaption, menuItem);
            }
            catch (Exception) { }

            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Styles_Rule_Builder_16;
                DXMenuItem menuItem = new DXMenuItem("Styling Editor...", new EventHandler(StylingEditor));
                menuItem.Image = image1;
                menuItem.Tag = e.HitInfo.Column;
                e.Menu.Items.Add(menuItem);
            }
            catch (Exception) { }

            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Expand_All_16;
                DXMenuItem menuItem = new DXMenuItem("Expand All", new EventHandler(ExpandAllMenuItem), image1);
                menuItem.BeginGroup = true;  // Put menu seperator in before menu item //
                menuItem.Tag = e.Menu;
                columnMenu.Items.Add(menuItem);  // Add item to end of menu //
            }
            catch (Exception) { }

            try
            {
                Image image1 = BaseObjects.Properties.Resources.Menu_Hide_All_16;
                DXMenuItem menuItem1 = new DXMenuItem("Collapse All", new EventHandler(CollapseAllMenuItem), image1);
                menuItem1.Tag = e.Menu;
                columnMenu.Items.Add(menuItem1);  // Add item to end of menu //
            }
            catch (Exception) { }

            if (gvView.OptionsSelection.MultiSelect == true)
            {
                try
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Expand_Select_All_16;
                    DXMenuItem menuItem = new DXMenuItem("Expand and Select All", new EventHandler(ExpandAndSelectAllMenuItem), image1);
                    menuItem.BeginGroup = true;  // Put menu seperator in before menu item //
                    menuItem.Tag = e.Menu;
                    columnMenu.Items.Add(menuItem);  // Add item to end of menu //
                }
                catch (Exception) { }

                try
                {
                    Image image1 = BaseObjects.Properties.Resources.Menu_Select_On_Input_16;
                    DXMenuItem menuItem = new DXMenuItem("Select based on Input Value...", new EventHandler(SelectOnInputValueMenuItem), image1);
                    menuItem.Tag = e.Menu;
                    columnMenu.Items.Add(menuItem);  // Add item to end of menu //
                }
                catch (Exception) { }
            }

            try
            {
                Image image1 = BaseObjects.Properties.Resources.copy_16;
                DXMenuItem menuItem = new DXMenuItem("Copy Column Contents To Clipboard...", new EventHandler(CopyColumnToClipboard), image1);
                menuItem.BeginGroup = true;  // Put menu seperator in before menu item //
                menuItem.Tag = e.HitInfo.Column;
                e.Menu.Items.Add(menuItem);  // Add item to end of menu //
            }
            catch (Exception) { }
        }
        private DXMenuItem GetItemByStringId(DXPopupMenu menu, GridStringId id)
        {
            foreach (DXMenuItem item in menu.Items)
            {
                if (item.Caption == GridLocalizer.Active.GetLocalizedString(id)) return item;
            }
            return null;
        }

        private void FooterMenuItem(Object sender, EventArgs e)
        {
            DXMenuItem Item = sender as DXMenuItem;
            GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
            gvView.OptionsView.ShowFooter = ! gvView.OptionsView.ShowFooter;
        }

        private void StylingEditor(object sender, EventArgs e)
        {
            GridColumn gColumn = (GridColumn)((DXMenuItem)sender).Tag;
            if (gColumn == null) return;
            GridView gView = (GridView)gColumn.View;
            new frmStyleExpressionEditor(gView).ShowDialog();
        }

        private void ChangeCaption(object sender, EventArgs e)
        {
            GridColumn gColumn = (GridColumn)((DXMenuItem)sender).Tag;
            GridView gView = (GridView)gColumn.View;
            //Rectangle columnBounds = ((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds;
            //Point formLocation = new Point(columnBounds.Left, columnBounds.Bottom);
            Point formLocation = (((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn] == null ? new Point(0, 0) : new Point(((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Left, ((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Bottom));
            formLocation = gView.GridControl.PointToScreen(formLocation);
            new frmChangeCaptionDialog(gColumn, formLocation).ShowDialog();
        }

        private void MakeColumnVisible(object sender, EventArgs e)
        {
            GridColumn gColumn = (GridColumn)((DXMenuItem)sender).Tag;
            if (gColumn == null) return;
            GridView gView = (GridView)gColumn.View;
            Point formLocation = (((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn] == null ? new Point(0, 0) : new Point(((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Left, ((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Bottom));
            formLocation = gView.GridControl.PointToScreen(formLocation);
            new frmMakeColumnVisible(gView, formLocation).ShowDialog();
        }

        private void SetSubTextColumn(object sender, EventArgs e)
        {
            GridColumn gColumn = (GridColumn)((DXMenuItem)sender).Tag;
            if (gColumn == null) return;
            GridView gView = (GridView)gColumn.View;
            Point formLocation = (((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn] == null ? new Point(0, 0) : new Point(((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Left, ((GridViewInfo)gView.GetViewInfo()).ColumnsInfo[gColumn].Bounds.Bottom));
            formLocation = gView.GridControl.PointToScreen(formLocation);
            new frmSetSubTextColumn(gView, formLocation).ShowDialog();
        }

        private void CopyColumnToClipboard(object sender, EventArgs e)
        {
            GridColumn gColumn = (GridColumn)((DXMenuItem)sender).Tag;
            if (gColumn == null) return;
            GridView gView = (GridView)gColumn.View;
            int rowcount = gView.DataRowCount;
            if (rowcount <= 0) return;

            StringBuilder sbData = new StringBuilder();

            for (int i = 0; i < rowcount; i++)
            {
                sbData.AppendLine(gView.GetRowCellValue(i, gColumn).ToString());
            }
            Clipboard.SetText(sbData.ToString());
        }

        private void ExpandAllMenuItem(Object sender, EventArgs e)
        {
            DXMenuItem Item = sender as DXMenuItem;
            GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
            gvView.BeginUpdate();
            gvView.ExpandAllGroups();
            gvView.EndUpdate();
        }

        private void CollapseAllMenuItem(Object sender, EventArgs e)
        {
            DXMenuItem Item = sender as DXMenuItem;
            GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
            gvView.BeginUpdate();
            gvView.CollapseAllGroups();
            gvView.EndUpdate();
        }

        private void ExpandAndSelectAllMenuItem(Object sender, EventArgs e)
        {
            DXMenuItem Item = sender as DXMenuItem;
            GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
            if (gvView.OptionsSelection.MultiSelect == false)
            {
                return;
            }
            //gvView.BeginUpdate();
            gvView.ExpandAllGroups();
            gvView.SelectAll();
            //gvView.EndUpdate();
        }

        private void SelectOnInputValueMenuItem(Object sender, EventArgs e)
        {
            DXMenuItem Item = sender as DXMenuItem;
            GridViewColumnMenu menu = Item.Tag as GridViewColumnMenu;
            if (gvView.OptionsSelection.MultiSelect == false)
            {
                return;
            }
            new frm_Select_Records_From_User_Input_Criteria(gvView, gcColumn).ShowDialog();
        }

        public void AddExpandAndEditItem(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
        }

        private void ExpandAndEditAllMenuItem(object sender, EventArgs e)
        {
            DXMenuItem Item = (DXMenuItem)sender;
            GridViewColumnMenu menu = (GridViewColumnMenu)Item.Tag;
            if (gvView.OptionsSelection.MultiSelect == false)
            {
                return;
            }

            gvView.ExpandAllGroups();
            gvView.SelectAll();

            MethodInfo method = gvView.GridControl.FindForm().GetType().GetMethod("OnEditEvent");
            method.Invoke(gvView.GridControl.FindForm(), new object[] { sender, e });
        }

        DXMenuCheckItem CreateCheckItem(string caption, GridColumn column, FixedStyle style, Image image)
        {
            DXMenuCheckItem item = new DXMenuCheckItem(caption, column.Fixed == style, image, new EventHandler(OnFixedClick));
            item.Tag = new MenuInfo(column, style);
            return item;
        }
        private void OnFixedClick(object sender, EventArgs e)
        {
            DXMenuItem item = sender as DXMenuItem;
            MenuInfo info = item.Tag as MenuInfo;
            if (info == null) return;
            info.Column.Fixed = info.Style;
        }
        class MenuInfo
        {
            public MenuInfo(GridColumn column, FixedStyle style)
            {
                this.Column = column;
                this.Style = style;
            }
            public FixedStyle Style;
            public GridColumn Column;
        }

     }
}

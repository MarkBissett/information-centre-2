using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    public class ExtendedLayoutControl : LayoutControl
    {
        public ExtendedLayoutControl()
        {
            OptionsCustomizationForm.ShowPropertyGrid = true;
        }
        
        public override LayoutGroup CreateLayoutGroup(LayoutGroup parent)
        {
            LayoutGroup group = base.CreateLayoutGroup(parent);
            group.ExpandButtonVisible = true;
            group.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            return group;
        }


    }

}



using System;
using System.Data;
using System.Configuration;

namespace BaseObjects
{
    public class MagnifierConfiguration
    {
        public int intLocationX;
        public int intLocationY;
        public bool boolCloseOnMouseUp;
        public bool boolDoubleBuffered;
        public bool boolHideMouseCursor;
        public bool boolRememberLastPoint;
        public bool boolReturnToOrigin;
        public bool boolTopMostWindow;
        public int intMagnifierWidth;
        public int intMagnifierHeight;
        public int intZoomFactor;
        public double doubleSpeedFactor;
        public int intMagnifierShape;

        public MagnifierConfiguration()
        {
            intLocationX = 0;
            intLocationY = 0;
            boolCloseOnMouseUp = false;
            boolDoubleBuffered = false;
            boolHideMouseCursor = false;
            boolRememberLastPoint = false;
            boolReturnToOrigin = false;
            boolTopMostWindow = false;
            intMagnifierWidth = 0;
            intMagnifierHeight = 0;
            intZoomFactor = 0;
            doubleSpeedFactor = (double)0;
            intMagnifierShape = 0;
        }

        public int LocationX
        {
            get
            {
                return intLocationX;
            }
            set
            {
                intLocationX = value;
            }
        }

        public int LocationY
        {
            get
            {
                return intLocationY;
            }
            set
            {
                intLocationY = value;
            }
        }

        public bool CloseOnMouseUp
        {
            get
            {
                return boolCloseOnMouseUp;
            }
            set
            {
                boolCloseOnMouseUp = value;
            }
        }

        public bool DoubleBuffered
        {
            get
            {
                return boolDoubleBuffered;
            }
            set
            {
                boolDoubleBuffered = value;
            }
        }

        public bool HideMouseCursor
        {
            get
            {
                return boolHideMouseCursor;
            }
            set
            {
                boolHideMouseCursor = value;
            }
        }

        public bool RememberLastPoint
        {
            get
            {
                return boolRememberLastPoint;
            }
            set
            {
                boolRememberLastPoint = value;
            }
        }

        public bool ReturnToOrigin
        {
            get
            {
                return boolReturnToOrigin;
            }
            set
            {
                boolReturnToOrigin = value;
            }
        }

        public bool TopMostWindow
        {
            get
            {
                return boolTopMostWindow;
            }
            set
            {
                boolTopMostWindow = value;
            }
        }

        public int MagnifierWidth
        {
            get
            {
                return intMagnifierWidth;
            }
            set
            {
                intMagnifierWidth = value;
            }
        }

        public int MagnifierHeight
        {
            get
            {
                return intMagnifierHeight;
            }
            set
            {
                intMagnifierHeight = value;
            }
        }

        public int ZoomFactor
        {
            get
            {
                return intZoomFactor;
            }
            set
            {
                intZoomFactor = value;
            }
        }

        public double SpeedFactor
        {
            get
            {
                return doubleSpeedFactor;
            }
            set
            {
                doubleSpeedFactor = value;
            }
        }

        public int MagnifierShape
        {
            get
            {
                return intMagnifierShape;
            }
            set
            {
                intMagnifierShape = value;
            }
        }

    }

}

using System;
using System.Collections.Generic;
using System.Text;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class EncodingConversions
    {
        /// <summary>
        /// 
        /// </summary>
        public EncodingConversions()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="characters"></param>
        /// <returns></returns>
        public String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        public Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }
    }
}

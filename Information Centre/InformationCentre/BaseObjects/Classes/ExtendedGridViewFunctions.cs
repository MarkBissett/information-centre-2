using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Card;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using System.IO;
using System.Threading;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using System.Reflection;

namespace BaseObjects
{
    /// <summary>
    /// Adds additional functionality to GridViews
    /// </summary>
    public class ExtendedGridViewFunctions
    {
        /// <summary>
        /// Adds additional functionality to GridViews
        /// </summary>
        public ExtendedGridViewFunctions()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void DragObjectDrop(object sender, DragObjectDropEventArgs e)
        {
            Boolean blPictureEdit = false;
            int intImageHeight = -1;
            // Check if row height needs adjusting (student image being shown) //
            Image img;
            GridView view = (GridView)sender;

            foreach (GridColumn col in view.Columns)
            {
                // Go through columns, get Picture Edit Repository item...
                if (col.VisibleIndex > -1 && col.RealColumnEdit.EditorTypeName == "PictureEdit")
                {
                    // Need to get the size of the image...
                    img = (Image)view.GetRowCellValue(0, col);
                    if (img != null)
                    {
                        intImageHeight = img.Height;
                        blPictureEdit = true;

                        if (intImageHeight > view.RowHeight)
                        {
                            view.RowHeight = intImageHeight;
                        }
                    }
                }
            }

            if (blPictureEdit == false)
            {
                view.RowHeight = -1;
            }
        }


        /// <summary>
        /// Locates a matching row in a gridview - checks multiple columns for a match.
        /// Mark Bissett
        /// 25/03/2009
        /// </summary>
        /// <param name="view"></param>
        /// <param name="columns"></param>
        /// <param name="values"></param>
        /// <param name="startRowHandle"></param>
        /// <returns></returns>
        public int LocateRowByMultipleValues(ColumnView view, GridColumn[] columns, object[] values, int startRowHandle)
        {
            // checking whether the arrays have the same length
            if (columns.Length != values.Length) return DevExpress.XtraGrid.GridControl.InvalidRowHandle;
            // obtaining the number of data rows within the view
            int dataRowCount;
            if (view is CardView)
            {
                dataRowCount = (view as CardView).RowCount;
            }
            else
            {
                dataRowCount = (view as GridView).DataRowCount;
            }
            // traversing the data rows to find a match
            bool match;
            object currValue;
            for (int currentRowHandle = startRowHandle; currentRowHandle < dataRowCount; currentRowHandle++)
            {
                match = true;
                for (int i = 0; i < columns.Length; i++)
                {
                    currValue = view.GetRowCellValue(currentRowHandle, columns[i]);
                    if (!currValue.Equals(values[i])) match = false;
                }
                if (match) return currentRowHandle;
            }
            // returning the invalid row handle if no matches found
            return DevExpress.XtraGrid.GridControl.InvalidRowHandle;
        }


        /// <summary>
        /// Draws a message in the centre of a grid when the grid contains no rows.
        /// Mark Bissett
        /// 02/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="message"></param>
        public void CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e, string message)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            if (string.IsNullOrWhiteSpace(message)) message = "No Records Available";
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(message, e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }


        /// <summary>
        /// Replaces Filter Edit screens default editor with drop down list of available values from calling grid column. 
        /// Mark Bissett
        /// 12/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                view.GridControl.Refresh();
            }
        }


        /// <summary>
        /// Replaces Filter Edit screens default editor with drop down list of available values from calling grid column. 
        /// Mark Bissett
        /// 12/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }


        /// <summary>
        /// Only allow non-group rows to be selected within the grid. 
        /// Mark Bissett
        /// 19/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="InternalRowFocusing"></param>
        /// <returns></returns>
        public void FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e, ref bool internalRowFocusing)
        {
            // Note grid's MouseDown_NoGroupSelection event also updated to handle Expand and Contract button clicking //        
            GridView view = (GridView)sender;
            if (internalRowFocusing)  return;
            try
            {
                internalRowFocusing = true;
                if (view.IsGroupRow(e.FocusedRowHandle))
                {
                    if (view.IsGroupRow(e.PrevFocusedRowHandle))
                    {
                        if (view.GetVisibleIndex(e.PrevFocusedRowHandle) > view.GetVisibleIndex(e.FocusedRowHandle))
                        {
                            view.FocusedRowHandle = GetBottomDataRowHandle(e.FocusedRowHandle, view);
                        }
                        else
                        {
                            view.FocusedRowHandle = GetTopDataRowHandle(e.FocusedRowHandle, view);
                        }
                    }
                    else
                    {
                        if (view.GetParentRowHandle(e.PrevFocusedRowHandle) == e.FocusedRowHandle)
                        {
                            int rowHandle = GetTopDataRowHandle(e.FocusedRowHandle, view);
                            if (rowHandle < 0)
                                view.FocusedRowHandle = e.PrevFocusedRowHandle;
                            else
                                view.FocusedRowHandle = rowHandle;

                        }
                        else
                        {
                            do
                            {
                                view.ExpandGroupRow(view.FocusedRowHandle, false);
                                view.MoveNext();
                            } while (view.IsGroupRow(view.FocusedRowHandle));
                        }
                    }
                }
            }
            finally
            {
                internalRowFocusing = false;
            }
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
        }
        private int GetTopDataRowHandle(int ARowHandle, GridView view)
        {
            if (view.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = view.GetVisibleRowHandle(view.GetVisibleIndex(ARowHandle) - 1);
                if (view.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle, view);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle, view);
                }
            }
            else return -1;
        }
        private int GetBottomDataRowHandle(int ARowHandle, GridView view)
        {
            int rowHandle = ARowHandle;
            if (view.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = view.GetChildRowHandle(rowHandle, view.GetChildRowCount(rowHandle) - 1);
                } while (view.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }

        /// <summary>
        /// Only allow non-group rows to be selected within the grid. 
        /// Mark Bissett
        /// 19/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            // Need following to handle expand and contract groups since grid's FocusedRowChanged_NoGroupSelection event is interfering with it due to no selection of groups //
            GridView view = sender as GridView;
            GridHitInfo hi = view.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.HitTest == GridHitTest.RowGroupButton)
            {
                if (view.GetRowExpanded(hi.RowHandle))
                {
                    view.CollapseGroupRow(hi.RowHandle);
                }
                else
                {
                    view.ExpandGroupRow(hi.RowHandle);
                }
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();  // Make sure grid is focused //
        }

        /// <summary>
        /// Make sure the grid is the focused grid on the parent screen if mouse right-click is used on grid. 
        /// Mark Bissett
        /// 22/01/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MouseDown_Standard(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && !view.GridControl.IsFocused) view.GridControl.Select();  // Make sure grid is focused //
        }

        /// <summary>
        /// Show the Extended Grid Menu. 
        /// Mark Bissett
        /// 01/02/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = (GridView)sender;
            ExtendedGridMenu egmMenu = new ExtendedGridMenu(view);
            egmMenu.PopupMenuShowing(sender, e);
        }

        /// <summary>
        /// Only allow non-group rows to be selected on a grid supporting multi-row selection. 
        /// Mark Bissett
        /// 09/02/2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="isRunning"></param>
        public void SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e, ref bool isRunning)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }




    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseObjects
{
    public class DateDiff
    {
        private int _Years = 0;
        private int _Months = 0;
        private int _Weeks = 0;
        private int _Days = 0;
        private int _Hours = 0;
        private int _Minutes = 0;
        private int _Seconds = 0;
        public int Years { get { return _Years; } }
        public int Months { get { return _Months; } }
        public int Weeks { get { return _Weeks; } }
        public int Days { get { return _Days; } }
        public int Hours { get { return _Hours; } }
        public int Minutes { get { return _Minutes; } }
        public int Seconds { get { return _Seconds; } }

        public override string ToString()
        {
            return String.Format("{0} year{1}, {2} month{3}, {4} week{5}, {6} day{7}, {8} hour{9}, {10} minute{11}, {12} second{13}"
                , Years, Years != 1 ? "s" : ""
                , Months, Months != 1 ? "s" : ""
                , Weeks, Weeks != 1 ? "s" : ""
                , Days, Days != 1 ? "s" : ""
                , Hours, Hours != 1 ? "s" : ""
                , Minutes, Minutes != 1 ? "s" : ""
                , Seconds, Seconds != 1 ? "s" : ""
                );
        }

        public static DateDiff Difference(DateTime dt1, DateTime dt2)
        {
            if (dt1 > dt2)
            {
                DateTime dtTemp = dt1;
                dt1 = dt2;
                dt2 = dtTemp;
            }

            DateDiff dd = new DateDiff();
            dd._Years = dt2.Year - dt1.Year;
            if (dd._Years > 0)
            {
                if (dt2.Month < dt1.Month)
                {
                    dd._Years--;
                }
                else if (dt2.Month == dt1.Month)
                {
                    if (dt2.Day < dt1.Day)
                    {
                        dd._Years--;
                    }
                    else if (dt2.Day == dt1.Day)
                    {
                        if (dt2.Hour < dt1.Hour)
                        {
                            dd._Years--;
                        }
                        else if (dt2.Hour == dt1.Hour)
                        {
                            if (dt2.Minute < dt1.Minute)
                            {
                                dd._Years--;
                            }
                            else if (dt2.Minute == dt1.Minute)
                            {
                                if (dt2.Second < dt1.Second)
                                {
                                    dd._Years--;
                                }
                            }
                        }
                    }
                }
            }

            dd._Months = dt2.Month - dt1.Month;
            if (dt2.Month < dt1.Month)
            {
                dd._Months = 12 - dt1.Month + dt2.Month;
            }
            if (dd._Months > 0)
            {
                if (dt2.Day < dt1.Day)
                {
                    dd._Months--;
                }
                else if (dt2.Day == dt1.Day)
                {
                    if (dt2.Hour < dt1.Hour)
                    {
                        dd._Months--;
                    }
                    else if (dt2.Hour == dt1.Hour)
                    {
                        if (dt2.Minute < dt1.Minute)
                        {
                            dd._Months--;
                        }
                        else if (dt2.Minute == dt1.Minute)
                        {
                            if (dt2.Second < dt1.Second)
                            {
                                dd._Months--;
                            }
                        }
                    }
                }
            }

            dd._Days = dt2.Day - dt1.Day;
            if (dt2.Day < dt1.Day)
            {
                dd._Days = DateTime.DaysInMonth(dt1.Year, dt1.Month) - dt1.Day + dt2.Day;
            }
            if (dd._Days > 0)
            {
                if (dt2.Hour < dt1.Hour)
                {
                    dd._Days--;
                }
                else if (dt2.Hour == dt1.Hour)
                {
                    if (dt2.Minute < dt1.Minute)
                    {
                        dd._Days--;
                    }
                    else if (dt2.Minute == dt1.Minute)
                    {
                        if (dt2.Second < dt1.Second)
                        {
                            dd._Days--;
                        }
                    }
                }
            }

            dd._Weeks = dd._Days / 7;
            dd._Days = dd._Days % 7;

            dd._Hours = dt2.Hour - dt1.Hour;
            if (dt2.Hour < dt1.Hour)
            {
                dd._Hours = 24 - dt1.Hour + dt2.Hour;
            }
            if (dd._Hours > 0)
            {
                if (dt2.Minute < dt1.Minute)
                {
                    dd._Hours--;
                }
                else if (dt2.Minute == dt1.Minute)
                {
                    if (dt2.Second < dt1.Second)
                    {
                        dd._Hours--;
                    }
                }
            }

            dd._Minutes = dt2.Minute - dt1.Minute;
            if (dt2.Minute < dt1.Minute)
            {
                dd._Minutes = 60 - dt1.Minute + dt2.Minute;
            }
            if (dd._Minutes > 0)
            {
                if (dt2.Second < dt1.Second)
                {
                    dd._Minutes--;
                }
            }

            dd._Seconds = dt2.Second - dt1.Second;
            if (dt2.Second < dt1.Second)
            {
                dd._Seconds = 60 - dt1.Second + dt2.Second;
            }
            return dd;
        }
    }
}
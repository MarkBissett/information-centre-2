using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Reflection;  // Required for MethodInfo used to notify parent when rectangle moved //

namespace BaseObjects
{
    public class UserRect
    {
        private PictureBox mPictureBox;
        public Rectangle rect;
        public bool allowDeformingDuringMovement = false;
        private bool mIsClick = false;
        private bool mMove = false;
        private int oldX;
        private int oldY;
        private int sizeNodeRect = 5;
        private Bitmap mBmp = null;
        //private PosSizableRect nodeSelected = PosSizableRect.None;
        private CursorOverlay nodeSelected = CursorOverlay.None;
        private int angle = 30;
        private bool boolAllowResize = false;

        private bool boolAllowMove = false;


        public UserRect(Rectangle r)
        {
            rect = r;
            mIsClick = false;
        }

        #region styles
        Pen _handleBorderPen = Pens.SteelBlue;
        Pen _itemBorderPen;
        Pen _selectionBorderPen;
        Color _handleLightColor = Color.FromArgb(220, 235, 235);
        Color _handleDarkColor = Color.FromArgb(115, 186, 230);
        Pen _canvasGuidesPen;
        Color _itemsBoundsColor = Color.FromArgb(20, Color.Yellow);
        SolidBrush _backBrush;
        Pen _backPen = Pens.YellowGreen;
        Font _itemNameFont = new Font("Arial", 7);
        Brush _itemNameBrush = Brushes.DimGray;

        //Prepare item border pen style
        private static Color _selectionBorderColor = Color.FromArgb(95, 166, 210);
        HatchBrush hb = new HatchBrush(HatchStyle.SmallCheckerBoard, _selectionBorderColor, Color.White);
        #endregion

        #region Metrics
        public const int DESIGN_HANDLE_SIZE = 8;
        #endregion

        public void Resize(int intWidth, int intHeight)
        {
            rect.Width = intWidth;
            rect.Height = intHeight;
            mPictureBox.Invalidate();
        }

        public void SetManualResize(bool value)
        {
            boolAllowResize = value;
        }

        public int GetX()
        {
            return rect.Location.X;
        }
        public int GetY()
        {
            return rect.Location.Y;
        }
        public Size GetSize()
        {
            return rect.Size;
        }

        public Rectangle GetDesignHandleRect(CursorOverlay cursorOverlay)
        {
            Rectangle r = rect;
            int x = r.X;
            int y = r.Y;
            int w = r.Width;
            int h = r.Height;

            switch (cursorOverlay)
            {
                case CursorOverlay.BottomHandler:
                    if (w <= 32 + DESIGN_HANDLE_SIZE)
                        return new Rectangle(x + (w - DESIGN_HANDLE_SIZE) / 2, y + h, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);
                    else
                        return new Rectangle(x + 16, y + h, w - 32, DESIGN_HANDLE_SIZE);

                case CursorOverlay.BottomRightHandler:
                    return new Rectangle(x + w, y + h, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);

                case CursorOverlay.RightHandler:
                    if (h <= 32 + DESIGN_HANDLE_SIZE)
                        return new Rectangle(x + w, y + (h - DESIGN_HANDLE_SIZE) / 2, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);
                    else
                        return new Rectangle(x + w, y + 16, DESIGN_HANDLE_SIZE, h - 32);

                case CursorOverlay.TopHandler:
                    if (w <= 32 + DESIGN_HANDLE_SIZE)
                        return new Rectangle(x + (w - DESIGN_HANDLE_SIZE) / 2, y - DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);
                    else
                        return new Rectangle(x + 16, y - DESIGN_HANDLE_SIZE, w - 32, DESIGN_HANDLE_SIZE);

                case CursorOverlay.LeftHandler:
                    if (h <= 32 + DESIGN_HANDLE_SIZE)
                        return new Rectangle(x - DESIGN_HANDLE_SIZE, y + (h - DESIGN_HANDLE_SIZE) / 2, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);
                    else
                        return new Rectangle(x - DESIGN_HANDLE_SIZE, y + 16, DESIGN_HANDLE_SIZE, h - 32);

                case CursorOverlay.TopLeftHandler:
                    return new Rectangle(x - DESIGN_HANDLE_SIZE, y - DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);

                case CursorOverlay.TopRightHandler:
                    return new Rectangle(x + w, y - DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);

                case CursorOverlay.BottomLeftHandler:
                    return new Rectangle(x - DESIGN_HANDLE_SIZE, y + h, DESIGN_HANDLE_SIZE, DESIGN_HANDLE_SIZE);

                default: return Rectangle.Empty;
            }
        }

        public void Draw(Graphics g)
        {
            //Prepare document back color brush
            _backBrush = new SolidBrush(_itemsBoundsColor);
            Rectangle backRectangle = rect;
            // with a little margin
            //backRectangle.Inflate(-20, -20);
            backRectangle.X = rect.X + 37;
            backRectangle.Y = rect.Y + 110;
            backRectangle.Width = rect.Width - 74;
            backRectangle.Height = rect.Height - 220;


            g.FillRectangle(_backBrush, backRectangle);
            g.DrawRectangle(_backPen, backRectangle);

            _itemBorderPen = new Pen(hb, 4);
            PaintSelection(g);

            // Get current DPI from screen //
            int intDPI = Convert.ToInt32(g.DpiX);

            for (CursorOverlay mode = CursorOverlay.TopHandler; mode <= CursorOverlay.TopLeftHandler; mode++)
            {
                PaintDesignHandle(g, mode);
            }
            PaintItemName(g, "Image Snapshot [" + Convert.ToString(Convert.ToInt32((float)rect.Width / intDPI * 25.4)) + "mm x " + Convert.ToString(Convert.ToInt32((float)rect.Height / intDPI * 25.4)) + "mm]", 5);
            //PaintItemName(g, "Image Snapshot [" + Convert.ToString(rect.Width) + "mm x " + Convert.ToString(rect.Height) + "mm]", 5);
        }

        public void PaintSelection(Graphics g)
        {
            g.DrawRectangle(_itemBorderPen, rect.X - 3, rect.Y - 3, rect.Width + 7, rect.Height + 7);
            //g.DrawRectangle(_itemBorderPen, rect);  
        }

        public void PaintDesignHandle(Graphics g, CursorOverlay cursorOverlay)
        {
            UserRect_ExtendedGraphics myG = new UserRect_ExtendedGraphics(g);

            //Rectangle r = GetDesignHandleRect(item, cursorOverlay, canvas);
            Rectangle r = GetDesignHandleRect(cursorOverlay);
            Brush sb;
            if (r.Width > r.Height)
                sb = new LinearGradientBrush(r, _handleLightColor, _handleDarkColor, LinearGradientMode.Vertical);
            else if (r.Width < r.Height)
                sb = new LinearGradientBrush(r, _handleLightColor, _handleDarkColor, LinearGradientMode.Horizontal);
            else
                sb = new LinearGradientBrush(r, _handleLightColor, _handleDarkColor, LinearGradientMode.ForwardDiagonal);

            myG.PaintRoundRect(_handleBorderPen, sb, r.X, r.Y, r.Width, r.Height, 6);

            sb.Dispose();
        }

        public void PaintItemName(Graphics g, string text, int nameOffset)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Rectangle bounds = rect;
                bounds.Inflate(nameOffset * 2, nameOffset * 2);

                StringFormat sf = new StringFormat(StringFormatFlags.LineLimit);
                SizeF fs = g.MeasureString(text, _itemNameFont, 200, sf);
                g.DrawString(text,
                             _itemNameFont,
                             _itemNameBrush,
                             new Rectangle(bounds.X, bounds.Y - (int)fs.Height, (int)fs.Width + 1, (int)fs.Height + 2),
                             sf);
                g.DrawString(text,
                             _itemNameFont,
                             _itemNameBrush,
                             new Rectangle(bounds.X + bounds.Width - (int)fs.Width, bounds.Y + bounds.Height, (int)fs.Width + 1, (int)fs.Height + 2),
                             sf);
                sf.Dispose();
            }
        }

        public void SetBitmapFile(string filename)
        {
            this.mBmp = new Bitmap(filename);
        }

        public void SetBitmap(Bitmap bmp)
        {
            this.mBmp = bmp;
        }

        public void SetPictureBox(PictureBox p)
        {
            this.mPictureBox = p;
            mPictureBox.MouseDown += new MouseEventHandler(mPictureBox_MouseDown);
            mPictureBox.MouseUp += new MouseEventHandler(mPictureBox_MouseUp);
            mPictureBox.MouseMove += new MouseEventHandler(mPictureBox_MouseMove);
            mPictureBox.Paint += new PaintEventHandler(mPictureBox_Paint);
            mPictureBox.Invalidate();
        }
        public void ClearPictureBox(PictureBox p)
        {
            mPictureBox.MouseDown -= new MouseEventHandler(mPictureBox_MouseDown);
            mPictureBox.MouseUp -= new MouseEventHandler(mPictureBox_MouseUp);
            mPictureBox.MouseMove -= new MouseEventHandler(mPictureBox_MouseMove);
            mPictureBox.Paint -= new PaintEventHandler(mPictureBox_Paint);
            this.mPictureBox = null;
        }

        private void mPictureBox_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Draw(e.Graphics);
            }
            catch (Exception exp)
            {
                System.Console.WriteLine(exp.Message);
            }
        }

        private void mPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            boolAllowMove = (rect.Contains(new Point(e.X, e.Y)) ? true : false);

            mIsClick = true;

            nodeSelected = CursorOverlay.None;
            nodeSelected = GetNodeSelectable(e.Location);

            if (rect.Contains(new Point(e.X, e.Y)))
            {
                mMove = true;
            }
            oldX = e.X;
            oldY = e.Y;
        }

        private void mPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            boolAllowMove = false;
            mIsClick = false;
            mMove = false;
        }

        private void mPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            ChangeCursor(e.Location);

            //if (!mMove && !rect.Contains(new Point(e.X, e.Y)) && mPictureBox.Cursor == Cursors.Default) return;  // Don;t want to progress unless me are in move mode or within the rectangle bounds // 

            if (mIsClick == false) return;

            Rectangle backupRect = rect;

            switch (nodeSelected)
            {
                case CursorOverlay.TopLeftHandler:
                    if (!boolAllowResize) return;
                    rect.X += e.X - oldX;
                    rect.Width -= e.X - oldX;
                    rect.Y += e.Y - oldY;
                    rect.Height -= e.Y - oldY;
                    break;
                case CursorOverlay.LeftHandler:
                    if (!boolAllowResize) return;
                    rect.X += e.X - oldX;
                    rect.Width -= e.X - oldX;
                    break;
                case CursorOverlay.BottomLeftHandler:
                    if (!boolAllowResize) return;
                    rect.Width -= e.X - oldX;
                    rect.X += e.X - oldX;
                    rect.Height += e.Y - oldY;
                    break;
                case CursorOverlay.BottomHandler:
                    if (!boolAllowResize) return;
                    rect.Height += e.Y - oldY;
                    break;
                case CursorOverlay.TopRightHandler:
                    if (!boolAllowResize) return;
                    rect.Width += e.X - oldX;
                    rect.Y += e.Y - oldY;
                    rect.Height -= e.Y - oldY;
                    break;
                case CursorOverlay.BottomRightHandler:
                    if (!boolAllowResize) return;
                    rect.Width += e.X - oldX;
                    rect.Height += e.Y - oldY;
                    break;
                case CursorOverlay.RightHandler:
                    if (!boolAllowResize) return;
                    rect.Width += e.X - oldX;
                    break;
                case CursorOverlay.TopHandler:
                    if (!boolAllowResize) return;
                    rect.Y += e.Y - oldY;
                    rect.Height -= e.Y - oldY;
                    break;

                default:
                    if (mMove)
                    {
                        rect.X = rect.X + e.X - oldX;
                        rect.Y = rect.Y + e.Y - oldY;
                    }
                    if (rect.Width < 5 || rect.Height < 5)
                    {
                        // Do Nothing as Rectangle won't move past here //
                    }
                    else
                    {
                        if (!boolAllowMove) return;
                        object objItem;
                        MethodInfo method = typeof(frmBase).GetMethod("RectangleMoved");
                        if (method != null)
                        {
                            objItem = mPictureBox.Parent;
                            //if (Convert.ToString(objItem).Contains("PopupContainerControl")) return;  // Added by MB 25/10/2007 to stop potential crash //
                            //if (Convert.ToString(objItem).Contains("LayoutControl")) return;  // Added by MB 01/12/2009 to stop potential crash //
                            while (objItem.GetType().BaseType.Name != "frmBase")
                            {
                                objItem = ((Control)objItem).Parent;
                                if (objItem == null)
                                {
                                    objItem = mPictureBox.Parent;
                                    while (objItem.GetType().BaseType.Name != "ctrlBase")
                                    {
                                        objItem = ((Control)objItem).Parent;
                                    }
                                    method = typeof(ctrlBase).GetMethod("RectangleMoved");
                                    break;
                                }
                            }
                            method.Invoke(objItem, new object[] { (object)this, e.X - oldX, e.Y - oldY });
                        }
                    }
                    break;
            }
            oldX = e.X;
            oldY = e.Y;

            if (rect.Width < 5 || rect.Height < 5)
            {
                rect = backupRect;
            }

            TestIfRectInsideArea();

            mPictureBox.Invalidate();
        }

        private void TestIfRectInsideArea()
        {
            // Test if rectangle still inside the area.
            if (rect.X < 0) rect.X = 0;
            if (rect.Y < 0) rect.Y = 0;
            if (rect.Width <= 0) rect.Width = 1;
            if (rect.Height <= 0) rect.Height = 1;

            if (rect.X + rect.Width > mPictureBox.Width)
            {
                rect.Width = mPictureBox.Width - rect.X - 1; // -1 to be still show 
                if (allowDeformingDuringMovement == false)
                {
                    mIsClick = false;
                }
            }
            if (rect.Y + rect.Height > mPictureBox.Height)
            {
                rect.Height = mPictureBox.Height - rect.Y - 1;// -1 to be still show 
                if (allowDeformingDuringMovement == false)
                {
                    mIsClick = false;
                }
            }
        }

        private Rectangle CreateRectSizableNode(int x, int y)
        {
            //return new Rectangle(x - sizeNodeRect / 2, y - sizeNodeRect / 2, sizeNodeRect, sizeNodeRect);
            return new Rectangle(x - sizeNodeRect, y - sizeNodeRect, sizeNodeRect, sizeNodeRect);
        }

        private Rectangle GetRect(CursorOverlay c)
        {
            return GetDesignHandleRect(c);
            /*switch (c)
            {
                case CursorOverlay.TopLeftHandler:
                    return CreateRectSizableNode(rect.X, rect.Y);

                case CursorOverlay.LeftHandler:
                    return CreateRectSizableNode(rect.X, rect.Y + rect.Height / 2);

                case CursorOverlay.BottomLeftHandler:
                    return CreateRectSizableNode(rect.X, rect.Y + rect.Height);

                case CursorOverlay.BottomHandler:
                    return CreateRectSizableNode(rect.X  + rect.Width / 2,rect.Y + rect.Height);

                case CursorOverlay.TopRightHandler:
                    return CreateRectSizableNode(rect.X + rect.Width,rect.Y );

                case CursorOverlay.BottomRightHandler:
                    return CreateRectSizableNode(rect.X  + rect.Width,rect.Y  + rect.Height);

                case CursorOverlay.RightHandler:
                    return CreateRectSizableNode(rect.X  + rect.Width, rect.Y  + rect.Height / 2);

                case CursorOverlay.TopHandler:
                    return CreateRectSizableNode(rect.X + rect.Width / 2, rect.Y);
                default :
                    return new Rectangle();
            }*/
        }

        private CursorOverlay GetNodeSelectable(Point p)
        {
            foreach (CursorOverlay r in Enum.GetValues(typeof(CursorOverlay)))
            {
                if (GetRect(r).Contains(p))
                {
                    return r;
                }
            }
            return CursorOverlay.None;
        }

        private void ChangeCursor(Point p)
        {
            mPictureBox.Cursor = GetCursor(GetNodeSelectable(p), p);
        }

        private Cursor GetCursor(CursorOverlay mode, Point p)
        {
            if (!boolAllowResize)
            {
                if (rect.Contains(new Point(p.X, p.Y)))
                {
                    return Cursors.SizeAll;
                }
                return Cursors.Default;
            }
            switch (mode)
            {
                case CursorOverlay.TopLeftHandler:
                    return Cursors.SizeNWSE;

                case CursorOverlay.LeftHandler:
                    return Cursors.SizeWE;

                case CursorOverlay.BottomLeftHandler:
                    return Cursors.SizeNESW;

                case CursorOverlay.BottomHandler:
                    return Cursors.SizeNS;

                case CursorOverlay.TopRightHandler:
                    return Cursors.SizeNESW;

                case CursorOverlay.BottomRightHandler:
                    return Cursors.SizeNWSE;

                case CursorOverlay.RightHandler:
                    return Cursors.SizeWE;

                case CursorOverlay.TopHandler:
                    return Cursors.SizeNS;
                default:
                    if (rect.Contains(new Point(p.X, p.Y)))
                    {
                        return Cursors.SizeAll;
                    }
                    return Cursors.Default;
            }
        }

        public enum CursorOverlay
        {
            Canvas,
            ItemClient,

            // WARNING: do not change the order of DragMode members
            // from TopHandler to TopLeftHandler because these are used as hi and low limits
            // for loops, e.g. for (CursorOverlay mode = CursorOverlay.TopHandler; mode < CursorOverlay.TopLeftHandler; mode++)
            TopHandler,
            TopRightHandler,
            RightHandler,
            BottomRightHandler,
            BottomHandler,
            BottomLeftHandler,
            LeftHandler,
            TopLeftHandler,
            None
        }
    }
}

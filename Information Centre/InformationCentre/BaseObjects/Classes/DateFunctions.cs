﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BaseObjects
{
    public class DateFunctions
    {
        //// This presumes that weeks start with Monday. Week 1 is the 1st week of the year with a Thursday in it. //
        //// EDIT: 26/07/2017 - At Request of Paul Reynolds - Changed calculation to used 4 days instead of 3 so that weeks run from Sunday morning instead of Monday morning //
        //public static int GetIso8601WeekOfYear(DateTime time)
        //{
        //    // If its Monday, Tuesday or Wednesday, then it'll be the same week# as whatever Thursday, Friday or Saturday are, and we always get those right //
        //    DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
        //    if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
        //    {
        //        time = time.AddDays(4);  // 3 // 
        //    }
        //    return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);  // Return the week of our adjusted day //
        //}

        // This presumes that weeks start with Monday. Week 1 is the 1st week of the year with a Thursday in it. //
        // EDIT: 26/07/2017 - At Request of Paul Reynolds - Changed calculation to used 4 days instead of 3 so that weeks run from Sunday morning instead of Monday morning //
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // If its Monday, Tuesday or Wednesday, then it'll be the same week# as whatever Thursday, Friday or Saturday are, and we always get those right //
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Sunday && day <= DayOfWeek.Tuesday)
            {
                time = time.AddDays(4);  // 3 // 
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);  // Return the week of our adjusted day //
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-4);  // -3 //
        }

    }
}

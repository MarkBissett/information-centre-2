using System;
using System.Data;
using System.Data.SqlClient;

namespace BaseObjects
{
    public class Default_Screen_Settings
    {

        public SqlDataAdapter sdaDefaultSettings;
        public DataSet dsDefaultSettings;
        public string ConnectionString;

        public Default_Screen_Settings()
        {
            sdaDefaultSettings = null;
            dsDefaultSettings = null;
        }                                     

        public int LoadDefaultScreenSettings(int intScreenID, int intUserID, string strConnectionString)
        {
            ConnectionString = strConnectionString;
            try
            {
                using (SqlConnection SQlConn = new SqlConnection(strConnectionString))
                {
                    SQlConn.Open();
                    using (SqlCommand cmd = new SqlCommand("sp00151_get_default_user_screen_settings", SQlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@UserID", intUserID));
                        cmd.Parameters.Add(new SqlParameter("@ScreenID", intScreenID));
                        dsDefaultSettings = new DataSet();
                        sdaDefaultSettings = new SqlDataAdapter(cmd);
                        sdaDefaultSettings.Fill(dsDefaultSettings, "Table");
                    }
                    using (SqlCommand InsertCmd = new SqlCommand("dbo.sp00152_insert_default_user_screen_settings", SQlConn))
                    {
                        InsertCmd.CommandType = CommandType.StoredProcedure;
                        InsertCmd.Parameters.Add(new SqlParameter("@ScreenID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ScreenID", DataRowVersion.Current, false, null, "", "", ""));
                        InsertCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "UserID", DataRowVersion.Current, false, null, "", "", ""));
                        InsertCmd.Parameters.Add(new SqlParameter("@ItemName", SqlDbType.VarChar, 100, ParameterDirection.Input, 0, 0, "ItemName", DataRowVersion.Current, false, null, "", "", ""));
                        InsertCmd.Parameters.Add(new SqlParameter("@ItemValue", SqlDbType.VarChar, 2147483647, ParameterDirection.Input, 0, 0, "ItemValue", DataRowVersion.Current, false, null, "", "", ""));
                        sdaDefaultSettings.InsertCommand = InsertCmd;
                    }
                    using (SqlCommand UpdateCmd = new SqlCommand("sp00153_update_default_user_screen_settings", SQlConn))
                    {
                        UpdateCmd.CommandType = CommandType.StoredProcedure;
                        UpdateCmd.Parameters.Add(new SqlParameter("@RecordID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "RecordID", DataRowVersion.Current, false, null, "", "", ""));
                        UpdateCmd.Parameters.Add(new SqlParameter("@ScreenID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ScreenID", DataRowVersion.Current, false, null, "", "", ""));
                        UpdateCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "UserID", DataRowVersion.Current, false, null, "", "", ""));
                        UpdateCmd.Parameters.Add(new SqlParameter("@ItemName", SqlDbType.VarChar, 100, ParameterDirection.Input, 0, 0, "ItemName", DataRowVersion.Current, false, null, "", "", ""));
                        UpdateCmd.Parameters.Add(new SqlParameter("@ItemValue", SqlDbType.VarChar, 2147483647, ParameterDirection.Input, 0, 0, "ItemValue", DataRowVersion.Current, false, null, "", "", ""));
                        sdaDefaultSettings.UpdateCommand = UpdateCmd;
                    }
                    using (SqlCommand DeleteCmd = new SqlCommand("dbo.sp00154_delete_default_user_screen_settings", SQlConn))
                    {
                        DeleteCmd.CommandType = CommandType.StoredProcedure;
                        DeleteCmd.Parameters.Add(new SqlParameter("@RecordID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "RecordID", DataRowVersion.Current, false, null, "", "", ""));
                        sdaDefaultSettings.DeleteCommand = DeleteCmd;
                    }

                    return 1;
                }
            }
            catch
            {
                return 0;
            }
        }

        public string RetrieveSetting(string strSettingName)
        {
            if (string.IsNullOrEmpty(strSettingName)) return "";
            foreach (DataRow dr in dsDefaultSettings.Tables[0].Rows)
            {
                if (dr["ItemName"].ToString() == strSettingName)
                {
                    return dr["ItemValue"].ToString();  // Exit out now record has been found //
                }
            }
            return "";
        }

        public int UpdateInMemoryDefaultScreenSetting(int intScreenID, int intUserID, string strSettingName, string strSettingValue)
        {
            if (string.IsNullOrEmpty(strSettingName)) return 0;
            foreach (DataRow dr in dsDefaultSettings.Tables[0].Rows)
            {
                if (dr["ItemName"].ToString() == strSettingName)
                {
                    if (dr["ItemValue"].ToString() != strSettingValue) dr["ItemValue"] = strSettingValue;
                    return 1;  // Exit out now record has been found [and updated if passed in value is different from current value] //
                }
            }
            // If we are here, setting was not found, so add it //
            DataRow newDR = dsDefaultSettings.Tables[0].NewRow();
            newDR["ScreenID"] = intScreenID;
            newDR["UserID"] = intUserID;
            newDR["ItemName"] = strSettingName;
            newDR["ItemValue"] = strSettingValue;
            dsDefaultSettings.Tables[0].Rows.Add(newDR);
            return 1;
            
            /*try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("sp00153_update_default_user_screen_settings", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ScreenID", intScreenID));
                        cmd.Parameters.Add(new SqlParameter("@UserID", intUserID));
                        cmd.Parameters.Add(new SqlParameter("@ItemName", strSettingName));
                        cmd.Parameters.Add(new SqlParameter("@ItemValue", strSettingValue));
                        sdaDefaultSettings.UpdateCommand = cmd;
                        cmd.ExecuteScalar().ToString();
                        conn.Close();
                    }
                }
            }
            catch (Exception)
            {
                return 0;
            }
            return 1;*/
        }

        public int SaveDefaultScreenSettings()
        {
            try
            {
                sdaDefaultSettings.DeleteCommand.Connection.ConnectionString = ConnectionString;  // Need to reapply connection string as it seems to get lost after creating in LoadDefaultScreenSettings Method //
                sdaDefaultSettings.InsertCommand.Connection.ConnectionString = ConnectionString;
                sdaDefaultSettings.UpdateCommand.Connection.ConnectionString = ConnectionString;

                sdaDefaultSettings.Update(dsDefaultSettings);  // Insert, Update and Delete queries defined in code in LoadDefaultScreenSettings Method //
                return 1;
            }
            catch
            {
                return 0;
            }
        }


    }
}

using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Windows.Forms;
using DevExpress.XtraCharts;
using System.ComponentModel;
using System.Drawing;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Drawing.Helpers;
using DevExpress.XtraPivotGrid;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors;
using BaseObjects;

namespace BaseObjects 
{
    public class ChartControlTabLookAndFeel : ChartControl
    {
        public ChartControlTabLookAndFeel() {
            UserLookAndFeel.Default.StyleChanged += new EventHandler(Default_StyleChanged);
            UpdateColors();
        }
        protected override void Dispose(bool disposing) {
            if(disposing) {
                UserLookAndFeel.Default.StyleChanged -= new EventHandler(Default_StyleChanged);
            }
            base.Dispose(disposing);
        }
        void Default_StyleChanged(object sender, EventArgs e) {
            UpdateColors();
        }
        void UpdateColors() {
            BackColor = new SkinElementInfo(TabSkins.GetSkin(UserLookAndFeel.Default)[TabSkins.SkinTabPane]).Element.Color.GetSolidImageCenterColor();
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new Color BackColor {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }
    }
    public class ChartControlBorderLookAndFeel : ChartControl {
        public ChartControlBorderLookAndFeel() {
            UserLookAndFeel.Default.StyleChanged += new EventHandler(Default_StyleChanged);
            UpdateBorderColor();
        }
        protected override void Dispose(bool disposing) {
            if(disposing) {
                UserLookAndFeel.Default.StyleChanged -= new EventHandler(Default_StyleChanged);
            }
            base.Dispose(disposing);
        }
        void Default_StyleChanged(object sender, EventArgs e) {
            UpdateBorderColor();
        }
        void UpdateBorderColor() {
            if(DesignMode) return;
            Color color = new SkinElementInfo(GridSkins.GetSkin(UserLookAndFeel.Default)[GridSkins.SkinBorder]).Element.Border.All;
            BorderOptions.Color = color;
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new RectangularBorder BorderOptions { get { return base.BorderOptions; } }
    }
    public delegate void ChartAppearanceEventHandler(object sender, ChartAppearanceEventArgs e);
    public class ChartAppearanceEventArgs : EventArgs {
        string palette, appearance;
        int color;
        public ChartAppearanceEventArgs(string palette, string appearance, int color) {
            this.palette = palette;
            this.appearance = appearance;
            this.color = color;
        }
        public string PaletteName { get { return palette; } }
        public string AppearanceName { get { return appearance; } }
        public int ColorIndex { get { return color; } }
    }
    public class ChartAppearanceManager {
        ChartControl chartControl;
        Chart chart;
        GalleryDropDown paletteGallery;
        RibbonGalleryBarItem styleGalleryBarItem;
        BarButtonItem paletteButton;
        ViewType viewType = ViewType.Bar;
        public event ChartAppearanceEventHandler StyleChanged;
        public ChartAppearanceManager(GalleryDropDown paletteGallery, RibbonGalleryBarItem styleGalleryBarItem, BarButtonItem paletteButton) {
            this.paletteGallery = paletteGallery;
            this.styleGalleryBarItem = styleGalleryBarItem;
            this.paletteButton = paletteButton;
            chartControl = new ChartControl();
            chart = ((IChartContainer)chartControl).Chart;
            this.paletteGallery.GalleryItemClick += new GalleryItemClickEventHandler(paletteGallery_GalleryItemClick);
            this.styleGalleryBarItem.GalleryInitDropDownGallery += new InplaceGalleryEventHandler(styleGalleryBarItem_GalleryInitDropDownGallery);
            this.styleGalleryBarItem.GalleryPopupClose += new InplaceGalleryEventHandler(styleGalleryBarItem_GalleryPopupClose);
            this.styleGalleryBarItem.GalleryItemClick += new GalleryItemClickEventHandler(styleGalleryBarItem_GalleryItemClick);
            InitChartPaletteGallery(this.paletteGallery);
            //InitChartAppearanceGallery(this.styleGalleryBarItem, chart.PaletteName);
        }
        public ViewType ViewType {
            get { return viewType; }
            set {
                if(ViewType == value) return;
                viewType = value;
                InitChartAppearanceGallery(styleGalleryBarItem, chart.PaletteName);
            }
        }
        void styleGalleryBarItem_GalleryPopupClose(object sender, InplaceGalleryEventArgs e) {
            List<GalleryItem> items = e.Item.Gallery.GetCheckedItems();
            if(items.Count > 0) {
                e.Item.Gallery.MakeVisible(items[0]);
                UpdateStyle();
            }
        }
        void styleGalleryBarItem_GalleryInitDropDownGallery(object sender, InplaceGalleryEventArgs e) {
            e.PopupGallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            e.PopupGallery.ColumnCount = 7;
            e.PopupGallery.DrawImageBackground = true;
            e.PopupGallery.ShowGroupCaption = true;
            e.PopupGallery.SynchWithInRibbonGallery = true;
            e.PopupGallery.AutoSize = GallerySizeMode.Vertical;
        }
        void paletteGallery_GalleryItemClick(object sender, GalleryItemClickEventArgs e) {
            chart.PaletteName = e.Item.Caption;
            UpdateStyle(true);
        }
        void styleGalleryBarItem_GalleryItemClick(object sender, GalleryItemClickEventArgs e) {
            UpdateStyle();
        }
        void UpdateStyle() {
            UpdateStyle(false);
        }
        static int GetPaletteColor(Palette palette, int value) {
            if(value < 0) return 0;
            if(value > palette.Count) return palette.Count;
            return value;
        }
        void UpdateStyle(bool updateAppearance) {
            string currentPalette = chart.PaletteName;
            chart.AppearanceName = CurrentAppearanceName;
            chart.PaletteBaseColorNumber = GetPaletteColor(chart.Palette, CurrentPaletteColor);
            if(currentPalette != chart.PaletteName || updateAppearance)
                InitChartAppearanceGallery(this.styleGalleryBarItem, chart.PaletteName);
            UpdateCurrentPalette();
            RaiseCtyleChanged();
        }
        void RaiseCtyleChanged() {
            if(StyleChanged != null) StyleChanged(this, new ChartAppearanceEventArgs(PaletteName, AppearanceName, ColorIndex));
        }
        public string PaletteName {
            get { return chart.PaletteName; }
            set { chart.PaletteName = value; }
        }
        public string AppearanceName {
            get { return chart.AppearanceName; }
            set { chart.AppearanceName = value; }
        }
        public int ColorIndex {
            get { return chart.PaletteBaseColorNumber; }
            set { chart.PaletteBaseColorNumber = value; }
        }
        #region CurrentStyle
        string CurrentAppearanceName {
            get {
                List<GalleryItem> innerCheckedList = this.styleGalleryBarItem.Gallery.GetCheckedItems();
                return innerCheckedList[0].Caption;
            }
        }
        int CurrentPaletteColor {
            get {
                List<GalleryItem> innerCheckedList = this.styleGalleryBarItem.Gallery.GetCheckedItems();
                return (int)innerCheckedList[0].Tag;
            }
        }
        #endregion
        #region InitData
        void InitChartPaletteGallery(GalleryDropDown gallery) {
            gallery.Gallery.DestroyItems();
            string[] paletteNames = chartControl.GetPaletteNames();
            gallery.Gallery.BeginUpdate();
            for(int i = 0; i < paletteNames.Length; i++) {
                GalleryItem gPaletteItem = new GalleryItem();
                gPaletteItem.Caption = paletteNames[i];
                gPaletteItem.Hint = paletteNames[i];
                gPaletteItem.Image = CreateEditorImage(chart.PaletteRepository[paletteNames[i]]);
                gallery.Gallery.Groups[0].Items.Add(gPaletteItem);
            }
            gallery.Gallery.EndUpdate();
            UpdateCurrentPalette();
        }
        public void UpdateCurrentPalette() {
            List<GalleryItem> innerList = this.paletteGallery.Gallery.GetAllItems();
            foreach(GalleryItem item in innerList) {
                if(item.Caption == chart.PaletteName) {
                    paletteGallery.Gallery.SetItemCheck(item, true, true);
                    paletteButton.Hint = string.Format(ConstStrings.PaletteButtonHint, chart.PaletteName);
                    return;
                }
            }
        }
        public void UpdateCurrentAppearance() {
            InitChartAppearanceGallery(this.styleGalleryBarItem, chart.PaletteName);
        }
        void InitChartAppearanceGallery(RibbonGalleryBarItem galleryBarItem, string paletteName) {
            GalleryItem checkedItem = null;
            galleryBarItem.Gallery.DestroyItems();
            galleryBarItem.Gallery.Groups.Clear();
            Palette palette = chart.PaletteRepository[paletteName];
            galleryBarItem.Gallery.BeginUpdate();
            foreach(DevExpress.XtraCharts.ChartAppearance appearance in chart.AppearanceRepository) {
                if(chart.Palette.Predefined && appearance != chart.Appearance &&
                    !String.IsNullOrEmpty(appearance.PaletteName) &&
                    appearance.PaletteName != chart.Palette.Name) continue;
                GalleryItemGroup group = new GalleryItemGroup();
                group.Caption = appearance.Name;
                galleryBarItem.Gallery.Groups.Add(group);
                for(int i = 0; i <= palette.Count; i++) {
                    GalleryItem gAppearanceItem = new GalleryItem();
                    gAppearanceItem.Caption = appearance.Name;
                    gAppearanceItem.Hint = GetStyleName(appearance, i);
                    gAppearanceItem.Tag = i;
                    gAppearanceItem.Image = AppearanceImageHelper.CreateImage(ViewType, appearance, palette, i);
                    group.Items.Add(gAppearanceItem);
                    if(chart.AppearanceName == appearance.Name &&
                        chart.PaletteBaseColorNumber == i) {
                        checkedItem = gAppearanceItem;
                    }
                }
            }
            galleryBarItem.Gallery.EndUpdate();
            galleryBarItem.Tag = palette.Count;
            galleryBarItem.Gallery.SetItemCheck(checkedItem, true, true);
            RibbonControl ribbon = ((RibbonBarManager)galleryBarItem.Manager).Ribbon;
            ribbon.UpdateViewInfo();
            checkedItem.MakeVisible();
        }
        static string GetStyleName(DevExpress.XtraCharts.ChartAppearance appearance, int index)
        {
            if(index == 0) return string.Format("{0} (All colors)", appearance.Name);
            return string.Format("{0} (Color {1})", appearance.Name, index);
        }
        public static Image CreateEditorImage(Palette palette) {
            const int imageSize = 10;
            Bitmap image = null;
            try {
                image = new Bitmap(palette.Count * (imageSize + 1) - 1, imageSize);
                using(Graphics g = Graphics.FromImage(image)) {
                    Rectangle rect = new Rectangle(Point.Empty, new Size(imageSize, imageSize));
                    for(int i = 0; i < palette.Count; i++, rect.X += 11) {
                        using(Brush brush = new SolidBrush(palette[i].Color))
                            g.FillRectangle(brush, rect);
                        Rectangle penRect = rect;
                        penRect.Width--;
                        penRect.Height--;
                        using(Pen pen = new Pen(Color.Gray))
                            g.DrawRectangle(pen, penRect);
                    }
                }
            }
            catch {
                if(image != null) {
                    image.Dispose();
                    image = null;
                }
            }
            return image;
        }
        #endregion
    }
    public class StickLookAndFeelForm : CustomTopForm {
        EmptySkinElementPainter painter = new EmptySkinElementPainter();
        public StickLookAndFeelForm() {
            UserLookAndFeel.Default.StyleChanged += new EventHandler(Default_StyleChanged);
            UpdateRegion();
        }

        void Default_StyleChanged(object sender, EventArgs e) {
            UpdateRegion();
        }
        protected override void OnPaint(PaintEventArgs e) {
            Skin skin = BarSkins.GetSkin(UserLookAndFeel.Default);
            GraphicsCache cache = new GraphicsCache(e);
            DrawContent(cache, skin);
            DrawTopElement(cache, skin);
            base.OnPaint(e);
        }
        Rectangle GetTopElementRectangle() {
            Rectangle r = this.ClientRectangle;
            return new Rectangle(r.X, r.Y, r.Width, 10);
        }
        void DrawContent(GraphicsCache graphicsCache, Skin skin) {
            SkinElement element = skin[BarSkins.SkinAlertWindow];
            SkinElementInfo eInfo = new SkinElementInfo(element, this.ClientRectangle);
            ObjectPainter.DrawObject(graphicsCache, SkinElementPainter.Default, eInfo);
        }
        void DrawTopElement(GraphicsCache graphicsCache, Skin skin) {
            SkinElement element = skin[BarSkins.SkinAlertCaptionTop];
            SkinElementInfo eInfo = new SkinElementInfo(element, GetTopElementRectangle());
            ObjectPainter.DrawObject(graphicsCache, painter, eInfo);
        }
        internal void UpdateRegion() {
            SkinElement se = BarSkins.GetSkin(UserLookAndFeel.Default)[BarSkins.SkinAlertWindow];
            if(se == null) {
                this.Region = null;
                return;
            }
            int cornerRadius = se.Properties.GetInteger(BarSkins.SkinAlertWindowCornerRadius);
            if(cornerRadius == 0) this.Region = null;
            else this.Region = NativeMethods.CreateRoundRegion(new Rectangle(Point.Empty, this.Size), cornerRadius);
        }
        protected override bool HasSystemShadow { get { return true; } }
        class EmptySkinElementPainter : SkinElementPainter {
            protected override void DrawSkinForeground(SkinElementInfo ee) { }
        }
    }
    public class FormAnimationSizer {
        Form parent;
        Size sizeInit;
        Timer tmr = null;
        int minHeight = 0;
        int step = 10;
        int heightStep = 0;
        public FormAnimationSizer(Form form) {
            parent = form;
            sizeInit = form.ClientSize;
            parent.StartPosition = FormStartPosition.Manual;
            parent.Location = new Point(Screen.PrimaryScreen.WorkingArea.Left + (Screen.PrimaryScreen.WorkingArea.Width - sizeInit.Width) / 2,
                Screen.PrimaryScreen.WorkingArea.Top + (Screen.PrimaryScreen.WorkingArea.Height - sizeInit.Height) / 2);
            tmr = new Timer();
            tmr.Enabled = false;
            tmr.Interval = 20;
            tmr.Tick += new EventHandler(tmr_Tick);
        }
        void tmr_Tick(object sender, EventArgs e) {
            int allowHeight = parent.ClientSize.Height + (minHeight > parent.ClientSize.Height ? heightStep : -heightStep);
            heightStep += step / 2;
            if(Math.Abs(allowHeight - minHeight) <= heightStep) {
                parent.ClientSize = new Size(sizeInit.Width, minHeight);
                tmr.Stop();
            }
            else {
                parent.ClientSize = new Size(sizeInit.Width, allowHeight);
            }
        }
        public void SetMinHeight(int height) {
            bool animation = minHeight != 0;
            minHeight = height;
            if(animation) {
                heightStep = step;
                tmr.Start();
            }
            else
                parent.ClientSize = new Size(sizeInit.Width, minHeight);
        }
    }
    public class HotLabel : LabelControl {
        string linkText = string.Empty;
        public HotLabel() {
            LookAndFeel.StyleChanged += new EventHandler(Default_StyleChanged);
            UpdateColors();
        }
        protected override void Dispose(bool disposing) {
            if(disposing) {
                LookAndFeel.StyleChanged -= new EventHandler(Default_StyleChanged);
            }
            base.Dispose(disposing);
        }
        [DefaultValue("")]
        public string LinkText {
            get { return linkText; }
            set {
                if(linkText == value) return;
                linkText = value;
            }
        }
        void Default_StyleChanged(object sender, EventArgs e) {
            UpdateColors();
        }
        public void UpdateColors() {
            if(DesignMode) return;
            this.Appearance.ForeColor = WinHelper.GetLinkColor(LookAndFeel);
        }
        protected override void OnMouseEnter(EventArgs e) {
            base.OnMouseEnter(e);
            if(DesignMode) return;
            Appearance.Font = new Font(Appearance.Font, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }
        protected override void OnMouseLeave(EventArgs e) {
            base.OnMouseLeave(e);
            if(DesignMode) return;
            Appearance.Font = new Font(Appearance.Font, FontStyle.Regular);
            Cursor = Cursors.Default;
        }
        protected override void OnMouseClick(MouseEventArgs e) {
            base.OnMouseClick(e);
            if(DesignMode || string.IsNullOrEmpty(LinkText)) return;
            Cursor = Cursors.WaitCursor;
            ObjectHelper.ShowWebSite(LinkText);
        }
    }
}

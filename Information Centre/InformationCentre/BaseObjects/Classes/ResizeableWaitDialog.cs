using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;

namespace BaseObjects
{
    public class ResizeableWaitDialog : WaitDialogForm
    {
        Font font = new Font("Arial", 9);
        Font boldFont = new Font("Arial", 9, FontStyle.Bold);
        int maxWidth = 1000;
        string title;
        public ResizeableWaitDialog(string caption, string title, Form parent) : base(caption, title, new Size(260, 50), parent) { this.title = title; }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            int intOriginalX = base.Location.X;
            int intOriginalWidth = base.Size.Width;

            using (Bitmap bmp = new Bitmap(10, 10))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = sf.LineAlignment = StringAlignment.Center;
                    sf.Trimming = StringTrimming.EllipsisCharacter;
                    int titleWidth = (int)g.MeasureString(title, boldFont, maxWidth, sf).Width;
                    int captionWidth = (int)g.MeasureString(Caption, font, maxWidth, sf).Width;
                    ClientSize = new Size(Math.Max(captionWidth, titleWidth) + 68, ClientSize.Height);
                }
            }
            base.Left = intOriginalX - ((ClientSize.Width - intOriginalWidth) / 2);
        }
    }
}

using System;
using System.Data;
using System.Configuration;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class GlobalSettings
    {
        private int intUserID;
        private string strUserType1;
        private string strUserType2;
        private string strUserType3;
        private string strUserSurname;
        private string strUserForename;
        private string strUsername;
        private int intToDoList;
        private int intCommentBank;
        private int intToolTips;
        private int intShowTipsOnStart;
        private int intShowFormOnStart;
        private int intUserScreenConfig;
        private int intContactGroups;
        private int intShowConfirmations;
        private DateTime dtViewedStartDate;
        private DateTime dtViewedEndDate;
        private DateTime dtLiveStartDate;
        private DateTime dtLiveEndDate;
        private int intPersonType;
        private int intLivePeriodID;
        private int intViewedPeriodID;
        private Boolean blDataEntryOff;
        private Boolean blMinimiseToTaskBar;
        private string strStaffImagePath;
        private string strStudentImagePath;
        private string strDefaultImage;
        private string strLiveDBName;
        private int intShowPhotoPopup;
        private string strConnectionString;
        private string strConnectionStringREADONLY;
        private Boolean boolDeleteScreenLayout;
        private Boolean boolDeleteScreenFilter;
        private string strSystemDataTransferMode;
        private int intCurrentSiteInspectionID;
        private string strCurrentSiteInspectionDescription;
        private int intCurrentTeamID;
        private string strCurrentTeamName;
        private string strCopiedUtilityJobIDs;
        private string strHRSecurityKey;

        public GlobalSettings()
        {
            intUserID = 0;
            strUserType1 = "";
            strUserType2 = "";
            strUserType3 = "";
            strUserSurname = "";
            strUserForename = "";
            strUsername = "";
            intToDoList = 0;
            intCommentBank = 0;
            intToolTips = 0;
            intShowTipsOnStart = 0;
            intShowFormOnStart = 0;
            intUserScreenConfig = 0;
            intContactGroups = 0;
            intShowConfirmations = 0;
            intLivePeriodID = 0;
            intViewedPeriodID = 0;
            blDataEntryOff = false;
            strLiveDBName = "WoodPlan_v5";
            intShowPhotoPopup = 1;
            boolDeleteScreenLayout = false;
            boolDeleteScreenFilter = false;
            intCurrentTeamID = 0;
            strCurrentTeamName = "";
       }
 
        public int UserID
        {
            get
            {
                return intUserID;
            }
            set
            {
                intUserID = value;
            }
        }

        public string UserType1
        {
            get
            {
                return strUserType1;
            }
            set
            {
                strUserType1 = value;
            }
        }

        public string UserType2
        {
            get
            {
                return strUserType2;
            }
            set
            {
                strUserType2 = value;
            }
        }

        public string UserType3
        {
            get
            {
                return strUserType3;
            }
            set
            {
                strUserType3 = value;
            }
        }

        public DateTime ViewedStartDate
        {
            get
            {
                return dtViewedStartDate;
            }
            set
            {
                dtViewedStartDate = value;
            }
        }

        public DateTime ViewedEndDate
        {
            get
            {
                return dtViewedEndDate;
            }
            set
            {
                dtViewedEndDate = value;
            }
        }

       public DateTime LiveStartDate
        {
            get
            {
                return dtLiveStartDate;
            }
            set
            {
                dtLiveStartDate = value;
            }
        }

        public DateTime LiveEndDate
        {
            get
            {
                return dtLiveEndDate;
            }
            set
            {
                dtLiveEndDate = value;
            }
        }

        public int LivePeriodID
        {
            get
            {
                return intLivePeriodID;
            }
            set
            {
                intLivePeriodID = value;
            }
        }

        public int ViewedPeriodID
        {
            get
            {
                return intViewedPeriodID;
            }
            set
            {
                intViewedPeriodID = value;
            }
        }

        public string UserSurname
        {
            get
            {
                return strUserSurname;
            }
            set
            {
                strUserSurname = value;
            }
        }

        public string UserForename
        {
            get
            {
                return strUserForename;
            }
            set
            {
                strUserForename = value;
            }
        }

        public string Username
        {
            get
            {
                return strUsername;
            }
            set
            {
                strUsername = value;
            }
        }

        public int ToDoList
        {
            get
            {
                return intToDoList;
            }
            set
            {
                intToDoList = value;
            }
        }

        public int CommentBank
        {
            get
            {
                return intCommentBank;
            }
            set
            {
                intCommentBank = value;
            }
        }

        public int ToolTips
        {
            get
            {
                return intToolTips;
            }
            set
            {
                intToolTips = value;
            }
        }

        public int ShowTipsOnStart
        {
            get
            {
                return intShowTipsOnStart;
            }
            set
            {
                intShowTipsOnStart = value;
            }
        }

        public int ShowFormOnStart
        {
            get
            {
                return intShowFormOnStart;
            }
            set
            {
                intShowFormOnStart = value;
            }
        }

        public int UserScreenConfig
        {
            get
            {
                return intUserScreenConfig;
            }
            set
            {
                intUserScreenConfig = value;
            }
        }

        public int ContactGroups
        {
            get
            {
                return intContactGroups;
            }
            set
            {
                intContactGroups = value;
            }
        }

        public int ShowConfirmations
        {
            get
            {
                return intShowConfirmations;
            }
            set
            {
                intShowConfirmations = value;
            }
        }

        public int PersonType
        {
            get
            {
                return intPersonType;
            }
            set
            {
                intPersonType = value;
            }
        }

        public Boolean DataEntryOff
        {
            get
            {
                return blDataEntryOff;
            }
            set
            {
                blDataEntryOff = value;
            }
        }

        public string StaffPhotoPath
        {
            get
            {
                return strStaffImagePath;
            }
            set
            {
                strStaffImagePath = value;
            }
        }

        public string StudentPhotoPath
        {
            get
            {
                return strStudentImagePath;
            }
            set
            {
                strStudentImagePath = value;
            }
        }

        public string DefaultPhoto
        {
            get
            {
                return strDefaultImage;
            }
            set
            {
                strDefaultImage = value;
            }
        }

        public Boolean MinimiseToTaskBar
        {
            get
            {
                return blMinimiseToTaskBar;
            }
            set
            {
                blMinimiseToTaskBar = value;
            }
        }

        public string LiveDBName
        {
            get
            {
                return strLiveDBName;
            }
            set
            {
                strLiveDBName = value;
            }
        }

        public int ShowPhotoPopup
        {
            get
            {
                return intShowPhotoPopup;
            }
            set
            {
                intShowPhotoPopup = value;
            }
        }

        public string ConnectionString
        {
            get
            {
                return strConnectionString;
            }
            set
            {
                strConnectionString = value;
            }
        }
        public string ConnectionStringREADONLY
        {
            get
            {
                return strConnectionStringREADONLY;
            }
            set
            {
                strConnectionStringREADONLY = value;
            }
        }

        public Boolean DeleteScreenLayout
        {
            get
            {
                return boolDeleteScreenLayout;
            }
            set
            {
                boolDeleteScreenLayout = value;
            }
        }

        public Boolean DeleteScreenFilter
        {
            get
            {
                return boolDeleteScreenFilter;
            }
            set
            {
                boolDeleteScreenFilter = value;
            }
        }

        public string SystemDataTransferMode
        {
            get
            {
                return strSystemDataTransferMode;
            }
            set
            {
                strSystemDataTransferMode = value;
            }
        }

        public int CurrentSiteInspectionID
        {
            get
            {
                return intCurrentSiteInspectionID;
            }
            set
            {
                intCurrentSiteInspectionID = value;
            }
        }

        public string CurrentSiteInspectionDescription
        {
            get
            {
                return strCurrentSiteInspectionDescription;
            }
            set
            {
                strCurrentSiteInspectionDescription = value;
            }
        }

        public int CurrentTeamID
        {
            get
            {
                return intCurrentTeamID;
            }
            set
            {
                intCurrentTeamID = value;
            }
        }

        public string CurrentTeamName
        {
            get
            {
                return strCurrentTeamName;
            }
            set
            {
                strCurrentTeamName = value;
            }
        }

        public string CopiedUtilityJobIDs
        {
            get
            {
                return strCopiedUtilityJobIDs;
            }
            set
            {
                strCopiedUtilityJobIDs = value;
            }
        }

        public string HRSecurityKey
        {
            get
            {
                return strHRSecurityKey;
            }
            set
            {
                strHRSecurityKey = value;
            }
        }

        
    }
}

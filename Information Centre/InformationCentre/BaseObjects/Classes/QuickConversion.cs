using System;
using System.Collections.Generic;
using System.Text;

namespace BaseObjects
{
    /// <summary>
    /// QuickConversion - This class is intended for the purpose of converting strings to 
    /// different types without the need for try()...catch() statements...
    /// </summary>
    public class QuickConversion
    {
        /// <summary>
        /// QuickConversion - Apparently using these methods will improve code efficiency...
        /// </summary>
        public QuickConversion()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToInt(string toConvert, ref int converted)
        {
            return int.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToInt16(string toConvert, ref Int16 converted)
        {
            return Int16.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToInt32(string toConvert, ref Int32 converted)
        {
            return Int32.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToInt64(string toConvert, ref Int64 converted)
        {
            return Int64.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToUInt(string toConvert, ref uint converted)
        {
            return uint.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToUInt16(string toConvert, ref UInt16 converted)
        {
            return UInt16.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToUInt32(string toConvert, ref UInt32 converted)
        {
            return UInt32.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToUInt64(string toConvert, ref UInt64 converted)
        {
            return UInt64.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToLong(string toConvert, ref long converted)
        {
            return long.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToFloat(string toConvert, ref float converted)
        {
            return float.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToULong(string toConvert, ref ulong converted)
        {
            return ulong.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToDouble(string toConvert, ref double converted)
        {
            return double.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToBool(string toConvert, ref Boolean converted)
        {
            return Boolean.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToShort(string toConvert, ref short converted)
        {
            return short.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToUShort(string toConvert, ref ushort converted)
        {
            return ushort.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToDecimal(string toConvert, ref decimal converted)
        {
            return decimal.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToSingle(string toConvert, ref Single converted)
        {
            return Single.TryParse(toConvert, out converted);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toConvert"></param>
        /// <param name="converted"></param>
        /// <returns></returns>
        public static Boolean ConvertToDateTime(string toConvert, ref DateTime converted)
        {
            return DateTime.TryParse(toConvert, out converted);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraCharts;
using BaseObjects;

namespace BaseObjects
{
    public class Charting
    {
        protected virtual ChartControl MainChart { get { return null; } }
        protected virtual ChartControl AlternateChart { get { return null; } }

        public void CheckChartStyles(ChartAppearanceManager manager)
        {
            WinHelper.SetChartStyle(MainChart, manager);
            WinHelper.SetChartStyle(AlternateChart, manager);
        }

        public void LoadChartPaletteAppearance(ChartAppearanceManager chartManager, string style)
        {
            string[] styles = style.Split('@');
            if (styles.Length < 3) return;
            chartManager.AppearanceName = styles[1];
            chartManager.ColorIndex = Convert.ToInt32(styles[2]);
            chartManager.PaletteName = styles[0];
            chartManager.UpdateCurrentPalette();
            chartManager.UpdateCurrentAppearance();
        }

    }


}

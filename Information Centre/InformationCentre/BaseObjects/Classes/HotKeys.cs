using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class HotKeys : IMessageFilter
    {
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, KeyModifiers fsModifiers, Keys vk);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        /// <summary>
        /// 
        /// </summary>
        public enum KeyModifiers
        {
            /// <summary>
            /// 
            /// </summary>
            None = 0,
            /// <summary>
            /// 
            /// </summary>
            Alt = 1,
            /// <summary>
            /// 
            /// </summary>
            Control = 2,
            /// <summary>
            /// 
            /// </summary>
            Shift = 4,
            /// <summary>
            /// 
            /// </summary>
            Windows = 8
        }

        const int WM_KEYDOWN = 0x0100;
        private const int id = 100;

        private IntPtr handle;
        /// <summary>
        /// 
        /// </summary>
        public IntPtr Handle
        {
            get { return handle; }
            set { handle = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public event KeyPressEventHandler HotKeyPressed;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="modifier"></param>
        /// <param name="hotKeyPressed"></param>
        public HotKeys(Keys key, KeyModifiers modifier, KeyPressEventHandler hotKeyPressed)
        {
            HotKeyPressed = hotKeyPressed;
            RegisterHotKey(key, modifier);
            Application.AddMessageFilter(this);
        }

        /// <summary>
        /// 
        /// </summary>
        ~HotKeys()
        {
            Application.RemoveMessageFilter(this);
            UnregisterHotKey(handle, id);
        }


        private void RegisterHotKey(Keys key, KeyModifiers modifier)
        {
            if (key == Keys.None)
                return;

            bool isKeyRegisterd = RegisterHotKey(handle, id, modifier, key);
            if (!isKeyRegisterd)
                throw new ApplicationException("Hotkey already in use");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool PreFilterMessage(ref Message msg)
        {
            if (msg.Msg == WM_KEYDOWN && (Keys)msg.WParam.ToInt32() == Keys.C && Control.ModifierKeys == Keys.Control)
            {
                KeyPressEventArgs e = new KeyPressEventArgs('C');
                HotKeyPressed(this, e);
            }
            else if (msg.Msg == WM_KEYDOWN && (Keys)msg.WParam.ToInt32() == Keys.V && Control.ModifierKeys == Keys.Control)
            {
                KeyPressEventArgs e = new KeyPressEventArgs('V');
                HotKeyPressed(this, e);
            }
            return false;
        }

    }
}

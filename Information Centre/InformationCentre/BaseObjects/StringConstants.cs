using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace BaseObjects 
{
    public class ConstStrings {
        public const string AboutGroup = "About";
        public const string RentalGroup = "Movie Rental";
        public const string CatalogGroup = "Catalog";
        public const string KPIGroup = "KPI";
        public const string StatisticsGroup = "Statistics";
        public const string ReportsGroup = "Reports";
        public const string AdminGroup = "Administrator";
        public const string EmployeesModule = "Employees";
        public const string RentModule = "Movie Rentals\r\n(for current customer)";
        public const string CustomersModule = "Customers";
        public const string MoviesModule = "Movies";
        public const string ActorsModule = "Actors/Crew";
        public const string CompaniesModule = "Companies";
        public const string MoviesKPIModule = "Movies KPI";
        public const string CustomersKPIModule = "Customers KPI";
        public const string CustomersDatesModule = "Customers by Dates";
        public const string Revenue12MonthsModule = "Revenue - Last 12 Months";
        public const string RevenueSplitModule = "Revenue Categories � Last 12 Months";
        public const string TopCustomersModule = "Top Customers";
        public const string CustomerRevenueModule = "Revenue by Customers";
        public const string MediaPerformanceModule = "Performance by Media Type";
        public const string RentScheduleModule = "Rental Calendar\r\n(by customer)";

        public const string ActiveGrid_Warning = "This operation might take a few minutes.\r\nDo you want to continue?";
        public const string RuleIsNotBlankWarning = "This value cannot be empty";
        public const string RuleGreaterOrEqualZeroWarning = "This value cannot be negative";
        public const string RuleUniqueName = "Name must be unique";
        public const string CloseCancelFormWarning = "Do you want to save changes?";
        public const string CloseCancelFormsWarning = "Some detail forms have been modified.\r\nDo you want to close these forms?";
        public const string DeleteRecord = "Do you want to delete the {0}?";
        public const string PhotoOf = "Photo: {0} of {1}";
        public const string CannotDeleteCustomer = "Cannot delete customer {0}. The 'Rental' table contains linked records.";
        public const string EditViewOptions = "Modify {0} List View settings";
        
        public const string AutoFilterRowHint = "To quickly locate a customer, type the customer's name or a part of his/her name in the Name column in the top row. You can also search against other columns in the same way.";
        public const string FindCustomerHint = "Please choose a customer";            
        public const string NewRecordsIDString = "<unassigned>";

        public const string ReturnAndPaymentLateFeeQuestion = "Return items and display payment receipt?";
        public const string ReturnQuestion = "Return items?";

        public const string Question = "Question";
        public const string Warning = "Warning";
        public const string Error = "Error";
        public const string Export = "Export";
        public const string Information = "Information";
        public const string AdvancedGrid = "Advanced grid";
        public const string SimpleGrid = "Simple grid";
        public const string Grid = "Grid";
        public const string Cards = "Cards";
        public const string UnableOpenDb = "Unable to open the database. A new database will be created.";
        public const string ObjectCanNotBeDeleted = "This object cannot be deleted.";
        public const string NotAllItemsCanBeDeleted = "Some of items cannot be deleted. Do you want to continue?";
        public const string ExportFileOpen = "Do you want to open this file?";
        public const string ExportError = "An error occured while exporting";
        public const string DateLess = "End Date is less than Start Date";

        public const string ExportDescription = "Export data into different file formats";
        public const string PrintPreviewDescription = "Preview a report...";
        public const string LayoutOptionDescription = "Customize layout options...";
        public const string PeriodDescription = "Specify data processing date intervals...";
        public const string AboutDescription = "About this application...";
        public const string ChooseSkinDescription = "Specify the application�s paint scheme";
        
        public const string DatabaseCreating = "To run the application, a sample database needs to be created and populated with data.\r\nPlease select the database type and connection options below.";
        public const string CustomerDetailTooltip = "Double click here to show the current customer details";
        public const string CurrentCustomer = "Current customer: {0}";
        public const string Nothing = "nothing";
        public const string DemoName = "Video Rental Demo (C#)";
        public const string DefaultPageName = "Home";
        public const string AddButtonHint = "Add a new {0} record";
        public const string EditButtonHint = "Edit the highlighted {0} record";
        public const string DeleteButtonHint = "Delete the highlighted {0} record";
        public const string RefreshButtonHint = "Refresh the {0} List View";
        public const string RefreshDefaultButtonHint = "Refresh data";
        public const string PrevButtonHint = "Previous {0} record";
        public const string NextButtonHint = "Next {0} record";
        public const string PrintPreviewButtonHint = "Generate {1} {0} report";
        public const string ExportButtonHint = "Export {0} report";
        public const string PrintPreviewButtonHintDefault = "Preview the report";
        public const string ExportButtonHintDefault = "Export the report";
        public const string HomeButtonHint = "Return to {0} List View";
        public const string PDFOpenFileFilter = "PDF document (*.pdf)|*.pdf";
        public const string HTMLOpenFileFilter = "HTML document (*.html)|*.html";
        public const string MHTOpenFileFilter = "MHT document (*.mht)|*.mht";
        public const string XLSOpenFileFilter = "XLS document (*.xls)|*.xls";
        public const string PNGOpenFileFilter = "PNG image (*.png)|*.png";
        public const string CustomerDiscountLevelToolTip = "<b>{0}</b>\r\nDiscount: <b>{1:p}</b>\r\nTotal Amount: <b>$ {2}</b>";
        
        public const string NewActor = "New Actor";
        public const string NewCompany = "New Company";
        public const string NewCustomer = "New Customer";
        public const string NewMovie = "New Movie";
        public const string NewCategory = "New Category";
        
        public const string Customer = "Customer";
        public const string Artist = "Actor/Crew";
        public const string Company = "Company";
        public const string Movie = "Movie";
        public const string Receipt = "Receipt";

        public const string Customers = "Customers";
        public const string Artists = "Actors/Crew";
        public const string Companies = "Companies";
        public const string Movies = "Movies";
        public const string Receipts = "Receipts";
        
        public const string Photo = "photo";
        public const string Screens = "Photo Gallery";
        public const string Revenue = "Revenue";
        public const string ReceiptCount = "Receipt Count";
        public const string Revenue12Months = "Revenue - Last 12 Months";
        public const string ReceiptCount12Months = "Receipt Count - Last 12 Months";
        public const string NullString = "(null)";
        public const string TimePeriod = "Time Period ({0:d} - {1:d})";
        public const string ComparePeriod = "Period Comparison ({0:d} - {1:d})";
        public const string MovieRevenues = "{0} Movie Revenues";
        public const string MostRentedMovies = "The Most Rented Movies on {0}";
        public const string Total = "Total";
        public const string TopCustomer = "Top Customer";
        public const string RevenueDifference = "Revenue Difference";
        public const string MovieRating = "Movie Rating";
        public const string RentType = "Rental type";
        public const string SubclassError = "Class has to be inherited from Form";
        public const string PublicConstructorError = "{0} doesn't have public constructor with empty parameters";
        public const string LookupDetails = "Lookup details...";
        public const string DatePeriodCaption = "From {0:d} to {1:d}";
        public const string ViewOptions = "List View Options";
        public const string ReceiptsViewOptions = "Receipts View Options";
        public const string AddMovieTo = "{0} - Link Movie";
        public const string AddArtistTo = "{0} - Add Actors/Crew";
        public const string AddCompanyTo = "{0} - Add Company";
        public const string ShowReportDesigner = "Show Report Designer";
        public const string AllFormats = "All Formats";
        public const string ItemsCountHtmlCaption = "Totals: <b>{0}</b>.";
        public const string ItemsCountDetailHtmlCaption = "<b>{0}</b> currently rented, <b>{1}</b> available for rent, <b>{2}</b> available for sale, <b>{3}</b> lost, <b>{4}</b> damaged, <b>{5}</b> sold.";
        public const string ExpectedOn = "Expected On";
        public const string ReturnedOn = "Returned On";
        public const string AboutFormVersion = "for WinForms " + AssemblyInfo.MarketingVersion;
        public const string AboutFormCopyright = AssemblyInfo.AssemblyCopyright + " All rights reserved.";
        public const string AboutFormDescription = "This application demonstrates some of the capabilities available to you when using DevExpress WinForms Controls and Libraries. To learn more about DevExpress and download your free evaluation, use the links below.";
        public const string AboutFormLink1 = "DevExpress.com";
        public const string AboutFormLink2 = "Download Free Trials";
        public const string AboutFormLink3 = "Purchase DevExpress Products";
        public const string AboutFormSeparator = "   |   ";
        public const string WelcomeText1 = "About this Application";
        public const string WelcomeText2 = "This sample WinForms application illustrates some of the capabilities available to you when using award-winning DevExpress WinForms Control Libraries.";
        public const string WelcomeText3 = "Controls used in this demo are:";
        public const string WelcomeText4 = "Press 'F1' to return again to this page";
        public const string WelcomeLink1 = "Learn More";
        public const string WelcomeLink2 = "Download Trial";
        public const string WelcomeLink3 = "Buy Now";
        public const string PriceError = "Must specify Price before saving the record.";
        public const string MovieList = "Movie List";
        public const string CustomerList = "Customer List";
        public const string ArtistList = "Actors/Crew List";
        public const string CompanyList = "Company List";
        public const string MovieItemStatusActive = "Available for Rent";
        public const string NoImageData = "No image for current record";
        public const string RenterDiscount = "{0} Renter";
        public const string DiscountNote = "These are auto-calculated values and are based on rental volume.";
        public const string PaletteButtonHint = "Specify a color palette ({0})";
        public const string ReadOnlyCaption = "{0} (Read Only)";

        public const string DXLink = "www.devexpress.com";
        public const string TrialsLink = "https://www.devexpress.com/ClientCenter/Downloads/#Trials";
        public const string PurchaseLink = "https://www.devexpress.com/ClientCenter/Purchase/";
    }
}

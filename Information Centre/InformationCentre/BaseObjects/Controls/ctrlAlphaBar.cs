using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ButtonEventHandler(object sender, ButtonEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlAlphaBar : UserControl
    {
        private string strSelected = "All";

        /// <summary>
        /// 
        /// </summary>
        public ctrlAlphaBar()
        {
            InitializeComponent();
        }

        private void ctrlAlphaBar_Load(object sender, EventArgs e)
        {
            Int32 intTop = 0;

            vScrollBar1.LargeChange = 24;
            vScrollBar1.SmallChange = 12;
            vScrollBar1.Value = 0;
            vScrollBar1.Minimum = 0;
            vScrollBar1.Maximum = 144;

            DevExpress.XtraEditors.SimpleButton btnCharacter = new SimpleButton();

            // Add buttons ?, 'All'...
            btnCharacter.Text = "?";
            btnCharacter.Width = 24;
            btnCharacter.Height = 24;
            btnCharacter.Anchor = AnchorStyles.Left;
            btnCharacter.Top = 0;
            btnCharacter.Click += new EventHandler(btnCharacter_Click);
            pcButtonPanel.Controls.Add(btnCharacter);
            btnCharacter = new SimpleButton();
            btnCharacter.Text = "All";
            btnCharacter.Width = 24;
            btnCharacter.Height = 24;
            btnCharacter.Anchor = AnchorStyles.Left;
            btnCharacter.Top = 24;
            btnCharacter.Click += new EventHandler(btnCharacter_Click);
            pcButtonPanel.Controls.Add(btnCharacter);

            intTop = 24;

            // Add 26 character buttons...
            for (int i = 0; i < 26; i++)
            {
                btnCharacter = new SimpleButton();
                intTop = intTop + 24;
                btnCharacter.Width = 24;
                btnCharacter.Height = 24;
                btnCharacter.Anchor = AnchorStyles.Left;
                btnCharacter.Top = intTop;

                switch (i)
                {
                    case 0:
                        btnCharacter.Text = "A";
                        break;
                    case 1:
                        btnCharacter.Text = "B";
                        break;
                    case 2:
                        btnCharacter.Text = "C";
                        break;
                    case 3:
                        btnCharacter.Text = "D";
                        break;
                    case 4:
                        btnCharacter.Text = "E";
                        break;
                    case 5:
                        btnCharacter.Text = "F";
                        break;
                    case 6:
                        btnCharacter.Text = "G";
                        break;
                    case 7:
                        btnCharacter.Text = "H";
                        break;
                    case 8:
                        btnCharacter.Text = "I";
                        break;
                    case 9:
                        btnCharacter.Text = "J";
                        break;
                    case 10:
                        btnCharacter.Text = "K";
                        break;
                    case 11:
                        btnCharacter.Text = "L";
                        break;
                    case 12:
                        btnCharacter.Text = "M";
                        break;
                    case 13:
                        btnCharacter.Text = "N";
                        break;
                    case 14:
                        btnCharacter.Text = "O";
                        break;
                    case 15:
                        btnCharacter.Text = "P";
                        break;
                    case 16:
                        btnCharacter.Text = "Q";
                        break;
                    case 17:
                        btnCharacter.Text = "R";
                        break;
                    case 18:
                        btnCharacter.Text = "S";
                        break;
                    case 19:
                        btnCharacter.Text = "T";
                        break;
                    case 20:
                        btnCharacter.Text = "U";
                        break;
                    case 21:
                        btnCharacter.Text = "V";
                        break;
                    case 22:
                        btnCharacter.Text = "W";
                        break;
                    case 23:
                        btnCharacter.Text = "X";
                        break;
                    case 24:
                        btnCharacter.Text = "Y";
                        break;
                    case 25:
                        btnCharacter.Text = "Z";
                        break;
                }
                btnCharacter.Click += new EventHandler(btnCharacter_Click);
                pcButtonPanel.Controls.Add(btnCharacter);
            }
            
            // Add 10 numeric buttons...
            for (int i = 0; i < 9; i++)
            {
                btnCharacter = new SimpleButton();
                intTop = intTop + 24;
                btnCharacter.Width = 24;
                btnCharacter.Height = 24;
                btnCharacter.Anchor = AnchorStyles.Left;
                btnCharacter.Top = intTop;

                switch (i)
                {
                    case 0:
                        btnCharacter.Text = "1";
                        break;
                    case 1:
                        btnCharacter.Text = "2";
                        break;
                    case 2:
                        btnCharacter.Text = "3";
                        break;
                    case 3:
                        btnCharacter.Text = "4";
                        break;
                    case 4:
                        btnCharacter.Text = "5";
                        break;
                    case 5:
                        btnCharacter.Text = "6";
                        break;
                    case 6:
                        btnCharacter.Text = "7";
                        break;
                    case 7:
                        btnCharacter.Text = "8";
                        break;
                    case 8:
                        btnCharacter.Text = "9";
                        break;
                    case 9:
                        btnCharacter.Text = "0";
                        break;
                }
                btnCharacter.Click += new EventHandler(btnCharacter_Click);
                pcButtonPanel.Controls.Add(btnCharacter);
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        public event ButtonEventHandler ButtonClicked;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btnCharacter_Click(object sender, EventArgs e)
        {
            ButtonEventArgs beaArgs;

            DevExpress.XtraEditors.SimpleButton btnTest = (DevExpress.XtraEditors.SimpleButton)sender;

            strSelected = btnTest.Text;

            // Need to send a message back to the parent form containing our newly selected 
            // character...
            beaArgs = new ButtonEventArgs(strSelected);
            ButtonClicked(this, beaArgs);
        }

        void pcButtonPanel_Resize(object sender, System.EventArgs e)
        {
            Int32 intTop = 0;

            for (int i = 0; i < pcButtonPanel.Controls.Count; i++)
            {
                if (i == 0)
                {
                    pcButtonPanel.Controls[i].Top = 0;
                }
                else
                {
                    pcButtonPanel.Controls[i].Top = intTop;
                }

                intTop = intTop + 24;
            }

            if (pcButtonPanel.Height >= 888)
            {
                vScrollBar1.Enabled = false;
            }
            else
            {
                vScrollBar1.Enabled = true;
            }

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            Int32 intTop = 0;

            if (e.NewValue == 0)
            {
                for (int i = 0; i < pcButtonPanel.Controls.Count; i++)
                {
                    if (i == 0)
                    {
                        pcButtonPanel.Controls[i].Top = 0;
                    }
                    else
                    {
                        pcButtonPanel.Controls[i].Top = intTop;
                    }

                    intTop = intTop + 24;
                }
            }
            else if (e.Type == ScrollEventType.LargeDecrement || e.Type == ScrollEventType.SmallDecrement)
            {
                if (e.OldValue != e.NewValue)
                {
                    for (int i = 0; i < pcButtonPanel.Controls.Count; i++)
                    {
                        pcButtonPanel.Controls[i].Top = pcButtonPanel.Controls[i].Top + e.NewValue;
                    }
                }
            }
            else if (e.Type == ScrollEventType.LargeIncrement || e.Type == ScrollEventType.SmallIncrement)
            {
                if (e.OldValue != e.NewValue)
                {
                    for (int i = 0; i < pcButtonPanel.Controls.Count; i++)
                    {
                        pcButtonPanel.Controls[i].Top = pcButtonPanel.Controls[i].Top - e.NewValue;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ButtonEventArgs : EventArgs
    {
        // Create Button Event class here...
        private string strSelected;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strButton"></param>
        public ButtonEventArgs(string strButton)
        {
            this.strSelected = strButton;
        }

        /// <summary>
        /// 
        /// </summary>
        public string ButtonText
        {
            get { return strSelected; }
        }

    }
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.Utils.Drawing;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    [UserRepositoryItem("RegisterMultilineButtonEdit")]
    public class RepositoryItemMultilineButtonEdit : RepositoryItemButtonEdit
    {
        static RepositoryItemMultilineButtonEdit() { RegisterMultilineButtonEdit(); }
        
        /// <summary>
        /// 
        /// </summary>
        public RepositoryItemMultilineButtonEdit()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public static void RegisterMultilineButtonEdit()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo("MultilineButtonEdit", typeof(MultilineButtonEdit), typeof(RepositoryItemMultilineButtonEdit), typeof(MultilineButtonEditViewInfo), new DevExpress.XtraEditors.Drawing.ButtonEditPainter(), true, null, typeof(DevExpress.Accessibility.PopupEditAccessible)));
        }

        /// <summary>
        /// 
        /// </summary>
        public override string EditorTypeName { get { return "MultilineButtonEdit"; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected override bool NeededKeysContains(Keys key)
        {
            if (key == Keys.Enter)
                return true;
            if (key == Keys.Tab)
                return true;
            if (key == Keys.Up)
                return true;
            if (key == Keys.Down)
                return true;
            return base.NeededKeysContains(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        public override bool IsNeededKey(Keys keyData)
        {
            if (keyData == (Keys.Enter | Keys.Control)) return false;
            bool res = base.IsNeededKey(keyData);
            if (res) return true;
            if (keyData == Keys.PageUp || keyData == Keys.PageDown) return true;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool AutoHeight { get { return false; } }
        
        /// <summary>
        /// 
        /// </summary>
        protected override bool UseMaskBox { get { return false; } }
    }

    /// <summary>
    /// 
    /// </summary>
    public class MultilineButtonEdit : DevExpress.XtraEditors.ButtonEdit
    {
        static MultilineButtonEdit()
        {
            RepositoryItemMultilineButtonEdit.RegisterMultilineButtonEdit();
        }

        /// <summary>
        /// 
        /// </summary>
        public MultilineButtonEdit()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="always"></param>
        protected override void UpdateMaskBoxProperties(bool always)
        {
            base.UpdateMaskBoxProperties(always);
            if (MaskBox == null) return;
            if (always || !MaskBox.Multiline) MaskBox.Multiline = true;
            if (always || !MaskBox.WordWrap) MaskBox.WordWrap = true;
            if (always || !MaskBox.AcceptsTab) MaskBox.AcceptsTab = true;
            if (always || !MaskBox.AcceptsReturn) MaskBox.AcceptsReturn = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            bool result = base.IsInputKey(keyData);
            if (result) return true;
            if (keyData == Keys.Enter) return true;
            if (keyData == Keys.Tab) return true;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public override string EditorTypeName { get { return "MultilineButtonEdit"; } }

        /// <summary>
        /// 
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemMultilineButtonEdit Properties
        {
            get { return base.Properties as RepositoryItemMultilineButtonEdit; }
        }

        /// <summary>
        /// 
        /// </summary>
		protected override bool AcceptsReturn { get { return true; } }
        
        /// <summary>
        /// 
        /// </summary>
		protected override bool AcceptsTab { get { return true; } }
	}

    /// <summary>
    /// 
    /// </summary>
	public class MultilineButtonEditViewInfo : ButtonEditViewInfo {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public MultilineButtonEditViewInfo(RepositoryItem item) : base(item) { }
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bounds"></param>
        protected override void CalcContentRect(Rectangle bounds) {
			base.CalcContentRect(bounds);
			this.fMaskBoxRect = ContentRect;
			if(!(BorderPainter is EmptyBorderPainter))
                this.fMaskBoxRect.Inflate(-1, -1);
//				;
//			else
//				this.fMaskBoxRect.Inflate(-1, -1);
		}

	}
}


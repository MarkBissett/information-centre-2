using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects.Controls
{
    /// <summary>
    /// 
    /// </summary>
    partial class ctrlDataMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            mnuAdd = new DevExpress.XtraBars.BarSubItem();
            mnuEdit = new DevExpress.XtraBars.BarSubItem();
            mnuDelete = new DevExpress.XtraBars.BarSubItem();
            mnuSave = new DevExpress.XtraBars.BarSubItem();
            mnuBlockAdd = new DevExpress.XtraBars.BarSubItem();
            mnuBlockEdit = new DevExpress.XtraBars.BarSubItem();
            mnuSort = new DevExpress.XtraBars.BarSubItem();
            mnuFilter = new DevExpress.XtraBars.BarSubItem();
            mnuFind = new DevExpress.XtraBars.BarSubItem();
            mnuSetColumnWidths = new DevExpress.XtraBars.BarSubItem();
            mnuSelectAllRecords = new DevExpress.XtraBars.BarSubItem();
            mnuRecordNavigation = new DevExpress.XtraBars.BarSubItem();
            mnuFirstRecord = new DevExpress.XtraBars.BarSubItem();
            mnuPriorRecord = new DevExpress.XtraBars.BarSubItem();
            mnuNextRecord = new DevExpress.XtraBars.BarSubItem();
            mnuLastRecord = new DevExpress.XtraBars.BarSubItem();
            //
            // mnuAdd
            //
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Caption = "Add";
            this.mnuAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuAdd_ItemClick);
            //
            // mnuEdit
            //
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Caption = "Edit";
            this.mnuEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuEdit_ItemClick);
            //
            // mnuDelete
            //
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Caption = "Delete";
            this.mnuDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuDelete_ItemClick);
            //
            // mnuSave
            //
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Caption = "Save";
            this.mnuSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSave_ItemClick);
            //
            // mnuBlockAdd
            //
            this.mnuBlockAdd.Name = "mnuBlockAdd";
            this.mnuBlockAdd.Caption = "Block Add";
            this.mnuBlockAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuBlockAdd_ItemClick);
            //
            // mnuBlockEdit
            //
            this.mnuBlockEdit.Name = "mnuBlockEdit";
            this.mnuBlockEdit.Caption = "Block Edit";
            this.mnuBlockEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuBlockEdit_ItemClick);
            //
            // mnuSort
            //
            this.mnuSort.Name = "mnuSort";
            this.mnuSort.Caption = "Sort";
            this.mnuSort.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSort_ItemClick);
            //
            // mnuFilter
            //
            this.mnuFilter.Name = "mnuFilter";
            this.mnuFilter.Caption = "Filter";
            this.mnuFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuFilter_ItemClick);
            //
            // mnuFind
            //
            this.mnuFind.Name = "mnuFind";
            this.mnuFind.Caption = "Find";
            this.mnuFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuFind_ItemClick);
            //
            // mnuSetColumnWidths
            //
            this.mnuSetColumnWidths.Name = "mnuSetColumnWidths";
            this.mnuSetColumnWidths.Caption = "Set Column Widths";
            this.mnuSetColumnWidths.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSetColumnWidths_ItemClick);
            //
            // mnuSelectAllRecords
            //
            this.mnuSelectAllRecords.Name = "mnuSelectAllRecords";
            this.mnuSelectAllRecords.Caption = "Select All Records";
            this.mnuSelectAllRecords.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSelectAllRecords_ItemClick);
            //
            // mnuFirstRecord
            //
            this.mnuFirstRecord.Name = "mnuFirstRecord";
            this.mnuFirstRecord.Caption = "First Record";
            this.mnuFirstRecord.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuFirstRecord_ItemClick);
            //
            // mnuPriorRecord
            //
            this.mnuPriorRecord.Name = "mnuPriorRecord";
            this.mnuPriorRecord.Caption = "Prior Record";
            this.mnuPriorRecord.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuPriorRecord_ItemClick);
            //
            // mnuNextRecord
            //
            this.mnuNextRecord.Name = "mnuNextRecord";
            this.mnuNextRecord.Caption = "Next Record";
            this.mnuNextRecord.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuNextRecord_ItemClick);
            //
            // mnuLastRecord
            //
            this.mnuLastRecord.Name = "mnuLastRecord";
            this.mnuLastRecord.Caption = "Last Record";
            this.mnuLastRecord.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuLastRecord_ItemClick);
            //
            // mnuRecordNavigation
            //
            this.mnuRecordNavigation.Name = "mnuRecordNavigation";
            this.mnuRecordNavigation.Caption = "Record Navigation";
            this.mnuRecordNavigation.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]{
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuFirstRecord),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuPriorRecord),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuNextRecord),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuLastRecord)});
            //
            // ctrlDataMenu
            //
            this.Name = "mnuDataMain";
            this.Caption = "Data";
            this.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuAdd),
                new DevExpress.XtraBars.LinkPersistInfo(mnuEdit),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuDelete),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuSave),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuBlockAdd),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuBlockEdit),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuSort,true),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuFilter),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuFind),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuSetColumnWidths),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuSelectAllRecords,true),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuRecordNavigation)});
         }
        #endregion

        private DevExpress.XtraBars.BarSubItem mnuAdd;
        private DevExpress.XtraBars.BarSubItem mnuEdit;
        private DevExpress.XtraBars.BarSubItem mnuDelete;
        private DevExpress.XtraBars.BarSubItem mnuSave;
        private DevExpress.XtraBars.BarSubItem mnuBlockAdd;
        private DevExpress.XtraBars.BarSubItem mnuBlockEdit;
        private DevExpress.XtraBars.BarSubItem mnuSort;
        private DevExpress.XtraBars.BarSubItem mnuFilter;
        private DevExpress.XtraBars.BarSubItem mnuFind;
        private DevExpress.XtraBars.BarSubItem mnuSetColumnWidths;
        private DevExpress.XtraBars.BarSubItem mnuSelectAllRecords;
        private DevExpress.XtraBars.BarSubItem mnuRecordNavigation;
        private DevExpress.XtraBars.BarSubItem mnuFirstRecord;
        private DevExpress.XtraBars.BarSubItem mnuPriorRecord;
        private DevExpress.XtraBars.BarSubItem mnuNextRecord;
        private DevExpress.XtraBars.BarSubItem mnuLastRecord;
    }
}

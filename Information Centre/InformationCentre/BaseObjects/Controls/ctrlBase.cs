using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

/*  VERY IMPORTANT DISCOVERY MESSAGE - DO NOT REMOVE THIS COMMENT
  
    If an exception starts appearing when you attempt to look at controls based on this one,
    check the BarManager code.  If it has a heap of menu items in it, replace the code with the
    code that follows below...
  
    // 
    // barManager1
    // 
    this.barManager1.DockControls.Add(this.barDockControl1);
    this.barManager1.DockControls.Add(this.barDockControl2);
    this.barManager1.DockControls.Add(this.barDockControl3);
    this.barManager1.DockControls.Add(this.barDockControl4);
    this.barManager1.Form = this;
    this.barManager1.MaxItemId = 0;
    this.barManager1.QueryShowPopupMenu += new DevExpress.XtraBars.QueryShowPopupMenuEventHandler(this.barManager1_QueryShowPopupMenu);

 */

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public struct stcPanelPermissions
    {
        /// <summary>
        /// 
        /// </summary>
        public int intPanelID;
        /// <summary>
        /// 
        /// </summary>
        public int intSubPartID;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blCreate;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blRead;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blUpdate;
        /// <summary>
        /// 
        /// </summary>
        public Boolean blDelete;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct stcFieldsForScript
    {
        /// <summary>
        /// 
        /// </summary>
        public string strOwnerTable;
        /// <summary>
        /// 
        /// </summary>
        public string strFieldName;
        /// <summary>
        /// 
        /// </summary>
        public Type typeField;
        /// <summary>
        /// 
        /// </summary>
        public int intLength;
        /// <summary>
        /// 
        /// </summary>
        public object objValue;
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlBase : UserControl
    {
        private int intStaffID = 0;
        private int intPanelID = 0;
        private object objObject;
        private GlobalSettings gsSettings = new GlobalSettings();
        private ArrayList alPanelPermissions = new ArrayList();
        private object objOriginator = null;
        /// <summary>
        /// 
        /// </summary>
        protected ArrayList alFieldsForScript = new ArrayList();

        /// <summary>
        /// 
        /// </summary>
        protected string strMode = "";

        /// <summary>
        /// 
        /// </summary>
        public ctrlBase()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);

            DevExpress.XtraSpellChecker.SpellCheckerISpellDictionary scdDictionary = new DevExpress.XtraSpellChecker.SpellCheckerISpellDictionary();
            BaseObjects.ExtendedCustomDictionary sccCustom = new BaseObjects.ExtendedCustomDictionary();

            scdDictionary.AlphabetPath = Environment.CurrentDirectory + "\\EnglishAlphabet.txt";
            scdDictionary.CaseSensitive = false;
            scdDictionary.Culture = System.Globalization.CultureInfo.GetCultureInfo("en-GB");
            scdDictionary.DictionaryPath = Environment.CurrentDirectory + "\\british.xlg";
            scdDictionary.Encoding = Encoding.GetEncoding(437);
            scdDictionary.GrammarPath = Environment.CurrentDirectory + "\\english.aff";

            sccCustom.AlphabetPath = Environment.CurrentDirectory + "\\EnglishAlphabet.txt";
            sccCustom.CaseSensitive = false;
            sccCustom.Culture = System.Globalization.CultureInfo.GetCultureInfo("en-GB");
            //sccCustom.DictionaryPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\CustomEnglish.dic";
            sccCustom.DictionaryPath = Environment.CurrentDirectory + "\\CustomEnglish.dic";
            sccCustom.Encoding = Encoding.GetEncoding(437);

            scSpellChecker.Dictionaries.Add(scdDictionary);
            scSpellChecker.Dictionaries.Add(sccCustom);
            scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Mode
        {
            get
            {
                return strMode;
            }
            set
            {
                strMode = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public object PassedObject
        {
            get
            {
                return objObject;
            }
            set
            {
                objObject = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int PanelID
        {
            get
            {
                return intPanelID;
            }
            set
            {
                intPanelID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int StaffID
        {
            get
            {
                return intStaffID;
            }
            set
            {
                intStaffID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public GlobalSettings GlobalSettings
        {
            get
            {
                return gsSettings;
            }
            set
            {
                gsSettings = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ArrayList PanelPermissions
        {
            get
            {
                return alPanelPermissions;
            }
            set
            {
                alPanelPermissions = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ArrayList Fields
        {
            get
            {
                return alFieldsForScript;
            }
            set
            {
                alFieldsForScript = value;
            }
        }

        #region Methods to handle events...

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAddEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBlockAddEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnEditEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBlockEditEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDeleteEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCreateFilterEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCutEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCopyEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPasteEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnUndoEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnRedoEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnClearEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSelectAllEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSelectGroupEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFindEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnReplaceEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPreviewEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPrintEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnPrintSetupEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBoldEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnItalicEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnUnderlineEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignLeftEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignCentreEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnAlignRightEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnBulletsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnImageEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontColorEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSpellCheckEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnGrammarCheckEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnMyOptionsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveLayoutEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSaveLayoutAsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnSystemOptionsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExportEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnExitEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnScratchPadEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnClipboardEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFontSizeEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnFilterManagerEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDefaultViewEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnToDoListEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLinkRecordToDoListEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCommentBankEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCommentBankCodesEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnLivePeriodEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnViewedPeriodEvent(object sender, EventArgs e, GlobalSettings strucGlobalSettings)
        {
            GlobalSettings = strucGlobalSettings; // Update so that updated viewed and live period info is available to form //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnEditSystemPeriodsEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnRemotePCUtilityEvent(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnForceLoseFocus(object sender, EventArgs e)
        {
        }

        #endregion

        private void bbiBlockAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnBlockAddEvent(objOriginator, ev);
        }

        private void bbiBlockEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnBlockEditEvent(objOriginator, ev);
        }

        private void bbiClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnClearEvent(objOriginator, ev);
        }

        private void bbiCopy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnCopyEvent(objOriginator, ev);
        }

        private void bbiCut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }
         
            OnCutEvent(objOriginator, ev);
        }

        private void bbiDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnDeleteEvent(objOriginator, ev);
        }

        private void bbiPaste_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnPasteEvent(objOriginator, ev);
        }

        private void bbiRedo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnRedoEvent(objOriginator, ev);
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnSaveEvent(objOriginator, ev);
        }

        private void bbiSelectAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnSelectAllEvent(objOriginator, ev);
        }

        private void bbiSingleAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnAddEvent(objOriginator, ev);
        }

        private void bbiSingleEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnEditEvent(objOriginator, ev);
        }

        private void bbiSpellChecker_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnSpellCheckEvent(objOriginator, ev);
        }

        private void bbiUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnUndoEvent(objOriginator, ev);
        }

        private void bsiAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnAddEvent(objOriginator, ev);
        }

        private void bsiEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnEditEvent(objOriginator, ev);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnCopyEvent(object sender, KeyPressEventArgs e) {}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void barManager1_QueryShowPopupMenu(object sender, DevExpress.XtraBars.QueryShowPopupMenuEventArgs e)
        {
            objOriginator = e.Control;
        }

        private void scSpellChecker_CheckCompleteFormShowing(object sender, DevExpress.XtraSpellChecker.FormShowingEventArgs e)
        {
            e.Handled = true;
        }

        private void bbiGrammarCheck_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EventArgs ev = new EventArgs();

            if (objOriginator == null)
            {
                objOriginator = sender;
            }

            OnGrammarCheckEvent(objOriginator, ev);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void PostViewClick(object sender, EventArgs e, int row)
        {
        }

    }
}

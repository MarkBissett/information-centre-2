using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects.Controls
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void DataMenuEventHandler(object sender, DataMenuEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlDataMenu : DevExpress.XtraBars.BarSubItem
    {
        /// <summary>
        /// 
        /// </summary>
        public ctrlDataMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        public event DataMenuEventHandler DataMenuItemClicked;

        void mnuLastRecord_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(15);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuNextRecord_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(14);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuPriorRecord_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(13);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuFirstRecord_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(12);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuSelectAllRecords_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(11);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuSetColumnWidths_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(10);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuFind_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(9);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuFilter_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(8);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuSort_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(7);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuBlockEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(6);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuBlockAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(5);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(4);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(3);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(2);
            DataMenuItemClicked(this, dmeArgs);
        }

        void mnuAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataMenuEventArgs dmeArgs = new DataMenuEventArgs(1);
            DataMenuItemClicked(this, dmeArgs);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DataMenuEventArgs : EventArgs
    {
        // Create Button Event class here...
        private int intMenuItemID;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMenuItem"></param>
        public DataMenuEventArgs(int intMenuItem)
        {
            this.intMenuItemID = intMenuItem;
        }

        /// <summary>
        /// 
        /// </summary>
        public int MenuItemClicked
        {
            get { return intMenuItemID; }
        }

    }

}

namespace BaseObjects
{
    partial class ctrlBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlBase));
            this.scSpellChecker = new BaseObjects.ExtendedSpellChecker();
            this.pmDataContextMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bsiAdd = new DevExpress.XtraBars.BarSubItem();
            this.bbiSingleAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bsiEdit = new DevExpress.XtraBars.BarSubItem();
            this.bbiSingleEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiUndo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRedo = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.bbiCut = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopy = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPaste = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSelectAll = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSpellChecker = new DevExpress.XtraBars.BarButtonItem();
            this.pmEditContextMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiGrammarCheck = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.Culture = new System.Globalization.CultureInfo("en-GB");
            this.scSpellChecker.GetEditControlContents = "";
            this.scSpellChecker.ParentContainer = null;
            this.scSpellChecker.SpellCheckMode = DevExpress.XtraSpellChecker.SpellCheckMode.AsYouType;
            this.scSpellChecker.SpellingFormType = DevExpress.XtraSpellChecker.SpellingFormType.Word;
            this.scSpellChecker.CheckCompleteFormShowing += new DevExpress.XtraSpellChecker.FormShowingEventHandler(this.scSpellChecker_CheckCompleteFormShowing);
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSave)});
            this.pmDataContextMenu.Manager = this.barManager1;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            this.pmDataContextMenu.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.pmDataContextMenu.Name = "pmDataContextMenu";
            // 
            // bsiAdd
            // 
            this.bsiAdd.Caption = "Add...";
            this.bsiAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiAdd.Glyph")));
            this.bsiAdd.Id = 0;
            this.bsiAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSingleAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAdd)});
            this.bsiAdd.Name = "bsiAdd";
            this.bsiAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiAdd_ItemClick);
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.Caption = "Add";
            this.bbiSingleAdd.Glyph = global::BaseObjects.Properties.Resources.add_16;
            this.bbiSingleAdd.Id = 1;
            this.bbiSingleAdd.Name = "bbiSingleAdd";
            this.bbiSingleAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSingleAdd_ItemClick);
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.Caption = "Block Add";
            this.bbiBlockAdd.Glyph = global::BaseObjects.Properties.Resources.blockadd_16;
            this.bbiBlockAdd.Id = 2;
            this.bbiBlockAdd.Name = "bbiBlockAdd";
            this.bbiBlockAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAdd_ItemClick);
            // 
            // bsiEdit
            // 
            this.bsiEdit.Caption = "Edit...";
            this.bsiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiEdit.Glyph")));
            this.bsiEdit.Id = 3;
            this.bsiEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSingleEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEdit)});
            this.bsiEdit.Name = "bsiEdit";
            this.bsiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiEdit_ItemClick);
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.Caption = "Edit";
            this.bbiSingleEdit.Glyph = global::BaseObjects.Properties.Resources.edit_16;
            this.bbiSingleEdit.Id = 4;
            this.bbiSingleEdit.Name = "bbiSingleEdit";
            this.bbiSingleEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSingleEdit_ItemClick);
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.Caption = "Block Edit";
            this.bbiBlockEdit.Glyph = global::BaseObjects.Properties.Resources.blockedit_16;
            this.bbiBlockEdit.Id = 5;
            this.bbiBlockEdit.Name = "bbiBlockEdit";
            this.bbiBlockEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEdit_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Delete";
            this.bbiDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.Glyph")));
            this.bbiDelete.Id = 6;
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Save";
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 7;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiAdd,
            this.bsiEdit,
            this.bbiDelete,
            this.bbiSave,
            this.bbiUndo,
            this.bbiRedo,
            this.bbiCut,
            this.bbiCopy,
            this.bbiPaste,
            this.bbiSelectAll,
            this.bbiClear,
            this.bbiSpellChecker,
            this.bbiGrammarCheck});
            this.barManager1.MaxItemId = 0;
            this.barManager1.QueryShowPopupMenu += new DevExpress.XtraBars.QueryShowPopupMenuEventHandler(this.barManager1_QueryShowPopupMenu);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(244, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 278);
            this.barDockControl2.Size = new System.Drawing.Size(244, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 278);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(244, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 278);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 0);
            this.barDockControlBottom.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Location = new System.Drawing.Point(0, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 0);
            // 
            // bbiUndo
            // 
            this.bbiUndo.Caption = "Undo";
            this.bbiUndo.Glyph = global::BaseObjects.Properties.Resources.undo_16;
            this.bbiUndo.Id = 8;
            this.bbiUndo.Name = "bbiUndo";
            this.bbiUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUndo_ItemClick);
            // 
            // bbiRedo
            // 
            this.bbiRedo.Caption = "Redo";
            this.bbiRedo.Glyph = global::BaseObjects.Properties.Resources.redo_16;
            this.bbiRedo.Id = 9;
            this.bbiRedo.Name = "bbiRedo";
            this.bbiRedo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRedo_ItemClick);
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 10;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // bbiCut
            // 
            this.bbiCut.Caption = "Cut";
            this.bbiCut.Glyph = global::BaseObjects.Properties.Resources.cut_16;
            this.bbiCut.Id = 11;
            this.bbiCut.Name = "bbiCut";
            this.bbiCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCut_ItemClick);
            // 
            // bbiCopy
            // 
            this.bbiCopy.Caption = "Copy";
            this.bbiCopy.Glyph = global::BaseObjects.Properties.Resources.copy_16;
            this.bbiCopy.Id = 12;
            this.bbiCopy.Name = "bbiCopy";
            this.bbiCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopy_ItemClick);
            // 
            // bbiPaste
            // 
            this.bbiPaste.Caption = "Paste";
            this.bbiPaste.Glyph = global::BaseObjects.Properties.Resources.paste_16;
            this.bbiPaste.Id = 13;
            this.bbiPaste.Name = "bbiPaste";
            this.bbiPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPaste_ItemClick);
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.Caption = "Select All";
            this.bbiSelectAll.Glyph = global::BaseObjects.Properties.Resources.select_all_16;
            this.bbiSelectAll.Id = 14;
            this.bbiSelectAll.Name = "bbiSelectAll";
            this.bbiSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectAll_ItemClick);
            // 
            // bbiClear
            // 
            this.bbiClear.Caption = "Clear";
            this.bbiClear.Glyph = global::BaseObjects.Properties.Resources.clear_16;
            this.bbiClear.Id = 15;
            this.bbiClear.Name = "bbiClear";
            this.bbiClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClear_ItemClick);
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.Caption = "SpellChecker";
            this.bbiSpellChecker.Glyph = global::BaseObjects.Properties.Resources.spellcheck_16;
            this.bbiSpellChecker.Id = 16;
            this.bbiSpellChecker.Name = "bbiSpellChecker";
            this.bbiSpellChecker.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSpellChecker_ItemClick);
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpellChecker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGrammarCheck)});
            this.pmEditContextMenu.Manager = this.barManager1;
            this.pmEditContextMenu.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.pmEditContextMenu.Name = "pmEditContextMenu";
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.Caption = "GrammarChecker";
            this.bbiGrammarCheck.Enabled = false;
            this.bbiGrammarCheck.Glyph = global::BaseObjects.Properties.Resources.grammercheck_16;
            this.bbiGrammarCheck.Id = 1;
            this.bbiGrammarCheck.Name = "bbiGrammarCheck";
            this.bbiGrammarCheck.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiGrammarCheck.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGrammarCheck_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // ctrlBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "ctrlBase";
            this.Size = new System.Drawing.Size(244, 278);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected ExtendedSpellChecker scSpellChecker;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu pmDataContextMenu;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarSubItem bsiAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSingleAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiBlockAdd;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarSubItem bsiEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSingleEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiBlockEdit;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiDelete;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSave;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiUndo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiRedo;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiCut;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiCopy;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiPaste;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSelectAll;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiClear;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem bbiSpellChecker;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu pmEditContextMenu;
        /// <summary>
        /// 
        /// </summary>
        
        protected DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiGrammarCheck;
    }
}

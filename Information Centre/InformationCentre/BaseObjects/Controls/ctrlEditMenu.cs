using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace BaseObjects.Controls
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void EditMenuEventHandler(object sender, EditMenuEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlEditMenu : DevExpress.XtraBars.BarSubItem
    {
        private bool blToDoIncluded = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blToDo"></param>
        public ctrlEditMenu(bool blToDo)
        {
            InitializeComponent();

            blToDoIncluded = blToDo;

            if (blToDoIncluded == true)
            {
                mnuCheckForNewTasks = new DevExpress.XtraBars.BarSubItem();
                this.mnuCheckForNewTasks.Name = "mnuCheckForNewTasks";
                this.mnuCheckForNewTasks.Caption = "Check For New Tasks";
                this.mnuCheckForNewTasks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuCheckForNewTasks_ItemClick);

                mnuLinkCurrentRecordToTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuLinkCurrentRecordToTask.Name = "mnuLinkCurrentRecordToTask";
                this.mnuLinkCurrentRecordToTask.Caption = "Link Current Record To Task";
                this.mnuLinkCurrentRecordToTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuLinkCurrentRecordToTask_ItemClick);

                mnuGotoLinkedTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuGotoLinkedTask.Name = "mnuGotoLinkedTask";
                this.mnuGotoLinkedTask.Caption = "Goto Linked Task";
                this.mnuGotoLinkedTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuGotoLinkedTask_ItemClick);

                mnuMarkTaskAsRead = new DevExpress.XtraBars.BarSubItem();
                this.mnuMarkTaskAsRead.Name = "mnuMarkTaskAsRead";
                this.mnuMarkTaskAsRead.Caption = "Mark Task As Read";
                this.mnuMarkTaskAsRead.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuMarkTaskAsRead_ItemClick);

                mnuMarkTaskAsUnread = new DevExpress.XtraBars.BarSubItem();
                this.mnuMarkTaskAsUnread.Name = "mnuMarkTaskAsUnread";
                this.mnuMarkTaskAsUnread.Caption = "Mark Task As Unread";
                this.mnuMarkTaskAsUnread.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuMarkTaskAsUnread_ItemClick);

                mnuSetTaskAsComplete = new DevExpress.XtraBars.BarSubItem();
                this.mnuSetTaskAsComplete.Name = "mnuSetTaskAsComplete";
                this.mnuSetTaskAsComplete.Caption = "Mark Task As Complete";
                this.mnuSetTaskAsComplete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSetTaskAsComplete_ItemClick);

                mnuUncompleteTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuUncompleteTask.Name = "mnuUncompleteTask";
                this.mnuUncompleteTask.Caption = "Uncomplete Task";
                this.mnuUncompleteTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuUncompleteTask_ItemClick);

                mnuDeleteCompletedTasks = new DevExpress.XtraBars.BarSubItem();
                this.mnuDeleteCompletedTasks.Name = "mnuDeleteCompletedTasks";
                this.mnuDeleteCompletedTasks.Caption = "Delete Completed Tasks";
                this.mnuDeleteCompletedTasks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuDeleteCompletedTasks_ItemClick);

                mnuToDoMain = new DevExpress.XtraBars.BarSubItem();
                this.mnuToDoMain.Name = "mnuToDoMain";
                this.mnuToDoMain.Caption = "ToDo List";
                this.mnuToDoMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]{
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuCheckForNewTasks),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuLinkCurrentRecordToTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuGotoLinkedTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuMarkTaskAsRead,true),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuMarkTaskAsUnread),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuSetTaskAsComplete,true),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuUncompleteTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuDeleteCompletedTasks,true)});

                this.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(this.mnuToDoMain, true));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public event EditMenuEventHandler EditMenuItemClicked;

        void mnuReplace_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(10);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuFind_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(9);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuSelectAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(8);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(7);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(6);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuPaste_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(5);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuCopy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(4);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuCut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(3);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuRedo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(2);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(1);
            EditMenuItemClicked(this, emeArgs);
        }

        private void ctrlEditMenu_Opening(object sender, CancelEventArgs e)
        {
            if (blToDoIncluded == true)
            {
                mnuCheckForNewTasks = new DevExpress.XtraBars.BarSubItem(); 
                this.mnuCheckForNewTasks.Name = "mnuCheckForNewTasks";
                this.mnuCheckForNewTasks.Caption = "Check For New Tasks";
                this.mnuCheckForNewTasks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuCheckForNewTasks_ItemClick);
                
                mnuLinkCurrentRecordToTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuLinkCurrentRecordToTask.Name = "mnuLinkCurrentRecordToTask";
                this.mnuLinkCurrentRecordToTask.Caption = "Link Current Record To Task";
                this.mnuLinkCurrentRecordToTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuLinkCurrentRecordToTask_ItemClick);
                
                mnuGotoLinkedTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuGotoLinkedTask.Name = "mnuGotoLinkedTask";
                this.mnuGotoLinkedTask.Caption = "Goto Linked Task";
                this.mnuGotoLinkedTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuGotoLinkedTask_ItemClick);

                mnuMarkTaskAsRead = new DevExpress.XtraBars.BarSubItem(); 
                this.mnuMarkTaskAsRead.Name = "mnuMarkTaskAsRead";
                this.mnuMarkTaskAsRead.Caption = "Mark Task As Read";
                this.mnuMarkTaskAsRead.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuMarkTaskAsRead_ItemClick);

                mnuMarkTaskAsUnread = new DevExpress.XtraBars.BarSubItem();
                this.mnuMarkTaskAsUnread.Name = "mnuMarkTaskAsUnread";
                this.mnuMarkTaskAsUnread.Caption = "Mark Task As Unread";
                this.mnuMarkTaskAsUnread.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuMarkTaskAsUnread_ItemClick);

                mnuSetTaskAsComplete = new DevExpress.XtraBars.BarSubItem();
                this.mnuSetTaskAsComplete.Name = "mnuSetTaskAsComplete";
                this.mnuSetTaskAsComplete.Caption = "Mark Task As Complete";
                this.mnuSetTaskAsComplete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuSetTaskAsComplete_ItemClick);

                mnuUncompleteTask = new DevExpress.XtraBars.BarSubItem();
                this.mnuUncompleteTask.Name = "mnuUncompleteTask";
                this.mnuUncompleteTask.Caption = "Uncomplete Task";
                this.mnuUncompleteTask.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuUncompleteTask_ItemClick);

                mnuDeleteCompletedTasks = new DevExpress.XtraBars.BarSubItem();
                this.mnuDeleteCompletedTasks.Name = "mnuDeleteCompletedTasks";
                this.mnuDeleteCompletedTasks.Caption = "Delete Completed Tasks";
                this.mnuDeleteCompletedTasks.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(mnuDeleteCompletedTasks_ItemClick);

                mnuToDoMain = new DevExpress.XtraBars.BarSubItem();
                this.mnuToDoMain.Name = "mnuToDoMain";
                this.mnuToDoMain.Caption = "ToDo List";
                this.mnuToDoMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]{
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuCheckForNewTasks),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuLinkCurrentRecordToTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuGotoLinkedTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuMarkTaskAsRead,true),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuMarkTaskAsUnread),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuSetTaskAsComplete,true),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuUncompleteTask),
                    new DevExpress.XtraBars.LinkPersistInfo(this.mnuDeleteCompletedTasks,true)});

                this.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(this.mnuToDoMain,true));
            }
        }

        void mnuDeleteCompletedTasks_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(18);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuUncompleteTask_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(17);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuSetTaskAsComplete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(16);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuMarkTaskAsUnread_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(15);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuMarkTaskAsRead_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(14);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuGotoLinkedTask_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(13);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuLinkCurrentRecordToTask_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(12);
            EditMenuItemClicked(this, emeArgs);
        }

        void mnuCheckForNewTasks_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditMenuEventArgs emeArgs = new EditMenuEventArgs(11);
            EditMenuItemClicked(this, emeArgs);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EditMenuEventArgs : EventArgs
    {
        // Create Button Event class here...
        private int intMenuItemID;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMenuItem"></param>
        public EditMenuEventArgs(int intMenuItem)
        {
            this.intMenuItemID = intMenuItem;
        }

        /// <summary>
        /// 
        /// </summary>
        public int MenuItemClicked
        {
            get { return intMenuItemID; }
        }

    }
}

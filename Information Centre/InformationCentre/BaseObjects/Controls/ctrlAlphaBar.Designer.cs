namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    partial class ctrlAlphaBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcButtonPanel = new DevExpress.XtraEditors.PanelControl();
            this.vScrollBar1 = new DevExpress.XtraEditors.VScrollBar();
            ((System.ComponentModel.ISupportInitialize)(this.pcButtonPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // pcButtonPanel
            // 
            this.pcButtonPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcButtonPanel.Location = new System.Drawing.Point(0, 0);
            this.pcButtonPanel.Name = "pcButtonPanel";
            this.pcButtonPanel.Size = new System.Drawing.Size(24, 250);
            this.pcButtonPanel.TabIndex = 0;
            this.pcButtonPanel.Text = "panelControl1";
            this.pcButtonPanel.Resize += new System.EventHandler(this.pcButtonPanel_Resize);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.Location = new System.Drawing.Point(24, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 250);
            this.vScrollBar1.TabIndex = 1;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // ctrlAlphaBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.pcButtonPanel);
            this.Name = "ctrlAlphaBar";
            this.Size = new System.Drawing.Size(41, 250);
            this.Load += new System.EventHandler(this.ctrlAlphaBar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcButtonPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pcButtonPanel;
        private DevExpress.XtraEditors.VScrollBar vScrollBar1;






    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlExtendedComboBoxEdit : ComboBoxEdit
    {
        /// <summary>
        /// 
        /// </summary>
        public ctrlExtendedComboBoxEdit()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetRealEditValue()
        {
            return base.RealEditValue.ToString();
        }
    }
}

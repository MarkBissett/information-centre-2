using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class ctrlXtraLayoutExtended: LayoutControl
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public override LayoutGroup CreateLayoutGroup(LayoutGroup parent)
        {
            LayoutGroup group = base.CreateLayoutGroup(parent);
            group.ExpandButtonVisible = true;
            group.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            return group;
        }
    }

}

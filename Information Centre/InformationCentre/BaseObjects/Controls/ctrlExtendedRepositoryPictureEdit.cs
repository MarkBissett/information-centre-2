using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using System.Reflection;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    [UserRepositoryItem("Register")]
    public class ctrlExtendedRepositoryPictureEdit : RepositoryItemPictureEdit
    {
        static ctrlExtendedRepositoryPictureEdit()
        {
            Register();
        }

        /// <summary>
        /// 
        /// </summary>
        public ctrlExtendedRepositoryPictureEdit() : base() { }

        internal const string EditorName = "CustomPictureEdit";
        
        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(ctrlPictureEditExtended2),
                typeof(ctrlExtendedRepositoryPictureEdit), typeof(DevExpress.XtraEditors.ViewInfo.PictureEditViewInfo),
                new DevExpress.XtraEditors.Drawing.PictureEditPainter(), true, null));
        }

        /// <summary>
        /// 
        /// </summary>
        public override string EditorTypeName
        {
            get
            {
                return EditorName;
            }
        }
    }
    /// <summary>
    /// MyButtonEdit is a descendant from ButtonEdit.
    /// It displays a dialog form below the text box when the edit button is clicked.
    /// </summary>
    public class ctrlPictureEditExtended2 : PictureEdit
    {
        PictureMenu menu;
        private string fFileName;

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get { return fFileName; }
            set { fFileName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override PictureMenu Menu
        {
            get
            {
                if (menu == null) menu = new ctrlPictureEditMenuExtended(this);
                return menu;
            }
        }

        static ctrlPictureEditExtended2()
        {
            ctrlExtendedRepositoryPictureEdit.Register();
        }

        /// <summary>
        /// 
        /// </summary>
        public ctrlPictureEditExtended2() : base() { }

        /// <summary>
        /// 
        /// </summary>
        public override string EditorTypeName
        {
            get 
            { 
                return ctrlExtendedRepositoryPictureEdit.EditorName; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new ctrlExtendedRepositoryPictureEdit Properties
        {
            get { return base.Properties as ctrlExtendedRepositoryPictureEdit; }
        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace BaseObjects.Controls
{
    partial class ctrlEditMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.mnuUndo = new DevExpress.XtraBars.BarSubItem();
            this.mnuRedo = new DevExpress.XtraBars.BarSubItem();
            this.mnuCut = new DevExpress.XtraBars.BarSubItem();
            this.mnuCopy = new DevExpress.XtraBars.BarSubItem();
            this.mnuPaste = new DevExpress.XtraBars.BarSubItem();
            this.mnuDelete = new DevExpress.XtraBars.BarSubItem();
            this.mnuClear = new DevExpress.XtraBars.BarSubItem();
            this.mnuSelectAll = new DevExpress.XtraBars.BarSubItem();
            this.mnuFind = new DevExpress.XtraBars.BarSubItem();
            this.mnuReplace = new DevExpress.XtraBars.BarSubItem();
            // 
            // mnuUndo
            // 
            this.mnuUndo.Name = "mnuUndo";
            this.mnuUndo.Caption = "Undo";
            this.mnuUndo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuUndo_ItemClick);
            // 
            // mnuRedo
            // 
            this.mnuRedo.Name = "mnuRedo";
            this.mnuRedo.Caption = "Redo";
            this.mnuRedo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRedo_ItemClick);
            // 
            // mnuCut
            // 
            this.mnuCut.Name = "mnuCut";
            this.mnuCut.Caption = "Cut";
            this.mnuCut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuCut_ItemClick);
            // 
            // mnuCopy
            // 
            this.mnuCopy.Name = "mnuCopy";
            this.mnuCopy.Caption = "Copy";
            this.mnuCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuCopy_ItemClick);
            // 
            // mnuPaste
            // 
            this.mnuPaste.Name = "mnuPaste";
            this.mnuPaste.Caption = "Paste";
            this.mnuPaste.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuPaste_ItemClick);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Caption = "Delete";
            this.mnuDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDelete_ItemClick);
            // 
            // mnuClear
            // 
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Caption = "Clear";
            this.mnuClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuClear_ItemClick);
            // 
            // mnuSelectAll
            // 
            this.mnuSelectAll.Name = "mnuSelectAll";
            this.mnuSelectAll.Caption = "Select All";
            this.mnuSelectAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuSelectAll_ItemClick);
            // 
            // mnuFind
            // 
            this.mnuFind.Name = "mnuFind";
            this.mnuFind.Caption = "Find";
            this.mnuFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuFind_ItemClick);
            // 
            // mnuReplace
            // 
            this.mnuReplace.Name = "mnuReplace";
            this.mnuReplace.Caption = "Replace";
            this.mnuReplace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuReplace_ItemClick);
            // 
            // ctrlEditMenu
            // 
            this.Name = "mnuEditMain";
            this.Caption = "Edit";
            this.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuUndo),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuRedo),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuCut,true),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuCopy),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuPaste),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuDelete),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuClear),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuSelectAll),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuFind,true),
                new DevExpress.XtraBars.LinkPersistInfo(this.mnuReplace)});
        }

        #endregion

        private DevExpress.XtraBars.BarSubItem mnuUndo;
        private DevExpress.XtraBars.BarSubItem mnuRedo;
        private DevExpress.XtraBars.BarSubItem mnuCut;
        private DevExpress.XtraBars.BarSubItem mnuCopy;
        private DevExpress.XtraBars.BarSubItem mnuPaste;
        private DevExpress.XtraBars.BarSubItem mnuDelete;
        private DevExpress.XtraBars.BarSubItem mnuClear;
        private DevExpress.XtraBars.BarSubItem mnuSelectAll;
        private DevExpress.XtraBars.BarSubItem mnuFind;
        private DevExpress.XtraBars.BarSubItem mnuReplace;
        private DevExpress.XtraBars.BarSubItem mnuToDoMain;
        private DevExpress.XtraBars.BarSubItem mnuCheckForNewTasks;
        private DevExpress.XtraBars.BarSubItem mnuLinkCurrentRecordToTask;
        private DevExpress.XtraBars.BarSubItem mnuGotoLinkedTask;
        private DevExpress.XtraBars.BarSubItem mnuMarkTaskAsRead;
        private DevExpress.XtraBars.BarSubItem mnuMarkTaskAsUnread;
        private DevExpress.XtraBars.BarSubItem mnuSetTaskAsComplete;
        private DevExpress.XtraBars.BarSubItem mnuUncompleteTask;
        private DevExpress.XtraBars.BarSubItem mnuDeleteCompletedTasks; 
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.Reflection;

namespace BaseObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class ctrlPictureEditExtended : PictureEdit
    {
        PictureMenu menu;
        private string fFileName;
        
        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get { return fFileName; }
            set { fFileName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ctrlPictureEditExtended() : base() { }

        /// <summary>
        /// 
        /// </summary>
        protected override PictureMenu Menu
        {
            get
            {
                if (menu == null) menu = new ctrlPictureEditMenuExtended(this);
                return menu;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ctrlPictureEditMenuExtended : PictureMenu
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuControl"></param>
        public ctrlPictureEditMenuExtended(IPictureMenu menuControl) : base(menuControl) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="im"></param>
        protected override void PasteImage(Image im)
        {
            FieldInfo fi = typeof(PictureMenu).GetField("openFile", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fi == null) return;
            OpenFileDialog od = fi.GetValue(this) as OpenFileDialog;
            string fileName = od.FileName;
            ctrlPictureEditExtended pe = MenuControl as ctrlPictureEditExtended;
            if (pe != null) pe.FileName = fileName;
            base.PasteImage(im);
        }
    }

}



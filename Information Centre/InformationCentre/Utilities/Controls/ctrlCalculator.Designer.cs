namespace Utilities
{
    partial class ctrlCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.txtNumber = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtMemoryIndicator = new DevExpress.XtraEditors.TextEdit();
            this.btnMemoryAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnMemorySave = new DevExpress.XtraEditors.SimpleButton();
            this.btnMemoryRecall = new DevExpress.XtraEditors.SimpleButton();
            this.btnMemoryClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnEquals = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlus = new DevExpress.XtraEditors.SimpleButton();
            this.btnDot = new DevExpress.XtraEditors.SimpleButton();
            this.btnPlusMinus = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.btnOneOverX = new DevExpress.XtraEditors.SimpleButton();
            this.btnMinus = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnPercent = new DevExpress.XtraEditors.SimpleButton();
            this.btnMultiply = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSqrt = new DevExpress.XtraEditors.SimpleButton();
            this.btnDivide = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearEntry = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackspace = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemoryIndicator.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // txtNumber
            // 
            this.scSpellChecker.SetCanCheckText(this.txtNumber, false);
            this.txtNumber.EditValue = "0";
            this.txtNumber.Location = new System.Drawing.Point(5, 5);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Properties.AllowFocused = false;
            this.txtNumber.Properties.Appearance.Options.UseTextOptions = true;
            this.txtNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtNumber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtNumber.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtNumber.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtNumber, false);
            this.txtNumber.Size = new System.Drawing.Size(222, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtNumber, optionsSpelling1);
            this.txtNumber.TabIndex = 8;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtMemoryIndicator);
            this.panelControl1.Controls.Add(this.btnMemoryAdd);
            this.panelControl1.Controls.Add(this.btnMemorySave);
            this.panelControl1.Controls.Add(this.btnMemoryRecall);
            this.panelControl1.Controls.Add(this.btnMemoryClear);
            this.panelControl1.Controls.Add(this.btnEquals);
            this.panelControl1.Controls.Add(this.btnPlus);
            this.panelControl1.Controls.Add(this.btnDot);
            this.panelControl1.Controls.Add(this.btnPlusMinus);
            this.panelControl1.Controls.Add(this.btn0);
            this.panelControl1.Controls.Add(this.btnOneOverX);
            this.panelControl1.Controls.Add(this.btnMinus);
            this.panelControl1.Controls.Add(this.btn3);
            this.panelControl1.Controls.Add(this.btn2);
            this.panelControl1.Controls.Add(this.btn1);
            this.panelControl1.Controls.Add(this.btnPercent);
            this.panelControl1.Controls.Add(this.btnMultiply);
            this.panelControl1.Controls.Add(this.btn6);
            this.panelControl1.Controls.Add(this.btn5);
            this.panelControl1.Controls.Add(this.btn4);
            this.panelControl1.Controls.Add(this.btnSqrt);
            this.panelControl1.Controls.Add(this.btnDivide);
            this.panelControl1.Controls.Add(this.btn9);
            this.panelControl1.Controls.Add(this.btn8);
            this.panelControl1.Controls.Add(this.btn7);
            this.panelControl1.Controls.Add(this.btnClear);
            this.panelControl1.Controls.Add(this.btnClearEntry);
            this.panelControl1.Controls.Add(this.btnBackspace);
            this.panelControl1.Controls.Add(this.txtNumber);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(233, 179);
            this.panelControl1.TabIndex = 9;
            // 
            // txtMemoryIndicator
            // 
            this.txtMemoryIndicator.Location = new System.Drawing.Point(5, 31);
            this.txtMemoryIndicator.Name = "txtMemoryIndicator";
            this.txtMemoryIndicator.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMemoryIndicator.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            //this.txtMemoryIndicator.Properties.IsOnGlass = false;
            this.txtMemoryIndicator.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtMemoryIndicator, true);
            this.txtMemoryIndicator.Size = new System.Drawing.Size(32, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtMemoryIndicator, optionsSpelling2);
            this.txtMemoryIndicator.TabIndex = 36;
            // 
            // btnMemoryAdd
            // 
            this.btnMemoryAdd.Location = new System.Drawing.Point(5, 147);
            this.btnMemoryAdd.Name = "btnMemoryAdd";
            this.btnMemoryAdd.Size = new System.Drawing.Size(32, 23);
            this.btnMemoryAdd.TabIndex = 35;
            this.btnMemoryAdd.Text = "M+";
            this.btnMemoryAdd.Click += new System.EventHandler(this.btnMemoryAdd_Click);
            // 
            // btnMemorySave
            // 
            this.btnMemorySave.Location = new System.Drawing.Point(5, 118);
            this.btnMemorySave.Name = "btnMemorySave";
            this.btnMemorySave.Size = new System.Drawing.Size(32, 23);
            this.btnMemorySave.TabIndex = 34;
            this.btnMemorySave.Text = "MS";
            this.btnMemorySave.Click += new System.EventHandler(this.btnMemorySave_Click);
            // 
            // btnMemoryRecall
            // 
            this.btnMemoryRecall.Location = new System.Drawing.Point(5, 89);
            this.btnMemoryRecall.Name = "btnMemoryRecall";
            this.btnMemoryRecall.Size = new System.Drawing.Size(32, 23);
            this.btnMemoryRecall.TabIndex = 33;
            this.btnMemoryRecall.Text = "MR";
            this.btnMemoryRecall.Click += new System.EventHandler(this.btnMemoryRecall_Click);
            // 
            // btnMemoryClear
            // 
            this.btnMemoryClear.Location = new System.Drawing.Point(5, 60);
            this.btnMemoryClear.Name = "btnMemoryClear";
            this.btnMemoryClear.Size = new System.Drawing.Size(32, 23);
            this.btnMemoryClear.TabIndex = 32;
            this.btnMemoryClear.Text = "MC";
            this.btnMemoryClear.Click += new System.EventHandler(this.btnMemoryClear_Click);
            // 
            // btnEquals
            // 
            this.btnEquals.Location = new System.Drawing.Point(195, 147);
            this.btnEquals.Name = "btnEquals";
            this.btnEquals.Size = new System.Drawing.Size(32, 23);
            this.btnEquals.TabIndex = 31;
            this.btnEquals.Text = "=";
            this.btnEquals.Click += new System.EventHandler(this.btnEquals_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.Location = new System.Drawing.Point(157, 147);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(32, 23);
            this.btnPlus.TabIndex = 30;
            this.btnPlus.Text = "+";
            this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
            // 
            // btnDot
            // 
            this.btnDot.Location = new System.Drawing.Point(119, 147);
            this.btnDot.Name = "btnDot";
            this.btnDot.Size = new System.Drawing.Size(32, 23);
            this.btnDot.TabIndex = 29;
            this.btnDot.Text = ".";
            this.btnDot.Click += new System.EventHandler(this.btnDot_Click);
            // 
            // btnPlusMinus
            // 
            this.btnPlusMinus.Location = new System.Drawing.Point(81, 147);
            this.btnPlusMinus.Name = "btnPlusMinus";
            this.btnPlusMinus.Size = new System.Drawing.Size(32, 23);
            this.btnPlusMinus.TabIndex = 28;
            this.btnPlusMinus.Text = "+/-";
            this.btnPlusMinus.Click += new System.EventHandler(this.btnPlusMinus_Click);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(43, 147);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(32, 23);
            this.btn0.TabIndex = 27;
            this.btn0.Text = "0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnOneOverX
            // 
            this.btnOneOverX.Location = new System.Drawing.Point(195, 118);
            this.btnOneOverX.Name = "btnOneOverX";
            this.btnOneOverX.Size = new System.Drawing.Size(32, 23);
            this.btnOneOverX.TabIndex = 26;
            this.btnOneOverX.Text = "1/x";
            this.btnOneOverX.Click += new System.EventHandler(this.btnOneOverX_Click);
            // 
            // btnMinus
            // 
            this.btnMinus.Location = new System.Drawing.Point(157, 118);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(32, 23);
            this.btnMinus.TabIndex = 25;
            this.btnMinus.Text = "-";
            this.btnMinus.Click += new System.EventHandler(this.btnMinus_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(119, 118);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(32, 23);
            this.btn3.TabIndex = 24;
            this.btn3.Text = "3";
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(81, 118);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(32, 23);
            this.btn2.TabIndex = 23;
            this.btn2.Text = "2";
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(43, 118);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(32, 23);
            this.btn1.TabIndex = 22;
            this.btn1.Text = "1";
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btnPercent
            // 
            this.btnPercent.Location = new System.Drawing.Point(195, 89);
            this.btnPercent.Name = "btnPercent";
            this.btnPercent.Size = new System.Drawing.Size(32, 23);
            this.btnPercent.TabIndex = 21;
            this.btnPercent.Text = "%";
            this.btnPercent.Click += new System.EventHandler(this.btnPercent_Click);
            // 
            // btnMultiply
            // 
            this.btnMultiply.Location = new System.Drawing.Point(157, 89);
            this.btnMultiply.Name = "btnMultiply";
            this.btnMultiply.Size = new System.Drawing.Size(32, 23);
            this.btnMultiply.TabIndex = 20;
            this.btnMultiply.Text = "*";
            this.btnMultiply.Click += new System.EventHandler(this.btnMultiply_Click);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(119, 89);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(32, 23);
            this.btn6.TabIndex = 19;
            this.btn6.Text = "6";
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(81, 89);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(32, 23);
            this.btn5.TabIndex = 18;
            this.btn5.Text = "5";
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(43, 89);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(32, 23);
            this.btn4.TabIndex = 17;
            this.btn4.Text = "4";
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btnSqrt
            // 
            this.btnSqrt.Location = new System.Drawing.Point(195, 60);
            this.btnSqrt.Name = "btnSqrt";
            this.btnSqrt.Size = new System.Drawing.Size(32, 23);
            this.btnSqrt.TabIndex = 16;
            this.btnSqrt.Text = "sqrt";
            this.btnSqrt.Click += new System.EventHandler(this.btnSqrt_Click);
            // 
            // btnDivide
            // 
            this.btnDivide.Location = new System.Drawing.Point(157, 60);
            this.btnDivide.Name = "btnDivide";
            this.btnDivide.Size = new System.Drawing.Size(32, 23);
            this.btnDivide.TabIndex = 15;
            this.btnDivide.Text = "/";
            this.btnDivide.Click += new System.EventHandler(this.btnDivide_Click);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(119, 60);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(32, 23);
            this.btn9.TabIndex = 14;
            this.btn9.Text = "9";
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(81, 60);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(32, 23);
            this.btn8.TabIndex = 13;
            this.btn8.Text = "8";
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(43, 60);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(32, 23);
            this.btn7.TabIndex = 12;
            this.btn7.Text = "7";
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(176, 31);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(51, 23);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "C";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClearEntry
            // 
            this.btnClearEntry.Location = new System.Drawing.Point(124, 31);
            this.btnClearEntry.Name = "btnClearEntry";
            this.btnClearEntry.Size = new System.Drawing.Size(46, 23);
            this.btnClearEntry.TabIndex = 10;
            this.btnClearEntry.Text = "CE";
            this.btnClearEntry.Click += new System.EventHandler(this.btnClearEntry_Click);
            // 
            // btnBackspace
            // 
            this.btnBackspace.Location = new System.Drawing.Point(43, 31);
            this.btnBackspace.Name = "btnBackspace";
            this.btnBackspace.Size = new System.Drawing.Size(75, 23);
            this.btnBackspace.TabIndex = 9;
            this.btnBackspace.Text = "Backspace";
            this.btnBackspace.Click += new System.EventHandler(this.btnBackspace_Click);
            // 
            // ctrlCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.panelControl1);
            this.Name = "ctrlCalculator";
            this.Size = new System.Drawing.Size(233, 179);
            this.Load += new System.EventHandler(this.ctrlCalculator_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMemoryIndicator.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtNumber;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnOneOverX;
        private DevExpress.XtraEditors.SimpleButton btnMinus;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btnPercent;
        private DevExpress.XtraEditors.SimpleButton btnMultiply;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btnSqrt;
        private DevExpress.XtraEditors.SimpleButton btnDivide;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnClearEntry;
        private DevExpress.XtraEditors.SimpleButton btnBackspace;
        private DevExpress.XtraEditors.SimpleButton btnEquals;
        private DevExpress.XtraEditors.SimpleButton btnPlus;
        private DevExpress.XtraEditors.SimpleButton btnDot;
        private DevExpress.XtraEditors.SimpleButton btnPlusMinus;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnMemoryAdd;
        private DevExpress.XtraEditors.SimpleButton btnMemorySave;
        private DevExpress.XtraEditors.SimpleButton btnMemoryRecall;
        private DevExpress.XtraEditors.SimpleButton btnMemoryClear;
        private DevExpress.XtraEditors.TextEdit txtMemoryIndicator;
    }
}

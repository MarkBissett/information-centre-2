using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Utilities.Properties;
using BaseObjects;

namespace Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ctrlCalculator : BaseObjects.ctrlBase
    {
        #region Private Variables...
        
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        
        #endregion

        #region Private Variables Specific to Calculator...
        private string strLastButtonPressed = "";
        private string strPreviousValue = "";
        private string strMemoryValue = "";
        private Boolean blNextKeyPressed = false;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public ctrlCalculator()
        {
            InitializeComponent();
        }

        #region Top Buttons

        private void btnBackspace_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text.Length == 1)
            {
                txtNumber.Text = "0";
            }
            if (txtNumber.Text.Length > 1)
            {
                txtNumber.Text = txtNumber.Text.Substring(0, txtNumber.Text.Length - 1);
            }
            else
            {
                txtNumber.Text = "0";
            }
        }

        private void btnClearEntry_Click(object sender, EventArgs e)
        {
            txtNumber.Text = "0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            strLastButtonPressed = "";
            strPreviousValue = "";
            txtNumber.Text = "0";
        }

        #endregion

        #region Number Buttons

        private void btn0_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == strPreviousValue && strLastButtonPressed != "")
            {
                txtNumber.Text = "0";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "0";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "0";
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "1";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "1";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "1";
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "2";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "2";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "2";
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "3";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "3";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "3";
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "4";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "4";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "4";
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "5";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "5";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "5";
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "6";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "6";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "6";
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "7";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "7";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "7";
            }
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "8";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "8";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "8";
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text == "0" && strLastButtonPressed != "")
            {
                txtNumber.Text = "9";
            }
            else if (blNextKeyPressed == false)
            {
                txtNumber.Text = "9";
                blNextKeyPressed = true;
            }
            else
            {
                txtNumber.Text = txtNumber.Text + "9";
            }
        }

        private void btnDot_Click(object sender, EventArgs e)
        {
            if (!txtNumber.Text.Contains("."))
            {
                txtNumber.Text = txtNumber.Text + ".";
            }
        }

        private void btnPlusMinus_Click(object sender, EventArgs e)
        {
            if (txtNumber.Text != "0")
            {
                if(txtNumber.Text.Contains("-"))
                {
                    txtNumber.Text = txtNumber.Text.Replace("-", "");
                }
                else
                {
                    txtNumber.Text = "-" + txtNumber.Text;
                }
            }
        }

        #endregion

        #region Function Buttons

        private void btnPlus_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + Convert.ToDouble(txtNumber.Text);
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - Convert.ToDouble(txtNumber.Text);
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / Convert.ToDouble(txtNumber.Text);
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * Convert.ToDouble(txtNumber.Text);
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }

            if (txtNumber.Text != "0")
            {
                strPreviousValue = txtNumber.Text;
            }

            blNextKeyPressed = false;
            strLastButtonPressed = "+";
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + Convert.ToDouble(txtNumber.Text);
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - Convert.ToDouble(txtNumber.Text);
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / Convert.ToDouble(txtNumber.Text);
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * Convert.ToDouble(txtNumber.Text);
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }

            if (txtNumber.Text != "0")
            {
                strPreviousValue = txtNumber.Text;
            }

            blNextKeyPressed = false;
            strLastButtonPressed = "-";
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + Convert.ToDouble(txtNumber.Text);
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - Convert.ToDouble(txtNumber.Text);
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / Convert.ToDouble(txtNumber.Text);
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * Convert.ToDouble(txtNumber.Text);
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }

            if (txtNumber.Text != "0")
            {
                strPreviousValue = txtNumber.Text;
            }

            blNextKeyPressed = false;
            strLastButtonPressed = "/";
        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + Convert.ToDouble(txtNumber.Text);
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - Convert.ToDouble(txtNumber.Text);
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / Convert.ToDouble(txtNumber.Text);
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * Convert.ToDouble(txtNumber.Text);
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }

            if (txtNumber.Text != "0")
            {
                strPreviousValue = txtNumber.Text;
            }

            blNextKeyPressed = false;
            strLastButtonPressed = "*";
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + Convert.ToDouble(txtNumber.Text);
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - Convert.ToDouble(txtNumber.Text);
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / Convert.ToDouble(txtNumber.Text);
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * Convert.ToDouble(txtNumber.Text);
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }
        }

        private void btnPercent_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strPreviousValue != "")
            {
                dblTemp = (Convert.ToDouble(txtNumber.Text) / 100) * Convert.ToDouble(strPreviousValue);

                switch (strLastButtonPressed)
                {
                    case "+":
                        dblTemp = Convert.ToDouble(strPreviousValue) + dblTemp;
                        break;
                    case "-":
                        dblTemp = Convert.ToDouble(strPreviousValue) - dblTemp;
                        break;
                    case "/":
                        dblTemp = Convert.ToDouble(strPreviousValue) / dblTemp;
                        break;
                    case "*":
                        dblTemp = Convert.ToDouble(strPreviousValue) * dblTemp;
                        break;
                    default:
                        break;
                }
                txtNumber.Text = dblTemp.ToString();
            }
        }

        private void btnOneOverX_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (txtNumber.Text != "0")
            {
                dblTemp = Convert.ToDouble(1) / Convert.ToDouble(txtNumber.Text);
            }

            txtNumber.Text = dblTemp.ToString();
        }

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            dblTemp = Math.Sqrt(Convert.ToDouble(txtNumber.Text));

            txtNumber.Text = dblTemp.ToString();
        }

        #endregion

        #region Memory Buttons
        
        private void btnMemoryClear_Click(object sender, EventArgs e)
        {
            strMemoryValue = "";
            txtMemoryIndicator.Text = "";
        }

        private void btnMemoryRecall_Click(object sender, EventArgs e)
        {
            if (strMemoryValue != "")
            {
                txtNumber.Text = strMemoryValue;
            }
        }

        private void btnMemorySave_Click(object sender, EventArgs e)
        {
            strMemoryValue = txtNumber.Text;
            txtMemoryIndicator.Text = "M";
        }

        private void btnMemoryAdd_Click(object sender, EventArgs e)
        {
            double dblTemp = 0;

            if (strMemoryValue == "")
            {
                strMemoryValue = "0";
            }

            dblTemp = Convert.ToDouble(strMemoryValue) + Convert.ToDouble(txtNumber.Text);

            strMemoryValue = dblTemp.ToString();
            txtMemoryIndicator.Text = "M";
        }

        #endregion

        UserActivityHooks actHook;

        private void ctrlCalculator_Load(object sender, EventArgs e)
        {
            strConnectionString = GlobalSettings.ConnectionString;
            actHook = new UserActivityHooks(); // crate an instance with global hooks
            actHook.KeyPress += new KeyPressEventHandler(ctrlCalculator_OnKeyPress);
        }

        void ctrlCalculator_OnKeyPress(object sender, KeyPressEventArgs e)
        {
            EventArgs ev = new EventArgs();

            switch (e.KeyChar)
            {
                case '0':
                    btn0_Click(btn0, ev);
                    break;
                case '1':
                    btn1_Click(btn1, ev);
                    break;
                case '2':
                    btn2_Click(btn2, ev);
                    break;
                case '3':
                    btn3_Click(btn3, ev);
                    break;
                case '4':
                    btn4_Click(btn4, ev);
                    break;
                case '5':
                    btn5_Click(btn5, ev);
                    break;
                case '6':
                    btn6_Click(btn6, ev);
                    break;
                case '7':
                    btn7_Click(btn7, ev);
                    break;
                case '8':
                    btn8_Click(btn8, ev);
                    break;
                case '9':
                    btn9_Click(btn9, ev);
                    break;
                case '+':
                    btnPlus_Click(btnPlus, ev);
                    break;
                case '-':
                    btnMinus_Click(btnMinus, ev);
                    break;
                case '*':
                    btnMultiply_Click(btnMultiply, ev);
                    break;
                case '/':
                    btnDivide_Click(btnDivide, ev);
                    break;
                case '=':
                    btnEquals_Click(btnEquals, ev);
                    break;
                case '.':
                    btnDot_Click(btnDot, ev);
                    break;
                case (char)13:
                    btnEquals_Click(btnEquals, ev);
                    break;
                case (char)8:
                    btnBackspace_Click(btnBackspace, ev);
                    break;
            }
        }
    }
}


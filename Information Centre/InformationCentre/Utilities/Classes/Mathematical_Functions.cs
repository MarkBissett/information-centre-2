using System;
using System.Collections;
using System.Text;

namespace Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class Mathematical_Functions
    {
        /// <summary>
        /// Variance
        /// </summary>
        /// <param name="data">Data to get the variance of</param>
        /// <returns>variance</returns>
        public static double Variance(double[] data)
        {
            if (data == null) return 0;
            int len = data.Length;
            double avg = Average(data);
            double sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += Math.Pow((data[i] - avg), 2);
            }

            return sum / len;
        }

        /// <summary>
        /// StandardDeviation
        /// Calculates the standard deviation
        /// </summary>
        /// <param name="data">data to use in the calculation</param>
        /// <returns>standard deviation</returns>
        public static double StandardDeviation(double[] data)
        {
            if (data == null) throw new ArgumentNullException("data");

            return Math.Sqrt(Variance(data));
        }

        /// <summary>
        /// Correlation
        /// Calculates the correlation between two sets of numbers.
        /// </summary>
        /// <param name="dataA"></param>
        /// <param name="dataB"></param>
        /// <param name="covariance"></param>
        /// <param name="pearson"></param>
        public static void Correlation(double[] dataA, double[] dataB, ref double covariance, ref double pearson)
        {
            if (dataA == null) return;
            if (dataB == null) return;
            if (dataA.Length != dataB.Length) return;

            double avgX = Average(dataA);
            double stdevX = StandardDeviation(dataA);
            double avgY = Average(dataB);
            double stdevY = StandardDeviation(dataB);
            int len = dataB.Length;

            for (int i = 0; i < len; i++)
            {
                covariance += (dataA[i] - avgX) * (dataB[i] - avgY);
            }

            covariance /= len;
            pearson = covariance / (stdevX * stdevY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doubleList"></param>
        /// <returns></returns>
        public static double Average(double[] doubleList)
        {
            double total = 0;
            foreach (double value in doubleList)
            {
                total += value;
            }
            double average = total / doubleList.Length;
            return average;
        }

    }
}

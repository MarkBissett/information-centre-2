using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;
using Utilities.Properties;

namespace Utilities
{
    class PassedSettings
    {
        public PassedSettings(byte[] param1, byte[] param2)
        {
            byte[] key = param1;
            byte[] IV = param2;
            ArrayList alBytes = new ArrayList();
            Settings set = Settings.Default;
            int nNodeType = 0;
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            string strConfigPath = config.FilePath;
            XmlTextReader xtrReader = new XmlTextReader(strConfigPath);

            while (xtrReader.Read())
            {
                if (xtrReader.NodeType == XmlNodeType.Element)
                {
                    if (xtrReader.Name == "setting")
                    {
                        while (xtrReader.MoveToNextAttribute())
                        {
                            switch (xtrReader.Value)
                            {
                                case "UserName":
                                    nNodeType = 1;
                                    break;
                                case "Password":
                                    nNodeType = 2;
                                    break;
                                case "ServerName":
                                    nNodeType = 3;
                                    break;
                                case "DatabaseName":
                                    nNodeType = 4;
                                    break;
                                case "ProjectManConnectionString":
                                    nNodeType = 5;
                                    break;
                            }
                        }
                    }
                }
                else if (xtrReader.NodeType == XmlNodeType.Text)
                {
                    switch (nNodeType)
                    {
                        case 1:
                            set.UserName = xtrReader.Value;
                            break;
                        case 2:
                            /*
                                                        RijndaelManaged cryptoProc = new RijndaelManaged();
                                                        ASCIIEncoding txtConverter = new ASCIIEncoding();
                                                        byte[] fromEncrypt;
                                                        byte[] encrypted;

                                                        string strTemp = xtrReader.Value;
                                                        string strFirst = "";
                                                        string strSecond = "";
                                                        string strThird = "";

                                                        foreach (char chrString in strTemp)
                                                        {
                                                            if (chrString == ',')
                                                            {
                                                                alBytes.Add(strFirst + strSecond + strThird);
                                                                strFirst = "";
                                                                strSecond = "";
                                                                strThird = "";
                                                            }
                                                            else
                                                            {
                                                                if (strFirst == "")
                                                                {
                                                                    strFirst = chrString.ToString();
                                                                }
                                                                else if (strSecond == "")
                                                                {
                                                                    strSecond = chrString.ToString();
                                                                }
                                                                else if (strThird == "")
                                                                {
                                                                    strThird = chrString.ToString();
                                                                }
                                                            }
                                                        }

                                                        encrypted = new byte[alBytes.Count];

                                                        for (int i = 0; i < alBytes.Count; i++)
                                                        {
                                                            encrypted[i] = Convert.ToByte(Convert.ToInt32(alBytes[i]));
                                                        }

                                                        ICryptoTransform decryptor = cryptoProc.CreateDecryptor(key, IV);

                                                        //Now decrypt the previously encrypted message using the decryptor
                                                        // obtained in the above step.
                                                        MemoryStream msDecrypt = new MemoryStream(encrypted);
                                                        CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

                                                        fromEncrypt = new byte[encrypted.Length];

                                                        //Read the data out of the crypto stream.
                                                        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                                                        //Convert the byte array back into a string.
                                                        string strEncryptedPWD = txtConverter.GetString(fromEncrypt);

                                                        strEncryptedPWD = strEncryptedPWD.Replace("\0", "");

                                                        set.Password = strEncryptedPWD;
                             */
                            break;
                        case 3:
                            set.ServerName = xtrReader.Value;
                            break;
                        case 4:
                            set.DatabaseName = xtrReader.Value;
                            break;
                        case 5:
                            set.WoodPlanConnectionString = xtrReader.Value;
                            break;
                    }
                }
            }
        }
    }
}

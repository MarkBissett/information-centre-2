using System;
using System.Collections.Generic;
using System.Text;

/*
namespace Utilities.ProjectManDataSetTableAdapters
{
    /// <summary>
    /// 
    /// </summary>
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnectionString"></param>
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}
*/

// Need one for each DataSet //
namespace Utilities.DataSet_Utilities_DatasetTableAdapters
{
    /// <summary>
    /// 
    /// </summary>
    public partial class QueriesTableAdapter : System.ComponentModel.Component
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnectionString"></param>
        public void ChangeConnectionString(string strConnectionString)
        {
            foreach (System.Data.SqlClient.SqlCommand s in this.CommandCollection)
            {
                s.Connection.ConnectionString = strConnectionString;
            }
        }
    }
}

using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using BaseObjects;

namespace Utilities
{
    public class DataSet_Selection
    {
        private GridView gvView;
        private string strConnectionString;

        public DataSet_Selection(object gridView, string ConnectionString)
        {
            gvView = (GridView)gridView;
            strConnectionString = ConnectionString;
        }

        public void SelectRecordsFromDataset(int intDatasetID, string strColumnName)
        {
            // HIGHLIGHTS ROW //
            if (gvView.OptionsSelection.MultiSelect == false) return;  // Check grid supports multi selection //
            if (intDatasetID <= 0) return;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            int intUpdateProgressThreshhold = gvView.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            // Load dataset members in comma seperated form //
            using (DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter GetMembers = new DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter())
            {
                GetMembers.ChangeConnectionString(strConnectionString);
                string strMembers = Convert.ToString(GetMembers.sp01238_AT_Dataset_selection_members_list_inverted(intDatasetID));
                if (strMembers == "")
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    return;
                }
                strMembers = "," + strMembers;
                // Add leading Comma to make search more accurate //
                int intCount = 0;
                gvView.BeginUpdate();
                gvView.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                gvView.ClearSelection();
                for (int i = 0; i < gvView.DataRowCount; i++)
                {
                    if (strMembers.Contains("," + gvView.GetRowCellValue(i, strColumnName).ToString() + ","))
                    {
                        gvView.SelectRow(i);
                        gvView.MakeRowVisible(i, false);
                        intCount++;
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                gvView.EndSelection();
                gvView.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                string strMessage;
                switch (intCount)
                {
                    case 0:
                        strMessage = "No Records Selected.";
                        break;
                    case 1:
                        strMessage = "1 Record selected.";
                        break;
                    default:
                        strMessage = intCount.ToString() + " Records Selected.";
                        break;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Dataset Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
   
        }

        public void SelectRecordsFromDataset(int intDatasetID, string strColumnName, string strFieldToTick, bool boolUpdateCheckMarkSelection)
        {
            // TICKS ROW BY UPDATING strFieldToTick with a value of 1 //
            if (gvView.OptionsSelection.MultiSelect == false) return;  // Check grid supports multi selection //
            if (intDatasetID <= 0) return;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            int intUpdateProgressThreshhold = gvView.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            // Load dataset members in comma seperated form //
            using (DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter GetMembers = new DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter())
            {
                GetMembers.ChangeConnectionString(strConnectionString);
                string strMembers = Convert.ToString(GetMembers.sp01238_AT_Dataset_selection_members_list_inverted(intDatasetID));
                if (strMembers == "")
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    return;
                }
                strMembers = "," + strMembers;
                // Add leading Comma to make search more accurate //
                int intCount = 0;
                gvView.BeginUpdate();
                for (int i = 0; i < gvView.DataRowCount; i++)
                {
                    if (strMembers.Contains("," + gvView.GetRowCellValue(i, strColumnName).ToString() + ","))
                    {
                        gvView.MakeRowVisible(i, false);
                        gvView.SetRowCellValue(i, strFieldToTick, 1);
                        if (boolUpdateCheckMarkSelection) gvView.SetRowCellValue(i, "CheckMarkSelection", 1);  // Also update second CheckMarkSelection Column if required [Used when called from Mapping screen which has 2 checkbox selections on the grid] //
                        intCount++;
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                gvView.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                string strMessage;
                switch (intCount)
                {
                    case 0:
                        strMessage = "No Records Selected.";
                        break;
                    case 1:
                        strMessage = "1 Record selected.";
                        break;
                    default:
                        strMessage = intCount.ToString() + " Records Selected.";
                        break;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Dataset Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public void SelectRecordsFromDatasetInverted(int intDatasetID, string strColumnName)
        {
            // HIGHLIGHTS ROW //
            if (gvView.OptionsSelection.MultiSelect == false) return;  // Check grid supports multi selection //
            if (intDatasetID <= 0) return;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            int intUpdateProgressThreshhold = gvView.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            // Load dataset members in comma seperated form //
            using (DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter GetMembers = new DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter())
            {
                GetMembers.ChangeConnectionString(strConnectionString);
                string strMembers = Convert.ToString(GetMembers.sp01238_AT_Dataset_selection_members_list_inverted(intDatasetID));
                if (strMembers == "")
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    return;
                }
                strMembers = "," + strMembers;
                // Add leading Comma to make search more accurate //
                int intCount = 0;
                gvView.BeginUpdate();
                gvView.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                gvView.ClearSelection();
                for (int i = 0; i < gvView.DataRowCount; i++)
                {
                    if (!strMembers.Contains("," + gvView.GetRowCellValue(i, strColumnName).ToString() + ","))
                    {
                        gvView.SelectRow(i);
                        gvView.MakeRowVisible(i, false);
                        intCount++;
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                gvView.EndSelection();
                gvView.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                string strMessage;
                switch (intCount)
                {
                    case 0:
                        strMessage = "No Records Selected.";
                        break;
                    case 1:
                        strMessage = "1 Record selected.";
                        break;
                    default:
                        strMessage = intCount.ToString() + " Records Selected.";
                        break;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Dataset Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void SelectRecordsFromDatasetInverted(int intDatasetID, string strColumnName, string strFieldToTick, bool boolUpdateCheckMarkSelection)
        {
            // TICKS ROW BY UPDATING strFieldToTick with a value of 1 //
            if (gvView.OptionsSelection.MultiSelect == false) return;  // Check grid supports multi selection //
            if (intDatasetID <= 0) return;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            int intUpdateProgressThreshhold = gvView.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            // Load dataset members in comma seperated form //
            using (DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter GetMembers = new DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter())
            {
                GetMembers.ChangeConnectionString(strConnectionString);
                string strMembers = Convert.ToString(GetMembers.sp01238_AT_Dataset_selection_members_list_inverted(intDatasetID));
                if (strMembers == "")
                {
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    return;
                }
                strMembers = "," + strMembers;
                // Add leading Comma to make search more accurate //
                int intCount = 0;
                gvView.BeginUpdate();
                for (int i = 0; i < gvView.DataRowCount; i++)
                {
                    if (!strMembers.Contains("," + gvView.GetRowCellValue(i, strColumnName).ToString() + ","))
                    {
                        gvView.MakeRowVisible(i, false);
                        gvView.SetRowCellValue(i, strFieldToTick, 1);
                        if (boolUpdateCheckMarkSelection) gvView.SetRowCellValue(i, "CheckMarkSelection", 1);  // Also update second CheckMarkSelection Column if required [Used when called from Mapping screen which has 2 checkbox selections on the grid] //
                        intCount++;
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                gvView.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                string strMessage;
                switch (intCount)
                {
                    case 0:
                        strMessage = "No Records Selected.";
                        break;
                    case 1:
                        strMessage = "1 Record selected.";
                        break;
                    default:
                        strMessage = intCount.ToString() + " Records Selected.";
                        break;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Dataset Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    
  
    }
}

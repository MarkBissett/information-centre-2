using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Utilities.Properties;
using BaseObjects;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;


namespace Utilities
{
    public partial class frmDatasetCreate : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        private ExtendedGridMenu egmMenu;  // Used to extend Grid Column Menu //
        GridHitInfo downHitInfo = null;

        public string i_str_dataset_type = "";
        public int i_int_selected_tree_count = 0;     
        public int i_int_selected_inspection_count = 0;
        public int i_int_selected_action_count = 0;
        public int i_int_selected_employee_count = 0;
        public int i_int_selected_job_count = 0;
        public int i_int_selected_dataset = 0;
        public int i_int_returned_action = 0;
        public string i_str_dataset_name = "";

        #endregion

        public frmDatasetCreate()
        {
            InitializeComponent();
        }

        private void frm_AT_dataset_create_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            checkEdit2.Checked = false;
            checkEdit3.Checked = false;
            labelAppend.Visible = false;
            textEditNewDataSetName.Visible = true;
            textEditNewDataSetName.Enabled = true;
            
            labelAppend.Visible = false;
            gridControl1.Visible = false;
            gridControl1.Enabled = false;

            sp01223_AT_Dataset_listTableAdapter.Connection.ConnectionString = strConnectionString;

            sp01237_AT_Dataset_create_typesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01237_AT_Dataset_create_typesTableAdapter.Fill(this.DataSet_Utilities_Dataset.sp01237_AT_Dataset_create_types, i_str_dataset_type);
            gridControl1.ForceInitialize();
            gridLookUpEdit1.EditValue = i_str_dataset_type;

            LoadSavedDataSets();
            SetRecordCount();
          
            this.UnlockThisWindow();  // ***** Don't use PostOpen to call this as it is only called on Sheets not modal formst ***** //
        }

        private void LoadSavedDataSets()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            sp01223_AT_Dataset_listTableAdapter.Fill(this.DataSet_Utilities_Dataset.sp01223_AT_Dataset_list, i_str_dataset_type);
            view.EndDataUpdate();
            view.EndUpdate();
        }

        private void SetRecordCount()
        {
            switch (i_str_dataset_type.ToLower())
            {
                case "tree":
                    textEdit3.Text = i_int_selected_tree_count.ToString();
                    break;
                case "inspection":
                    textEdit3.Text = i_int_selected_inspection_count.ToString();
                    break;
                case "action":
                    textEdit3.Text = i_int_selected_action_count.ToString();
                    break;
                case "employee":
                    textEdit3.Text = i_int_selected_employee_count.ToString();
                    break;
                case "job":
                    textEdit3.Text = i_int_selected_job_count.ToString();
                    break;
                default:
                    textEdit3.Text = "0";
                    break;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (checkEdit1.Checked)
            {
                i_str_dataset_name = textEditNewDataSetName.Text;
                if (i_str_dataset_name == null || i_str_dataset_name.Trim() == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter the New Dataset Name before proceeding.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    // Check name is unique //
                    DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter GetCount = new DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                    GetCount.ChangeConnectionString(strConnectionString);
                    int intMatches = Convert.ToInt32(GetCount.sp01225_AT_Dataset_check_name_unique(i_str_dataset_type, i_str_dataset_name));
                    if (intMatches > 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The name entered for the new dataset is already in use.\n\nPlease enter a different name before proceeding.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        i_int_returned_action = 1;
                    }
                }
            }
            else 
            {
                if (i_int_selected_dataset <= 0)
                {
                    string strMessage = "";
                    if (checkEdit2.Checked)
                    {
                        strMessage = "Select the Dataset to Append to before proceeding.";
                    }
                    else
                    {
                        strMessage = "Select the Dataset to Replace records in before proceeding.";
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;

                }
                else
                {
                    if (checkEdit2.Checked)
                    {
                        i_int_returned_action = 2;
                    }
                    else
                    {
                        i_int_returned_action = 3;
                    }
                    
                }
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked)
            {
                checkEdit2.Checked = false;
                checkEdit3.Checked = false;

                labelNew.Visible = true;
                textEditNewDataSetName.Visible = true;
                textEditNewDataSetName.Enabled = true;
                
                labelAppend.Visible = false;
                gridControl1.Visible = false;
                gridControl1.Enabled = false;
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit2.Checked)
            {
                checkEdit1.Checked = false;
                checkEdit3.Checked = false;

                labelNew.Visible = false;
                textEditNewDataSetName.Visible = false;
                textEditNewDataSetName.Enabled = false;

                labelAppend.Visible = true;
                gridControl1.Visible = true;
                gridControl1.Enabled = true;
                labelAppend.Text = "Select the Dataset to <B>Append</B> to:";
            }
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked)
            {
                checkEdit1.Checked = false;
                checkEdit2.Checked = false;

                labelNew.Visible = false;
                textEditNewDataSetName.Visible = false;
                textEditNewDataSetName.Enabled = false;

                labelAppend.Visible = true;
                gridControl1.Visible = true;
                gridControl1.Enabled = true;
                labelAppend.Text = "Select the Dataset to <B>Replace</B> records:";
            }
        }


        #region GridView1

        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            i_str_dataset_type = gridLookUpEdit1.EditValue.ToString();
            LoadSavedDataSets();
            SetRecordCount();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = (GridView)sender;
            egmMenu = new ExtendedGridMenu(view);
            egmMenu.PopupMenuShowing(sender, e);
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // Note grid's MouseDown event also updated to handle Expand and Contract button clicking //
            if (internalRowFocusing)
                return;
            try
            {
                internalRowFocusing = true;
                if (gridView1.IsGroupRow(e.FocusedRowHandle))
                    if (gridView1.IsGroupRow(e.PrevFocusedRowHandle))
                    {
                        if (gridView1.GetVisibleIndex(e.PrevFocusedRowHandle) > gridView1.GetVisibleIndex(e.FocusedRowHandle))
                            gridView1.FocusedRowHandle = GetBottomDataRowHandle(e.FocusedRowHandle);
                        else
                            gridView1.FocusedRowHandle = GetTopDataRowHandle(e.FocusedRowHandle);
                    }
                    else
                    {
                        if (gridView1.GetParentRowHandle(e.PrevFocusedRowHandle) == e.FocusedRowHandle)
                        {
                            int rowHandle = GetTopDataRowHandle(e.FocusedRowHandle);
                            if (rowHandle < 0)
                                gridView1.FocusedRowHandle = e.PrevFocusedRowHandle;
                            else
                                gridView1.FocusedRowHandle = rowHandle;

                        }
                        else
                        {
                            do
                            {
                                gridView1.ExpandGroupRow(gridView1.FocusedRowHandle, false);
                                gridView1.MoveNext();
                            } while (gridView1.IsGroupRow(gridView1.FocusedRowHandle));
                        }
                    }
            }
            finally
            {
                internalRowFocusing = false;
            }
            i_int_selected_dataset = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DatasetID"));
        }

        private int GetTopDataRowHandle(int ARowHandle)
        {
            if (gridView1.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = gridView1.GetVisibleRowHandle(gridView1.GetVisibleIndex(ARowHandle) - 1);
                if (gridView1.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle);
                }
            }
            else
                return -1;
        }

        private int GetBottomDataRowHandle(int ARowHandle)
        {
            int rowHandle = ARowHandle;
            if (gridView1.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = gridView1.GetChildRowHandle(rowHandle, gridView1.GetChildRowCount(rowHandle) - 1);
                } while (gridView1.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

            // Need following to handle expand and contract groups since grid's FocusedRowChanged event is interfering with it due to no selection of groups //
            GridHitInfo hi = view.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.HitTest == GridHitTest.RowGroupButton)
            {
                if (view.GetRowExpanded(hi.RowHandle))
                {
                    view.CollapseGroupRow(hi.RowHandle);
                }
                else
                {
                    view.ExpandGroupRow(hi.RowHandle);
                }
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }

        }

        #endregion


    
    }
}


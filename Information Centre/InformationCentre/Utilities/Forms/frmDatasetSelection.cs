using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using Utilities.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;

namespace Utilities
{
    public partial class frmDatasetSelection : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        private ExtendedGridMenu egmMenu;  // Used to extend Grid Column Menu //
        GridHitInfo downHitInfo = null;

        public string i_str_dataset_types = "";
        public int i_int_selected_dataset = 0;
        public string i_str_selected_dataset_type = "";

        #endregion
       
        public frmDatasetSelection()
        {
            InitializeComponent();
        }

        private void frmDatasetSelection_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            
            strConnectionString = GlobalSettings.ConnectionString;
            textEditDataSetType.Text = i_str_dataset_types;
  
            sp01228_AT_Dataset_selection_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01228_AT_Dataset_selection_listTableAdapter.Fill(this.dataSet_Utilities_Dataset.sp01228_AT_Dataset_selection_list, i_str_dataset_types);

            this.UnlockThisWindow();  // ***** Don't use PostOpen to call this as it is only called on Sheets not modal formst ***** //
        }


        #region GridView1

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = (GridView)sender;
            egmMenu = new ExtendedGridMenu(view);
            egmMenu.PopupMenuShowing(sender, e);
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // Note grid's MouseDown event also updated to handle Expand and Contract button clicking //
            if (internalRowFocusing)
                return;
            try
            {
                internalRowFocusing = true;
                if (gridView1.IsGroupRow(e.FocusedRowHandle))
                    if (gridView1.IsGroupRow(e.PrevFocusedRowHandle))
                    {
                        if (gridView1.GetVisibleIndex(e.PrevFocusedRowHandle) > gridView1.GetVisibleIndex(e.FocusedRowHandle))
                            gridView1.FocusedRowHandle = GetBottomDataRowHandle(e.FocusedRowHandle);
                        else
                            gridView1.FocusedRowHandle = GetTopDataRowHandle(e.FocusedRowHandle);
                    }
                    else
                    {
                        if (gridView1.GetParentRowHandle(e.PrevFocusedRowHandle) == e.FocusedRowHandle)
                        {
                            int rowHandle = GetTopDataRowHandle(e.FocusedRowHandle);
                            if (rowHandle < 0)
                                gridView1.FocusedRowHandle = e.PrevFocusedRowHandle;
                            else
                                gridView1.FocusedRowHandle = rowHandle;

                        }
                        else
                        {
                            do
                            {
                                gridView1.ExpandGroupRow(gridView1.FocusedRowHandle, false);
                                gridView1.MoveNext();
                            } while (gridView1.IsGroupRow(gridView1.FocusedRowHandle));
                        }
                    }
            }
            finally
            {
                internalRowFocusing = false;
            }
            i_int_selected_dataset = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DatasetID"));
            i_str_selected_dataset_type = Convert.ToString(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DatasetType"));
        }

        private int GetTopDataRowHandle(int ARowHandle)
        {
            if (gridView1.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = gridView1.GetVisibleRowHandle(gridView1.GetVisibleIndex(ARowHandle) - 1);
                if (gridView1.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle);
                }
            }
            else
                return -1;
        }

        private int GetBottomDataRowHandle(int ARowHandle)
        {
            int rowHandle = ARowHandle;
            if (gridView1.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = gridView1.GetChildRowHandle(rowHandle, gridView1.GetChildRowCount(rowHandle) - 1);
                } while (gridView1.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

            // Need following to handle expand and contract groups since grid's FocusedRowChanged event is interfering with it due to no selection of groups //
            GridHitInfo hi = view.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.HitTest == GridHitTest.RowGroupButton)
            {
                if (view.GetRowExpanded(hi.RowHandle))
                {
                    view.CollapseGroupRow(hi.RowHandle);
                }
                else
                {
                    view.ExpandGroupRow(hi.RowHandle);
                }
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }

        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (i_int_selected_dataset <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Dataset to use for selecting records before proceeding.", "Dataset Selection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }




    }
}

